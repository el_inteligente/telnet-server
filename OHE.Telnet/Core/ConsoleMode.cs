using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ohe.Telnet.Core;

namespace VS.HandHeldServer
{
    public partial class ConsoleMode : Form
    {
        public ConsoleMode()
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(ConsoleMode_FormClosed);
        }

        void ConsoleMode_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (EntryPoint.StopServer())
            {
                //MessageBox.Show("������ ������ ������� ����������.", "Ohe Telnet Server");
            }
            else
                MessageBox.Show("������ ��� ��������� ������ �������"
                                + Environment.NewLine
                                + " ��� ��������� ������ ���������� �������� ��� ����������.", "Volga Telnet Server", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (EntryPoint.StartServer())
            {
               
                //MessageBox.Show("������ ������ ������� ���������.", "Ohe Telnet Server");
            }
            else
                MessageBox.Show("������ ��� ������� ������ �������"
                                + Environment.NewLine
                                + " ��� ��������� ������ ���������� �������� ��� ����������.", "Volga Telnet Server", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(EntryPoint.StopServer())
            {
                //MessageBox.Show("������ ������ ������� ����������.", "Ohe Telnet Server");
            }
            else
                MessageBox.Show("������ ��� ��������� ������ �������"
                                + Environment.NewLine
                                + " ��� ��������� ������ ���������� �������� ��� ����������.", "Volga Telnet Server", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (EntryPoint.StopServer())
            {
                //MessageBox.Show("������ ������ ������� ����������.", "Ohe Telnet Server");
            }
            else
                MessageBox.Show("������ ��� ��������� ������ �������"
                                + Environment.NewLine
                                + " ��� ��������� ������ ���������� �������� ��� ����������.", "Volga Telnet Server", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //Application.Exit();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (EntryPoint.RestartServer())
            {
                MessageBox.Show("������ ������ ������� �����������.", "Volga Telnet Server");
            }
            else
                MessageBox.Show("������ ��� ����������� ������ �������"
                                + Environment.NewLine
                                + " ��� ��������� ������ ���������� �������� ��� ����������.", "Volga Telnet Server", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}