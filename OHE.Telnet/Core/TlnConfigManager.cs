﻿using System;
using System.Resources;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Xml;
using Ohe.Telnet.Controls;
using Ohe.Telnet.Properties;
using Ohe.Utils;
using Ohe.Telnet.Data;
using System.IO;
using ConfigurationException = Ohe.Telnet.Exception.ConfigurationException;

namespace Ohe.Telnet.Configuration
{
    /// <summary>
    /// Считывает данные из xml файла конфигурации
    /// И управляет конфигурацией приложения
    /// </summary>
    public class TlnConfigManager
    {
        #region Properties
        private static int _menuLines = 6; // определяет количество строчек меню
        private static int _port = 23;    // порт на котором поднят сервер
        private static int _webPort = _port + 1;
        private static int _timeout = 600; // время бездействия пользователя
        //private List<OheUser> _users = new List<OheUser>(); // список пользователей системы

        private List<Menu> _menus; //список меню системы
        //private List<RfForm> _forms; 
        private Dictionary<int, RfForm> _forms; //список форм системы
        private static readonly string _sync = Guid.NewGuid().ToString();
        private bool _isDirty = false; // переменная отслеживает внесены ли в конфигурацию изменения
        private Thread _thread = null;
        private static string _connstring; // строка подсключения
        private static int _remoteport = 8081; // номер порта для удаленного соединения
        private static string _compile = "0";  // Версия компиляции 
        private static bool isContinue = true;
        private static string _beepException = "\a";

        private static string _shortDate = null; // формат даты
        public static string ShortDateTime { get { return _shortDate; } set { _shortDate = value; } }

        private static int _telnet_widht = 20;
        public static int TELNET_WIDTH { get { return _telnet_widht; } set { _telnet_widht = value; } }

        private static int _telnet_height = 6;
        public static int TELNET_HEIGHT { get { return _telnet_height; } set { _telnet_height = value; } }

        private static string _telnet_HELP_STR = null; // описывает кнопку для помощи в меню
        public static string TELNET_HELP_STR { get { return _telnet_HELP_STR; } set { _telnet_HELP_STR = value; } }

        private static string _telnet_KEY_Y = "Y";   // клавиши для YES / NO
        public static string TELNET_KEY_Y { get { return _telnet_KEY_Y; } set { _telnet_KEY_Y = value; } }

        private static string _telnet_KEY_N = "N";
        public static string TELNET_KEY_N { get { return _telnet_KEY_N; } set { _telnet_KEY_N = value; } }

        private static string _telnet_CURSOR_X = "15"; // позиция ввода курсора на экране меню
        public static string TELNET_CURSOR_X { get { return _telnet_CURSOR_X; } set { _telnet_CURSOR_X = value; } }

        private static string _telnet_CURSOR_Y = "9";
        public static string TELNET_CURSOR_Y { get { return _telnet_CURSOR_Y; } set { _telnet_CURSOR_Y = value; } }

        private static string _telnet_up = "9";
        // описывает кнопку пролистывания меню вверх
        public static string TELNET_MENU_UP { get { return _telnet_up; } set { _telnet_up = value; } }

        private static string _telnet_down = "8"; // описывает кнопку пролистывания меню вниз
        public static string TELNET_MENU_DOWN { get { return _telnet_down; } set { _telnet_down = value; } }

        private static string _engine_encoding = "utf-8"; // описывает кодировку вывода сервера
        public static string ServerEncoding { get { return _engine_encoding; } set { _engine_encoding = value; } }

        public static string BEEP
        {
            get
            {

                return _beepException;
            }
            set
            {
                _beepException = value;
            }
        }

        public static string TELNET_HEAD_MENUBOT
        {
            get
            {
                return " " + Utils.Utils.AllTrim(TlnConfigManager.TELNET_MENU_DOWN)
                        + "-> " + Utils.Utils.AllTrim(TlnConfigManager.TELNET_MENU_UP) + " <-";
            }
        }

        /// <summary>
        /// Ключ запуска модуля оповещений
        /// </summary>
        public static string TelnetKeyNotify { get; set; }


        /// <summary>
        /// Строка подключения
        /// </summary>
        public static string ConnectionString { get { return _connstring; } set { _connstring = value; } }

        private static string _client_encoding = "KOI8"; // описывает кодировку ввода данных клиентом
        public static string ClientEncoding { get { return _client_encoding; } set { _client_encoding = value; } }

        /// <summary>
        /// Определяет, возвращает значение порта для
        /// удаленного мониторинга пользователей
        /// </summary>
        public static int RemotePort { get { return _remoteport; } set { _remoteport = value; } }


        /// <summary>
        /// Возвращает количество строк в меню на экране
        /// </summary>
        public static int MenuLinesCount { get { return _menuLines; } }

        private static string _ipPlatform = "localhost";
        /// <summary>
        /// Ip Адрес ядра
        /// </summary>
        public static string IpPlatform { get { return _ipPlatform; } }

        private static int _platformPort = 5500;
        /// <summary>
        /// Порт на котором работает ядро
        /// </summary>
        public static int PlatformPort { get { return _platformPort; } }


        /// <summary>
        /// Timeout работы пользователя
        /// в секундах
        /// </summary>
        public static int Timeout { get { return _timeout; } }

        /// <summary>
        /// Определяет записал ли пользователь данные в конфигурацию
        /// после старта сервера
        /// </summary>
        public bool IsDirty { get { return _isDirty; } set { _isDirty = value; } }

        /// <summary>
        /// Порт
        /// </summary>
        public static int Port { get { return _port; } }

        /// <summary>
        /// Порт
        /// </summary>
        public static int WebPort { get { return _webPort; } }

        /// <summary>
        /// Возвращает список пользователей из меню
        /// </summary>
        public List<Menu> GetMenuList { get { return _menus; } }

        /// <summary>
        /// Возвращает список всех форм из конфигурации
        /// </summary>
        //public List<RfForm> GetForms { get { return _forms; } }
        public Dictionary<int, RfForm> GetForms { get { return _forms; } }
        /// <summary>
        /// Версия, которая скомпилирована в конфигурации
        /// </summary>
        public string Version { get { return _compile; } set { _compile = value; } }


        #endregion


        #region Constructor
        static TlnConfigManager()
        {
            TelnetKeyNotify = "~F9";
            lock(_sync)
                ConfigureAppSettings(); 
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public TlnConfigManager()
        {
            lock (_sync)
            {
                MenuList();
                LoadConfiguration();
            }
        }
        #endregion


        #region GetLoginScreen - Возвращает Экран Входа в систему
        public RfForm GetLoginScreen()
        {
            XmlTextReader xml;
            var doc = new XmlDocument();
            string xmlSource = string.Empty;
            try
            {
                xml = new XmlTextReader(Utils.Utils.assemblyPath + @"config.xml");
                try
                {
                    doc.Load(xml);
                    xml.Close();
                }
                catch (System.Exception)
                {
                    xmlSource = Resources.login;
                    doc.LoadXml(xmlSource);
                }

                return GetLoginScreen(doc);

            }
            catch (FormatException ex)
            {
                throw new ConfigurationException(ex, "Ошибка конвертации параметров при компиляции");
            }
            catch (System.Exception e)
            {
                throw new ConfigurationException(e.Message);
            }

        }


        /// <summary>
        /// Заполняет список формами из xml файла
        /// </summary>
        public RfForm GetLoginScreen(XmlDocument doc)
        {
            RfForm form = null; // класс форма

            try
            {
                lock (_sync)
                {
                    XmlNodeList d = doc.GetElementsByTagName("logon");

                    foreach (XmlNode c in d)
                    {
                        XmlNodeList goodNode = c.ChildNodes;
                        foreach (XmlNode child in goodNode)
                        {
                            // ПРоверка на то что это форма
                            if (Utils.Utils.AllTrim(child.Name.ToUpperInvariant()) == "FORM")
                            {
                                #region Создание класса форма

                                form = new RfForm(null, -10, 0);
                                form.Permission = "A"; 
                                form.InitControlFromXML(child);

                                #endregion
                            }
                        }
                    }
                }

                return form;
            }
            catch (FormatException ex)
            {
                throw new ConfigurationException(ex, "Ошибка конвертации параметров при компиляции");
            }
            catch (System.Exception e)
            {
                throw new ConfigurationException(e.Message);
            }
        }
        #endregion

        private void LoadConfiguration()
        {
            XmlTextReader xml;
            XmlDocument doc = new XmlDocument();
            string xmlSource = string.Empty;
            try
            {
                xml = new XmlTextReader(Utils.Utils.assemblyPath + @"config.xml");
                try
                {
                    doc.Load(xml);
                    xml.Close();
                }
                catch (System.Exception)
                {
                    xmlSource = Resources.config;
                    doc.LoadXml(xmlSource);
                }
                LoadConfiguration(doc);

            }
            catch (FormatException ex)
            {
                throw new ConfigurationException(ex, "Ошибка конвертации параметров при компиляции");
            }
            catch (System.Exception e)
            {
                throw new ConfigurationException(e.Message);
            }

        }


        #region Обработка XML файла конфигурации
        /// <summary>
        /// Заполняет список формами из xml файла
        /// </summary>
        private void LoadConfiguration(XmlDocument file)
        {
            _forms = new Dictionary<int, RfForm>();
            RfForm form = null; // класс форма

            try
            {
                XmlNodeList d = file.GetElementsByTagName("configuration");

                // Список процессов
                foreach (XmlNode proc in d)
                {
                    XmlNodeList dd = proc.ChildNodes;
                    #region Список процессов
                    foreach (XmlNode formdd in dd)
                    {
                        if (formdd.Name.ToUpperInvariant() == "PROCESS" && formdd.Attributes["version"].Value == _compile)
                        {
                            XmlNodeList goodNode = formdd.ChildNodes;
                            foreach (XmlNode child in goodNode)
                            {
                                // ПРоверка на то что это форма
                                if (Utils.Utils.AllTrim(child.Name.ToUpperInvariant()) == "FORM")
                                {
                                    form = new RfForm(null);
                                    form.InitControlFromXML(child);
                                    _forms.Add(form.Id, form);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (FormatException ex)
            {
                throw new ConfigurationException(ex, "Ошибка конвертации параметров при компиляции");
            }
            catch (System.Exception e)
            {
                throw new ConfigurationException(e.Message);
            }
        }


        // Заполняет список меню
        private void MenuList()
        {
            try
            {

                try
                {
                    // Пытаемся заполнить меню из таблицы
                    MenuListBD();
                    return;
                }
                catch { }

                XmlTextReader xml;
                var doc = new XmlDocument();

                xml = new XmlTextReader(Utils.Utils.assemblyPath + @"config.xml");
                try
                {
                    doc.Load(xml);
                }
                catch (System.Exception)
                {

                    xml = new XmlTextReader(Utils.Utils.assemblyPath + @"menu.xml");
                    doc.Load(xml);
                }               

                xml.Close();

                MenuList(doc);

            }
            catch (System.Exception e)
            {
                throw new ConfigurationException(e.Message);
            }
        }


        private void MenuListBD()
        {
            _menus = new List<Menu>();

            List<RfControlEventsParams> _params = new List<RfControlEventsParams>();
            Dictionary<string, object> _dic = new Dictionary<string, object>();
            _params.Add(new RfControlEventsParams("@permiss","varchar",10,""));

            object table = ExecuteSqlQuery.ExecuteEvent("[sp_wms_get_user_menu]", _params, _dic, null);
            Ohe.Data.OheDataTable tbl = new Ohe.Data.OheDataTable(table as DataTable);

            for(int i=0; i< tbl.RowsCount; i++)
            {
                _menus.Add(
                            new Menu(
                                      tbl.GetInt(i,"menu_id"),
                                      tbl.GetString(i,"descript").Replace("я", "Я"),
                                      tbl.GetString(i,"permiss").ToUpperInvariant(),
                                      tbl.GetInt(i,"target"),
                                      tbl.GetInt(i, "parent")
                                     )
                          );
            }

            if (_menus.Count == 0)
                throw new System.Exception("Меню не заполнено");
        }


        /// <summary>
        /// Возвращает список меню пользователей
        /// </summary>
        /// <returns></returns>
        private void MenuList(XmlDocument file)
        {
            _menus = new List<Menu>();
            try
            {
                XmlNodeList d = file.GetElementsByTagName("headmenu");

                //XmlNode d = doc["users"];
                foreach (XmlNode c in d)
                {
                    XmlNodeList goodNode = c.ChildNodes;
                    foreach (XmlNode child in goodNode)
                    {
                        if (child.NodeType == XmlNodeType.Element && child.Name == "menu")
                            #region Добавление списка меню
                            try
                            {
                                _menus.Add(
                                    new Menu(
                                              int.Parse(child.Attributes["id"].Value),
                                              Utils.Utils.AllTrim(child.Attributes["descript"].Value.Replace("я", "Я")),
                                              Utils.Utils.AllTrim(child.Attributes["permiss"].Value.ToUpper()),
                                              int.Parse(child.Attributes["target"].Value),
                                              int.Parse(child.Attributes["parent"].Value)
                                    //child.Attributes["type"].Value.ToString().ToUpper().Trim()
                                             )
                                           );

                                if (child.HasChildNodes)
                                    MenuListFromNode(child.ChildNodes);

                            }
                            catch (System.Exception)
                            {
                                #region Без параметров по умолчанию
                                try
                                {
                                    _menus.Add(
                                new Menu(
                                          int.Parse(child.Attributes["id"].Value),
                                          Utils.Utils.AllTrim(child.Attributes["descript"].Value.Replace("я", "Я")),
                                          Utils.Utils.AllTrim(child.Attributes["permiss"].Value.ToUpper()),
                                          int.Parse(child.Attributes["target"].Value),
                                          -1
                                        //child.Attributes["type"].Value.ToString().ToUpper().Trim()
                                         )
                                       );

                                    if (child.HasChildNodes)
                                        MenuListFromNode(child.ChildNodes);
                                }
                                catch (System.Exception ex)
                                {

                                    throw new ConfigurationException("Ошибка компиляции приложения:"
                                                                    + Environment.NewLine
                                                                    + ex.Message);
                                }

                                #endregion
                            }

                            #endregion
                    }
                }
                
            }
            catch (System.Exception e)
            {
                throw new ConfigurationException(e.Message);
            }
        }

        private void MenuListFromNode(XmlNodeList list)
        {
            foreach (XmlNode child in list)
            {
                if (child.NodeType == XmlNodeType.Element && child.Name == "menu")
                    #region Добавление списка меню
                    try
                    {
                        _menus.Add(
                            new Menu(
                                      int.Parse(child.Attributes["id"].Value),
                                      Utils.Utils.AllTrim(child.Attributes["descript"].Value.Replace("я", "Я")),
                                      Utils.Utils.AllTrim(child.Attributes["permiss"].Value.ToUpper()),
                                      int.Parse(child.Attributes["target"].Value),
                                      int.Parse(child.Attributes["parent"].Value)
                                     )
                                   );

                        if (child.HasChildNodes)
                            MenuListFromNode(child.ChildNodes);

                    }
                    catch (System.Exception)
                    {
                        #region Без параметров по умолчанию
                        try
                        {
                            _menus.Add(
                        new Menu(
                                  int.Parse(child.Attributes["id"].Value),
                                  Utils.Utils.AllTrim(child.Attributes["descript"].Value.Replace("я", "Я")),
                                  Utils.Utils.AllTrim(child.Attributes["permiss"].Value.ToUpper()),
                                  int.Parse(child.Attributes["target"].Value),
                                  -1
                                 )
                               );

                            if (child.HasChildNodes)
                                MenuListFromNode(child.ChildNodes);

                        }
                        catch (System.Exception ex)
                        {

                            throw new ConfigurationException("Ошибка компиляции приложения:"
                                                            + Environment.NewLine
                                                            + ex.Message);
                        }
                        #endregion
                    }
                    #endregion
            }
        }

        #endregion


        #region Methods

        #region Выгрузка всех хранимок в файлы конфигурации
        /// <summary>
        /// Заполняет список формами из xml файла
        /// </summary>
        public static void SaveAllObjectToFile()
        {
            RfForm form = null; // класс форма

            try
            {
                XmlTextReader xml = new XmlTextReader(Utils.Utils.assemblyPath + @"config.xml");
                XmlDocument doc = new XmlDocument();
                doc.Load(xml);
                XmlNodeList d = doc.GetElementsByTagName("configuration");

                // Список процессов
                foreach (XmlNode proc in d)
                {
                    XmlNodeList dd = proc.ChildNodes;
                    #region Список процессов
                    foreach (XmlNode formdd in dd)
                    {
                        if (formdd.Name.ToUpperInvariant() == "PROCESS" && formdd.Attributes["version"].Value == _compile)
                        {
                            DirectoryInfo _path = new DirectoryInfo("SourceCode\\" + formdd.Attributes["title"].Value + "_v"+ formdd.Attributes["version"].Value);
                            if (!_path.Exists)
                                _path.Create(); //formdd.Attributes["descript"].Value + formdd.Attributes["version"].Value + DateTime.Now.Millisecond.ToString());

                            XmlNodeList goodNode = formdd.ChildNodes;
                            foreach (XmlNode child in goodNode)
                            {
                                // ПРоверка на то что это форма
                                if (Utils.Utils.AllTrim(child.Name.ToUpperInvariant()) == "FORM")
                                {
                                    form = new RfForm(null);
                                    form.InitControlFromXML(child);

                                    RfControlEvents _event = form.OnConrolSubmitEvent;
                                        WriteSource(_event,_path.FullName);

                                    _event = form.OnControlChangeEvent;
                                        WriteSource(_event, _path.FullName);

                                    _event = form.OnControlExitEvent;
                                        WriteSource(_event, _path.FullName);

                                    _event = form.OnControlInitEvent;
                                        WriteSource(_event, _path.FullName);

                                        foreach (RfControl cntl in form.Controls)
                                        {
                                            _event = cntl.OnConrolSubmitEvent;
                                            WriteSource(_event, _path.FullName);

                                            _event = cntl.OnControlChangeEvent;
                                            WriteSource(_event, _path.FullName);

                                            _event = cntl.OnControlExitEvent;
                                            WriteSource(_event, _path.FullName);

                                            _event = cntl.OnControlInitEvent;
                                            WriteSource(_event, _path.FullName);

                                        }
                                }
                            }


                        }
                    }
                    #endregion
                }
                xml.Close();
            }
            catch (FormatException ex)
            {
                throw new ConfigurationException(ex, "Ошибка конвертации параметров при компиляции");
            }
            catch (System.Exception e)
            {
                throw new ConfigurationException(e.Message);
            }
        }


        /// <summary>
        /// Заполняет список формами из xml файла
        /// </summary>
        public static void SaveAllObjectToFile2()
        {
            RfForm form = null; // класс форма

            List<RfControlEvents> _shareObject = new List<RfControlEvents>();

            List<string[]> _processObject = new List<string[]>();

            List<string> _processObjectAll = new List<string>();

            try
            {
                XmlTextReader xml = new XmlTextReader(Utils.Utils.assemblyPath + @"config.xml");
                XmlDocument doc = new XmlDocument();
                doc.Load(xml);
                XmlNodeList d = doc.GetElementsByTagName("configuration");
                DirectoryInfo _path;

                // Список процессов
                foreach (XmlNode proc in d)
                {
                    XmlNodeList dd = proc.ChildNodes;
                    #region Список процессов
                    foreach (XmlNode formdd in dd)
                    {
                        if (formdd.Name.ToUpperInvariant() == "PROCESS" && formdd.Attributes["version"].Value == _compile)
                        {

                            _path = new DirectoryInfo("SourceCode\\" + formdd.Attributes["title"].Value + "_v"+ formdd.Attributes["version"].Value);
                            if (!_path.Exists)
                                _path.Create(); //formdd.Attributes["descript"].Value + formdd.Attributes["version"].Value + DateTime.Now.Millisecond.ToString());
                            
                            XmlNodeList goodNode = formdd.ChildNodes;
                            foreach (XmlNode child in goodNode)
                            {
                                // ПРоверка на то что это форма
                                if (Utils.Utils.AllTrim(child.Name.ToUpperInvariant()) == "FORM")
                                {
                                    form = new RfForm(null);
                                    form.InitControlFromXML(child);

                                    #region Forms Events
                                    RfControlEvents _event = form.OnConrolSubmitEvent;
                                    if (_event != null)
                                    {
                                        WriteSource(_event, _path.FullName);

                                        if (!_processObjectAll.Contains(_event.SpName))
                                        {
                                            _processObjectAll.Add(_event.SpName);
                                        }
                                        else
                                        {
                                            _shareObject.Add(_event);
                                        }
                                    }

                                    _event = form.OnControlChangeEvent;

                                    if (_event != null)
                                    {
                                        WriteSource(_event, _path.FullName);

                                        if (!_processObjectAll.Contains(_event.SpName))
                                        {
                                            _processObjectAll.Add(_event.SpName);
                                        }
                                        else
                                        {
                                            _shareObject.Add(_event);
                                        }
                                    }

                                    _event = form.OnControlExitEvent;
                                    if (_event != null)
                                    {
                                        WriteSource(_event, _path.FullName);

                                        if (!_processObjectAll.Contains(_event.SpName))
                                        {
                                            _processObjectAll.Add(_event.SpName);
                                        }
                                        else
                                        {
                                            _shareObject.Add(_event);
                                        }
                                    }


                                    _event = form.OnControlInitEvent;
                                    if (_event != null)
                                    {
                                        WriteSource(_event, _path.FullName);

                                        if (!_processObjectAll.Contains(_event.SpName))
                                        {
                                            _processObjectAll.Add(_event.SpName);
                                        }
                                        else
                                        {
                                            _shareObject.Add(_event);
                                        }
                                    }
                                    #endregion

                                    foreach (RfControl cntl in form.Controls)
                                    {
                                        #region Control Events
                                        _event = cntl.OnConrolSubmitEvent;
                                        if (_event != null)
                                        {
                                            WriteSource(_event, _path.FullName);

                                            if (!_processObjectAll.Contains(_event.SpName))
                                            {
                                                _processObjectAll.Add(_event.SpName);
                                            }
                                            else
                                            {
                                                _shareObject.Add(_event);
                                            }
                                        }


                                        _event = cntl.OnControlChangeEvent;
                                        if (_event != null)
                                        {
                                            WriteSource(_event, _path.FullName);

                                            if (!_processObjectAll.Contains(_event.SpName))
                                            {
                                                _processObjectAll.Add(_event.SpName);
                                            }
                                            else
                                            {
                                                _shareObject.Add(_event);
                                            }
                                        }


                                        _event = cntl.OnControlExitEvent;
                                        if (_event != null)
                                        {
                                            WriteSource(_event, _path.FullName);

                                            if (!_processObjectAll.Contains(_event.SpName))
                                            {
                                                _processObjectAll.Add(_event.SpName);
                                            }
                                            else
                                            {
                                                _shareObject.Add(_event);
                                            }
                                        }
                                        _event = cntl.OnControlInitEvent;
                                        if (_event != null)
                                        {
                                            WriteSource(_event, _path.FullName);

                                            if (!_processObjectAll.Contains(_event.SpName))
                                            {
                                                _processObjectAll.Add(_event.SpName);
                                            }
                                            else
                                            {
                                                _shareObject.Add(_event);
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }


                        }
                    }
                    #endregion
                }
                xml.Close();

                _path = new DirectoryInfo("SourceCode\\общиеХП");
                if (!_path.Exists)
                    _path.Create(); //formdd.Attributes["descript"].Value + formdd.Attributes["version"].Value + DateTime.Now.Millisecond.ToString());

                foreach(RfControlEvents sql in  _shareObject)
                {
                    WriteSource(sql, _path.FullName);
                }
            }
            catch (FormatException ex)
            {
                throw new ConfigurationException(ex, "Ошибка конвертации параметров при компиляции");
            }
            catch (System.Exception e)
            {
                throw new ConfigurationException(e.Message);
            }
        }

        #endregion



        private static void WriteSource(RfControlEvents Event,string FileName)
        {
            if (Event != null)
            {
                Ohe.Data.OheDataClass _db = new Ohe.Data.OheDataClass("sql", TlnConfigManager.ConnectionString);
                Ohe.Data.OheDataTable _table = _db.GetTableByQuery("SELECT OBJECT_DEFINITION(OBJECT_ID(N'" + Event.SpName + "','P'));");
                StringBuilder _source = new StringBuilder();

                _source.AppendLine(string.Format(@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[{0}]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO", Event.SpName.Replace("[","").Replace("]","")));

                for (int i = 0; i < _table.RowsCount; i++)
                {
                    _source.AppendLine(_table.GetString(i, 0));
                }

                Utils.Utils.WriteSourceCode(_source.ToString(), FileName +"\\" + Event.SpName + ".sql");

            }

        }
        #endregion


        #region ConfigureAppSettings
        /// <summary>
        /// Определяет все переменные конфигурации system
        /// </summary>
        private static void ConfigureAppSettings()
        {
            XmlTextReader xml = new XmlTextReader(Utils.Utils.assemblyPath + @"system.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(xml);
            XmlNodeList d = doc.GetElementsByTagName("configuration");

            foreach (XmlNode c in d)
            {
                XmlNodeList goodNode = c.ChildNodes;
                foreach (XmlNode child in goodNode)
                {
                    if (child.NodeType == XmlNodeType.Element)
                        switch (Utils.Utils.AllTrim(child.Attributes["key"].Value.ToUpperInvariant()))
                        {
                            #region Switch

                            #region String values
                            case "CONSTRING":
                                _connstring = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "DATEFORMAT":
                                _shortDate = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "TELNETKEY_Y":
                                _telnet_KEY_Y = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "TELNETKEY_N":
                                _telnet_KEY_N = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "TELNETKEY_HELP":
                                _telnet_HELP_STR = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "TELNETKEY_MENUDOWN":
                                _telnet_down = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "TELNETKEY_MENUUP":
                                _telnet_up = Utils.Utils.AllTrim(child.Attributes["value"].Value) == string.Empty ? "ENT" : Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "TELNETKEY_MENUCURSOR_X":
                                _telnet_CURSOR_X = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "TELNETKEY_MENUCURSOR_Y":
                                _telnet_CURSOR_Y = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "ENGINE_ENCODING":
                                _engine_encoding = child.Attributes["value"].Value;
                                break;
                            case "CLIENT_ENCODING":
                                _client_encoding = child.Attributes["value"].Value;
                                break;
                            case "COMPILEVERSION":
                                _compile = child.Attributes["value"].Value;
                                break;
                            case "PLATFORMIP":
                                _ipPlatform = child.Attributes["value"].Value;
                                break;

                            case "TELNETKEY_NOTIFY":
                                TelnetKeyNotify = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;

                            case "BEEP":
                                _beepException = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;


                            #endregion

                            #region Int Values
                            case "PORT":
                                int.TryParse(child.Attributes["value"].Value, out _port);
                                break;
                            case "WEBPORT":
                                int.TryParse(child.Attributes["value"].Value, out _webPort);
                                break;
                            case "REMOTE_PORT":
                                int.TryParse(child.Attributes["value"].Value, out _remoteport);
                                break;
                            case "TELNET_MENULINES":
                                int.TryParse(child.Attributes["value"].Value, out _menuLines);
                                break;
                            case "TELNET_WIDHT":
                                int.TryParse(child.Attributes["value"].Value, out _telnet_widht);
                                break;
                            case "TELNET_HEIGHT":
                                int.TryParse(child.Attributes["value"].Value, out _telnet_height);
                                break;
                            case "TELNET_TIMEOUT":
                                int.TryParse(child.Attributes["value"].Value, out _timeout);
                                break;
                            case "PLATFORMPORT":
                                int.TryParse(child.Attributes["value"].Value, out _platformPort);
                                break;
                            
                            #endregion

                            #endregion
                        }
                }
            }
            xml.Close();
        }
        #endregion


        #region Проверка валидности конфигурации
        /// <summary>
        /// Стартует поток для проверки на корректность конфигурации
        /// </summary>
        public void ConfigStartMaintenance()
        {
            lock (_sync)
            {
                if (_thread == null)
                {
                    _thread = new Thread(new ThreadStart(CheckConfig));
                    _thread.Priority = ThreadPriority.Lowest;
                    _thread.Start();
                }
            }
        }

        /// <summary>
        /// Останавливает поток для проверки корректности конфиграции
        /// </summary>
        public void ConfigStopMaintenance()
        {
            lock(_sync)
                isContinue = false;
        }

        /// <summary>
        /// Проверяет наличие обновлений в конфигурационном файле
        /// </summary>
        private void CheckConfig()
        {
            while (isContinue)
            {
                if (IsDirty)
                {
                    lock (_sync)
                    {
                        LoadConfiguration();
                        MenuList();
                        IsDirty = false;
                    }
                }
                Thread.Sleep(1000);
            }
        }
        #endregion

       
        #region GetEvent (comment)
    /*
    /// <summary>
    /// Генерирует событие для форм, или контролов
    /// </summary>
    /// <param name="FormId">Идентификатор формы</param>
    /// <param name="Child">Идентификатор контрола</param>
    /// <param name="EventName">Имя события</param>
    /// <param name="events">Кусок xml файла</param>
    /// <returns>RfControlEvents</returns>
    private RfControlEvents GetEvent(int FormId, int Child, string EventName, XmlNode events)
    {
        // Определяем имя хранимой процедуры
        string store_proc;
        string _tableId = string.Empty; // Идентификатор таблицы куда возвращаются параметры
        try
        {
            store_proc = Utils.Utils.AllTrim(events.Attributes["spname"].Value);

            // Проверк на необязательный атрибут id таблицы
            if (events.Attributes["id"] != null)
                _tableId = Utils.Utils.AllTrim(events.Attributes["id"].Value);
        }
        catch
        {
            throw new Ohe.Telnet.Exceptions.ConfigurationException("Ошибка:Нет хранимой процедуры"
                                   + Environment.NewLine
                                   + "Идентификатор формы: " + FormId.ToString()
                                   + Environment.NewLine
                                   + "Идентификатор контрола: " + Child.ToString()
                                   + Environment.NewLine
                                   + "Событие: " + EventName
                                  );
        }
        // Создаем событие
        RfControlEvents evnt = new RfControlEvents(FormId, Child, EventName, store_proc);

        // Параметр таблицы
        evnt.TableId = _tableId;

        // парметр хранимой процедуры
        RfControlEventsParams pr;
        // Добавляем параметры к событию
        if (events.HasChildNodes)
            #region Цикл по параметрам
            foreach (XmlNode prms in events.ChildNodes)
            {
                if (Utils.Utils.AllTrim(prms.Name.ToUpperInvariant()) == "PARAMS"
                            && prms.NodeType == XmlNodeType.Element)
                {
                    XmlAttributeCollection attrColl = prms.Attributes;
                    IEnumerator ienum = attrColl.GetEnumerator();

                    #region Define values
                    // название параметра
                    string name = string.Empty;
                    // тип параметра
                    string type = string.Empty;
                    // размер значения параметра
                    int size = -1;
                    // значение параметра
                    string val = string.Empty;
                    // идентификатор контрола с которого берется параметр
                    string id = string.Empty;
                    #endregion

                    // Получаем все атрибуты формы
                    while (ienum.MoveNext())
                    {
                        XmlAttribute attr = (XmlAttribute)ienum.Current;

                        #region Switch
                        switch (Utils.Utils.AllTrim(attr.Name.ToUpperInvariant()))
                        {
                            case "NAME":
                                name = Utils.Utils.AllTrim(attr.Value);
                                break;
                            case "TYPE":
                                type = Utils.Utils.AllTrim(attr.Value);
                                break;
                            case "VALUE":
                                val = Utils.Utils.AllTrim(attr.Value);
                                break;
                            case "SIZE":
                                try
                                {
                                    size = int.Parse((attr.Value != string.Empty ? attr.Value : "0"));
                                }
                                catch (FormatException ex)
                                {
                                    throw new Ohe.Telnet.Exceptions.ConfigurationException(ex,
                                                      "Ошибка: неверный тип параметра"
                                                      + Environment.NewLine
                                                      + "Идентификатор формы: " + FormId
                                                      + Environment.NewLine
                                                      + (Child < 0 ? string.Empty : "Идентификатор Контрола:" + Child)
                                                      + Environment.NewLine
                                                      + "Событие:  " + store_proc + Environment.NewLine
                                                      + "Параметр: " + name + Environment.NewLine
                                                      + "Значение: " + val + Environment.NewLine
                                                      + "Размер: " + attr.Value + Environment.NewLine
                                                      );
                                }
                                break;
                            case "ID":
                                id = Utils.Utils.AllTrim(attr.Value);
                                break;
                        }
                        #endregion
                    }

                    // Генерируем ошибку при недостаточном количестве параметров
                    if (name == string.Empty || type == string.Empty)
                        throw new Ohe.Telnet.Exceptions.ConfigurationException("Не заданы основные значения для"
                                                      + Environment.NewLine
                                                      + "параметров хранимой процедуры"
                                                      + Environment.NewLine
                                                      + "Идентификатор формы: " + FormId
                                                      + Environment.NewLine
                                                      + (Child < 0 ? string.Empty : "Идетинификатор Контрола:" + Child)
                                                      + Environment.NewLine
                                                      + "Событие: " + EventName);

                    pr = new RfControlEventsParams(name, type, size, val, id);
                    // Добавляем параметр к хранимой процедуре
                    evnt.AddParametr(pr);
                }
            }
            #endregion

        return evnt;
    }
    */
    #endregion


        #region Old Properties
        /*
        public static string ShortDateTime
        {
            get
            {
                if (_shortDate == null)
                {
                    ConfigureAppSettings();
                    return _shortDate;
                }
                else
                    return _shortDate;
            }
            set { _shortDate = value; }
        }

        private static int _telnet_widht = -1;
        public static int TELNET_WIDTH
        {
            get
            {
                if (_telnet_widht == -1)
                {
                    ConfigureAppSettings();
                    return _telnet_widht;
                }
                else
                    return _telnet_widht;
            }
            set { _telnet_widht = value; }
        }

        private static int _telnet_height = -1;
        public static int TELNET_HEIGHT
        {
            get
            {
                if (_telnet_height == -1)
                {
                    ConfigureAppSettings();
                    return _telnet_height;
                }
                else
                    return _telnet_height;
            }
            set { _telnet_height = value; }
        }

        private static string _telnet_HELP_STR = null; // описывает кнопку для помощи в меню
        public static string TELNET_HELP_STR
        {
            get
            {
                if (_telnet_HELP_STR == null)
                {
                    ConfigureAppSettings();
                    return _telnet_HELP_STR;
                }
                else
                    return _telnet_HELP_STR;
            }
            set { _telnet_HELP_STR = value; }
        }

        private static string _telnet_KEY_Y = null;   // клавиши для YES / NO
        public static string TELNET_KEY_Y
        {
            get
            {
                if (_telnet_KEY_Y == null)
                {
                    ConfigureAppSettings();
                    return _telnet_KEY_Y;
                }
                else
                    return _telnet_KEY_Y;
            }
            set { _telnet_KEY_Y = value; }
        }

        private static string _telnet_KEY_N = null;
        public static string TELNET_KEY_N
        {
            get
            {
                if (_telnet_KEY_N == null)
                {
                    ConfigureAppSettings();
                    return _telnet_KEY_N;
                }
                else
                    return _telnet_KEY_N;
            }
            set { _telnet_KEY_N = value; }
        }

        private static string _telnet_CURSOR_X = null; // позиция ввода курсора на экране меню
        public static string TELNET_CURSOR_X
        {
            get
            {
                if (_telnet_CURSOR_X == null)
                {
                    ConfigureAppSettings();
                    return _telnet_CURSOR_X;
                }
                else
                    return _telnet_CURSOR_X;
            }
            set { _telnet_CURSOR_X = value; }
        }

        private static string _telnet_CURSOR_Y = null;
        public static string TELNET_CURSOR_Y
        {
            get
            {
                if (_telnet_CURSOR_Y == null)
                {
                    ConfigureAppSettings();
                    return _telnet_CURSOR_Y;
                }
                else
                    return _telnet_CURSOR_Y;
            }
            set { _telnet_CURSOR_Y = value; }
        }

        private static string _telnet_up = null;
        //public static string  = "ENT"; // описывает кнопку пролистывания меню вверх
        public static string TELNET_MENU_UP
        {
            get
            {
                if (_telnet_up == null)
                {
                    ConfigureAppSettings();
                    return _telnet_up;
                }
                else
                    return _telnet_up;
            }
            set { _telnet_up = value; }
        }

        private static string _telnet_down = null; // описывает кнопку пролистывания меню вниз
        public static string TELNET_MENU_DOWN
        {
            get
            {
                if (_telnet_down == null)
                {
                    ConfigureAppSettings();
                    return _telnet_down;
                }
                else
                    return _telnet_down;
            }
            set { _telnet_down = value; }
        }

        public static string TELNET_HEAD_MENUBOT
        {
            get
            {
                return " " + Utils.Utils.AllTrim(TlnConfigManager.TELNET_MENU_DOWN)
                        + "-> " + Utils.Utils.AllTrim(TlnConfigManager.TELNET_MENU_UP) + " <-";
            }
        }

        private static string _engine_encoding = null; // описывает кодировку вывода сервера
        public string ServerEncoding
        {
            get
            {
                if (_engine_encoding == null)
                {
                    ConfigureAppSettings();
                    return _engine_encoding;
                }
                else
                    return _engine_encoding;
            }
            set { _engine_encoding = value; }
        }

        private static string _client_encoding = null; // описывает кодировку ввода данных клиентом
        public string ClientEncoding
        {
            get
            {
                if (_client_encoding == null)
                {
                    ConfigureAppSettings();
                    return _client_encoding;
                }
                else
                    return _client_encoding;
            }
            set { _client_encoding = value; }
        }
        */
        #endregion


        #region GetControl (comment)
    /*
    /// <summary>
    /// Генерирует событие для форм, или контролов
    /// </summary>
    /// <param name="FormId">Идентификатор формы</param>
    /// <param name="events">Кусок xml файла</param>
    /// <returns>RfControlEvents</returns>
    private RfControl GetControl(int FormId, XmlNode events)
    {
        RfControl control = null;
        XmlAttributeCollection attrColl = events.Attributes;
        IEnumerator ienum = attrColl.GetEnumerator();

        #region Define values
        // идентификатор контрола с которого берется параметр
        string id = string.Empty;
        // Имя контрола использется как промежуточная переменная 
        // Если контрол имеет название колонки как параметр
        string cntl = string.Empty;
        // тип контрола
        string controltype = string.Empty;
        // тест контрола
        string descript = string.Empty;
        // тип значения контрола
        string type = string.Empty;
        // определяет контрол только для чтения
        bool readonlys = false;
        // определяет ввод обязателен для типа input
        bool mandatory = false;
        // определяет номер строки контрола на экране
        int line = -99;
        // определяет параметры курсора для ввода значений
        int cursor_x = -99;
        int cursor_y = -99;  // параметр расчитывает относительно значения line
        // значение контрола
        string val = string.Empty;
        // опередляет номер колонки контрола исли значение есть таблица    
        string column = "-99";
        // Определяет горизонтальное положение контрола
        string align = "left";
        // Определяет идентификатор контрола при нажатии функциональной клавиши
        int target = -99;

        bool child = false; //определяет, является ли функциональная клавиша,
                            // клавишей перехода, или результат возвращается в 
                            // в вызвавшую его форму
        bool mark = false;   // Определяет менять ли цвет фона для контрола или нет

        string _checkvalue = string.Empty; // определяет видимость контрола взависимости
                                          // от значений глобального кеша

        #endregion
        // Получаем все атрибуты контрола
        while (ienum.MoveNext())
        {
            XmlAttribute attr = (XmlAttribute)ienum.Current;
            #region Switch
            switch (Utils.Utils.AllTrim(attr.Name.ToUpperInvariant()))
            {
                case "ID":
                    id = Utils.Utils.AllTrim(attr.Value.ToUpper());
                    break;
                case "CONTROLTYPE":
                    controltype = Utils.Utils.AllTrim(attr.Value.ToUpperInvariant());
                    break;
                case "DESCRIPT":
                    descript = Utils.Utils.AllTrim(attr.Value.Replace("я","Я"));
                    break;
                case "TYPE":
                    type = Utils.Utils.AllTrim(attr.Value.ToLowerInvariant());
                    break;
                case "READONLY":
                    readonlys = Utils.Utils.StrToBool(Utils.Utils.AllTrim(attr.Value));
                    break;
                case "MANDATORY":
                    mandatory = Utils.Utils.StrToBool(Utils.Utils.AllTrim(attr.Value));
                    break;
                case "LINE":
                    #region line
                    try
                    {
                        line = int.Parse(attr.Value);
                    }
                    catch (FormatException ex)
                    {
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(ex,
                                          "Ошибка: неверный тип параметра"
                                          + Environment.NewLine
                                          + "Идентификатор формы: " + FormId
                                          + Environment.NewLine
                                          + (id == string.Empty ? string.Empty : "Идентификатор Контрола: " + id)
                                          + Environment.NewLine
                                          + "Параметр: line = " + attr.Value);
                    }

                    #endregion
                    break;
                case "CURSOR_X":
                    #region cursor_x
                    try
                    {
                        cursor_x = int.Parse(attr.Value);
                    }
                    catch (FormatException ex)
                    {
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(ex,
                                          "Ошибка: неверный тип параметра"
                                          + Environment.NewLine
                                          + "Идентификатор формы: " + FormId
                                          + Environment.NewLine
                                          + (id == string.Empty ? string.Empty : "Идентификатор Контрола: " + id)
                                          + Environment.NewLine
                                          + "Параметр: cursor_x = " + attr.Value);
                    }

                    #endregion
                    break;
                case "CURSOR_Y":
                    #region cursor_y
                    try
                    {
                        cursor_y = int.Parse(attr.Value);
                    }
                    catch (FormatException ex)
                    {
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(ex,
                                          "Ошибка: неверный тип параметра"
                                          + Environment.NewLine
                                          + "Идентификатор формы: " + FormId
                                          + Environment.NewLine
                                          + (id == string.Empty ? string.Empty : "Идентификатор Контрола: " + id)
                                          + Environment.NewLine
                                          + "Параметр: cursor_y = " + attr.Value);
                    }

                    #endregion
                    break;
                case "VALUE":
                    val = Utils.Utils.AllTrim(attr.Value);
                    break;
                case "TARGET":
                    #region target
                    try
                    {
                        target = int.Parse(attr.Value);
                    }
                    catch (FormatException ex)
                    {
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(ex,
                                          "Ошибка: неверный тип параметра"
                                          + Environment.NewLine
                                          + "Идентификатор формы: " + FormId
                                          + Environment.NewLine
                                          + (id == string.Empty ? string.Empty : "Идентификатор Контрола: " + id)
                                          + Environment.NewLine
                                          + "Параметр: target = " + attr.Value);
                    }

                    #endregion
                    break;
                case "ALIGN":
                    align = Utils.Utils.AllTrim(attr.Value);
                    break;
                case "CHILD":
                    child = Utils.Utils.StrToBool(attr.Value);
                    break;
                case "ISMARK":
                    mark = Utils.Utils.StrToBool(attr.Value);
                    break;
                case "CHECKVALUE":
                    _checkvalue = Utils.Utils.AllTrim(attr.Value);
                    break;
                }
            #endregion
        }

        if (controltype == string.Empty)
            throw new Ohe.Telnet.Exceptions.ConfigurationException(
                                          "Ошибка: неверный тип контрола"
                                          + Environment.NewLine
                                          + "Идентификатор формы: " + FormId
                                          + Environment.NewLine
                                          + (id == string.Empty ? string.Empty : "Идентификатор Контрола: " + id)
                                          + Environment.NewLine
                                          + "Параметр: controltype неопределен");

        // Определение типов контрола для формы
        try
        {
            switch (controltype)
            {
                case "TEXT":
                    #region Контрол типа текст
                    if (line == -99)
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(
                                                      "Ошибка: неверный параметр контрола"
                                                      + Environment.NewLine
                                                      + "Идентификатор формы: " + FormId
                                                      + Environment.NewLine
                                                      + "Тип контрола: " + controltype);
                    if (id.Contains("(") && id.Contains(")"))
                    {
                        int start = id.IndexOf("(");
                        cntl = id.Substring(0, start);
                        column = id.Substring(start + 1, (id.Length - start) - 2);
                        id = cntl;
                    }

                    control = new RfControl(line, FormId, OheConstant.Type.Text, descript, val,
                                                (cursor_x == -99 ? descript.Length + 1 : cursor_x),
                                                (cursor_y == -99 ? 0 : cursor_y));
                    // Определяет номер колонки для записи.
                    if (column != "-99")
                        control.Column = column;

                    control.ValueID = id;   
                    // Этот параметр обязателен для контрола
                    control.typeValue = "string";
                    // Горизотальное положение контрола
                    control.Align = align;
                    // Признак изменения фона контрола
                    control.IsMark = mark;

                    // Признак видимости контрола в зависимости от других значений
                    control.CheckValue = _checkvalue;
                    #endregion
                    break;
                case "INPUT":
                    #region Контрол типа ввода данных
                    if (line == -99 || id == string.Empty
                                    || type == string.Empty)
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(
                                                      "Ошибка: нет основных параметров контрола"
                                                      + Environment.NewLine
                                                      + "Идентификатор формы: " + FormId
                                                      + Environment.NewLine
                                                      + "Идентификатор Контрола: " + (id == string.Empty ? "Неопределен" : id)
                                                      + Environment.NewLine
                                                      + "Позиция Контрола: " + (line == -99 ? "Неопределена" : line.ToString())
                                                      + Environment.NewLine
                                                      + "Тип данных: " + (type == string.Empty ? "Неопределен" : type)
                                                      );

                    // Контрол имеет название колонки которую надо выводить
                    // Надо ее определить
                    if (id.Contains("(") && id.Contains(")"))
                    {
                        int start = id.IndexOf("(");
                        // Определяем название записи (контрола)
                        cntl = id.Substring(0, start);
                        // Имя колонки для вывода информации 
                        column = id.Substring(start + 1, (id.Length - start - 2));
                        id = cntl;
                    }

                    // Создание контрола для возврата
                    control = new RfControl(line, FormId, OheConstant.Type.InputBox, descript, val,
                                                (cursor_x == -99 ? descript.Length + 1 : cursor_x),
                                                (cursor_y == -99 ? 0 : cursor_y));

                    // Определяет номер колонки для записи.
                    if (column != "-99")
                        control.Column = column;

                    control.typeValue = type;
                    control.ReadOnly = readonlys;
                    control.ValueID = id;
                    // Горизотальное положение контрола
                    control.Align = align;
                    control.Mandatory = mandatory;

                    // Признак видимости контрола в зависимости от других значений
                    control.CheckValue = _checkvalue;
                    #endregion
                    break;
                case "LIST":
                    #region Контрол типа лист
                    if (line == -99 || id == string.Empty)
                                    //|| type == string.Empty)
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(
                                                      "Ошибка: нет основных параметров контрола"
                                                      + Environment.NewLine
                                                      + "Идентификатор формы: " + FormId
                                                      + Environment.NewLine
                                                      + "Идентификатор Контрола: " + (id == string.Empty ? "Неопределен" : id)
                                                      + Environment.NewLine
                                                      + "Позиция Контрола: " + (line == -99 ? "Неопределена" : line.ToString())
                                                      + Environment.NewLine
                                                      + "Тип данных: " + (type == string.Empty ? "Неопределен" : type)
                                                      );
                    // Создание контрола для возврата
                    control = new RfControlList(line, TlnConfigManager.MenuLinesCount,
                                            TlnConfigManager.TELNET_MENU_DOWN, TlnConfigManager.TELNET_MENU_UP);
                    control.typeValue = type;
                    control.ValueID = id;
                    control.Y = (cursor_y == -99 ? -1 : cursor_y);
                    control.X = (cursor_x == -99 ? 0 : cursor_x);
                    control.Mandatory = mandatory;

                    // Признак видимости контрола в зависимости от других значений
                    control.CheckValue = _checkvalue;
                    #endregion
                    break;
                case "FUNCTION":
                    #region Контрол типа function
                    if (target == -99)
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(
                                                      "Ошибка: нет основных параметров контрола"
                                                      + Environment.NewLine
                                                      + "Идентификатор формы: " + FormId
                                                      + Environment.NewLine
                                                      + "Идентификатор формы target: Неопределен"
                                                      + Environment.NewLine
                                                      + "Тип контрола: function"
                                                      );
                    // Создание контрола для возврата
                    control = new RfControlKey(line, FormId, target, val, descript, child);

                    // Признак видимости контрола в зависимости от других значений
                    control.CheckValue = _checkvalue;
                    #endregion
                    break;
                case "HELP":
                    control = new RfControlHelp(101, FormId, OheConstant.Type.Text, string.Empty, val, 0, 0);
                    // Горизотальное положение контрола
                    control.Align = align;
                    break;
                case "MESSAGE":
                    #region Контрол типа сообщение
                    if (line == -99)
                        throw new Ohe.Telnet.Exceptions.ConfigurationException(
                                  "Ошибка: нет основных параметров контрола"
                                  + Environment.NewLine
                                  + "Идентификатор формы: " + FormId
                                  + Environment.NewLine
                                  + "Положение на экране (line): Неопределено"
                                  + Environment.NewLine
                                  + "Тип контрола: message"
                                  );

                    control = new RfControlMessage(line, FormId, OheConstant.Type.Text, descript, string.Empty,
                                       (cursor_x == -99 ? 0 : cursor_x),
                                               (cursor_y == -99 ? TlnConfigManager.TELNET_HEIGHT : cursor_y));

                    // Горизотальное положение контрола
                    control.Align = align;
                    #endregion
                    break;
            }
        }
        catch (Exception ex)
        {
            
            throw new Ohe.Telnet.Exceptions.ConfigurationException(
                          ex,
                          "Ошибка: компиляции контрола"
                          + Environment.NewLine
                          + "Идентификатор формы: " + FormId
                          + Environment.NewLine
                          + (id == string.Empty ? string.Empty : "Идентификатор Контрола: " + id)
                          + Environment.NewLine
                          + "Параметр: id = " + id + Environment.NewLine
                          + ex.Message);
        }

        // Добавляем параметры к событию
        if (events.HasChildNodes)
        {
            _dt.Rows.Clear();
            #region Цикл по параметрам
            foreach (XmlNode prms in events.ChildNodes)
            {
                if (prms.NodeType == XmlNodeType.Element)
                {
                    switch (Utils.Utils.AllTrim(prms.Name.ToUpperInvariant()))
                    {
                        case "INIT":
                            //control.AddEventHanler(GetEvent(FormId, line, "OnInit", prms));
                            control.AddEvent(GetEvent(FormId, line, "OnInit", prms));
                            break;
                        case "CHANGE":
                            //control.AddEventHanler(GetEvent(FormId, line, "OnChange", prms));
                            control.AddEvent(GetEvent(FormId, line, "OnChange", prms));
                            break;
                        case "OPTION":
                            DataRow dr = _dt.NewRow();
                            dr[0] = Utils.Utils.AllTrim(prms.Attributes["id"].Value);
                            dr[1] = Utils.Utils.AllTrim(prms.Attributes["value"].Value);
                            _dt.Rows.Add(dr);
                            break;
                    }
                }
            }
            #endregion
        }

        if (_dt.Rows.Count > 0 && control is RfControlList)
            ((RfControlList)control).FillStaticValue(_dt);


        return control;
    }
    */
    #endregion


        #region GetAllFormFromXML (comment)
    /*
    /// <summary>
    /// Заполняет список формами из xml файла
    /// </summary>
    private void GetAllFormFromXML()
    {
        //_forms = new List<RfForm>();
        //_forms = new Hashtable(new OheCultureComparer());
        _forms = new Dictionary<int, RfForm>();
        RfForm form = null; // класс форма

        try
        {
            XmlTextReader xml = new XmlTextReader(Utils.Utils.assemblyPath + @"config.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(xml);
		    XmlNodeList d = doc.GetElementsByTagName("configuration");

			 // Список процессов
			 foreach (XmlNode proc in d)
			 {
				 XmlNodeList dd = proc.ChildNodes;
				 #region Список процессов
				 foreach (XmlNode formdd in dd)
				 {
					 if (formdd.Name.ToUpperInvariant() == "PROCESS" && formdd.Attributes["version"].Value == _compile)
					 {
						 XmlNodeList goodNode = formdd.ChildNodes;
						 foreach (XmlNode child in goodNode)
						 {
							 // ПРоверка на то что это форма
							 if (Utils.Utils.AllTrim(child.Name.ToUpperInvariant()) == "FORM")
							 {
								 #region Создание класса форма
								 XmlAttributeCollection attrColl = child.Attributes;
								 IEnumerator ienum = attrColl.GetEnumerator();

								 #region Define values
								 // идентификатор формы
								 int formId = -1;
								 // target идентификатор
								 int target = -1;
								 // привилегия формы
								 string permiss = string.Empty;
								 // привилегия формы
								 int parent = -2;
								 // определяет находится ли форма в цикле бизнес процесса
								 bool cycle = false;
								 // Описание формы
								 string descript = string.Empty;

								 // Признак автоматической очистки кеша
								 bool _autorefresh = false;

								 // Признак проверки значений глобального кеша
								 string _checkstring = string.Empty;

								 // Признак проверки товара на погрузчике
								 bool _checkfork = false;

								 // Признак того что форма есть форма проверки
								 // параметров
								 bool _isif = false;

								 #endregion

								 // Получаем все атрибуты формы
								 while (ienum.MoveNext())
								 {
									 XmlAttribute attr = (XmlAttribute)ienum.Current;
									 string name = attr.Name;
									 #region Switch
									 switch (Utils.Utils.AllTrim(name.ToUpperInvariant()))
									 {
										 case "ID":
											 try
											 {
												 formId = int.Parse(attr.Value);
											 }
											 catch (FormatException ex)
											 {
												 throw new Ohe.Telnet.Exceptions.ConfigurationException(ex, "Ошибка: Неверный формат параметра формы", attr.Value);
											 }
											 break;
										 case "TARGET":
											 try
											 {
												 target = int.Parse(attr.Value);
											 }
											 catch (FormatException ex)
											 {
												 throw new Ohe.Telnet.Exceptions.ConfigurationException(ex, "Ошибка: Неверный формат параметра формы", attr.Value);
											 }
											 break;
										 case "PERMISS":
											 permiss = Utils.Utils.AllTrim(attr.Value.ToUpperInvariant());
											 break;
										 case "PARENT":
											 try
											 {
												 parent = int.Parse(attr.Value);
											 }
											 catch (FormatException ex)
											 {
												 throw new Ohe.Telnet.Exceptions.ConfigurationException(ex, "Ошибка: Неверный формат параметра формы", attr.Value);
											 }
											 break;
										 case "CYCLE":
											 cycle = Utils.Utils.StrToBool(attr.Value);
											 break;
										 case "TITLE":
											 descript = Utils.Utils.AllTrim(attr.Value);
											 break;
										 case "AUTOREFRESH":
											 _autorefresh = Utils.Utils.StrToBool(attr.Value);
											 break;
										 case "CHECKVALUE":
											 _checkstring = Utils.Utils.AllTrim(attr.Value);
											 break;
										 case "CHECKFORK":
											 _checkfork = Utils.Utils.Str2Bool(attr.Value);
											 break;
										 case "ISIF":
											 _isif = Utils.Utils.Str2Bool(attr.Value);
											 break;

									 }
									 #endregion
								 }

								 // Проверка на наличие обязательных атрибутов
								 if (formId < 0 || target < 0 || permiss == string.Empty)
									 throw new Ohe.Telnet.Exceptions.ConfigurationException("Ошибка: Не определены основные параметры формы"
																			  + Environment.NewLine
																			  + (formId < 0 ? "Не определен идентификатор формы" : "Идентификатор формы:" + formId.ToString())
																			  + Environment.NewLine
																			  + (target < 0 ? "Не определена форма перехода" : string.Empty)
																			  + Environment.NewLine
																			  + (permiss == string.Empty ? "Не задана привилегия для формы" : string.Empty)
																			 );
								 if (_isif && parent == -2)
									 throw new Ohe.Telnet.Exceptions.ConfigurationException("Ошибка: Не определены основные параметры формы"
																			  + Environment.NewLine
																			  + (formId < 0 ? "Не определен идентификатор формы" : "Идентификатор формы:" + formId.ToString())
																			  + Environment.NewLine
																			  + (parent < 0 ? "Не определена форма перехода для ветки условия" : string.Empty)
																			 );

                                 if (_forms.ContainsKey(formId))
                                     continue;

								 // создаем класс формы
								 form = new RfForm(formId, target, permiss,
														  parent, cycle);
								 // Описание формы
								 form.Description = descript;
								 form.IsAutoRefresh = _autorefresh;
								 // строка котору надо сравнивать
								 form.CheckValue = _checkstring;
								 form.IsCheckFork = _checkfork;
								 form.IsIf = _isif;

								 #endregion

								 // Поиск подчиненных элементов
								 // Поиск событий формы и контролов
								 if (child.HasChildNodes)
								 {
									 #region Добавление событий и контролов формы
									 XmlNodeList cnl = child.ChildNodes;
									 foreach (XmlNode opt in cnl)
									 {
										 if (opt.NodeType == XmlNodeType.Element)
										 {
											 // Форма поддерживает только два события
											 switch (Utils.Utils.AllTrim(opt.Name.ToUpperInvariant()))
											 {
												 case "INIT":
													 form.AddEvent(GetEvent(formId, -1, "OnInit", opt));
													 break;
												 case "SUBMIT":
													 form.AddEvent(GetEvent(formId, -1, "OnSubmit", opt));
													 break;
												 case "EXIT":
													 form.AddEvent(GetEvent(formId, -1, "OnExit", opt));
													 break;
												 case "CONTROLS":
													 #region Добавление контролов
													 if (opt.HasChildNodes)
													 {
														 XmlNodeList elem = opt.ChildNodes;
														 foreach (XmlNode cntls in elem)
														 {
															 if (cntls.NodeType == XmlNodeType.Element)
															 {
																 switch (Utils.Utils.AllTrim(cntls.Name.ToUpperInvariant()))
																 {

																	 case "CONTROL":
																		 // вызов метода добавления контрола
																		 form.AddControl(GetControl(formId, cntls));
																		 // вызов метода добавления события к контролу
																		 break;
																 }
															 }
														 }
													 }
													 else
														 throw new Ohe.Telnet.Exceptions.ConfigurationException("Ошибка: Нет определения контролов для формы"
																								 + Environment.NewLine
																								 + "Идентификатор формы: " + formId.ToString()
																									);

													 #endregion
													 break;
											 }
										 }
									 }
									 #endregion
								 }
								 else
									 throw new Ohe.Telnet.Exceptions.ConfigurationException("Нет определяния основных параметров формы");


								 _forms.Add(form.Id,form);
							 }
						 }
					 }
				 }
				 #endregion
			 }
            xml.Close();
        }
        catch (FormatException ex)
        {
            throw new Ohe.Telnet.Exceptions.ConfigurationException(ex, "Ошибка конвертации параметров при компиляции");
        }
        catch (Exception e)
        {
            throw new Ohe.Telnet.Exceptions.ConfigurationException(e.Message);
        }
    }
    */
    #endregion


        #region Работаем с пользователями - Not Use

    #region Old (not use)

    #region AddUser - Добавляет нового пользователя
    /// <summary>
    /// Добавляет нового пользователя 
    /// </summary>
    /// <param name="Login"></param>
    /// <param name="Pass"></param>
    /// <param name="Permiss"></param>
    /*
    public void AddUser(string Login, string Pass, string Permiss, string Name, string Whse)
    {
        lock (_sync)
        {
            try
            {
                List<RfControlEventsParams> prms = new List<RfControlEventsParams>();
                prms.Add(new RfControlEventsParams("@login", "VARCHAR", 10, Login));
                prms.Add(new RfControlEventsParams("@password", "VARCHAR", 10, Pass));
                prms.Add(new RfControlEventsParams("@name", "VARCHAR", 30, Name));
                prms.Add(new RfControlEventsParams("@menu", "CHAR", 1, Permiss));
                prms.Add(new RfControlEventsParams("@whse_id", "VARCHAR", 10, Whse));
            try
            {
                ExecuteSqlQuery.ExecuteEvent("vs_AddUser", prms);
            }
            catch (Exception ex)
            {
                Utils.Utils.WriteLog("Ошибка Аутентификации пользователя:"
                                    + Environment.NewLine
                                    + "Пользователь: " + Login
                                    + Environment.NewLine
                                    + ex.Message
                                    );
            }
            }
            catch { }

        }
        this._isDirty = true;
    }
    */
    #endregion

    #region GetAllUsers()
    /// <summary>
    /// Возвращает список всех пользователей из конфигурации, 
    /// Надо сделать чтобы брал из xml файла
    /// </summary>
    /// <returns></returns>
    //public List<TelnetUser> GetAllUsers() { return _users; }
    #endregion

    /// <summary>
    /// Возвращает список всех пользователей из файла
    /// </summary>
    /// <returns></returns>
    /*
    public ArrayList GetAllUsersList()
    {
        ArrayList ar = new ArrayList();
        lock (_users)
        {
            foreach (TelnetUser usr in _users)
            {
                ar.Add(usr.UserId + "|" + usr.Password + "|" + usr.Permission);
            }
        }
        return ar;
    }
    */

    #endregion

    #endregion

    }
}
