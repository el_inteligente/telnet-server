using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceProcess;
using System.Threading;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Collections;
using Ohe.Telnet.Core;
using Ohe.Telnet.Server;
using Ohe.Utils;
using Ohe.Telnet.Configuration;

namespace VS.HandHeldServer
{
    class Program
    {
        #region Main Entry
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(GlobalExceptionsHandler);

            bool console = false;

            foreach (string prm in args)
            {
                if (prm == "/console")
                {
                    console = true;
                    break;
                }

                if (prm == "/source")
                {
                    TlnConfigManager.SaveAllObjectToFile2();
                    return;
                }
            }
            
            if (console)
            {
                Application.Run(new ConsoleMode());
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { new EntryPoint() };
                ServiceBase.Run(ServicesToRun);
            }
        }
        #endregion

        #region GlobalExceptionsHandler �������� �� �� ��� ����� �������� ����
        static void GlobalExceptionsHandler(object sender, UnhandledExceptionEventArgs args)
        {
            try
            {
                Exception e = (Exception)args.ExceptionObject;
                if (e is ThreadAbortException)
                    return;
                Utils.WriteLog("Error: ������ �������� �����������." + Environment.NewLine + "���������� ������: " + e.Message + Environment.NewLine 
                    + "���� �������: " + e.StackTrace );
            }
            catch
            { }
        }
        #endregion
    }
}
