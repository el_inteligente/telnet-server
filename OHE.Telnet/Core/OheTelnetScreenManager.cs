﻿//#define NOTIFY
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Ohe.Telnet.Controls;
using Ohe.Telnet.Data;
using Ohe.Telnet.Configuration;
using Ohe.Telnet.Exception;
using Ohe.Utils;

namespace Ohe.Telnet.Core
{
    /// <summary>
    /// Описывает логику работу с формами и меню
    /// </summary>
    [Serializable]
    public partial class OheTelnetScreenManager 
    {

        #region Define private Values
        public enum OheFormStatus { IsBool, IsException, IsScreen, IsHelp, IsHeadMenu, IsNotify, Default, IsRestoreSession }

        private OheFormStatus _currentStatus = OheFormStatus.Default;
        private OheFormStatus _previousStatus = OheFormStatus.Default;

        private bool _isConnected;  // пользователь залогинился

        private string _result; // Возвращает результат пользователю

        private readonly string _user; // логин пользователя который работает

        [NonSerialized]
        private TlnConfigManager _config; // объект содержит конфигурацию системы

        [NonSerialized]
        private RfForm _childForm;  // подчиненная форма 

        [NonSerialized]
        private RfForm _form;   // форма на которой находится пользователь

        [NonSerialized]
        //private List<RfForm> _formList; // содержит список форм
        //private Hashtable _formList; // содержит список форм
        private Dictionary<int, RfForm> _formList;

        [NonSerialized]
        private UserMenu _menu;  // меню пользователя в зависимости от привилегий

        private OheConnectionType _connectionType; // тип соединения

        [NonSerialized]
        private OheTelnetNotifyClient _notifyClient;


        #region Старый параметры
        [Obsolete("Устаревший параметр: используйте CurrentStatus")]
        private bool _isBool;    // определяет что пользователь находится на экране выбора

        [Obsolete("Устаревший параметр: используйте CurrentStatus")]
        private bool _isHeadMenu; // пользователь находится в меню
        [Obsolete("Устаревший параметр: используйте CurrentStatus")]
        private bool _isScreen;  // определяет что пользователь начал операции (приемка и т.д.)
        [Obsolete("Устаревший параметр: используйте CurrentStatus")]
        private bool _hasChild;  // определяет что пользователь находится на подчиненном экране
        [Obsolete("Устаревший параметр: используйте CurrentStatus")]
        private bool _isHelp;    // определяет что пользователь находится на экране подсказок

        // после окончания процесса.
        [Obsolete("Устаревший параметр: используйте CurrentStatus")]
        private bool _isExpеtion = false;  // пользователь находится на экране ошибки хранимой процедуры
        #endregion


        #endregion


        #region Define Public Values

        public Dictionary<string, object> ProcessValues = new Dictionary<string, object>(); //Глобальный кеш пользовательского соединения


        public string Permiss; // привилегия пользователя

        #endregion


        #region Properties

        #region CurrentFormStatus
        /// <summary>
        /// Текущий статус бизнес-логики
        /// </summary>
        public OheFormStatus CurrentFormStatus { get { return _currentStatus; }
            set { _currentStatus = value; }
        }
        #endregion


        #region IsConnected
        /// <summary>
        /// Определяет пользователь залогинился или нет
        /// </summary>
        public bool IsConnected { get { return _isConnected; } set { _isConnected = value; } }

        #endregion


        #region IsHeadMenu
        /// <summary>
        /// Определяет пользователь находится в главном меню или нет
        /// </summary>
        public bool IsHeadMenu { get { return _isHeadMenu; } set { _isHeadMenu = value; _currentStatus = OheFormStatus.IsHeadMenu; } }

        #endregion


        #region Menu
        /// <summary>
        /// Устанавливает список меню пользователей.
        /// </summary>
        public UserMenu Menu { get { return _menu; } set { _menu = value; } }

        #endregion


        #region Config
        /// <summary>
        /// Возвращает конфигурацию текущего пользователя
        /// </summary>
        public TlnConfigManager Config { get { return _config; } set { _config = value; } }

        #endregion


        #region Forms
        /// <summary>
        /// Возвращает список форм пользователя
        /// </summary>
        public Dictionary<int,RfForm> Forms { get { return _formList; } set { _formList = value; } }

        #endregion


        #region OheConnectionType
        /// <summary>
        /// Возвращает тип подключения пользователя
        /// </summary>
        public OheConnectionType OheConnectionType { get { return _connectionType; } set { _connectionType = value; } }
        #endregion


        #region ActiveFormId

        /// <summary>
        /// Возвращает текущую активную форму на которой находится пользователь
        /// </summary>
        public int ActiveFormId { get; private set; }

        #endregion

        #endregion


        #region Constructor

        #region TlnWorkLogic
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="menu">Список меню из конфигурации</param>
        /// <param name="configuration">Класс описывает конфигурацию</param>
        public OheTelnetScreenManager(UserMenu menu, TlnConfigManager configuration, Dictionary<string, object> cache, string user, OheConnectionType type)
        {
            _menu = menu;
            _formList = configuration.GetForms;
            _config = configuration;
            ProcessValues = cache;
            _user = user;
            _connectionType = type;
            _isBool = false;
        }
        #endregion

        public OheTelnetScreenManager(OheConnectionType type) { _isConnected = false; _connectionType = type;
            _isBool = false;
        }

        #endregion


        #region InsertString(public) - Обрабатывает строку которая пришла от пользователя

        /// <summary>
        /// Обрабатывает строку которая пришла от пользователя
        /// </summary>
        public void InsertString(string str) 
        { 
            ParseString2(str); 
        }

        #endregion


        #region ParseString2(private) - Парсит результат ввода пользователя - Новая процедура
        /// <summary>
        /// Парсит результат ввода пользователя. Логика переписана на работы с Current State
        /// </summary>
        /// <param name="userInput"></param>
        private void ParseString2(string userInput)
        {
            userInput = Utils.Utils.AllTrim(userInput);
            switch (_currentStatus)
            {

#if NOTIFY
                case OheFormStatus.IsNotify:

                    #region Пользователь ввел ESC
                    if (userInput.IndexOf((char)27) >= 0)
                    {
                        if (_previousStatus == OheFormStatus.IsHeadMenu)
                        {
                            _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") +
                                      _menu.GetCurrentMenuPermiss2();
                        }
                        else if (_previousStatus== OheFormStatus.IsScreen)
                        {
                            _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") +
                                      GetProcessScreen2(_form.Parent, false, true);
                        }
                        _currentStatus = _previousStatus;

                        _previousStatus = OheFormStatus.IsNotify;
                        return;
                    }
                    #endregion


                    #region Обработка функциональных клавиш.
                    if (_form != null)
                    {
                        // Возможно к нам пришли управляющие значения которые вызывают подчиненную форму
                        foreach (RfControlKey cnl in _form.ContrrolKeys)
                        {
                            // Текущая форма в которой находится пользователь
                            ActiveFormId = _form.Id;

                            // Пользователь нажал функциональную клавишу принять задание
                            if (Utils.Utils.AllTrim(cnl.Value.ToUpper()) == userInput)
                            {
                                _form.ClearForm();
                                _result = (_connectionType == OheConnectionType.HandHeld
                                               ? OheConstant.TELNET_CLEAR_SEQ
                                               : "") + GetProcessScreen2(cnl.Target, false, true);

                                if(_notifyClient != null)
                                // Подтверждаем событие показа оповещения
                                    _notifyClient.OnSubmit();


                                // Произошла системная ошибка в конфигурации или в хранимой процедуре
                                if (IsSystemException(_form, ActiveFormId, false))
                                {
                                    _previousStatus = OheFormStatus.IsScreen;
                                    return;
                                }

                                _currentStatus = OheFormStatus.IsScreen;
                                return;
                            }
                        }
                    }

                    #endregion


                    #region Пользователь нажал следующее сообщение
                    if (userInput == TlnConfigManager.TELNET_MENU_DOWN)
                    {
                        if (_notifyClient != null)
                        {
                            _form = _notifyClient.GetNextNotify();
                            _result = _form.ToString(true, ProcessValues, _connectionType);

                            // Подтверждаем событие показа оповещения
                            _notifyClient.OnSubmit();
                            return;
                        }
                    }
                    #endregion

                    #region Пользователь нажал предыдущее сообщение
                    if (userInput == TlnConfigManager.TELNET_MENU_UP)
                    {
                        if (_notifyClient != null)
                        {
                            _form = _notifyClient.GetNextNotify();
                            _result = _form.ToString(true, ProcessValues, _connectionType);

                            // Подтверждаем событие показа оповещения
                            _notifyClient.OnSubmit();
                            return;
                        }
                    }
                    #endregion



                    #region Пришли на форму первый раз или ничего не нажали
                    _notifyClient = new OheTelnetNotifyClient(_user, GetFormById(9001), _form != null ? _form.Id : 0);
                    _form = _notifyClient.GetNotifyForm();
                    _result = _form.ToString(true, ProcessValues, _connectionType);
                    // Подтверждаем событие показа оповещения
                    _notifyClient.OnSubmit();
                    #endregion

                    break;

#endif
                case OheFormStatus.IsRestoreSession:
                    #region Восстановление сессии
                    _currentStatus = OheFormStatus.IsScreen;
                    _previousStatus = OheFormStatus.IsScreen;

                    // Инициализация формы пытаемся повторно сделать инициализацию,
                    // чтобы проверить ошибки и дать повторно
                    if (_form != null)
                    {
                        _form.CurrentEvent = Events.OnInit;
                        _form.SetCurrentEvent(userInput, false, ref ProcessValues);

                        if (IsSystemException(_form, ActiveFormId, false))
                        {
                            ParseString2("");
                            return;
                        }

                        _form.CurrentEvent = Events.OnChange;

                        _result = _form.ToString(true, ProcessValues, _connectionType) +
                                  (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                    }
                    // Инициализация формы пытаемся повторно сделать инициализацию,
                    // чтобы проверить ошибки и дать повторно
                    else if (_childForm != null)
                    {
                        _childForm.CurrentEvent = Events.OnInit;
                        _childForm.SetCurrentEvent(userInput, true, ref ProcessValues);

                        if (IsSystemException(_childForm, ActiveFormId, true))
                        {
                            ParseString2("");
                            return;
                        }

                        _childForm.CurrentEvent = Events.OnChange;

                        _result = _childForm.ToString(true, ProcessValues, _connectionType) +
                                  (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                    }
                    else
                    {
                        _currentStatus = OheFormStatus.IsHeadMenu;
                        _previousStatus = OheFormStatus.IsHeadMenu;
                        // Посылаем в процедуру меню, которого нет, просто чтобы перерисовать меню
                        ParseString2("99");
                        return;
                    }

                    #endregion
                    break;

                case OheFormStatus.IsHelp:
                    #region Пользователь на меню подсказки
                    if (_previousStatus == OheFormStatus.IsHeadMenu)
                    {
                        _result = _menu.GetCurrentMenuPermiss2();
                        _result = (_result == "screen" ? string.Empty : _result);
                    }

                    if (_previousStatus == OheFormStatus.IsScreen)
                        _result = GetProcessScreen2(_form.Id, _childForm != null, false);

                    _currentStatus = _previousStatus;
                    #endregion
                    return;

                case OheFormStatus.IsBool:
                    #region Пользователь на экране завершить процесс
                    if (userInput == TlnConfigManager.TELNET_KEY_Y)
                    {
                        ClearUserParams(); // очищаем пользовательские параметры из общего кеша
                        _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen2(_form.Parent, false, true);
                    }
                    else
                    {
                        ParseString2(((char)27).ToString());
                    }
                    //_isBool = false;
                    _currentStatus =  OheFormStatus.IsScreen;
                    return;
                    #endregion


                case OheFormStatus.IsException:
                    #region Обработка Системной Ошибки
                    _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_SYSTEM_ECXEPTION + TlnConfigManager.BEEP : OheConstant.TELNET_SYSTEM_EXCEPTON_WEBCONNECTION);
                    _currentStatus = _previousStatus;
                    _previousStatus = OheFormStatus.IsException;
                    return;
                    #endregion

                case OheFormStatus.IsScreen:
                    #region Пользователь находится на одном из экранов

#if NOTIFY
                    /*
                    if (userInput == "~F2")
                    {
                        if (ExistsFormById(9001))
                        {
                            RfForm _frm = GetFormById(9001);

                            OheTelnetNotifyClient client = new OheTelnetNotifyClient();
                            _form = client.GetNextNotify(_frm, _form.Id);
                            _result = _form.ToString(true, ProcessValues, _connectionType);
                            CurrentStatus = OheFormStatus.IsNotify;
                            return;
                        }
                    }
                     */

#endif
                    if (_previousStatus == OheFormStatus.IsException)
                    {
                        _previousStatus = OheFormStatus.IsScreen;

                        // Инициализация формы пытаемся повторно сделать инициализацию,
                        // чтобы проверить ошибки и дать повторно
                        if (_childForm == null)
                        {
                            _form.CurrentEvent = Events.OnInit;
                            _form.SetCurrentEvent(userInput, false, ref ProcessValues);

                            if (IsSystemException(_form, ActiveFormId, false))
                            {
                                ParseString2("");
                                return;
                            }
                            _result = _form.ToString(true, ProcessValues, _connectionType) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                        }
                        else
                        {
                            _childForm.CurrentEvent = Events.OnInit;
                            _childForm.SetCurrentEvent(userInput, true, ref ProcessValues);

                            if (IsSystemException(_childForm, ActiveFormId, true))
                            {
                                ParseString2("");
                                return;
                            }
                            _result = _childForm.ToString(true, ProcessValues, _connectionType) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                        }
                        return;
                    }

                    // Пользователь ввел клавишу подсказки
                    #region Пользователь ввел клавишу подсказки
                    if (userInput == TlnConfigManager.TELNET_HELP_STR)
                    {
                        // проверка на то что пользователь на подчиненной форме
                        if (_childForm != null)
                        {
                            if (_childForm.HasHelpMenu())
                            {
                                _result = _childForm.GetControlHelp().ToString(_connectionType);
                                _currentStatus = OheFormStatus.IsHelp;
                            }
                        }
                        else if (_form.HasHelpMenu())
                        {
                            _result = _form.GetControlHelp().ToString(_connectionType);
                            _currentStatus = OheFormStatus.IsHelp;
                        }

                        _previousStatus = OheFormStatus.IsScreen;

                        return;
                    }
                    #endregion

                    // Надо определить еще специальные клавиши управления на форме
                    #region Пользователь ввел ESC
                    if (userInput.IndexOf((char)27) >= 0)
                    {
                        #region Проверки возможности выхода по ESC
		                // блокируем выход из экрана если находится
                        // на контроле список на подчиненной форме
                        if (_childForm != null)
                            if (_childForm.GetActiveControl() is RfControlList)
                                return;

                        // Проверяем возможен ли выход из формы
                        // Если значение определено что нельзя выходить с формы 
                        // Parent = -1 то выводим сообщение о недоступности
                        if (_form.Parent == -1)
                        {
                            _form.SetMessage("НЕВЕРНАЯ ОПЦИЯ");
                            _result = _form.ToString(true, ProcessValues, _connectionType) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                            return;
                        }

                        // проверка что товар на погрузкчике
                        // 10.07.2008 - добавлена функция отслеживания погрузкчика
                        if (_form.IsCheckFork && ExecuteSqlQuery.ExecuteScalar("select count(*) from t_binlocat (nolock) where binlabel='@" + _user + "'") > 0)
                        {
                            _form.SetMessage("ТОВАР НА ПОГРУЗЧИКЕ");
                            _result = _form.ToString(true, ProcessValues, _connectionType) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                            return;
                        }

                        // Инициаруем вызов события выхода с экрана
                        _form.CurrentEvent = Events.OnExit;
                        _form.SetCurrentEvent(string.Empty, false, ref ProcessValues);

                        // Блокировка если пользователь не определил меню или форму выхода из экрана
                        if (_form.Parent > -1 && _menu.ExistsMenuById(_form.Parent))
                        {
                            _menu.EscMenu = _form.Parent; // переправляем пользователя на меню
                        }
                        else
                        {
                            // Проверка на то что для пользователя назначен переход
                            // не на меню а на предыдущий экран
                            if (_form.Parent > -1 && ExistsFormById(_form.Parent))
                            {
                                _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen2(_form.Parent, false, true);
                                return;
                            }
                            _menu.EscMenu = 0;
                        }
 	                    #endregion

                        // Новый параметр определяет текущий статус объекта
                        _currentStatus = OheFormStatus.IsHeadMenu;
                        ClearUserParams(); // очищаем пользовательские параметры из общего кеша
                        _form.ClearForm();
                        _form = null;
                        // возвращаем результат
                        _result = _menu.GetCurrentMenuPermiss2();

                        ActiveFormId = 0;

                        return;
                    }
                    #endregion

                    if (_childForm != null)
                    {
                        // Сначала обрабатываем подчиненную форму 
                        //(на подчиненной форме не должно быть управляющих ключей)
                        #region Обработка подчиненной формы если она есть

                        // Проверка на то что контрол в нормальном состоянии
                        if (_childForm.CurrentState == FormState.Exception)
                        {
                            // Ошибка на форме, проверка того что застряли на
                            // контроле типа list, надо переходить на другой контрол
                            if (_childForm.GetActiveControl() is RfControlList)
                            {
                                _childForm.GetActiveControl().IsActivate = false;
                                _result = _childForm.ToString(true, ProcessValues, _connectionType);

                                // Очищаем форму нечего больше выбирать
                                // произошла ошибка
                                _childForm.CurrentEvent = Events.OnEnd;

                                _previousStatus = OheFormStatus.IsScreen;
                                _currentStatus = OheFormStatus.IsException;

                                ParseString2("");
                                return;
                            }
                        }

                        // Определяем следующее событие для подчиненной формы
                        _childForm.SetCurrentEvent(userInput, true, ref ProcessValues);

                        _result = _childForm.ToString(true, ProcessValues, _connectionType);

                        // Возвращаем результат текущего состояния формы
                        if (!_childForm.HasActiveControls())
                        {
                            ((RfControl)_form.GetActiveControl()).Value = _childForm.ResultFromChild;
                            _result = _form.ToString(true, ProcessValues, _connectionType);

                            //_hasChild = false;

                            _childForm.SetCurrentEvent(userInput, true, ref ProcessValues);
                            // Очищаем форму после обработки
                            _childForm.ClearForm();

                            _childForm = null;
                            // Вызываем процедуру с подтверждением ввода
                            // параметра пришедшего из списка
                            ParseString2("");
                            return;
                        }
                        #endregion
                    }
                    else
                    {
                        // проверка на то что форма активирована
                        #region Обработка значений с ОСНОВНОЙ формы
                        if (_form.IsActivate)
                        {
                            #region Обработка функциональных клавиш. Есть ключи для экрана для запуска child
                            // Возможно к нам пришли управляющие значения которые вызывают подчиненную форму
                            foreach (RfControlKey cnl in _form.ContrrolKeys)
                            {
                                // Текущая форма в которой находится пользователь
                                ActiveFormId = _form.Id;
                                // Да есть управляющие элементы и нет child форм еще
                                if (Utils.Utils.AllTrim(cnl.Value.ToUpper()) == userInput)
                                {
                                    // Проверка на то что соответствуют параметры для запуска ключа
                                    if (!cnl.IsVisible(ProcessValues))
                                    {
                                        _form.SetMessage("ОПЦИЯ НЕ ДОСТУПНА");
                                        _result = _form.ToString(true, ProcessValues, _connectionType) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                                        return;
                                    }

                                    // Функциональная клавиша есть подчиненная форма
                                    if (cnl.IsChild)
                                    {
                                        //_hasChild = true;
                                        _childForm = null; // Подчиненная форма определяется далее в GetProcessScreen2
                                        _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen2(cnl.Target, true, true);
                                    }
                                    else
                                    {
                                        _form.ClearForm();
                                        _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen2(cnl.Target, false, true);

                                        // Произошла системная ошибка в конфигурации или в хранимой процедуре
                                        if (_form.CurrentState == FormState.Exception)
                                        {
                                            _previousStatus = OheFormStatus.IsScreen;
                                            IsSystemException(_form, ActiveFormId, false);
                                            ParseString2("");
                                            return;
                                        }
                                    } 
                                    return;
                                }
                            }
                            #endregion

                            #region Работаем с основным экраном
                            if(_childForm == null)
                            {
                                //Serialization
                                ActiveFormId = _form.Id;
                                // Обрабатываем контролы на форме
                                _form.SetCurrentEvent(userInput, false, ref ProcessValues);

                                // Проверка на наличие системных ошибок
                                // при при событии обновления onchange
                                if (IsSystemException(_form, ActiveFormId, false))
                                {
                                    _previousStatus = OheFormStatus.IsScreen;
                                    ParseString2("");
                                    return;
                                }

                                // Проверка на то что несколько строк возращается из хранимой процедуры
                                if (GenMultiResultException(_form)) return;

                                // Проверка на то что процесс в цикле закончен
                                /*
                                if (_form.CurrentState == FormState.InValid
                                    && _form.CurrentEvent == Events.OnInit)
                                {
                                    _result = _form.Message.ToString(_connectionType);
                                    _currentStatus = OheFormStatus.IsBool;
                                    return;
                                }
                                */

                                _result = _form.ToString(true, ProcessValues, _connectionType)
                                    + (_form.CurrentState == FormState.InValid 
                                    && _connectionType == OheConnectionType.HandHeld
                                    && !_form.IsIf ? TlnConfigManager.BEEP : "");

                                // у нас выполнено событие надо переходить на другую форму
                                if (!_form.HasActiveControls() && _form.CurrentEvent == Events.OnEnd)
                                {
                                    // Вызываем событие окончания обработки формы
                                    // Очищаем форму
                                    _form.SetCurrentEvent(userInput, false, ref ProcessValues);

                                    ActiveFormId = _form.Id;

                                    // Проверяем что идентификатор перехода у нас меню
                                    if (_menu.ExistsMenuById(_form.Target))
                                    {
                                        _result = GotoMenu2(_form.Target);
                                        ActiveFormId = 0;
                                        return;
                                    }

                                    // Переходим на другую форму
                                    _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen2(_form.Target, false, true);

                                    // Проверка на наличие системных ошибок
                                    // 20.09.2008
                                    if (IsSystemException(_form, ActiveFormId, false))
                                    {
                                        ParseString2("");
                                        return;
                                    }

                                    // Serialization
                                    ActiveFormId = _form.Id;
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                    break;

                case OheFormStatus.IsHeadMenu:
#if NOTIFY
                    #region Пользователь нажал клавишу Оповещений
                    if (Utils.Utils.AllTrim(userInput) == TlnConfigManager.TelnetKeyNotify)
                    {
                        _previousStatus = OheFormStatus.IsHeadMenu;
                        _currentStatus = OheFormStatus.IsNotify;
                        // пользователь на одном из меню
                        ParseString2("");
                        return;
                    }
                    #endregion
#endif
                    ActiveFormId = 0;

                    #region Пользователь находится в меню

                    #region Пользователь нажал клавишу помощи
                    if (Utils.Utils.AllTrim(userInput) == TlnConfigManager.TELNET_HELP_STR)
                    {
                        // пользователь на одном из меню
                        if (_menu.HasHelpMenu())
                        {
                            _result = _menu.GetControlHelp();
                            _previousStatus = OheFormStatus.IsHeadMenu;
                            _currentStatus = OheFormStatus.IsHelp;
                            return;
                        }
                    }
 	                #endregion

                    if (userInput.IndexOf((char)27) >= 0)
                    {
                        #region Пользователь ввел ESC
                        // Переходим в меню уровнем выше
                        _result = _menu.GetSubMenuOrScreen(999);
                        if (Utils.Utils.AllTrim(_result) == "false")
                        {
                            _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "<div class=\"form\"><span class=\"message\">Сеанс работы пользователя завершен.</span></div>");
                            IsConnected = false;
                        }
                        #endregion
                    }
                    else
                    {
                        string userVal = userInput.ToUpperInvariant();

                        #region Обрабатываем другие данные от пользователя
                        if (userVal == TlnConfigManager.TELNET_MENU_DOWN)
                        {
                            // пользователь решил перейти на следующую страницу
                            // возвращаем результат
                            _result = _menu.MoveNextPage();
                            return;
                        }

                        if (userVal == TlnConfigManager.TELNET_MENU_UP)
                        {
                            // пользователь переходит на экран назад
                            _result = _menu.MovePriveousPage();
                            return;
                        }

                        #endregion

                        // Обрабатываем какие-то числа от пользователя
                        #region Обработка чисел от пользователя
                        try
                        {
                            int inter;
                            if (!int.TryParse(userVal, out inter))
                            {
                                _result = _menu.GetCurrentMenuPermiss2();
                                if (_result == "screen")
                                {
                                    _result = OheConnectionType == OheConnectionType.Web ? OheConstant.TELNET_CONFIGURATION_MENU_EXCEPTON : OheConstant.TELNET_SYSTEM_ECXEPTION;
                                    //_isHeadMenu = true;
                                    //_isScreen = false;
                                    //_isHelp = true;
                                    _menu.EscMenu = 0;
                                }
                                return;
                            }


                            if (inter < 1 && inter > TlnConfigManager.MenuLinesCount)
                                return;
                            // пользователь выбрал какой то из пунктов меню
                            // Возможно что мы попали на подменю
                            _result = _menu.GetSubMenuOrScreen(inter);
                            // проверка что мы действительно попали в подменю
                            // может мы уже на экране, надо проверить
                            if (_menu.GetBoolMenuPermiss() == 0)
                            {
                                // Надо делать процесс
                                //_isHeadMenu = false;
                                //_isScreen = true;
                                _currentStatus = OheFormStatus.IsScreen;

                                _formList = _config.GetForms; //GetFormsFromXml(int.Parse(userInput));
                                try
                                {
                                    _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen2(_menu.CurrentTarget, false, true);

                                    //20.09.2008 ДОбавление отслеживания системных ошибок
                                    if (_form.CurrentState == FormState.Exception)
                                    {
                                        _form.ClearForm();
                                        //_isHeadMenu = true;
                                        //_isScreen = false;
                                        //_isExpеtion = true;
                                        _previousStatus = OheFormStatus.IsHeadMenu;
                                        _currentStatus = OheFormStatus.IsException;
                                        _menu.EscMenu = 0;
                                        ParseString2(userInput);
                                        return;
                                    }
                                }
                                catch (ConfigurationException ex)
                                {
                                    //_isHeadMenu = true;
                                    //_isScreen = false;
                                    //_isHelp = true;
                                    _currentStatus = OheFormStatus.IsHeadMenu;
                                    _menu.EscMenu = 0;
                                    _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ
                                                + CenterString("ОШИБКА:")
                                                + Environment.NewLine
                                                + CenterString("В КОНФИГУРАЦИИ") + Environment.NewLine
                                                + CenterString("обратитесь к") + Environment.NewLine
                                                + CenterString("АДМИНИСТРАТОРУ") + Environment.NewLine
                                                + OheConstant.SetCursorPosition(0, 0) :
                                                OheConstant.TELNET_CONFIGURATION_EXCEPTON_WEBCONNECTION);

                                    Utils.Utils.WriteLog(ex.ToString());
                                }
                            }

                        }
                        catch (System.Exception exc)
                        {
                            Utils.Utils.WriteLog(exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.StackTrace);
                            _result = _menu.GetCurrentMenuPermiss2();
                        }

                        #endregion
                    }
                    #endregion

                    break;

                default:
                    break;

            }

        }
        #endregion


        #region GotoMenu2
        private string GotoMenu2(int MenuId)
        {
            _menu.EscMenu = MenuId; // переправляем пользователя на меню
            //_isScreen = false;
            //_isHeadMenu = true;
            _currentStatus = OheFormStatus.IsHeadMenu;

            ClearUserParams(); // очищаем пользовательские параметры из общего кеша

            _form.ClearForm();

            // возвращаем результат.
            return _menu.GetCurrentMenuPermiss2();
        }
        #endregion


        #region IsSystemException - обработка Системных Ошибок
        /// <summary>
        /// Процедура проверяет возникновение исключения на форме.
        /// Если исключение сгенерировалось, то возвращается форма с исключением
        /// </summary>
        /// <param name="form"></param>
        /// <param name="id"></param>
        /// <returns>Bool</returns>
        private bool IsSystemException(RfForm form, int id, bool isChiled)
        {
            if (form.CurrentState == FormState.Exception)
            {
                form.ClearForm();
                _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") 
                                         + GetProcessScreen2(id, isChiled, true)
                                         + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");

                _currentStatus = OheFormStatus.IsException;
                return true;
            }
            return false;
        }
        #endregion


        #region GetProcessScreen2 - обрабатывает форму и возвращает текущий экран пользователя
        /// <summary>
        /// Устанавлиет текущий экран пользователя
        /// </summary>
        /// <param name="id">Указывает идентификатор формы, если 999 - определяется автоматически</param>
        /// <param name="child">Определяет какая форма основная или дочерняя</param>
        /// <param name="isFirst">Определяет пришла ли форма в первый раз или нет</param>
        /// <returns>string</returns>
        public string GetProcessScreen2(int id, bool child, bool isFirst)
        {
            var rez = string.Empty;
            RfForm current = null;
            // Переопределяем текущее значение формы
            if (child)
                current = _childForm;
            else
            {
                current = _form;
                _childForm = null;
            }
            // Условие для начального входа пользователя в процесс
            if (isFirst)
            {
                #region Предварительная обработка форм если пришли в первый раз
                current = GetFormById(id); // определяем текущую форму пользователя
                //Serialization
                if (!child)
                    ActiveFormId = current.Id;

                #region Обработка форм типа IsIf
                // Форма - есть форма условие?
                if (current.IsIf)
                {
                    // Проверка на выполнение условий CheckValue для форм условий
                    if (current.CheckValue != string.Empty)
                    {
                        // Надо очищать глобальный кеш от значений
                        // Этой формы, т.к. она может не отображаться
                        RemoveCacheValuesByForm(current);

                        if (!current.IsVisible(ProcessValues))
                        {
                            current.ClearForm();
                            return GetProcessScreen2(current.Target, false, true);
                        }
                    }

                    current.CurrentEvent = Events.OnInit;
                    current.SetCurrentEvent("", child, ref ProcessValues);

                    if (child)
                        _childForm = current;
                    else
                        _form = current;

                    switch (current.CurrentState)
                    {
                        case FormState.Valid:
                            return GetProcessScreen2(current.Target, false, true);
                        case FormState.InValid:
                            return GetProcessScreen2(current.Parent, false, true);

                        default:
                            return current.ToString(ProcessValues, _connectionType);
                    }
                }
                #endregion

                current.ClearForm();

                if (child)
                    _childForm = current;
                else
                    _form = current;


                #region Дополнительная проверка на условие checkvalue
                // Определяет переходы между экранами 
                // Атоматически перескакивает экраны, если необходимо
                // Динамически отслеживать какие-либо флаги
                if (current.CheckValue != string.Empty)
                {
                    // Надо очищать глобальный кеш от значений
                    // Этой формы, т.к. она может не отображаться
                       RemoveCacheValuesByForm(current);

                    if (!current.IsVisible(ProcessValues))
                    {
                        current.ClearForm();
                        return GetProcessScreen2(current.Target, false, true);
                    }
                }
                #endregion

                if (current.IsAutoRefresh)
                    ClearUserParams();

                if (child)
                    _childForm = current;
                else
                    _form = current;


                #endregion
            }

            #region Дальнейшая обработка форм
            // выполняем текущее событие формы
            // вызываем текущее событие
            current.SetCurrentEvent(string.Empty, child, ref ProcessValues);

            // надо установить текущую позицию курсора
            if (current.HasActiveControls()) // установлен текущий активный контрол
                rez = current.ToString(true, ProcessValues, _connectionType);
            else
            {
                // проверка на то, что все события формы выполнены
                if (current.CurrentEvent != Events.OnInit || child)
                {
                    current.SetCurrentEvent(string.Empty, child, ref ProcessValues);
                    rez = current.ToString(true, ProcessValues, _connectionType);
                }
                // проверка на то, что при событии инициализации формы
                // произошла какая-то ошибка или инициализация не выполнилась
                else if (current.CurrentEvent == Events.OnInit)
                {
                    rez = current.ToString(true, ProcessValues, _connectionType);
                }
            }

            // Возвращаем текущее значение формы
            if (child)
                _childForm = current;
            else
                _form = current;
            #endregion

            // возвращаем экран
            return rez;
        }
        #endregion



        #region GenMultiResultException - Проверяет статус формы возврат нескольких значений
        /// <summary>
        /// Процедура генерирует дополнительную форму если при событии обновления
        /// хранимая процедура возвратила несколько строк
        /// </summary>
        /// <param name="form"></param>
        /// <returns>Bool true - если несколько строк</returns>
        private bool GenMultiResultException(RfForm form)
        {
            // Проверка на то что процедура возвратила несколько значений
            switch (form.CurrentState)
            {
                case FormState.MultiResult:
                    {
                        string rr = form.GetActiveControl().ValueID;

                        if (ExistsFormById(9000))
                        {
                            RfForm frm = GetFormById(9000);
                            //_formList.Remove(_frm);
                            _formList.Remove(frm.Id);

                            ((RfControlList)frm.GetControlById(3)).FillStaticValue((DataTable)ProcessValues[rr]);
                            frm.Parent = form.Parent;
                            frm.Permission = form.Permission;

                            //_formList.Insert(0, _frm);
                            _formList.Add(frm.Id, frm);

                        }

                        _hasChild = true;

                        _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") 
                                         + GetProcessScreen2(9000, true, true)
                                         + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");

                        return true;
                    }
                default:
                    return false;
            }
        }

        #endregion
        

        #region GetFormById(int Id) - Возвращает форму по идентификатору
        /// <summary>
        /// Возвращает форму по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private RfForm GetFormById(int id)
        {
            if(!_formList.ContainsKey(id))
                throw new ConfigurationException("Ошибка: Нет объекта с кодом - " + id.ToString());
            return _formList[id];
        }
        #endregion


        #region TrySetCurrentFormById
        /// <summary>
        /// Устанавливает текущую форму для пользователя
        /// (Используется для сериализации объектов)
        /// </summary>
        /// <returns>bool</returns>
        public bool TrySetCurrentFormById(ref Dictionary<string,object> cache)
        {
            if (!ExistsFormById(ActiveFormId))
                return false;

            if (_childForm != null)
            {
                _childForm = GetFormById(ActiveFormId);
                _childForm.IsActivate = true;
                _childForm.CurrentEvent = Events.UnBounded;
                _childForm.SetNextActiveControl(ref cache);
            }
            else
            {
                _form = GetFormById(ActiveFormId);
                _form.IsActivate = true;
                _form.CurrentEvent = Events.UnBounded;
                _form.SetNextActiveControl(ref cache);
            }

            return true;
        }

        #endregion


        #region ExistsFormById(int Id) - Возвращает true/false на существование формы или нет
        /// <summary>
        /// Возвращает true если форма есть в списке, в противном случае false
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool ExistsFormById(int id) { return _formList.ContainsKey(id); }
        #endregion


        #region ReturnStrung - Возвращает результат которые возвращается пользователю
        /// <summary>
        /// Возвращает результат которые возвращается пользователю
        /// </summary>
        /// <returns></returns>
        public string ReturnString() { return _result; }
	    #endregion    


        #region Методы обработки переменных сессии
        /// <summary>
        /// Возвращает параметр из массива данных
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public object GetParamById(string id) { return ProcessValues[Utils.Utils.AllTrim(id.ToUpper())]; }

        /// <summary>
        /// Добавляет параметр к массиву данных
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetParam(string key, string value)        
        {            
            if (!ProcessValues.ContainsKey(key))
                ProcessValues.Add(Utils.Utils.AllTrim(key.ToUpper()), Utils.Utils.AllTrim(value.ToUpper()));
        }

/*
        /// <summary>
        /// Устанавливает значения параметров, которые отслеживается во время сессии
        /// Эти параметры являются общими. Читаются из xml файла конфигурации
        /// </summary>
        private void SetDefaultParams(string [,] obj)
        {
            try
            {
                // Добавляем значения глобальных параметров
                // которые пришли при логине пользователя
                for (int i = 0; i < obj.Length / 2; i++)
                {
                    if (ProcessValues.ContainsKey(obj[i, 0].ToString()))
                        ProcessValues[obj[i, 0].ToString()] = obj[i, 1].ToString();
                    else
                        ProcessValues.Add(obj[i, 0].ToString(), obj[i, 1].ToString());
                }
            }
            catch (Exception ex)
            {
                Utils.Utils.WriteLog(ex.Message);
            }
        }
*/
        
        #endregion


        #region ClearUserParams
        /// <summary>
        /// Удаляет пользовательские данные из процесса, кроме системных
        /// </summary>
        private void ClearUserParams()
        {
            Dictionary<string, object> newobj = ProcessValues.Where(kvp => kvp.Key.StartsWith("SYS", StringComparison.InvariantCultureIgnoreCase)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            /*
            foreach (KeyValuePair<string, object> kvp in ProcessValues)
            {
                if (kvp.Key.StartsWith("SYS", StringComparison.InvariantCultureIgnoreCase))
                    newobj.Add(kvp.Key, kvp.Value);
            }
            */
            ProcessValues = newobj;
        }
        #endregion


        #region RemoveCacheValuesByForm
        /// <summary>
        /// Удаляет данные из глобального кеша для форм, которые показываются по
        /// определенным параметрам
        /// </summary>
        /// <param name="form">форма с которой идет поиск глобальных переменных</param>
        private void RemoveCacheValuesByForm(RfForm form)
        {
            foreach (var cnl in form.Controls.Where(cnl => cnl.OheControlType == OheConstant.OheTelnetControlType.InputBox || cnl.OheControlType == OheConstant.OheTelnetControlType.List))
            {
                ProcessValues.Remove(cnl.ValueID);
            }

            /*
            foreach (RfControl cnl in form.Controls)
            {
                if (cnl.ControlType == OheConstant.Type.InputBox || cnl.ControlType == OheConstant.Type.List)
                {
                    ProcessValues.Remove(cnl.ValueID);
                }
            }
            */
        }

        #endregion


        #region CenterString - выравнивает строку по горизонтали

        /// <summary>
        /// Выравнивает строку по центру
        /// </summary>
        /// <returns></returns>
        private string CenterString(string str)
        {
            int width = TlnConfigManager.TELNET_WIDTH;
            if (str.Length > width || str.Length == 0)
                return str;

            int center = (int) Math.Round((decimal) width/2, 0);
            int width2 = (int) Math.Round((decimal) str.Length/2, 0);

            string rez = string.Empty;
            for (int line = 1; line < center - width2; line++)
                rez += " ";

            rez += str +
                   "                                      ".Substring(0,
                                                                      (width - (rez + str).Length > 0
                                                                           ? width - (rez + str).Length
                                                                           : 1));

            return rez;
        }

        #endregion


        public OheTelnetScreenManager Clone()
        {
            return (OheTelnetScreenManager)this.MemberwiseClone();
        }
    }
}
