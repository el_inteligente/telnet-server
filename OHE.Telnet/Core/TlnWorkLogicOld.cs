﻿using System;
using Ohe.Telnet.Controls;
using Ohe.Telnet.Data;
using Ohe.Telnet.Configuration;
using Ohe.Telnet.Exception;
using Ohe.Utils;

namespace Ohe.Telnet.Core
{
    /// <summary>
    /// Описывает логику работу с формами и меню
    /// </summary>
    public partial class OheTelnetScreenManager

    {
        #region ParseString(private) - Парсит результат ввода пользователя
        /// <summary>
        /// Парсит результат ввода пользователя
        /// </summary>
        [Obsolete("Устаревшая процедура: Используйте ParseString2")]
        private void ParseString(string str)
        {

            #region Пользователь на меню подсказки
            // Пользователь в текущий момент смотрит подсказку по экрану
            if (_isHelp)
            {
                if (_isHeadMenu)
                {
                    _result = _menu.GetCurrentMenuPermiss();
                    _result = (_result == "screen" ? string.Empty : _result);
                }
                if (_isScreen)
                    if (_hasChild)
                        _result = GetProcessScreen(_form.Id, true, false);
                    else
                        _result = GetProcessScreen(_form.Id, false, false);

                _isHelp = false;
                return;
            }
            #endregion

            else

            #region Пользователь нажал меню подсказки
            {
                // К нам пришел запрос на получение помощи для меню и т.д.
                if (Utils.Utils.AllTrim(str) == TlnConfigManager.TELNET_HELP_STR)
                {
                    // пользователь на одном из меню
                    if (_isHeadMenu)
                        if (_menu.HasHelpMenu())
                        {
                            _result = _menu.GetControlHelp();
                            _isHelp = true;
                        }

                    // Пользователь на экране процесса
                    if (_isScreen)
                        // проверка на то что пользователь на подчиненной форме
                        if (_hasChild)
                        {
                            if (_childForm.HasHelpMenu())
                            {
                                _result = _childForm.GetControlHelp().ToString(_connectionType);
                                _isHelp = true;
                            }
                        }
                        else
                            if (_form.HasHelpMenu())
                            {
                                _result = _form.GetControlHelp().ToString(_connectionType);
                                _isHelp = true;
                            }
                    return;
                }
            }

            #endregion


            #region Пользователь находится на одном из экранов
            if (_isScreen)
            {
                // Надо определить еще специальные клавиши управления на форме
                #region Пользователь ввел ESC
                if (str.IndexOf((char)27) >= 0)
                {
                    // блокируем выход из экрана если находится
                    // на контроле список на подчиненной форме
                    if (_hasChild)
                    {
                        if (_childForm.GetActiveControl() is RfControlList)
                            return;
                    }

                    // Проверяем возможен ли выход из формы
                    // Если значение определено что нельзя выходить с формы 
                    // Parent = -1 то выводим сообщение о недоступности
                    if (_form.Parent == -1)
                    {
                        _form.SetMessage("НЕВЕРНАЯ ОПЦИЯ");
                        _result = _form.ToString(true, ProcessValues, _connectionType) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                        return;
                    }

                    // проверка что товар на погрузкчике
                    // 10.07.2008 - добавлена функция отслеживания погрузкчика
                    if (_form.IsCheckFork && ExecuteSqlQuery.ExecuteScalar("select count(*) from t_binlocat (nolock) where binlabel='@" + _user + "'") > 0)
                    {
                        _form.SetMessage("ТОВАР НА ПОГРУЗЧИКЕ");
                        _result = _form.ToString(true, ProcessValues, _connectionType) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                        return;
                    }

                    // Инициаруем вызов события выхода с экрана
                    _form.CurrentEvent = Events.OnExit;
                    _form.SetCurrentEvent(string.Empty, _hasChild, ref ProcessValues);

                    // Блокировка если пользователь не определил меню или форму выхода из экрана
                    if (_form.Parent > -1 && _menu.ExistsMenuById(_form.Parent))
                    {
                        _menu.EscMenu = _form.Parent; // переправляем пользователя на меню
                    }
                    else
                    {
                        // Проверка на то что для пользователя назначен переход
                        // не на меню а на предыдущий экран
                        if (_form.Parent > -1 && ExistsFormById(_form.Parent))
                        {
                            _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen(_form.Parent, false, true);
                            return;
                        }
                        _menu.EscMenu = 0;
                    }

                    _isScreen = false;
                    _isHeadMenu = true;


                    ClearUserParams(); // очищаем пользовательские параметры из общего кеша

                    _form.ClearForm();

                    // возвращаем результат
                    _result = _menu.GetCurrentMenuPermiss();
                    return;
                }
                #endregion

                if (_hasChild)
                {
                    // Сначала обрабатываем подчиненную форму 
                    //(на подчиненной форме не должно быть управляющих ключей)
                    #region Обработка подчиненной формы если она есть

                    // Проверка на то что контрол в нормальном состоянии
                    if (_childForm.CurrentState == FormState.Exception)
                    {
                        // Ошибка на форме, проверка того что застряли на
                        // контроле типа list, надо переходить на другой контрол
                        if (_childForm.GetActiveControl() is RfControlList)
                        {
                            _childForm.GetActiveControl().IsActivate = false;
                            _result = _childForm.ToString(true, ProcessValues, _connectionType);

                            // Очищаем форму нечего больше выбирать
                            // произошла ошибка
                            _childForm.CurrentEvent = Events.OnEnd;

                            // 20.09.2008 Добалена строка ниже
                            _isExpеtion = true;
                            ParseString("");
                            return;
                        }
                    }

                    // Определяем следующее событие для подчиненной формы
                    _childForm.SetCurrentEvent(str, true, ref ProcessValues);

                    _result = _childForm.ToString(true, ProcessValues, _connectionType);

                    // Возвращаем результат текущего состояния формы
                    if (!_childForm.HasActiveControls())
                    {
                        ((RfControl)_form.GetActiveControl()).Value = _childForm.ResultFromChild;
                        _result = _form.ToString(true, ProcessValues, _connectionType);

                        _hasChild = false;
                        _childForm.SetCurrentEvent(str, true, ref ProcessValues);
                        // Очищаем форму после обработки
                        _childForm.ClearForm();
                        // Вызываем процедуру с подтверждением ввода
                        // параметра пришедшего из списка
                        ParseString("");
                        return;
                    }
                    #endregion
                }
                else
                {
                    // Проверка на то что пользователь находится на
                    // экране выбора завершить процесс или продолжить
                    if (_isBool)
                    {
                        #region Пользователь на экране завершить процесс
                        if (Utils.Utils.AllTrim(str) == TlnConfigManager.TELNET_KEY_Y)
                        {
                            ClearUserParams(); // очищаем пользовательские параметры из общего кеша
                            _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen(_form.Parent, false, true);
                        }
                        else
                        {
                            ParseString(((char)27).ToString());
                        }
                        _isBool = false;
                        return;
                        #endregion
                    }

                    // Произошла СИСТЕМНАЯ ОШИБКА
                    #region Обработка Системной Ошибки
                    if (_isExpеtion)
                    {
                        _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_SYSTEM_ECXEPTION + TlnConfigManager.BEEP : OheConstant.TELNET_SYSTEM_EXCEPTON_WEBCONNECTION);
                        _isExpеtion = false;
                        return;
                    }
                    #endregion

                    // проверка на то что форма активирована
                    #region Обработка значений с ОСНОВНОЙ формы
                    if (_form.IsActivate)
                    {

                        #region Обработка функциональных клавиш. Есть ключи для экрана для запуска child
                        // Возможно к нам пришли управляющие значения которые вызывают подчиненную форму
                        foreach (RfControlKey cnl in _form.ContrrolKeys)
                        {
                            // Текущая форма в которой находится пользователь
                            ActiveFormId = _form.Id;
                            // Да есть управляющие элементы и нет child форм еще
                            if (Utils.Utils.AllTrim(cnl.Value.ToUpper()) == Utils.Utils.AllTrim(str))
                            {
                                // Проверка на то что соответствуют параметры для запуска ключа
                                if (!cnl.IsVisible(ProcessValues))
                                {
                                    _form.SetMessage("ОПЦИЯ НЕ ДОСТУПНА");
                                    _result = _form.ToString(true, ProcessValues, _connectionType) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");
                                    return;
                                }

                                // Функциональная клавиша есть подчиненная форма
                                if (cnl.IsChild)
                                {
                                    _hasChild = true;
                                    _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen(cnl.Target, true, true);
                                    // Наличие системных ошибок проверяется дальше при следующем
                                    // запуске этой процедуры
                                }
                                else
                                {
                                    _form.ClearForm();
                                    _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen(cnl.Target, false, true);

                                    // Произошла системная ошибка в конфигурации или в хранимой процедуре
                                    if (_form.CurrentState == FormState.Exception)
                                    {
                                        GenSystemException(_form, ActiveFormId, false);
                                        return;
                                    }
                                }
                                return;
                            }
                        }
                        #endregion


                        #region Работаем с основным экраном
                        if (!_hasChild)
                        {
                            //Serialization
                            ActiveFormId = _form.Id;

                            // Обрабатываем контролы на форме
                            _form.SetCurrentEvent(str, false, ref ProcessValues);

                            // Проверка на наличие системных ошибок
                            // при при событии обновления onchange
                            if (GenSystemException(_form, _form.Id, false)) return;

                            // Проверка на то что несколько строк возращается из хранимой процедуры
                            if (GenMultiResultException(_form)) return;

                            // Проверка на то что процесс в цикле закончен
                            if (_form.CurrentState == FormState.InValid
                                && _form.CurrentEvent == Events.OnInit)
                            {
                                _result = _form.Message.ToString(_connectionType);
                                _currentStatus = OheFormStatus.IsBool;
                                return;
                            }

                            _result = _form.ToString(true, ProcessValues, _connectionType)
                                + (_form.CurrentState == FormState.InValid && _connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");

                            // у нас выполнено событие надо переходить на другую форму
                            if (!_form.HasActiveControls() && _form.CurrentEvent == Events.OnEnd)
                            {
                                // Вызываем событие окончания обработки формы
                                // Очищаем форму
                                _form.SetCurrentEvent(str, false, ref ProcessValues);

                                ActiveFormId = _form.Id;

                                // Проверяем что идентификатор перехода у нас меню
                                if (_menu.ExistsMenuById(_form.Target))
                                {
                                    _result = GotoMenu(_form.Target);
                                    return;
                                }

                                // Переходим на другую форму
                                _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen(_form.Target, false, true);

                                // Проверка на наличие системных ошибок
                                // 20.09.2008
                                if (GenSystemException(_form, ActiveFormId, false)) return;

                                // Serialization
                                ActiveFormId = _form.Id;
                            }
                        }
                        #endregion
                    }
                    #endregion
                }


            }
            #endregion


            #region Пользователь находится в Menu
            if (_isHeadMenu)
            {
                // Произошла СИСТЕМНАЯ ОШИБКА
                #region Обработка Системной Ошибки
                if (_isExpеtion)
                {
                    _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_SYSTEM_ECXEPTION + TlnConfigManager.BEEP : OheConstant.TELNET_SYSTEM_EXCEPTON_WEBCONNECTION);
                    _isExpеtion = false;
                    return;
                }
                #endregion


                if (str.IndexOf((char)27) >= 0)
                {
                    #region Пользователь ввел ESC
                    // Переходим в меню уровнем выше
                    _result = _menu.GetSubMenuOrScreen(999);
                    if (Utils.Utils.AllTrim(_result) == "false")
                    {
                        _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "<div class=\"form\"><span class=\"message\">Сеанс работы пользователя завершен.</span></div>");
                        this.IsConnected = false;
                    }
                    #endregion
                }
                else
                {
                    string UserVal = Utils.Utils.AllTrim(str.ToUpper());

                    #region Обрабатываем другие данные от пользователя
                    if (UserVal == TlnConfigManager.TELNET_MENU_DOWN)
                    {
                        // пользователь решил перейти на следующую страницу
                        // возвращаем результат
                        _result = _menu.MoveNextPage();
                        return;
                    }
                    else if (UserVal == TlnConfigManager.TELNET_MENU_UP)
                    {
                        // пользователь переходит на экран назад
                        _result = _menu.MovePriveousPage();
                        return;
                    }
                    #endregion

                    // Обрабатываем какие-то числа от пользователя
                    #region Обработка чисел от пользователя
                    try
                    {
                        int inter;
                        if (!int.TryParse(UserVal, out inter))
                        {
                            _result = _menu.GetCurrentMenuPermiss();
                            if (_result == "screen")
                            {
                                if (OheConnectionType == OheConnectionType.Web)
                                    _result = OheConstant.TELNET_CONFIGURATION_MENU_EXCEPTON;
                                else
                                    _result = OheConstant.TELNET_SYSTEM_ECXEPTION;

                                _isHeadMenu = true;
                                _isScreen = false;
                                //_isHelp = true;
                                _menu.EscMenu = 0;
                            }
                            return;
                        }


                        if (inter < 1 && inter > TlnConfigManager.MenuLinesCount)
                            return;
                        // пользователь выбрал какой то из пунктов меню
                        // Возможно что мы попали на подменю
                        _result = _menu.GetSubMenuOrScreen(inter);
                        // проверка что мы действительно попали в подменю
                        // может мы уже на экране, надо проверить
                        if (_menu.GetBoolMenuPermiss() == 0)
                        {
                            // Надо делать процесс
                            _isHeadMenu = false;
                            _isScreen = true;
                            _formList = _config.GetForms; //GetFormsFromXml(int.Parse(userInput));
                            try
                            {
                                _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen(_menu.CurrentTarget, false, true);

                                //20.09.2008 ДОбавление отслеживания системных ошибок
                                if (_form.CurrentState == FormState.Exception)
                                {
                                    _form.ClearForm();
                                    _isHeadMenu = true;
                                    _isScreen = false;
                                    _isExpеtion = true;
                                    _menu.EscMenu = 0;
                                    ParseString(str);
                                    return;
                                }
                            }
                            catch (ConfigurationException ex)
                            {
                                _isHeadMenu = true;
                                _isScreen = false;
                                _isHelp = true;
                                _menu.EscMenu = 0;
                                _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ
                                            + CenterString("ОШИБКА:")
                                            + Environment.NewLine
                                            + CenterString("В КОНФИГУРАЦИИ") + Environment.NewLine
                                            + CenterString("обратитесь к") + Environment.NewLine
                                            + CenterString("АДМИНИСТРАТОРУ") + Environment.NewLine
                                            + OheConstant.SetCursorPosition(0, 0) :
                                            OheConstant.TELNET_CONFIGURATION_EXCEPTON_WEBCONNECTION);

                                Utils.Utils.WriteLog(ex.ToString());
                            }
                        }

                    }
                    catch (System.Exception exc)
                    {
                        Utils.Utils.WriteLog(exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.StackTrace);
                        _result = _menu.GetCurrentMenuPermiss();
                        return;
                    }

                    #endregion
                }
            }
            #endregion
        }
        #endregion

        [Obsolete("Устаревшая процедура: Используйте GotoMenu2")]
        private string GotoMenu(int menuId)
        {
            _menu.EscMenu = menuId; // переправляем пользователя на меню
            _isScreen = false;
            _isHeadMenu = true;
            ClearUserParams(); // очищаем пользовательские параметры из общего кеша

            _form.ClearForm();

            // возвращаем результат
            return _menu.GetCurrentMenuPermiss();
        }


        #region GenSystemException - обработка Системных Ошибок
        /// <summary>
        /// Процедура генерирет иключение, которое связано с базой данных
        /// </summary>
        /// <param name="form"></param>
        /// <param name="id"></param>
        [Obsolete("Устаревшая процедура: Используйте IsSystemException")]
        private bool GenSystemException(RfForm form, int id, bool isChiled)
        {
            if (form.CurrentState == FormState.Exception)
            {
                form.ClearForm();
                _result = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "") + GetProcessScreen(id, isChiled, true) + (_connectionType == OheConnectionType.HandHeld ? TlnConfigManager.BEEP : "");

                _isExpеtion = true;
                ParseString("");
                return true;
            }
            return false;
        }
        #endregion


        #region GetProcessScreen - обрабатывает форму и возвращает текущий экран пользователя
        /// <summary>
        /// Устанавлиет текущий экран пользователя
        /// </summary>
        /// <param name="Id">Указывает идентификатор формы, если 999 - определяется автоматически</param>
        /// <param name="Child">Определяет какая форма основная или дочерняя</param>
        /// <param name="IsFirst">Определяет пришла ли форма в первый раз или нет</param>
        /// <returns>string</returns>
        [Obsolete("Используйте метод GetProcessScreen2")]
        public string GetProcessScreen(int Id, bool Child, bool IsFirst)
        {
            string rez = string.Empty;
            string cursor = string.Empty; // устанавливает курсор на место 

            // Условие для начального входа пользователя в процесс
            if (IsFirst)
                #region Предварительная обработка форм если пришли в первый раз
                if (!Child)
                {
                    #region обработка основной формы
                    _form = GetFormById(Id); // определяем текущую форму пользователя
                    //Serialization
                    ActiveFormId = _form.Id;

                    #region Обработка форм типа IsIf
                    // Форма - есть форма условие?
                    if (_form.IsIf)
                    {
                        // Проверка на выполнение условий CheckValue для форм условий
                        if (_form.CheckValue != string.Empty)
                        {
                            // Надо очищать глобальный кеш от значений
                            // Этой формы, т.к. она может не отображаться
                            RemoveCacheValuesByForm(_form);

                            if (!_form.IsVisible(ProcessValues))
                            /*    if (Utils.Utils.GetParamValue(Utils.Utils.GetObjName(_form.CheckValue)                                  , Utils.Utils.GetObjColumn(_form.CheckValue)                                  , ProcessValues) == "N")                            */
                            {
                                _form.ClearForm();
                                return GetProcessScreen(_form.Target, false, true);

                            }
                        }

                        _form.CurrentEvent = Events.OnInit;

                        _form.SetCurrentEvent("", false, ref ProcessValues);

                        switch (_form.CurrentState)
                        {
                            case FormState.Valid:
                                return GetProcessScreen(_form.Target, false, true);
                            case FormState.InValid:
                                //return GetProcessScreen(_form.Parent, false, true) + (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_BEEP_SEQ : "");
                                return GetProcessScreen(_form.Parent, false, true);
                            default:
                                return _form.ToString(ProcessValues, _connectionType);
                        }
                    }
                    #endregion

                    _form.ClearForm();

                    #region Дополнительная проверка на условие checkvalue
                    // Определяет переходы между экранами 
                    // Атоматически перескакивает экраны, если необходимо
                    // Динамически отслеживать какие-либо флаги
                    if (_form.CheckValue != string.Empty)
                    {
                        // Надо очищать глобальный кеш от значений
                        // Этой формы, т.к. она может не отображаться
                        RemoveCacheValuesByForm(_form);

                        if (!_form.IsVisible(ProcessValues))
                        {
                            _form.ClearForm();
                            return GetProcessScreen(_form.Target, false, true);
                        }
                    }
                    #endregion

                    if (_form.IsAutoRefresh)
                        ClearUserParams();
                    #endregion
                }
                else
                {
                    #region обработка подчиненной формы
                    _childForm = GetFormById(Id); // определяем текущую форму пользователя
                    // Форма - есть форма условие?
                    if (_childForm.IsIf)
                    {
                        // Проверка на выполнение условий CheckValue для форм условий
                        if (_form.CheckValue != string.Empty)
                        {
                            // Надо очищать глобальный кеш от значений
                            // Этой формы, т.к. она может не отображаться
                            RemoveCacheValuesByForm(_form);

                            if (!_form.IsVisible(ProcessValues))
                            {
                                _form.ClearForm();
                                return GetProcessScreen(_form.Target, false, true);
                            }
                        }

                        _childForm.CurrentEvent = Events.OnInit;
                        _childForm.SetCurrentEvent("", true, ref ProcessValues);

                        switch (_childForm.CurrentState)
                        {
                            case FormState.Valid:
                                return GetProcessScreen(_childForm.Target, false, true);
                            case FormState.InValid:
                                return GetProcessScreen(_childForm.Parent, false, true);
                            default:
                                return _childForm.ToString(ProcessValues, _connectionType);
                        }

                    }
                    _childForm.ClearForm();

                    if (_childForm.IsAutoRefresh)
                        ClearUserParams();

                    #region Дополнительная проверка на условие checkvalue
                    // Определяет переходы между экранами 
                    // Атоматически перескакивает экраны, если необходимо
                    // Динамически отслеживать какие-либо флаги
                    if (_childForm.CheckValue != string.Empty)
                    {
                        // Надо очищать глобальный кеш от значений
                        // Этой формы, т.к. она может не отображаться
                        RemoveCacheValuesByForm(_form);

                        if (!_childForm.IsVisible(ProcessValues))
                        /*                        if (Utils.Utils.GetParamValue(Utils.Utils.GetObjName(_childForm.CheckValue)                                                      , Utils.Utils.GetObjColumn(_childForm.CheckValue)                                                      , ProcessValues) == "N")*/
                        {
                            _childForm.ClearForm();
                            return GetProcessScreen(_childForm.Target, false, true);
                        }
                    }
                    _childForm.ClearForm();
                    #endregion

                    #endregion
                }

                #endregion

            // Дальнейшая обработка форм
            // выполняем текущее событие формы
            if (!Child)
            {
                #region Обработка основной формы
                // вызываем текущее событие
                _form.SetCurrentEvent(string.Empty, Child, ref ProcessValues);

                // надо установить текущую позицию курсора
                if (_form.HasActiveControls()) // установлен текущий активный контрол
                {
                    rez = _form.ToString(true, ProcessValues, _connectionType);
                }
                // проверка на то, что все события формы выполнены
                else if (!_form.HasActiveControls()
                            && _form.CurrentEvent != Events.OnInit)
                {
                    _form.SetCurrentEvent(string.Empty, Child, ref ProcessValues);
                    rez = _form.ToString(true, ProcessValues, _connectionType);
                }
                // проверка на то, что при событии инициализации формы
                // произошла какая-то ошибка или инициализация не выполнилась
                else if (!_form.HasActiveControls()
                            && (_form.CurrentEvent == Events.OnInit))
                {
                    rez = _form.ToString(true, ProcessValues, _connectionType);
                }
                #endregion
            }
              else
            {
                #region Обработка подчиненной формы
                _childForm.SetCurrentEvent(string.Empty, Child, ref ProcessValues);
                // надо установить текущую позицию курсора
                if (_childForm.HasActiveControls())
                {
                    //_screenId = ((RfControl)_childForm.GetActiveControl()).Id;    // определяем идентификатор экрана где находится пользователь
                    //Добавил удаление сообщения об ошибке для этого надо перерисовать форму
                    rez = _childForm.ToString(true, ProcessValues, _connectionType);// +_childForm.GetCursorPosition();
                }
                else
                {
                    _childForm.SetCurrentEvent(string.Empty, Child, ref ProcessValues);
                    rez = _form.ToString(true, ProcessValues, _connectionType);// +_form.GetCursorPosition();
                }

                #endregion
            }

            // возвращаем экран
            return rez;
        }
        #endregion

    }
}
