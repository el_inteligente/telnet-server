using System;
using System.ServiceProcess;
using System.Windows.Forms;
using Ohe.Telnet.Server;

namespace Ohe.Telnet.Core
{
    public class EntryPoint : ServiceBase
    {

		#region Properties
        private System.ComponentModel.Container components = null;
		#endregion


		#region Constructor / Dispose

            public EntryPoint() { InitializeComponent(); }


		    protected override void Dispose( bool disposing )
		    {
			    if( disposing )
			    {
				    if (components != null) 
				    {
					    components.Dispose();
				    }
			    }
			    base.Dispose( disposing );
		    }

        #region InitializeComponent
            private void InitializeComponent() { this.ServiceName = "OpenHandheldEngine"; }
        #endregion

		#endregion


		#region OnStart Handler
            protected override void OnStart(string[] args) { StartServer(); }
		#endregion


		#region OnStop Handler
            protected override void OnStop() { StopServer(); }
		#endregion


		#region StartServer
		    public static bool StartServer()
		    {
			    try
				    {
                        // �������� ������ ������
                        TelnetServer.Start();
					    return true;
				    }
                catch (System.Exception ex)
				    {
					    Utils.Utils.WriteLog("Error: ������ ��� ������ ������." 
                                                + Environment.NewLine 
                                                + "������ ����� ���������."
                                                + Environment.NewLine 
                                                + ex.ToString());
					    return false;
				    }
		    }
		#endregion


		#region StopServer
        public static bool StopServer()
		{
			try
			{
                // ������������� ������ ������
                TelnetServer.Stop();
				return true;
			}
            catch (System.Exception ex)
			{
				MessageBox.Show("�� ������� ���������� ������" 
                                + Environment.NewLine
                                + "������ ����� ���������.");
                Utils.Utils.WriteLog("Error: �� ������� ���������� ������."
                                            + Environment.NewLine
                                            + "������ ����� ���������."
                                            + Environment.NewLine
                                            + ex.ToString());
				return false;
			}
		}
		#endregion


        #region Restart
        public static bool RestartServer()
        {
            try
            {
                if (StopServer())
                    return StartServer();
                else
                    return false;
            }
            catch { return false; }
        } 
        #endregion
  
    }
}
