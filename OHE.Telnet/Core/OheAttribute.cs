using System;
using System.ComponentModel;
using System.Globalization;

namespace Ohe.Telnet.ComponentModel
{
    public sealed class NameExpandableObjectConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context,
                                         CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                DisplayNameAttribute mna = context.PropertyDescriptor.Attributes[typeof(DisplayNameAttribute)] as DisplayNameAttribute;
                if (mna != null)
                    return mna.ToString();
                return context.PropertyDescriptor.Name;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
