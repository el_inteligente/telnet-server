using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;
using Ohe.Utils;

namespace VS.HandHeldServer
{
    [RunInstaller(true)]
    public partial class Telnet_Installer : Installer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        public Telnet_Installer()
        {
            InitializeComponent();
        }

        protected override void OnBeforeInstall(IDictionary savedState)
        {
            try
            {
                ServiceManager.UnInstallService("OpenHandheldEngine");
            }
            catch
            {
            }

            base.OnBeforeInstall(savedState);
        }

        public override void Install(IDictionary stateSaver)
        {
            //if (Utils.StrToBool(Context.Parameters["dependedonsql"]))
            //    serviceInstaller.ServicesDependedOn = new string[] { "MSSQLSERVER" };

            base.Install(stateSaver);
        }

        protected override void OnAfterInstall(IDictionary savedState)
        {
            try
            {
                //
                string conStr;
                if (Utils.AllTrim(Context.Parameters["dbuser"]) == string.Empty)
                {
                    conStr = "Persist Security Info=False;Integrated Security=SSPI;database=" + Context.Parameters["dbname"]
                        + ";server=" + Context.Parameters["dbhost"];
                    //Data Source=INGENEER_1_;Integrated Security=SSPI;Initial Catalog=vist; Asynchronous Processing=true;MultipleActiveResultSets=True
                }
                else
                {
                    conStr = "Persist Security Info=False;Integrated Security=false;database=" + Context.Parameters["dbname"]
                        + ";server=" + Context.Parameters["dbhost"]
                        + ";user Id=" + Context.Parameters["dbuser"]
                        + ";pwd=" + Context.Parameters["dbpassword"];
                }

                string str = Utils.SetConnString(conStr,"system.xml");


                if (str.Length > 0)
                {
                    Utils.WriteLog("�� ���� ���������������� ������ ������������� � ���� ������:"
                                        + Environment.NewLine
                                        + "������ ����������: " + str);
                }

            }
            catch (Exception e)
            {
                Utils.WriteLog("�� ���� ���������������� ������ ������������� � ���� ������:"
                                    + Environment.NewLine
                                    + "������:" + Environment.NewLine + e.Message);
            }


            #region ������ ������ ������� ���� �� ���������
            /*
            try
            {
                ServiceController sc = new ServiceController("OpenHandheldEngine");

                if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) ||
                    (sc.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    // Start the service if the current status is stopped.
                    sc.Start();
                }
                else if ((sc.Status.Equals(ServiceControllerStatus.Paused)) ||
                    (sc.Status.Equals(ServiceControllerStatus.PausePending)))
                {
                    if (sc.CanPauseAndContinue)
                        // Start the service if the current status is paused.
                        sc.Continue();
                }

                // Refresh and display the current service status.
                sc.Refresh();

            }
            catch (Exception sce)
            {
                Utils.Utils.WriteLog("�� ���� ��������� ������ ������!!!" +
                                    Environment.NewLine
                                    + sce.Message);
                MessageBox.Show("�� ���� ��������� ������ ������!!!", "������ ������ �������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            #endregion

            #region Not Use
            
            #region �������� �������� ������� ������
            bool _start = false;
            bool _trying = true;
            int hasTried = 0;
            try
            {
                while (!_start && _trying)
                {
                    string[] status = ServiceManager.GetServiceStatus("OpenHandheldEngine");
                    if (status[0].ToLower() == "running")
                    {
                        _start = true;
                        _trying = false;
                        break;
                    }
                    else
                    {
                        //try 20 times before popup to the user
                        if (hasTried > 20)
                        {
                            _trying = (DialogResult.Yes == MessageBox.Show(
                                "������ ������ �� �������!!!" + Environment.NewLine + Environment.NewLine +
                                "������� �� ����� ��������� �������, ��� ��� ��� ������ �� ���������." + Environment.NewLine
                                ,
                                "Open Handheld Engine",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Stop));
                        }
                    }
                    hasTried++;
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e2)
            {
                Utils.Utils.WriteLog("������ ������ �� �������!!!" + Environment.NewLine+ e2.Message);
                _start = false;
            }

            if (!_start)
            {
                throw new Exception("������ ������ �� �������!!!");
            }
            #endregion
            */

            #endregion


            base.OnAfterInstall(savedState);
        }
    }
}