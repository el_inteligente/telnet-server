using System;
using System.Collections;
using Ohe.Interfaces;

namespace Ohe.Telnet.Server
{
    /// <summary>
    /// ������������ ��� ���������� �������������� � ����������� ���������� ��������������
    /// </summary>
    public class TelnetServerManager : MarshalByRefObject, IOheMonitor
    {
        // ��� remoting
        public override object InitializeLifetimeService() { return null; }

        #region IsAlive
        public bool IsAlive() { return true; }
        #endregion

        #region UserConsoles - ������� ������ ���� ������������ �������������
        /// <summary>
        /// ���������� ������ ���� �������������� �������������
        /// </summary>
        public ArrayList UsersConsoles { get { return TelnetServer.UsersConsoles; } }
        #endregion

        #region Constructor
        public TelnetServerManager() { }
        #endregion

        #region Methods (not use)
        /// <summary>
        /// ��������� ������������ � ������������
        /// </summary>
        /// <param name="Login"></param>
        /// <param name="Pass"></param>
        /// <param name="Permiss"></param>
        
        /*
        public void AddUser(string Login, string Pass, string Permiss,string Name, string Whse)
        {
            TelnetServer._cnfServer.AddUser(Login, Pass, Permiss,Name, Whse);
        }
        */


        /// <summary>
        /// ���������� ������ ���� �������������
        /// </summary>
        //public ArrayList Users { get { return TelnetServer._cnfServer.GetAllUsersList(); } }

        #endregion
    }
}

