using System;
using System.Collections;
using System.Linq;
using System.Threading;
using Ohe.Telnet.Configuration;
using Ohe.Telnet.Data;
using Ohe.Utils;
using Ohe.Utils.WCF;
using System.ServiceModel;
using System.Collections.Generic;

namespace Ohe.Telnet.Server
{
    /// <summary>
    /// ��������� �������� ������������� � �� �������������
    /// </summary>
    public sealed class TelnetSessionManager
    {

        #region Define Variables
        private readonly int _timeout; // ����� ����������� ������������ � ��������
        private string _msg;  // ��������� �� ������� � �.�.
        private static ArrayList _usersConnections = new ArrayList();
        private static readonly string SyncObject = Guid.NewGuid().ToString();

        // ������� ������ ������ ���������
        public delegate void KillSessionDelegate(UserTelnetConnection sender);
        public event KillSessionDelegate OnKillSession;

        private Thread _killer;
        private bool _isContinue = true;
        // ������ ���������� � ����������
        private static OheTelnetModuleEventSubscriber _proxy;

        #endregion


        #region Properties
        /// <summary>
        /// ��������� ��� ������� ?
        /// </summary>
        public string Message { get { return _msg; } set { _msg = value; } }

        /// <summary>
        /// ������ ���� �������������� �������������
        /// </summary>
        public ArrayList Connections { get { return _usersConnections; } set { _usersConnections = value; } }


        private static OheTelnetModuleEventSubscriber Proxy
        {
            get
            {
                try
                {
                    if (_proxy.isAlive())
                    {
                        if (_proxy.State == CommunicationState.Faulted)
                        {
                            StartProxy();
                        }
                        return _proxy;
                    }
                    StartProxy();
                }
                catch
                {
                    if(_proxy != null)
                        _proxy.Close();

                    StartProxy();
                }
                return _proxy;
            }
        }


        public static int ModuleID { get { return Proxy.ModuleID; } }

        #endregion        


        #region Constructors

        public TelnetSessionManager(int timeout)//, int max)
        {
            _timeout = timeout;

            // ������� ����� ��� ��������� �������� ������,
            // ���� �� �������� ���� �������� � ����������������
            _killer = new Thread(new ThreadStart(this.KillSession));
            _killer.Priority = ThreadPriority.Lowest;
            _killer.Start();
        }

        #endregion        


        #region StartProxy - �������� ��������� ������
        /// <summary>
        /// ����� �������� ��������� ����������
        /// </summary>
        private static void StartProxy()
        {
#if DEBUG
#else
            try
            {
                _proxy = new OheTelnetModuleEventSubscriber(System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString() + ":" + TlnConfigManager.Port.ToString());
                _proxy.OnSubscribe(_proxy.ModuleID, Interfaces.EventType.Stop);
                _proxy.OnSubscribe(_proxy.ModuleID, Interfaces.EventType.Start);
                _proxy.OnSubscribe(_proxy.ModuleID, Interfaces.EventType.Remove);

                _proxy.RemoveUsers += TelnetServer.RemoteRemoveUser;

                // �������� ���������� � ��������
                SendModuleInfo();
            }
            catch (TimeoutException timeProblem)
            {
                Utils.Utils.WriteLog("������ �������� ���������� �� ��������. " + timeProblem.Message);
            }
            catch (FaultException unknownFault)
            {
                Utils.Utils.WriteLog("�� ������� �������� �������� ����������� ����������. " + unknownFault.Message);
            }
            catch (CommunicationException commProblem)
            {
                Utils.Utils.WriteLog("��� ���������� � �������� ��������. " + commProblem.Message + commProblem.StackTrace);
            }
            // �������� ������ - ������������� ����������
            catch (System.Exception exc)
            {
                Utils.Utils.WriteLog(exc.Message);
            }
#endif
        }

        #endregion


        #region SendModuleInfo - ���������� � ��������
        /// <summary>
        /// ����� �������� ���������� � ��������
        /// </summary>
        private static void SendModuleInfo()
        {
            string _version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            // ����� ��������� �������� ������� ���������� �� ����
            OheModuleInfo _moduleInfo = new OheModuleInfo
            {
                IP = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString() + ":" + TlnConfigManager.Port.ToString(),
                AppDomain = Environment.UserDomainName,
                LifeTime = (Environment.TickCount / (1000 * 60)).ToString(),
                Memory = Environment.WorkingSet.ToString(),
                ProcessorCount = Environment.ProcessorCount.ToString(),
                Name = System.Net.Dns.GetHostName(),
                Version = _version,
                WindowsVersion = Environment.OSVersion.VersionString + "(" + Environment.OSVersion.Platform.ToString() + ")",
                ModuleName = "������ ���������� ����������������",
                ModuleID = Proxy.ModuleID,
                ModuleType = OheConstant.TELNET
            };

            Proxy.AddModuleInfo(_moduleInfo);
        }

        #endregion


        #region RemoveUser
        /// <summary>
        /// ������� �������� ������������� �� ������ �������� ����������
        /// ������� � ������
        /// </summary>
        /// <param name="login"></param>
        public static void RemoveUser(string login)
        {
            try
            {
                lock (SyncObject)
                    Proxy.RemoveUser(login, OheConnectionType.HandHeld);
            }
            catch (System.Exception exc)
            {
                Utils.Utils.WriteLog(exc.Message);
            }
        }
        #endregion


        #region RemoteRemoveUser - �������� ���������� �������������
        /// <summary>
        /// �������� ������������. ������������ � windows ���������� ���������
        /// </summary>
        /// <param name="login"></param>
        public void RemoteRemoveUser(string [] login)
        {
            try
            {
                ArrayList remove = new ArrayList();
                foreach (string user in login)
                {
                    //Utils.Utils.WriteLog("RemoteRemoveUser �������� ������������:" + user);
                    foreach (UserTelnetConnection usr in _usersConnections)
                    {
                        if (usr.UserID == Utils.Utils.AllTrim(user))
                        {
                            remove.Add(usr);
                        }
                    }
                }

                foreach(UserTelnetConnection usr in remove)
                    RemoteRemoveUser(usr.UserID);
            }
            catch (System.Exception exc)
            {
                Utils.Utils.WriteLog("������ � ������ RemoteRemoveUser " + exc.Message);
            }

        }


        /// <summary>
        /// �������� ������������. ������������ � windows ���������� ���������
        /// </summary>
        /// <param name="login"></param>
        private void RemoteRemoveUser(string login)
        {
            foreach (UserTelnetConnection usr in _usersConnections)
            {
                if (usr.UserID == login)
                {
                    // ���������� �������� ������ �� ������ ��� ��������
                    usr.LogOff = true;
                    Utils.Utils.WriteLog("TlnSessionManager.RemoteRemoveUser() " + login);
                    OnKillSession(usr);
                    break;
                }
            }
        }
        #endregion


        #region ReconnectUser - ��������������� ������������ ����� ������ ������, �.�. ���������� �����
        public void ReconnectUser(UserTelnetConnection user, string login)
        {
            ArrayList remove = new ArrayList();
            lock (_usersConnections.SyncRoot)
            {
                foreach (UserTelnetConnection usr in _usersConnections)
                    if (usr.UserID == login)
                    {
                        // ����������� �������� ������� ����������, �.�. � ��������� ����� 
                        // ������ ��� �� ����������, ����� OnClientDisconnect �������� ������������ ������������,
                        // ��� �������� � ���� ��� ������� ������� ������������ �� ����, � ��� ������� 
                        // ������������ ������ ������������.
                        usr.RemoveEventHandlers();
                        remove.Add(usr);
                    }

                foreach (UserTelnetConnection rem in remove)
                {
                    //Utils.Utils.WriteLog("�������������� ������: TlnSessionManager.ReconnectUser: " + rem.UserID + "�������: " + rem.UserConsole + "IP:" + rem.ClientIP);
                    //Utils.Utils.WriteLog("�������� ����������:" + _usersConnections.Count.ToString());
                    _usersConnections.Remove(rem);
                    //Utils.Utils.WriteLog("�������� ���������� ����� ��������:" + _usersConnections.Count.ToString());
                    rem.CloseClientConnection();
                }

                _usersConnections.Add(user);
            }

        }
        #endregion


        #region AddUser
        /// <summary>
        /// ��������� ������ ������������ �� ������
        /// </summary>
        /// <param name="user"></param>
        public static void AddUser(OheUser user)
        {
            lock (SyncObject)
                Proxy.AddUser(user);
        }
        #endregion


        #region MaxUsers
        /// <summary>
        /// ���������� �������� ���-�� ������������� � ������� ��������
        /// </summary>
        /// <returns></returns>
        public int MaxUsers()
        {
            return  Proxy.GetMaxTelnetUsers();
        }
        #endregion


        #region ModuleIsAvailable
        public bool ModuleIsAvailable()
        {
            return Proxy.ModuleIsAvailable(OheConstant.TELNET);
        }
        #endregion


        #region Stop
        /// <summary>
        /// ��������� ���� ������������� �� �������
        /// + ������� �������� �� �������
        /// </summary>
        public void Stop()
        {
            ArrayList remove = new ArrayList();
            lock (_usersConnections.SyncRoot)
                foreach (UserTelnetConnection connection in _usersConnections)
                    remove.Add(connection);


            foreach (UserTelnetConnection conn in remove)
            {
                conn.LogOff = true;
                Utils.Utils.WriteLog("TlnSessionManager.Stop() " + conn.UserID);
                OnKillSession(conn);
#if DEBUG
#else
                RemoveUser(conn.UserID);
#endif
            }
            _usersConnections = new ArrayList();
            StopMaintenance();
        }
        #endregion


        #region IsActiveUser - �������� ��� ������������ ��� ��������
        /// <summary>
        /// ��������� ������� �� ������������
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static bool IsActiveUser(string login)
        {

            lock (_usersConnections.SyncRoot)
            {
                if (_usersConnections.Cast<UserTelnetConnection>().Any(connection => connection.UserID == login && connection.IsActive))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion


        #region StopMaintenance
        private void StopMaintenance()
        {
            _isContinue = false;
#if DEBUG
#else
            try
            {
                if (_proxy != null)
                {
                    _proxy.RemoveUsers -= TelnetServer.RemoteRemoveUser;
                    _proxy.Close();
                }
            }
            catch (TimeoutException timeProblem)
            {
                Utils.Utils.WriteWebLog("The service operation timed out. " + timeProblem.Message);
            }
            catch (FaultException unknownFault)
            {
                Utils.Utils.WriteWebLog("An unknown exception was received. " + unknownFault.Message);
            }
            catch (CommunicationException commProblem)
            {
                Utils.Utils.WriteWebLog("There was a communication problem. " + commProblem.Message + commProblem.StackTrace);
            }
            // �������� ������ - ������������� ����������
            catch (System.Exception exc)
            {
                Utils.Utils.WriteWebLog(exc.Message);
            }
#endif
        }
        #endregion

        
        #region Connect
        /// <summary>
        /// ��������� ����� ���������� � ������ 
        /// </summary>
        /// <param name="usr"></param>
        public void Connect(UserTelnetConnection usr)
        {
            lock (_usersConnections.SyncRoot)
                _usersConnections.Insert(0, usr);
        }       
        #endregion


        #region Disconnect
        /// <summary>
        /// ������� ������������� �� ������ ���������� �������������
        /// </summary>
        /// <param name="usr"></param>
        public void Disconnect(UserTelnetConnection usr)
        {
            try
            {
                lock (_usersConnections.SyncRoot)
                {
                    _usersConnections.Remove(usr);
                    // ������� ���������� � ��������
#if DEBUG
#else
                    RemoveUser(usr.UserID);
#endif

                }
            }
            catch (System.Exception e)
            {
                Utils.Utils.WriteLog("������ � ������ ������������ �������������:"
                                          + Environment.NewLine
                                          + e.Message);
            }
        }
        #endregion


        #region KillSession
        /// <summary>
        /// ��������� ������� ������ ������������� ���� ����� timeout
        /// </summary>
        [Obsolete("���� ��������� ���������� ���� ������������� �� ��������, ���� ������ ���������� � ������� ������")]
        private void KillSession()
        {
            ArrayList remove = new ArrayList();
            while (_isContinue)
            {
                // �������� �� ������� ����� ���� ����������� � �������
                lock (_usersConnections.SyncRoot)
                    foreach (UserTelnetConnection usr in _usersConnections)
                    {
                        // �������������� �������� ���� ������ �������� ����������
                        if (!usr.IsActive)
                            remove.Add(usr);

                        // ��������� timeout ��� ������
                        int d = (int)((TimeSpan)(DateTime.Now - usr.LastActivityDate)).TotalSeconds;

                       
                        // �������� ���������� ������������ �� �������
                        // �������������� �������� ��� �� ���������� ������������ ��� ������
                        if (d > _timeout && usr.IsConnected) //(!usr.IsConnected))
                        {
                            /*
                            int t = 0;
                            try
                            {
                                t = ExecuteSqlQuery.ExecuteScalar("select count(product) from t_binlocat (nolock) where binlabel='@" + usr.UserID + "'");
                            }
                            catch { }
                            */

                            //if (t == 0)
                            //{
                            // ������������ ����� �����, ���� ������ ��� �� ����������.
                            // ��� ���� ����� ��� �� ����� ������� �� ����� �����.

                            // ����� ������, ���� ��������� ������������ ��� �����, ����� �� ��� ������������ � ������� ������������.
                            // �.�. ���� �� ����� ��������� ������ ��������, �� �� ����� ������������ ������ � ����� ������������.
                            // �� ���� ������������ ��� ������ � ����� ������� �� ����������, ��� �� ���������.
                            // ���� ����� ���� �� ���������� � ���� ������ ���� �������� ���������� ������������ � ���������
                            // ������ � �����
                            //usr.LogOff = (t > 0 ? false : true);

                            usr.LogOff = true;

                            lock (remove.SyncRoot)
                                remove.Add(usr);
                            /*
                            }
                            else
                                usr.LastActivityDate = DateTime.Now;
                            */
                        }
                    }

                // ������� ������������� �� ������
                foreach (UserTelnetConnection r in remove)
                {
                    //Utils.Utils.WriteLog("TlnSessionManager.KillSession() " + r.UserID + " Console:" + r.UserConsole + "IP" + r.ClientIP);
                    OnKillSession(r);
#if DEBUG
#else
                    RemoveUser(r.UserID);
#endif
                }

                // ������� �� ����������� ������
                lock (_usersConnections.SyncRoot)
                    foreach (UserTelnetConnection r in remove)
                        _usersConnections.Remove(r);

                remove = new ArrayList();
                Thread.Sleep(10000);
            }
        }
        
        #endregion


        #region TryToReconnect
        public UserTelnetConnection TryToReconnect(string user)
        {
            lock (_usersConnections.SyncRoot)
            {
                foreach (UserTelnetConnection usersConnection in _usersConnections)
                {
                    if (usersConnection.MacAddress == user && (usersConnection.IsActive || usersConnection.OheConnectionType == OheConnectionType.Web))
                    {
                        Utils.Utils.WriteLog("�������������� ������: TlnSessionManager.TryToReconnect: " + user + "�������: " + usersConnection.UserConsole + "IP:" + usersConnection.ClientIP);
                        return usersConnection;
                    }
                }
                return null;
            }
        }
        #endregion

    }
}
