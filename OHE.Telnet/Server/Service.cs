//  Copyright (c) Microsoft Corporation. All rights reserved.

using System;
using System.Threading;
using System.Collections.Specialized;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using OpenSourceHHWebClient.Controls;
using Ohe.Telnet.Configuration;
using Ohe.Utils;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Ohe.Telnet.Web;
using System.Text;

namespace Microsoft.WebProgrammingModel.Samples
{
    [ServiceContract()]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,//PerSession, 
    ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    /*
       � �������� ��������.
     * �������� ������: ��� ���������� ������� ����� � ������������� html ����
     * ��� ��������� ���������� �������� ������ �������
     */
    class OheWebTelnetService
    {
        private static string _webRootPath = Utils.assemblyPath + "Web/";
        private static string WebRootPath
        {
            get
            {
                return _webRootPath;
            }
        }

        [OperationContract]
        [WebGet(UriTemplate = "webterm/")]
        public Message WebtermSlash()
        {
            return Webterm();
        }

        [OperationContract]
        [WebGet(UriTemplate = "webterm")]
        private Message Webterm()
        {
            string message = SendRequest("localhost", TlnConfigManager.WebPort, string.Empty);

            return StreamMessageHelper.CreateMessage(
                MessageVersion.None,
                string.Empty,
                "text/html",
                delegate(Stream output)
                {
                    TextWriter writer = new StreamWriter(output);
                    writer.WriteLine(message);
                    writer.Flush();
                }
            );
        }

        private string SendRequest(string server, int port, string data)
        {
            try
            {
                // set up IP address of server
                IPAddress ipAddress = null;
                IPHostEntry ipHostInfo = Dns.GetHostEntry(server);
                for (int i = 0; i < ipHostInfo.AddressList.Length; ++i)
                {
                    if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[i];
                        break;
                    }
                }
                if (ipAddress == null)
                    throw new Exception("Unable to find an IPv4 address for server");

                TcpClient client = new TcpClient();
                client.Connect(ipAddress, port); // connect to the server

                NetworkStream networkStream = client.GetStream();
                StreamWriter writer = new StreamWriter(networkStream);
                StreamReader reader = new StreamReader(networkStream);

                writer.WriteLine(data);
                writer.Flush();

                string response = reader.ReadLine();
                if (response == null)
                {
                    // ����� ������ ��������, ����� � ������ ��� ���������...
                    // ���� ����� � ����, ��� ������� ��������� �����
                    response = "WARNING: Empty response";
                }

                client.Close();

                return response;

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        } // SendRequest


        [OperationContract]
        [WebGet(UriTemplate = "css/{fileName}")]
        public Message GetCss(string fileName)
        {
            return CreateTextAssetMessage("css", fileName);
        }

        [OperationContract]
        [WebGet(UriTemplate = "js/{fileName}")]
        public Message GetJS(string fileName)
        {
            return CreateTextAssetMessage("js", fileName);
        }

        [OperationContract]
        [WebGet(UriTemplate = "css/images/{baseName}.{type}")]
        public Message GetCssImage(string baseName, string type)
        {
            var filePath = string.Format(WebRootPath + "css/images/{0}.{1}", baseName, type);

            return CreateImageAssetMessage(type, filePath);
        }

        [OperationContract]
        [WebGet(UriTemplate = "css/images/icons-{type}/{fileName}")]
        public Message GetCssIcon(string type, string fileName)
        {
            var filePath = string.Format(WebRootPath + "css/images/icons-{0}/{1}", type, fileName);

            return CreateImageAssetMessage(type, filePath);
        }

        private Message CreateImageAssetMessage(string type, string filePath)
        {
            if (!File.Exists(filePath))
            {
                return Create404Message();
            }

            string contentType = type == "svg" ? "image/svg+xml" : "image/" + type;

            return StreamMessageHelper.CreateMessage(
                MessageVersion.None,
                string.Empty,
                contentType,
                delegate(Stream output)
                {
                    BinaryWriter writer = new BinaryWriter(output);
                    writer.Write(File.ReadAllBytes(filePath));
                    writer.Flush();
                }
            );
        }

        private Message CreateTextAssetMessage(string assetType, string path)
        {
            string filePath = string.Empty;
            switch (assetType)
            {
                case "css":
                case "js":
                    filePath = WebRootPath + assetType + Path.DirectorySeparatorChar + path;
                    break;
            }

            if (filePath == string.Empty || !File.Exists(filePath))
            {
                return Create404Message();
            }

            string contents = File.ReadAllText(filePath);
            string contentType = "text/" + assetType;

            return StreamMessageHelper.CreateMessage(
                MessageVersion.None,
                string.Empty,
                contentType,
                delegate(Stream output)
                {
                    TextWriter writer = new StreamWriter(output);
                    writer.WriteLine(contents);
                    writer.Flush();
                }
            );
        }

        private Message Create404Message()
        {
            const string HtmlContentType = "text/html";
            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
            WebOperationContext.Current.OutgoingResponse.ContentType = HtmlContentType;
            return StreamMessageHelper.CreateMessage(
                MessageVersion.None,
                string.Empty,
                HtmlContentType,
                delegate(Stream output)
                {
                    TextWriter writer = new StreamWriter(output);
                    //TODO: use HtmlTemplater to insert current url into 404.html template
                    writer.Write(File.ReadAllText(WebRootPath + "404.html"));
                    writer.Flush();
                }
            );
        }

        [OperationContract]
        [WebInvoke(UriTemplate = "SendData/")]
        public Message SendDataSlash(NameValueCollection postData)
        {
            return SendData(postData);
        }

        [OperationContract]
        [WebInvoke(UriTemplate = "SendData")]
        public Message SendData(NameValueCollection postData)
        {
            string userData = postData.Count > 0 ? postData[0] : string.Empty;
            string response = SendRequest("localhost", TlnConfigManager.WebPort, userData);

            return StreamMessageHelper.CreateMessage(
                MessageVersion.None,
                string.Empty,
                "text/html",
                delegate(Stream output)
                {
                    TextWriter writer = new StreamWriter(output);
                    writer.WriteLine(response);
                    writer.Flush();
                }
            );
        }
    }
}
