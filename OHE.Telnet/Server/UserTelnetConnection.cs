//#define DEBUG
//#undef DEBUG
// ��� ����������������� ���������, ���������� ��� ���������� �
// ���������� ��� ������ ����������.
// ���� ���������� ������ ������ ����� remoting
//#define RBCOMUNICATION
//#define REMOTINGVERSION

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Ohe.Telnet.Configuration;
using Ohe.Telnet.Controls;
using Ohe.Telnet.Core;
using Ohe.Telnet.Exception;
using Ohe.Utils;
using Ohe.Telnet.Web;

#if RBCOMUNICATION
using Ohe.Telnet.RbTelnetServer;
#endif
namespace Ohe.Telnet.Server
{
    /// <summary>
    /// ������ �� ������������� ������������� � ���������� ������ �������
    /// � �������� �� ������� ������
    /// ��������� 20.05.2009
    /// �������� �������������� ����� ������� ����������� ������������� TelnetUser
    /// </summary>
	public sealed class UserTelnetConnection: IDisposable
	{
		#region public events

        public delegate void ClientCloseDelegate(UserTelnetConnection sender);
	    public event ClientCloseDelegate OnClientClose;

        public delegate void ClientCloseRemoteDelegate(string user, IPAddress ip);
        public event ClientCloseRemoteDelegate OnClientRemoteClose;

		#endregion


        #region Private Variables

        private const int ReadBufferSize = 2048;
        private Socket _client = null;
        private OheUser _telnetUser;
        private string _login=string.Empty; // ����������� ��� ���������� ������������ �������������
        private string _password = string.Empty; // �������� ������ ������������
        //private bool _IsConnected;   // ���������� �������� ����������� �� ��� ���
        private UserMenu _userMenu;
        private readonly UserMenu _usersmenu; // ������ ���� ���� � �������
        private readonly TlnConfigManager _config; // ����� ����������� ������������ �� xml �����
        private readonly IPAddress _clientIp;
        private readonly int _clientPort;
        private readonly string _clientId;
        private StringBuilder _extraData = new StringBuilder();
        private readonly byte[] _readBuffer = new byte[ReadBufferSize];
        // ������� ������������ ��� �����������
        private RfForm _loginForm;
        Dictionary<string, object> _sessionValues;
        private readonly TelnetUserConsole _userConsole;// _userConsole = new TelnetUserConsole();

        private readonly string _clientEncoding;
        private readonly string _serverEncoding;
        public readonly string MacAddress;

        private OheConnectionType _connectionType = OheConnectionType.HandHeld;

#if RBCOMUNICATION
        //private RbWebConnectionClient _rfBaseSoapClient;
#endif

        #endregion


        #region Public Varibles

        public OheTelnetScreenManager Logic; // �������� � �������

        public bool LogOff = false; // ���������� ���������� ��������

        #endregion


		#region Public Properties

        #region ClientIP
        /// <summary>
        /// IP ����� �������
        /// </summary>
        public IPAddress ClientIP { get { return _clientIp; } }
        
        #endregion


        #region UserID
        /// <summary>
        /// ����� ������������
        /// </summary>
        public string UserID
        {
            get
            {
                if (_telnetUser != null)
                    return _telnetUser.UserId;
                return string.Empty;
            }
        }
        
        #endregion


        #region ClientPort
        /// <summary>
        /// ���� �������
        /// </summary>
        public int ClientPort { get { return _clientPort; } }

        #endregion


        #region ClientId
        /// <summary>
        /// ID �������
        /// </summary>
        public string ClientId { get { return _clientId; } }

        #endregion


        #region IsConnected
        /// <summary>
        /// ��������� �� ������ ��� ��� (bool)
        /// </summary>
        //public bool IsConnected { get { return logic.IsConnected _IsConnected; } set { _IsConnected = value; } }
        public bool IsConnected { get { return Logic.IsConnected; } }

        #endregion


        #region UserConsole
        /// <summary>
        /// ���������� ������� ����� ������ ���������
        /// </summary>
        public string UserConsole 
        { 
            get 
            {
                if (OheConnectionType == OheConnectionType.Web)
                    return _userWebConsole;
                else
                    return _userConsole.UserConsole; 
            } 
        }

        private string _userWebConsole = string.Empty;

        #endregion


        #region LastActivityDate
        /// <summary>
        /// ��������� ����� ����������� � �������
        /// </summary>
        public DateTime LastActivityDate
        {
            get
            {
                if (_telnetUser != null)
                    return _telnetUser.LastActivityDate;

                return DateTime.Now;

            }
            set
            {
                if (_telnetUser != null)
                    _telnetUser.LastActivityDate = value;
            }
        }

        #endregion


        #region ConnectedDate
        public DateTime ConnectedDate
        {
            get
            {
                if (_telnetUser != null)
                    return _telnetUser.ConnectedDate;

                return DateTime.Now;

            }
            set
            {
                if (_telnetUser != null)
                    _telnetUser.ConnectedDate = value;
            }
        }

        #endregion


        #region IsActive
        /// <summary>
        /// ������� �� ������ ��� ��� �������� ����������
        /// </summary>
        public bool IsActive
        {
            get
            {
                return (_client == null ? false: true);
            }
        }
        #endregion


        #region OheConnectionType
        /// <summary>
        /// ��� ���������� ������������
        /// </summary>
        public OheConnectionType OheConnectionType { get { return _connectionType; } set { _connectionType = value; } }
        #endregion

        /// <summary>
        /// �������� ������������ ��� ������������� ������ � ��������������� ���������� ��� ���-��������
        /// </summary>
        public object WebSync { get { return _client; } }


        #endregion


        #region GetMacAddress
        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        public static extern int SendARP(int DestIP, int SrcIP, byte[] pMacAddr, ref uint PhyAddrLen);

        private string GetMacAddress(IPAddress ipAddr)
        {
            try
            {
                var macAddr = new byte[6];
                var macAddrLen = (uint)macAddr.Length;

                if (SendARP((int)ipAddr.Address, 0, macAddr, ref macAddrLen) != 0)
                    return ipAddr.Address.ToString();

                var str = new string[(int)macAddrLen];
                for (var i = 0; i < macAddrLen; i++)
                    str[i] = macAddr[i].ToString("x2");
                if(str.Length > 0)
                    return string.Join("", str);
                else
                {
                    return ipAddr.Address.ToString();
                }
            }
            catch (System.Exception)
            {
                return ipAddr.Address.ToString();
            }
        }
        #endregion


        #region Constructor  / Dispose

        #region Constructor
        /// <summary>
        /// ����������� 
        /// </summary>
        /// <param name="client"></param>
        public UserTelnetConnection(Socket client, OheConnectionType connectionType = OheConnectionType.HandHeld)
        {
            this._connectionType = connectionType;

            _client = client;

            _clientIp = ((IPEndPoint)client.RemoteEndPoint).Address;
            _clientPort = ((IPEndPoint)client.RemoteEndPoint).Port;
            _clientId = _clientIp + ":" + _clientPort.ToString();
            
            MacAddress = GetMacAddress(_clientIp);

            _clientEncoding = TlnConfigManager.ClientEncoding;
            _serverEncoding = TlnConfigManager.ServerEncoding;

            _config = new TlnConfigManager();

            _userConsole = new TelnetUserConsole(TlnConfigManager.TELNET_HEIGHT, TlnConfigManager.TELNET_WIDTH);

            _usersmenu = new UserMenu(_config.GetMenuList, TlnConfigManager.MenuLinesCount);
            _usersmenu.OheConnectionType = _connectionType;

            Logic = new OheTelnetScreenManager(_connectionType);

#if RBCOMUNICATION
            //_rfBaseSoapClient = new RbWebConnectionClient("http://localhost" +  + ":" + RbDbConfig.GetValue("rfbase_soapport"));
            //_rfBaseSoapClient = new RbWebConnectionClient("http://localhost"  + ":8080");
#endif

            #region set socket options
            /*
		    int timeout = 30;
            if (timeout > 0)
		    {
			    _client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, timeout);
			    _client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, timeout);
		    }*/

            _client.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, 1);
            _client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, 1);
            #endregion

            // ��������� ���������� ������ � ����������� ������
            _client.BeginReceive(_readBuffer, 0, ReadBufferSize, 0, new AsyncCallback(StreamReceiver), null);
        }
        
        #endregion


        #region Dispose
        /// <summary>
        /// ������� �����
        /// </summary>
        public void Dispose()
        {
            if (OnClientClose != null)
            {
                string inform = string.Empty;
                try
                {
                    inform = "ID =" + this.UserID + ", Console= " + this.UserConsole + ",IP=" + this.ClientIP;
                }
                catch{}
                Utils.Utils.WriteLog("UserTelnetConnection.Dispose():" + inform);
                OnClientClose(this);
            }
            _client = null;

            Logic = null;

        }
        
        #endregion		

        #endregion


        #region Public Methods

        #region CloseClientConnection - ���������� ����� ������ ��������������
        public void CloseClientConnection()
        {
            if (_client != null)
            {
                _client.Shutdown(SocketShutdown.Both);
                _client.Close();
                _client = null;
            }
        }
        #endregion


        #region ForceReceiveString
        public void ForceReceiveString(string data) { OnReceiveString(data); }
        #endregion


        #region SendData - ��������� ������ �������
        public void SendData(string data)
        {
            // ����������� ������ � ������������ � �����������
            _userConsole.DispatchString(data);

            // ������������ ��� web ����������
            _userWebConsole = data.Replace(TlnConfigManager.BEEP, "")
                                  .Replace(OheConstant.TELNET_CLEAR_SEQ,"")
                                  .Replace(OheConstant.TELNET_ESC_SEQ,"");
            try
            {
                if (_client != null)
                {
                    if (OheConnectionType == Utils.OheConnectionType.Web)
                    {
                        var templater = new HtmlTemplater();
                        var utf8 = Encoding.UTF8;

                        templater.Variables.Add("content", data);
                        templater.Variables.Add("charset", utf8.WebName);
                        var html = templater.Process("main");
                        _client.Send(utf8.GetBytes(html));
                    }
                    else
                    {
                        _client.Send(Encoding.GetEncoding(_clientEncoding).GetBytes(data.Replace("�", "��")));
                    }
                    
                }
            }
            catch
            {
                if (OnClientClose != null)
                {
                    string inform = string.Empty;
                    try
                    {
                        inform = "ID =" + this.UserID + ", Console= " + this.UserConsole + ",IP=" + this.ClientIP;
                    }
                    catch { }

                    Utils.Utils.WriteLog("UserTelnetConnection.SendData():" + inform);

                    OnClientClose(this);
                }
            }
        }
        #endregion
        
        #endregion


        #region private Methods

	    #region private StreamReceiver - ���������� �������� � ����������� ������
        // � ���� �� ������� ������
	    private void StreamReceiver(IAsyncResult ar)
        {
            lock (WebSync) // �� telnet ������� �� ������ ��������, ��� ��� ��� ��� ������ �� �����������
                           // � ��� ��� �������� ����, ���� ���������� �� �������� ������ �������������� ��������
            {
		        int bytesRead = 0;
		        string message;

		        try
		        {
                    if (_client != null)
                    {
                        bytesRead = _client.EndReceive(ar);
                        if(_telnetUser != null)
                            _telnetUser.LastActivityDate = DateTime.Now;
                    }

			        if (bytesRead < 1 || (bytesRead == 1 && _readBuffer[0] == 0))
			        {
                        // �� ������� ������ �� ������ �������� ���
                        if (OnClientClose != null)
                        {
                            /*
                            string inform = string.Empty;
                            try
                            {
                                inform = "ID =" + this.UserID + ", Console= " + this.UserConsole + ",IP=" + this.ClientIP;
                            }
                            catch { }
                            //Utils.Utils.WriteLog("UserTelnetConnection.StreamReceiver():" + inform);
                            */
                            OnClientClose(this);
                        }
				        return;
			        }

                    message = Encoding.GetEncoding(_serverEncoding).GetString(_readBuffer, 0, bytesRead);

                    /* ����������� ������ 28 MC-9190G*/
                    if(message.Length == 3)
                    {
                        if (message.Substring(0, 1).ToCharArray()[0] == (char)27
                            && message.Substring(1, 1) == "[")
                        {
                            switch (message.Substring(2, 1))
                            {
                                case "D":
                                    message = "~F2\r\n";
                                    break;
                                case "A":
                                    message = "~F3\r\n";
                                    break;
                                case "B":
                                    message = "~F4\r\n";
                                    break;
                                case "C":
                                    message = "~F5\r\n";
                                    break;
                            }
                        }
                
                    }


                    if (message.Length > 0)
                    {
                        // ���� �� ������ ������� ������� �� ����������� ���������
                        if (message.IndexOf("\r") == -1
                            && message.IndexOf("\n") == -1
                            && message.IndexOf((char)27) == -1)
                        {
                            // ��������� � Message
                            _extraData.Append(message);
                        }
                        else
                        {
                            // ����� ������ ��������� � ���������
                            _extraData.Append(message);
                            // �������� ������� ��������� ���������
                            OnReceiveString(ParseTelnetString());
                            // �������� ���������
                            _extraData = new StringBuilder();
                        }
                    }
                    // �������� ���������� ��������� ���� ������ ���� ������
                    if (_client != null)
                    {
                        _client.BeginReceive(_readBuffer, 0, ReadBufferSize, SocketFlags.None, new AsyncCallback(StreamReceiver), null);
                    }

                }
                catch(System.Exception exc)
                {
                    if (OnClientClose != null)
                    {
                        /*
                        string inform = string.Empty;
                        try
                        {
                            inform = "ID =" + this.UserID + ", Console= " + this.UserConsole + ",IP=" + this.ClientIP;
                        }
                        catch { }

                        Utils.Utils.WriteLog("UserTelnetConnection.StreamReceiver():" + inform + exc.Message);
                        */
                        OnClientClose(this);
                    }
		        }
	        }
        }
	    #endregion


	    #region private ParseTelnetString - ������������ �������������� ������� ���������
        // ������������� ��� �������� ������ ������� ������ �� �������
	    private string ParseTelnetString()
	    {
		    string receivedString = _extraData.ToString();

		    StringBuilder rez = new StringBuilder();

		    char charBuf;

		    for (int i=0; i < receivedString.Length; i++)
		    {
			    if (receivedString[i] == 27 && receivedString.Length>1 && (receivedString.Length-1)< i)
			    {
				    if (receivedString[i+1] == 79)
				    {
					    if (receivedString[i+2] < 80)
						    charBuf = '5';		// F5
					    else
						    charBuf = (char)(receivedString[i+2]-31);			// F1,F2,F3,F4
					    rez.Append('f');
					    i += 2;	
				    }
				    else if (receivedString[i+1]==91 && receivedString[i+2]==49 && receivedString[i+4]==126)
				    {
					    if (receivedString[i+3] == 53)
						    charBuf = '5';		// F5
					    else
						    charBuf = (char)(receivedString[i+3]-1);	// F6,F7,F8
					    rez.Append('f');
					    i += 4;	
				    }	
				    else if (receivedString[i+1]==91 && receivedString[i+2]==50 && receivedString[i+3]==48 && receivedString[i+4]==126)
				    {
					    charBuf = '9';		// F9
					    rez.Append('f');
					    i += 4;	
				    }	
				    else if (receivedString[i+1]==91 && receivedString[i+2]==50 && receivedString[i+3]==49 && receivedString[i+4]==126)
				    {
					    charBuf = '0';		// F0 == F10
					    rez.Append('f');
					    i += 4;	
				    }	
				    else
					    charBuf = receivedString[i];
			    }
			    else if (receivedString[i] > 96 && receivedString[i] < 123)
				    charBuf = (char)(receivedString[i]-32);		// convert in UPPER case
			    else if (receivedString[i] == 13)
			    {
				    if (i == receivedString.Length-1 || i == receivedString.Length-2)
					    continue;
				    else
					    charBuf = ',';
			    }
			    else if (receivedString[i] == 10)
				    continue;
			    else
				    charBuf = receivedString[i];

			    if (charBuf == 8 && rez.Length > 0)
				    rez = new StringBuilder(rez.ToString().Substring(0, rez.Length-1));
			    else
				    rez.Append(charBuf);
		    }

		    return rez.ToString();
	    }
	    #endregion


        #region private OnReceiveString -- ��������� ������� ���������� ���������
        // �������� ��������� ��������� ������� �������� �� ������� � �������� ����������
        // ��������� ��������������� ������� ������ ������������ ���������� �����
        // � ���������� ��������� �������
        /// <summary>
        /// ������������ ��������� ������ �� ������������
        /// </summary>
        /// <param name="login"></param>
	    private void OnReceiveString(string userData)
	    {
		    try
		    {
                userData = Utils.Utils.Str2TlnServerString(userData);
                string response = string.Empty;
#if RBCOMUNICATION
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(_rfBaseSoapClient.RequestHandhelds(_clientId, data, "1", "1"));
                //response = doc["message"].InnerText;

                //SendData(response);

                //return;
#endif                
                try
                {
                    #region ����� ��� ���������� � ���������� �����
                    if (Logic.IsConnected)
                    {
                        Logic.InsertString(userData);
                        response = Logic.ReturnString();
                    }
                    #endregion

                    // ������������ ���� ESC � �������� �� �� ��� ������������ 
                    // ������������
                    if (!Logic.IsConnected)
                    {
                        if (_login != string.Empty && userData.IndexOf((char)27) >= 0)
                        {
                            #region ��������� ������ ���� ������������ ��������������� ���
#if DEBUG
#else
                            LogOff = true;
                            TelnetSessionManager.RemoveUser(_login);
#endif
                            _login = string.Empty;
                            _loginForm = _config.GetLoginScreen();
                            _loginForm.SetCurrentEvent(string.Empty, false, ref _sessionValues);

                            // �������� ������� �������� ������ ������������
                            _loginForm.CurrentEvent = Events.OnExit;

                            _loginForm.SetCurrentEvent("", false, ref _sessionValues);

                            // ������� ��� ���������� � ������ ������������
                            _sessionValues = new Dictionary<string, object>();
                            SetSystemParams(ref _sessionValues);

                            // ������� ����� � �������� ���������
                            _loginForm.ClearForm();
                            _loginForm.SetCurrentEvent(string.Empty, false, ref _sessionValues);
                            response = _loginForm.ToString(true, _sessionValues, _connectionType).Replace("<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='x'>", "");

                            _telnetUser.UserId = string.Empty;

                            SendData(response);
                            return;
                            #endregion
                        }
                    }

                    #region ����������� �������������
                    // ������������������ �������� �������� �����������
                    if (!Logic.IsConnected)
                    {
                        // ������������ ���� ESC � �������� �� �� ��� ������������ 
                        // ������������
                        if (userData.IndexOf((char)27) >= 0 && _login == string.Empty)
                        {
                            LogOff = true;

                            /*
                            string inform = string.Empty;
                            try
                            {
                                inform = "ID =" + this.UserID + ", Console= " + this.UserConsole + ",IP=" + this.ClientIP;
                            }
                            catch { }

                            Utils.Utils.WriteLog("UserTelnetConnection.OnReceiveString():" + inform);
                            */


                            if(OnClientClose != null)
                                OnClientClose(this);
                            return;
                        }

                        // �������� ��� ������� ����� ���� � web (�������� <WEB>)
                        // ���������� ��� ���� �������������� ������
                        if (this.OheConnectionType == OheConnectionType.Web && _loginForm == null)
                        {
                            //Utils.Utils.WriteLog("����� ���������: ��������� ����� " + data);
                            // ����� ��� ������ �� ������������ ������� ������������� �
                            //submit �����, ��� ��� ��� �� ��������� ������ � ���������� ���
                            _loginForm = _config.GetLoginScreen();
                            _loginForm.SetCurrentEvent(userData, false, ref _sessionValues);

                            _sessionValues = new Dictionary<string, object>();
                            SetSystemParams(ref _sessionValues);

                            response = _loginForm.ToString(true, _sessionValues, _connectionType).Replace("<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='x'>", "");

                            SendData(response);

                            return;
                        }
                        // �������� ������������� ������
                        try
                        {
                            if (!string.IsNullOrEmpty(userData) &&
                                File.Exists(Utils.Utils.assemblyPath + @"\Session\" + userData + ".user"))
                            {
                                #region �������������� ������
                                using (var stream = File.OpenRead(Utils.Utils.assemblyPath + @"Session\" + userData + ".user"))
                                {
                                    var writer = new BinaryFormatter();
                                    Logic = (OheTelnetScreenManager) writer.Deserialize(stream);
                                    // �������������� ��� ����������� ���� ������������ ��������
                                    // �� � web
                                    Logic.OheConnectionType = OheConnectionType;
                                }
                                // ������� ���� � ������� �������
                                File.Delete(Utils.Utils.assemblyPath + @"Session\" + userData + ".user");

                                //Utils.Utils.WriteLog("����� ���������: �������������� ������ " + logic.IsConnected.ToString());

                                // ���� ������������ �� ���������, �� ������� ��� ������,
                                // �������� ��������� ��������
                                if (!Logic.IsConnected)
                                {
                                    Logic = new OheTelnetScreenManager(OheConnectionType);
                                    _loginForm = _config.GetLoginScreen();
                                    _sessionValues = new Dictionary<string, object>();

                                    SetSystemParams(ref _sessionValues);
                                    OnReceiveString(userData);
                                    return;
                                }

                                // ������� ��������������� ������
                                Logic.Config = _config;
                                var oheUserMenu = new UserMenu(_usersmenu.PermissHeadMenu(Logic.Permiss),
                                                                     TlnConfigManager.MenuLinesCount, userData)
                                {
                                    OheConnectionType = _connectionType
                                };

                                Logic.Menu = oheUserMenu;


                                Logic.Forms = _config.GetForms;
                                _sessionValues = Logic.ProcessValues;

                                SetSystemParams(ref _sessionValues);

                                if (!Logic.TrySetCurrentFormById(ref _sessionValues))
                                {
                                    Logic.IsHeadMenu = true;
                                    Logic.IsConnected = true;
                                }
                                else
                                {
                                    // ������ ������� ������ ������ ���� ������������ ��������� �� ������
                                    Logic.CurrentFormStatus = OheTelnetScreenManager.OheFormStatus.IsRestoreSession;    
                                }

                                _login = userData;
                                
                                _telnetUser = new OheUser(_login, Logic.Permiss, ClientIP.ToString())
                                {
                                    ConnectionType = OheConnectionType.HandHeld,
                                    SessionID = ClientId,
#if DEBUG
                                    ModuleId = 123456
#else
                                    ModuleId = TelnetSessionManager.ModuleID
#endif
                                };

                                

                                SendData((_connectionType == OheConnectionType.Web
                                              ? OheConstant.TELNET_RESTORE_SESSION_MESSAGE_WEBCONNETION
                                              : OheConstant.TELNET_RESTORE_SESSION_MESSAGE + TlnConfigManager.BEEP));

#if DEBUG
#else
                                    TelnetSessionManager.AddUser(_telnetUser);
#endif
                                return;
                                #endregion
                            }
                        }
                        catch (System.Exception exc)
                        {
                            Utils.Utils.WriteLog("������ �������������� ������: " + exc.Message + Environment.NewLine + exc.InnerException);
                        }

                        // �������� ��� ������������ �������.
                        if (!string.IsNullOrEmpty(userData) && TelnetSessionManager.IsActiveUser(userData))
                        {
                            /*
                            UserTelnetConnection usr = TelnetSessionManager.TryToReconnect(MacAddress);
                           // throw new LogicException("������������ ��������");
                            if (usr != null)
                            {
                                if (Deserialize(usr.Logic, login))
                                {
                                    SendData((_connectionType == OheConnectionType.Web
                                                  ? OheConstant.TELNET_RESTORE_SESSION_MESSAGE_WEBCONNETION
                                                  : OheConstant.TELNET_RESTORE_SESSION_MESSAGE));
                                    return;
                                }
                                else
                                {
                                    OnReceiveString(login);
                                    return;
                                }
                            }
                            else
                            */
                                throw new LogicException("������������ ��������");
                        }

                        //TelnetServer.RemoteRemoveUser(login);

                        // ������������ �������� �� �����
                        _loginForm.SetCurrentEvent(userData, false, ref _sessionValues);

                        // �������� �� �� ��� ������� � ����� ��������
                        if (_loginForm.CurrentState == FormState.InValid
                                && _loginForm.CurrentEvent == Events.OnInit)
                        {

                            response = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ : "")
                                  + _loginForm.Message.ToString(_connectionType);

                            SendData(_connectionType == OheConnectionType.HandHeld
                                         ? response
                                         : _loginForm.ToString(_sessionValues, _connectionType)
                                                     .Replace(
                                                         "<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='x'>",
                                                         ""));
                            return;
                        }

                        response = _loginForm.ToString(true, _sessionValues, _connectionType).Replace("<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='x'>", "");

                        // � ��� ��������� ������� ���� ���������� �� ������ �����
                        if (_loginForm.CurrentEvent == Events.OnEnd && !_loginForm.HasActiveControls())
                        {
                            DataTable dt = (DataTable)_sessionValues["SYS_VALUE"];

                            // ������� ���� ������������ ������������ �� ����������
                            string permiss = dt.Rows[0].ItemArray[dt.Columns["SYS_PERMISS"].Ordinal].ToString();

                            _login = dt.Rows[0].ItemArray[dt.Columns["SYS_ID"].Ordinal].ToString();

                            _userMenu = new UserMenu(_usersmenu.PermissHeadMenu(permiss), TlnConfigManager.MenuLinesCount, _login);
                            _userMenu.OheConnectionType = _connectionType;
                            
                            // �������� ��� ������������ �������.
                            if (TelnetSessionManager.IsActiveUser(_login))
                            {
                                _loginForm = null;
                                _login = string.Empty;
                                throw new LogicException("������������ ��������");
                            }


                            // ������� ����� ����������� ������������
                            #region ��������� 20.05.2009
                            _telnetUser = new OheUser(_login, permiss, ClientIP.ToString())
                            {
                                SessionID = ClientId,
                                ConnectionType = OheConnectionType.HandHeld,
#if DEBUG
#else
                                ModuleId = TelnetSessionManager.ModuleID
#endif
                            };

                            string[] rrr = new string[5] { _telnetUser.SessionID, _telnetUser.UserId, _telnetUser.Password, _telnetUser.Permission, _telnetUser.IpAddress };

#if DEBUG
#else
                            //TelnetServer.AddUser(_telnetUser);
                            TelnetSessionManager.AddUser(_telnetUser);

                            #region CommentCode
                            // OheUserLicenseClient _client = new OheUserLicenseClient();
                            // _client.AddTelnetUser(_rrr);

                            /*
                            EndpointAddress address = new EndpointAddress("net.tcp://192.168.1.14:5500/OheEventManager");
                            //EndpointAddress address = new EndpointAddress("net.tcp://localhost:5500/OheEventManager");
                            NetTcpBinding binding = new NetTcpBinding();
                            binding.Security.Mode = SecurityMode.None;

                            ChannelFactory<IOheUserLicense> factory = new ChannelFactory<IOheUserLicense>(binding, address);
                            //factory.Credentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Delegation;
                            //factory.Credentials.UserName.UserName = "test1";
                            //factory.Credentials.UserName.Password = "1tset";
                            IOheUserLicense channel = factory.CreateChannel();
                            
                            channel.AddTelnetUser(_rrr);

                            int _max = channel.GetMaxTelnetUsers();

                            
                            ((IChannel)channel).Close();
                            */
                            #endregion
#endif

                            #endregion

                            // ���������� ����� ����������� ��� �������� ������
                            Logic = new OheTelnetScreenManager(_userMenu, _config, _sessionValues, _login, _connectionType)
                            {
                                    IsHeadMenu = true,
                                    IsConnected = true,
                                    Permiss = permiss
                            };

                            OnReceiveString("");
                            return;
                        }
                    }

                    #endregion
                }
                catch (LogicException ex)
                {
                    response = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ + CenterString("������������")
                                + Environment.NewLine
                                + CenterString("��� ��������") + Environment.NewLine
                                + CenterString("����������� ������") + Environment.NewLine
                                + CenterString("����� ��� �����������") + Environment.NewLine
                                + OheConstant.SetCursorPosition(0, 0) : OheConstant.TELNET_ACTIVE_USER_MENU_EXCEPTON);
                    Utils.Utils.WriteLog("������� ����������� ��� �������� �������: " + userData);

                }
                catch (ConfigurationException ex)
                {
                    response = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ + CenterString("������:")
                                + Environment.NewLine
                                + CenterString("� ������������") + Environment.NewLine
                                + CenterString("���������� �") + Environment.NewLine
                                + CenterString("��������������") + Environment.NewLine
                                + OheConstant.SetCursorPosition(0, 0) : OheConstant.TELNET_CONFIGURATION_EXCEPTON_WEBCONNECTION);
                    ex.User = _login;
                    Utils.Utils.WriteLog(ex.ToString());
                }
                catch (System.Exception e)
                {
                    response = (_connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_SYSTEM_ECXEPTION : OheConstant.TELNET_SYSTEM_EXCEPTON_WEBCONNECTION);

                    Utils.Utils.WriteLog("������ ��� ������� �������: " + Environment.NewLine + e.Message
                                        + Environment.NewLine + e.StackTrace + e.InnerException.Message);
                }

                SendData(response);

            }
            catch (System.Exception e)
		    {
			    Utils.Utils.WriteLog("������ ������: ������ � ������: OnReceiveString method." +
                                            Environment.NewLine + e.Message
                                        + Environment.NewLine + e.StackTrace + e.InnerException.Message);
            }
	    }
	    #endregion


	    #region CenterString - ����������� ������ �� �����������
	    /// <summary>
	    /// ����������� ������ �� ������
	    /// </summary>
	    /// <param name="userInput">������</param>
	    /// <param name="width">������ ������</param>
	    /// <param name="heigth"> ������ ������</param>
	    /// <returns></returns>
	    private string CenterString(string str)
	    {
		    int width = TlnConfigManager.TELNET_WIDTH;
		    int heigth = TlnConfigManager.TELNET_HEIGHT;

		    if (str.Length > width || str.Length == 0)
			    return str;

		    int center = (int)Math.Round((decimal)width / 2, 0);
		    int width2 = (int)Math.Round((decimal)str.Length / 2, 0);

		    string rez = string.Empty;
		    for (int line = 1; line < center - width2; line++)
			    rez += " ";

		    rez += str + "                                      ".Substring(0, (width - (rez + str).Length > 0 ? width - (rez + str).Length : 1));

		    return rez;
	    }
	    #endregion


        #region Deserialize - �������������� ������������� �������
        /// <summary>
        /// ��������������� ������ ������������
        /// </summary>
        /// <param name="logic"></param>
        /// <param name="login"></param>
        /// <returns></returns>
        public bool Deserialize(OheTelnetScreenManager logic, string login)
        {
            // ���� ������������ �� ���������, �� ������� ��� ������,
            // �������� ��������� ��������
            if (!logic.IsConnected)
            {
                Logic = new OheTelnetScreenManager(OheConnectionType);
                _loginForm = _config.GetLoginScreen();
                _sessionValues = new Dictionary<string, object>();
                SetSystemParams(ref _sessionValues);
                return false;
            }

            Logic = logic.Clone();
            // ������� ��������������� ������
            Logic.Config = _config;
            var oheUserMenu = new UserMenu(_usersmenu.PermissHeadMenu(logic.Permiss),
                                                 TlnConfigManager.MenuLinesCount, login)
            {
                OheConnectionType = _connectionType
            };

            Logic.Menu = oheUserMenu;

            Logic.Forms = _config.GetForms;
            _sessionValues = logic.ProcessValues;
            SetSystemParams(ref _sessionValues);

            if (Logic.TrySetCurrentFormById(ref _sessionValues))
            {
                Logic.IsHeadMenu = false;
                Logic.IsConnected = true;
            }
            else
            {
                Logic.IsHeadMenu = true;
                Logic.IsConnected = true;
            }

            //RestoreTelnetUser(login, logic.Permiss);
            _login = login;

            return true;
        }
        #endregion 


        #region RestoreTelnetUser - ��������������� ��������� ������ ������������
        /// <summary>
        /// ��������������� �������� ������ ������������
        /// </summary>
        /// <param name="Login"></param>
        /// <param name="Permiss"></param>
        public void RestoreTelnetUser(string Login, string Permiss, OheConnectionType connType)
        {
            _telnetUser = new OheUser(Login, Permiss, ClientIP.ToString())
            {
                ConnectionType = connType,
                SessionID = ClientId
            };
        }
        #endregion 


        #region RemoveEventHandlers - ������� ����������� ���� �������
        /// <summary>
        /// ������� ��� ����������� ������� ��� �������� �������
        /// </summary>
        public void RemoveEventHandlers()
        {
            try
            {
                Delegate retDelegate = null;
                foreach (EventInfo ev in this.GetType().GetEvents())
                {
                    FieldInfo fi = this.GetType().GetField(ev.Name, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
                    object value = fi.GetValue(this);


                    if (value != null)
                    {
                        retDelegate = (Delegate)value;
                        if (retDelegate != null)
                            ev.RemoveEventHandler(this, retDelegate);
                    }
                }
            }
            catch (System.Exception exc)
            {
                Utils.Utils.WriteLog("RemoveEventHandlers: " + exc.Message);
            }
        }
        #endregion


        private void SetSystemParams(ref Dictionary<string, object> Cashe)
        {
            if (Cashe.ContainsKey("SYS_IP"))
                Cashe["SYS_IP"] = MacAddress;
            else
            {
                Cashe.Add("SYS_IP", MacAddress);
            }

            if (Cashe.ContainsKey("SYS_MAC"))
                Cashe["SYS_MAC"] = ClientIP.ToString();
            else
            {
                Cashe.Add("SYS_MAC", ClientIP.ToString());
            }

            //Utils.Utils.WriteLog(string.Format("ip:{0}, mac:{1}", ClientIP,MacAddress));

        }
        #endregion
    }

}


