﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using OpenSourceHHWebClient;
using OpenSourceHHWebClient.Controls;
using Ohe.Web.Configuration;
using Ohe.Utils;
using Ohe.Utils.WCF;

namespace Microsoft.WebProgrammingModel.Samples
{
    /// <summary>
    /// Управляет сессиями пользователей
    /// </summary>
    public sealed class TlnWebSessionManager
    {

        #region private values

        private static Hashtable _sessions = new Hashtable(new OheCultureComparer());
        private static readonly string _syncObject = Guid.NewGuid().ToString();

        // Декларирование объекта
        private static int _sessionTimeout = 0;
        private static bool _isContinue = true;
        private static Thread _thread = null;

        #endregion


        #region Public Properties

        #endregion


        #region static Constructor
        static TlnWebSessionManager()
        {
           // StartThread();
        }
        #endregion


        #region Private Methods

        #region StartThread - стартует поток отслеживания сессий
        /// <summary>
        /// Стартует поток на отслеживание сессий
        /// </summary>
        private static void StartThread()
        {
            _sessionTimeout = 100000;
            if (_sessionTimeout == 0)
                _sessionTimeout = 600000;

            _thread = new Thread(new ThreadStart(MaintenanceThread));
            _thread.Priority = ThreadPriority.Lowest;
            _thread.Name = "OheSessionManagement";
            _thread.Start();
        }

        #endregion


        #region MaintenanceThread
        /// <summary>
        /// Управляет активностью пользователей
        /// </summary>
        private static void MaintenanceThread()
        {
            while (_isContinue)
            {
                try
                {
                    //DateTime currentDate = DateTime.Now;

                    lock (_sessions.SyncRoot)
                    {
                        ArrayList tickets2remove = new ArrayList();

                        /*
                        foreach (string sessionTicket in _sessions.Keys)
                        {
                            if (currentDate.Subtract(((WebUserSession)_sessions[sessionTicket]).LastAccessTime).TotalMilliseconds > _sessionTimeout)
                            {
                                tickets2remove.Add(new string[] { sessionTicket, ((WebUserSession)_sessions[sessionTicket]).UserId });
                            }
                        }

                        foreach (string[] sessionTicketId in tickets2remove)
                        {
                            #if DEBUG
                            #else
                                                        // Сообщение пользователю об удалении 
                                                        Proxy.RemoveWebUser(sessionTicketId[1]);
                            #endif
                            lock (_syncObject)
                                _sessions.Remove(sessionTicketId[0]);
                        }
                        */
                    }
                }
                catch (Exception e)
                {
                    Utils.WriteWebLog("Модуль управления Web пользователями:" + Environment.NewLine + e.Message);
                }
                Thread.Sleep(10000);
            }
        }
        #endregion

        #endregion


        #region Public Methods


        #region StopMaintenance
        public static void StopMaintenance()
        {
            _isContinue = false;
        }
        #endregion


        #region ClearAllSessions
        /// <summary>
        /// Удаляет все сессии из приложения + посылает данные платформе
        /// </summary>
        public static void ClearAllSessions()
        {
            lock (_sessions.SyncRoot)
            {
                _sessions.Clear();
                // Останавливаем сервер
                StopMaintenance();
            }
        }
        #endregion


        #region RemoveUser
        /// <summary>
        /// Удаляет пользователя из активных, посылает данные платформе
        /// </summary>
        public static void RemoveUser(OheTerminalClient Session)
        {
            if (Session == null) return;

            try
            {
                //lock (_sessions.SyncRoot)
                lock (_syncObject)
                {
                    _sessions.Remove(Session.SessionId);
                }
            }
            catch { }
        }

        #endregion


        #region PutSessionTicket
        /// <summary>
        /// Устанавливает идентификатор сессии для пользователя
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="sessionTicket"></param>
        public static void PutSessionTicket(string sessionId, OheTerminalClient sessionTicket)
        {
            lock (_syncObject)
            {
                if (sessionTicket != null)
                    _sessions[Utils.AllTrim(sessionId)] = sessionTicket;
            }
        }
        #endregion


        #region GetSessionTicket
        /// <summary>
        /// Возвращает класс сесиий по идентификатору
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        //public static RbTerminalClient GetSessionTicket(string sessionId)
        public static OheTerminalClient GetSessionTicket(string sessionId)
        {
            lock (_syncObject)
            {
                OheTerminalClient sessionTicket = _sessions[Utils.AllTrim(sessionId)] as OheTerminalClient;

                return sessionTicket;
            }
        }
        #endregion

        #endregion
    }
}
