using System;
using System.Text;
using System.Text.RegularExpressions;
using Ohe.Utils;

// ������ ������ ���� ����� 
namespace Ohe.Telnet.Server
{
        /// <summary>
        /// ����� ����������� ��������� ������ ��� ������� � ������ ������
        /// ������� ������ �� �������
        /// </summary>
        public sealed class TelnetUserConsole
        {

            #region Properties
            // ���������� ��������� ������� (������ F1-9)
            public const int FKeysCount = 9;

            private readonly string[] _fKeys = new string[FKeysCount];
            public string[] FKeys { get { return _fKeys; } }

            #region UserConsole
            private char[][] _console;

            // ���������� ������(� ����� � ���) ������ �������� 20,8
            public string UserConsole
            {
                get
                {
                    StringBuilder rez = new StringBuilder();
                    lock (_console.SyncRoot)
                    {
                        for (int row = 0; row < _console.Length; row++)
                        {
                            for (int col = 0; col < _console[row].Length; col++)
                                rez.Append(_console[row][col]);
                            rez.Append(Environment.NewLine);
                        }
                    }
                    return rez.ToString().Replace(OheConstant.TELNET_CLEAR_SEQ,"");
                }
            }
            #endregion

            // ���������� ��������� ������
            private int _currentRow = 0;
            private int _currentColumn = 0;
            private int _consoleHeight = 8;
            private int _consoleWidth = 20;

            /// <summary>
            /// ����� ������ ���������
            /// </summary>
            public int Height { get { return _consoleHeight; } }

            /// <summary>
            /// ������ ������ ���������
            /// </summary>
            public int Width { get { return _consoleWidth; } }
    
            #endregion


            #region class RbTerminalCommand - ��������������� �����
            // ���������� ���������
            private class RbTerminalCommand
            {
                // ���������� ����������� ������� ������
                public enum RbTerminalCommandType
                {
                    NEWLINE = -3,
                    PRINT = -2,
                    ERROR_PARAMS = -1,
                    ERROR = 0,
                    ERASE_DISPLAY = 1,
                    SET_POS = 2,
                    BELL_ALERT = 4
                }

                public RbTerminalCommandType CommandType = RbTerminalCommandType.ERROR;
                public int ParametersCount;
                public int[] Parameters = new int[2];
                public string Param0 = string.Empty;
                public string Command = string.Empty;
            }
            #endregion


            #region Constructors - �����������
            public TelnetUserConsole()
                : this(8, 20)
            { }

            /// <summary>
            /// ����������� - ���������� ������� ������ ������ - _console
            /// � ������ � ���������� ��������� _fKeys
            /// </summary>
            /// <param name="height">�������������� ������</param>
            /// <param name="width">������������ ������</param>
            public TelnetUserConsole(int height, int width)
            {
                _consoleHeight = height;
                _consoleWidth = width;

                // �������� ������ � �������
                _console = new char[_consoleHeight][];
                for (int i = 0; i < _consoleHeight; i++)
                    _console[i] = new char[_consoleWidth];

                for (int i = 0; i < FKeysCount; i++)
                    _fKeys[i] = string.Empty;
            }
            #endregion


            #region DispatchString - ������������ ������ ��������������� �������
            public void DispatchString(string query)
            {
                StringBuilder commands = new StringBuilder();

                // ESC['mode'J - ED - Erase In display
                // 0 - �������� � �������� ������� �� ����� ������ 
                //Erase from the active position to the end of the screen, inclusive (default)
                // 1 - �������� �� ������ ������ �� �������� ������� 
                // Erase from start of the screen to the active position, inclusive
                // 2 - �������� ����� ������ ������ ��� ��������� Erase all of the display � all lines are erased, changed to single-width, and the cursor does not move.
                commands.Append("\\e\\[[0-9]+J");
                commands.Append("\\e\\[J");

                // ��������� ������ ��� ������� 
                // ESC['line';'column'H - CUP - Cursor Position
                // ESC[H - CUP - Cursor Position (move to home)
                commands.Append("|\\e\\[[0-9]+;[0-9]+H");
                //"\u001b2J\u001b1;0H";
                commands.Append("|\\e\\[H");

                // ������������ ������� �������
                // ESC['line';'column'f - HVP � Horizontal and Vertical Position
                // ESC[f - HVP � Horizontal and Vertical Position (move to home)
                commands.Append("|\\e\\[[0-9]+;[0-9]+f");
                commands.Append("|\\e\\[f");

                // \a - Bell (alert)
                commands.Append("|\a");

                // \r\n - end of line
                commands.Append("|\r\n");

                Regex commandsRegex = new Regex("(" + commands + ")");

                string[] splittedCommands = commandsRegex.Split(query);

                if (splittedCommands.Length > 0)
                    foreach (string commandStr in splittedCommands)
                        if (commandStr != null && commandStr.Length > 0)
                        {
                            // ����������� ��� ������ ������� ����� �� ������
                            RbTerminalCommand tcmd = new RbTerminalCommand();

                            if (commandStr == "\r\n")
                            {
                                tcmd.CommandType = RbTerminalCommand.RbTerminalCommandType.NEWLINE;
                                DispatchCommand(tcmd);
                                continue;
                            }

                            if (commandStr == "\a")
                            {
                                tcmd.CommandType = RbTerminalCommand.RbTerminalCommandType.BELL_ALERT;
                                DispatchCommand(tcmd);
                                continue;
                            }

                            if (commandStr[0] == '\u001b')
                            {
                                DispatchCommand(ParseCommand(commandStr));
                                continue;
                            }

                            tcmd.CommandType = RbTerminalCommand.RbTerminalCommandType.PRINT;
                            tcmd.ParametersCount = 1;
                            tcmd.Param0 = commandStr;
                            DispatchCommand(tcmd);
                        }

                /*String finstr = this.Text;
                int tsel = this.SelectionStart;
                if (finstr.Length > 1)
                    this.Text = finstr.Remove(finstr.Length-2, 2);
                this.Select(tsel,0);*/
            }
            #endregion


            #region private ParseParameters - ���� ��������� � ������
            private static bool ParseParameters(string pms, ref RbTerminalCommand tcmd)
            {
                if (pms == null || pms.Length == 0)
                {
                    tcmd.ParametersCount = 0;
                    return true;
                }

                String[] pmsArray = pms.Split(new char[] { ';' });

                if (pmsArray == null || pmsArray.Length == 0)
                {
                    tcmd.ParametersCount = 0;
                    return true;
                }

                int parCount = 0;
                for (; parCount < pmsArray.Length && parCount < 2; parCount++)
                {
                    try
                    {
                        tcmd.Parameters[parCount] = Convert.ToInt32(pmsArray[parCount]);
                    }
                    catch (System.Exception)
                    {
                        tcmd.ParametersCount = 0;
                        return false;
                    }
                }

                tcmd.ParametersCount = parCount;
                return true;
            }
            #endregion


            #region private ParseCommand - ��� � ���� ��������� ������ ��� ������������ ���������
            private static RbTerminalCommand ParseCommand(string commandStr)
            {
                RbTerminalCommand tcmd = new RbTerminalCommand();

                if (commandStr == null || commandStr.Length < 3)
                {
                    tcmd.CommandType = RbTerminalCommand.RbTerminalCommandType.ERROR;
                    return tcmd;
                }

                char cmd = commandStr[commandStr.Length - 1];
                string pms = commandStr.Substring(2, commandStr.Length - 3);

                if (!ParseParameters(pms, ref tcmd))
                {
                    tcmd.CommandType = RbTerminalCommand.RbTerminalCommandType.ERROR_PARAMS;
                    tcmd.Command = commandStr;
                    return tcmd;
                }

                switch (cmd)
                {
                    case 'J':
                        tcmd.CommandType = RbTerminalCommand.RbTerminalCommandType.ERASE_DISPLAY;
                        if (tcmd.ParametersCount == 0)
                        {
                            tcmd.ParametersCount = 1;
                            tcmd.Parameters[0] = 0;
                        }
                        return tcmd;

                    case 'H':
                    case 'f':
                        tcmd.CommandType = RbTerminalCommand.RbTerminalCommandType.SET_POS;
                        return tcmd;

                    default:
                        tcmd.CommandType = RbTerminalCommand.RbTerminalCommandType.ERROR;
                        tcmd.Command = commandStr;
                        return tcmd;
                }
            }
            #endregion


            #region private DispatchCommand - ������ ������ �� ������ � ������
            private void DispatchCommand(RbTerminalCommand tcmd)
            {
                switch (tcmd.CommandType)
                {
                    case RbTerminalCommand.RbTerminalCommandType.PRINT:
                        #region print command
                        if (tcmd.Param0.Length >= 43)
                        {
                            int fKey = Utils.Utils.Str2Int(tcmd.Param0.Substring(42, 1), 1) - 1;
                            if (fKey < 0)
                                fKey = 0;
                            if (tcmd.Param0.Substring(43).Length == 0)
                                _fKeys[fKey] = "-not assigned";
                            else
                                _fKeys[fKey] = tcmd.Param0.Substring(43);
                        }

                        if (_currentRow < _consoleHeight && _currentColumn < _consoleWidth)
                        {
                            lock (_console.SyncRoot)
                                for (int col = _currentColumn; col < tcmd.Param0.Length && col < _consoleWidth; col++)
                                    _console[_currentRow][col] = tcmd.Param0[col];
                        }
                        #endregion
                        return;

                    case RbTerminalCommand.RbTerminalCommandType.NEWLINE:
                        #region new line <\r\n> command
                        _currentColumn = 0;
                        _currentRow++;
                        #endregion
                        return;

                    case RbTerminalCommand.RbTerminalCommandType.SET_POS:
                        #region set position <row,col> command
                        switch (tcmd.ParametersCount)
                        {
                            case 2:
                                if (tcmd.Parameters[0] == 0)
                                    tcmd.Parameters[0] = 1;
                                if (tcmd.Parameters[1] == 0)
                                    tcmd.Parameters[1] = 1;

                                _currentRow = tcmd.Parameters[0] - 1;
                                _currentColumn = tcmd.Parameters[1] - 1;
                                break;

                            default:
                                break;
                        }
                        #endregion
                        return;

                    case RbTerminalCommand.RbTerminalCommandType.ERASE_DISPLAY:
                        #region erase console command
                        switch (tcmd.ParametersCount)
                        {
                            case 1:
                                if (tcmd.Parameters[0] == 2)
                                {
                                    lock (_console.SyncRoot)
                                    {
                                        for (int row = 0; row < _consoleHeight; row++)
                                            for (int col = 0; col < _consoleWidth; col++)
                                                _console[row][col] = ' ';
                                    }
                                }
                                _currentRow = 0;
                                _currentColumn = 0;
                                break;

                            default:
                                break;
                        }
                        #endregion
                        return;

                    case RbTerminalCommand.RbTerminalCommandType.ERROR_PARAMS:
                    case RbTerminalCommand.RbTerminalCommandType.BELL_ALERT:
                    case RbTerminalCommand.RbTerminalCommandType.ERROR:
                    default:
                        return;
                }
            }
            #endregion
        }
}

