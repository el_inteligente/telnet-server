// ��� ����������������� ���������, ���������� ��� ���������� �
// ���������� ��� ������ ����������.
// ���� ���������� ������ ������ ����� remoting
//#define RBCOMUNICATION
//#define REMOTINGVERSION
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using Ohe.Interfaces;
using Ohe.Telnet.Configuration;
using Ohe.Telnet.Controls;
using Ohe.Telnet.Core;
using Ohe.Telnet.Data;
using Ohe.Telnet.Exception;
using Ohe.Utils;
//using Ohe.Telnet.;

namespace Ohe.Telnet.Server
{
    /// <summary>
    /// ����� ������������ ����������� ������� � �������
    /// � ��������� �������������� (������������)
    /// </summary>
    /// ��� ��� ������ ���� ������� WCF ������ Single!!!
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,//PerSession, 
        ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    public sealed class TelnetServer : IDisposable, IOheUserManagementService
    {
        #region Private Properties
        // ���������� ������ �� ������ ��� ������������ ������������
        private static readonly string SyncObject = Guid.NewGuid().ToString();
        // ��������� ���������� ���������� ���������� ��� �� ���������
        private static bool _isContinue = true;
        // �����
        private static Thread _serverThread = null;
        // ������ �� �����
        private static TelnetServer _server = null;

        #region Private Server Properties - �������� ������ �� ���������
        private Thread _listenerThread;
        private TcpListener _listener;
        private TcpListener _webListener;
        #endregion

        private static TelnetSessionManager _userManager;// = new TelnetSessionManager(10000);

        private HttpChannel ch;

        private static TlnConfigManager _cnfServer; // ������������ ������� 

        private static DateTime _dateTimeConnectMonitor = DateTime.Now;


        #region UsersConsoles - �������� �������� ������ ���� ������������ �������������
        public static ArrayList UsersConsoles
        {
            get
            {
                ArrayList rez = new ArrayList();
                lock (SyncObject)
                {
                    if (_server == null)
                        return rez;

                    lock (_userManager.Connections.SyncRoot)
                    {
                        foreach (UserTelnetConnection user in _userManager.Connections)
                            if (user.IsConnected)
                            {
                                rez.Add(user.UserID + "|" + user.ClientIP + "|" + user.UserConsole.ToString() + "|" + user.ConnectedDate.ToString() + "|"
                                    + user.LastActivityDate.ToString()
                                    + "|" +
                                    ((int)((TimeSpan)(user.LastActivityDate - _dateTimeConnectMonitor)).TotalSeconds > 0 ? "1" : "0")
                                    );
                            }
                    }
                    _dateTimeConnectMonitor = DateTime.Now;
                }
                return rez;
            }
        }
        #endregion

        #endregion


        #region Constructor
        private TelnetServer() { }
        #endregion


        #region public static Start / Stop - �������� �����
        public static void Start()
        {
            lock (SyncObject)
            {
                if (_serverThread != null)
                    throw new System.Exception("OHE.Telnet ������ ��� ��������");

                try
                {
                    _serverThread = new Thread(new ThreadStart(ServerThread));
                    _serverThread.Priority = ThreadPriority.Lowest;
                    _serverThread.Start();

                }
                catch (System.Exception ex)
                {
                    //  throw new Exception(ex.Message, ex);
                    Utils.Utils.WriteLog(ex.Message);
                }
            }
        }

        public static void RemoteRemoveUser(string []login)
        {
           // Utils.Utils.WriteLog("�������� ���������� ������������� RemoteRemoveUser " + login.Length.ToString());
           _userManager.RemoteRemoveUser(login);
           
        }

        public static void RemoteStart()
        {
            if (_serverThread != null)
            {
                if (_serverThread.ThreadState == ThreadState.Stopped || _serverThread.ThreadState == ThreadState.Aborted)
                    _serverThread = null;
                else
                    return;
            }
            try
            {
                _serverThread.Priority = ThreadPriority.Lowest;
                _serverThread.Start();
            }
            catch (System.Exception ex)
            {
                Utils.Utils.WriteLog(ex.Message + (ex.StackTrace != null ? ex.StackTrace : string.Empty));
                throw new System.Exception(ex.Message, ex);
            }
        }

        // ������������� ����� � �������������
        public static void Stop()
        {
            lock (SyncObject)
                _isContinue = false;
        }
        #endregion


        #region ActiveTelnetUser - ���������� ������ �������� �������������
        public ArrayList ActiveTelnetUser()
        {
                ArrayList rez = new ArrayList();
                lock (SyncObject)
                {
                    if (_server == null)
                        return rez;

                    lock (_userManager.Connections.SyncRoot)
                    {
                        foreach (UserTelnetConnection user in _userManager.Connections)
                            if (user.IsConnected)
                            {
                                rez.Add(user.UserID + "|" + user.ClientIP + "|" + user.UserConsole.ToString() + "|" + user.ConnectedDate.ToString() + "|"
                                    + user.LastActivityDate.ToString()
                                    + "|" +
                                    ((int)((TimeSpan)(user.LastActivityDate - _dateTimeConnectMonitor)).TotalSeconds > 0 ? "1" : "0")
                                    );

                            }

                    }
                    _dateTimeConnectMonitor = DateTime.Now;
                }
                
                return rez;
        }
        #endregion


        #region private StartServer - ���������� ��������� ��� �������� �������������
        private void StartServer()
        {
            _listenerThread = new Thread(new ParameterizedThreadStart(DoListen));
            _listenerThread.Start(new object()); // just to be not null

            var webThread = new Thread(new ParameterizedThreadStart(DoListen));
            webThread.Start(null);
        }
        #endregion


        #region private StopServer -- ��������� ������� ���������� ���� �������������
        private void StopServer()
        {
            try
            {
                if(_listener != null)
                    _listener.Stop();

                if (_webListener != null)
                    _webListener.Stop();
                // ������������� ����� ������������ ��������� � ������������
                _cnfServer.ConfigStopMaintenance();
                _listener = null;
                _webListener = null;

                _userManager.Stop();
            }
            catch (System.Exception exc)
            {
                Utils.Utils.WriteLog(exc.Message);
            }
        }
        #endregion


        #region private ServerThread - �������� ������
        private static void ServerThread()
        {
            Utils.Utils.WriteLog("OHE.Telnet ������ ���������.");
            try
            {
                ServiceHost serviceHost = null;
                lock (SyncObject)
                {

                    _userManager = new TelnetSessionManager(TlnConfigManager.Timeout);

                    #region �������� ��������

#if DEBUG

#else
                   try
                   {
                       if (!_userManager.ModuleIsAvailable())
                            throw new System.Exception("������: ������ ��������." + Environment.NewLine
                                                + "������ ���������� ����������� ��� ����������." + Environment.NewLine
                                                + "��������� ��� ������ ���� ����������: Open Handheld Engine Platform ��������" + Environment.NewLine
                                                + "��������� ������ ����: " + Environment.NewLine
                                                + "IP �����:    " + TlnConfigManager.IpPlatform + Environment.NewLine
                                                + "�������� ������ ������ �� �������� � ����� ������������.");
                    }
                    catch (TimeoutException timeOut)
                    {
                        throw new System.Exception("������ ������� ���������� �������� �� ������� OHE Platform"
                            + Environment.NewLine + "���������� ������:" + timeOut.Message);
                    }
                    catch (FaultException exc)
                    {
                        throw new System.Exception("����������� ���������� �� ������� ��������� OHE Platform"
                            + Environment.NewLine + "���������� ������:" + exc.Message);
                    }
                    catch (CommunicationObjectFaultedException exc)
                    {
                        string massageError = "������ ��� ���������� � ��������� OHE Platform"
                            + Environment.NewLine + "��������� ������ �������� � ������ Open Source HandHeld Platform"
                            + "� ������������� ������ ����." + Environment.NewLine
                            + "���������� ������: " + exc.Message;
                        throw new System.Exception(massageError, exc);
                    }
                    catch (System.Exception ex)
                    {
#if DEBUG
#else
                        /*
                        if (_proxy != null)
                        {
                            _proxy.OnUnsubscribe(_proxy.ModuleID, Ohe.Interfaces.EventType.Stop);
                            _proxy.OnUnsubscribe(_proxy.ModuleID, Ohe.Interfaces.EventType.Start);
                            _proxy.OnUnsubscribe(_proxy.ModuleID, Ohe.Interfaces.EventType.Remove);
                            _proxy = null;
                        }
                        */
#endif
                        Utils.Utils.WriteLog("���������� ����������" + Environment.NewLine + ex.ToString() + Environment.NewLine);
                        throw new System.Exception(ex.Message, ex);
                    }

#endif
                    #endregion

                    // ������� ������������ �������
                    _cnfServer = new TlnConfigManager();
                    // �������� �������� �� ������������
                    _cnfServer.ConfigStartMaintenance();
                }


                // �������� ������� ��������� �����
                lock (SyncObject)
                {
                    serviceHost = new ServiceHost(typeof(TelnetServer));
                    // Open the ServiceHost to create listeners and start listening for messages.
                    serviceHost.Open();
                    //ServiceHost serviceHost2 = new ServiceHost(typeof(RbTelnetServer.RBWMS_SoapService));
                    // Open the ServiceHost to create listeners and start listening for messages.
                    //serviceHost2.Open();

                    /*
                    Ohe.Telnet.WCF_Web_Service.Class1 cl = new WCF_Web_Service.Class1();
                    cl.Start();
                    */
                    //-------------------------------------------------------------------------------//
                    //--- ������ ��� ������������ ��� web �������� ������� ��������������� ������������.
                    // �� ������ ��������� ���������� ���!!! ������� ���������.
                    //-------------------------------------------------------------------------------//
                    
                    ServiceHost host = new ServiceHost(typeof(Microsoft.WebProgrammingModel.Samples.OheWebTelnetService), new Uri("http://localhost:81"));
                    ServiceEndpoint endpoint = host.AddServiceEndpoint(typeof(Microsoft.WebProgrammingModel.Samples.OheWebTelnetService), new WebHttpBinding(), "");
                    endpoint.Behaviors.Add(new Microsoft.WebProgrammingModel.Samples.FormProcessingBehavior());

                    ServiceMetadataBehavior smb = host.Description.Behaviors.Find<ServiceMetadataBehavior>();

                    if (smb != null)
                    {
                        smb.HttpGetEnabled = true;
                        smb.HttpsGetEnabled = true;
                    }

                    ServiceDebugBehavior sdb = host.Description.Behaviors.Find<ServiceDebugBehavior>();
                    if (sdb != null)
                    {
                        sdb.HttpHelpPageEnabled = false;
                    }


                    host.Open();


                    //-------------------------------------------------------------------------------//
                    //--- ������ ��� ������������ ��� web ��������
                    //-------------------------------------------------------------------------------//

                    _server = new TelnetServer();

                    // ��������� ������ ����� � ������� �������� ���������
                    _server.StartServer();
                }

                if (!_isContinue)
                    _isContinue = true;

                #region ���� ������������ ��� �����������
                // ���������� ����������� ���� ��� ��������� ���� �����������
                while (true)
                {
                    lock (SyncObject)
                        if (!_isContinue)
                            break;

                    Thread.Sleep(500);
                }
                #endregion

                serviceHost.Abort();
            }
            catch (EndpointNotFoundException exp)
            {
                string _message = "������: OHE.Telnet" + Environment.NewLine
                                   + "��������� ������� ���������� � ���������� OHE Platform" + Environment.NewLine
                                   + "��������� ��� ��������� ����������." + Environment.NewLine
                                   + "�������� ������:" + exp.Message;

                Utils.Utils.WriteLog(_message);
            }
            catch (FaultException<ExceptionDetail> exp)
            {
                Utils.Utils.WriteLog(exp.Detail.InnerException.Message);
            }
            catch (System.Exception e)
            {
                Utils.Utils.WriteLog(e.Message + (e.InnerException != null ? e.InnerException.Message : "") + e.StackTrace);
            }
            finally
            {
                lock (SyncObject)
                    if (_server != null)
                    {
                        try 
                        {
                            _server.Dispose(); 
                        }
                        catch (System.Exception ex)
                        {
                            Utils.Utils.WriteLog(ex.Message);
                        }
                    }

                Utils.Utils.WriteLog("OHE.Telnet ����������.");
            }
        }
        #endregion


        #region private DoListen
        /// <summary>
        /// ����������� ��� ����������� �� ������������� � ��������� ������
        /// </summary>
        private void DoListen(object param)
        {
            try
            {
                bool originalBehavior = param != null;
                if (originalBehavior)
                {
                    _listener = new TcpListener(IPAddress.Any, TlnConfigManager.Port);
                    _listener.Start();

                    // ���������� ��������� �������
                    _userManager.OnKillSession += OnClientDisconnect;

                    // ��������� ������������� ������ ��� ���������� �����������
                    if (ChannelServices.GetChannel("http") == null)
                    {
                        ch = new HttpChannel(TlnConfigManager.RemotePort);
                        ChannelServices.RegisterChannel(ch, false);
                        RemotingConfiguration.RegisterWellKnownServiceType(typeof(TelnetServerManager), "Users", WellKnownObjectMode.Singleton);
                    }
                }
                else
                {
                    _webListener = new TcpListener(IPAddress.Any, TlnConfigManager.WebPort);
                    _webListener.Start();
                }

                // � ���� ������ ���� 
                while (true)
                {
                    UserTelnetConnection client;
                    try
                    {
                        Socket socket;
                        if (originalBehavior)
                        {
                            if (_listener == null)
                                break;

                            socket = _listener.AcceptSocket();
                        }
                        else
                        {
                            if (_webListener == null)
                                break;

                            socket = _webListener.AcceptSocket();
                        }

                        if (socket == null)
                        {
                            Utils.Utils.WriteLog("DoListen, �� ���� ������� ������ ��� ���������� � �������.");
                            continue;
                        }
                        else
                        {
                            if (originalBehavior)
                            {
                                client = new UserTelnetConnection(socket);
                            }
                            else
                            {
                                Monitor.Enter(socket); // ����� ���� ������ ������ ����� �������� ����� client.WebSync
                                client = new UserTelnetConnection(socket, OheConnectionType.Web);
                            }
                        }

                        //�������� ������������ ������, ���� � ��� ���������� ���������� Wi-Fi
                        var reConnection = _userManager.TryToReconnect(client.MacAddress);
                        if (reConnection != null)
                        {
                            client.RestoreTelnetUser(reConnection.UserID, reConnection.Logic.Permiss, reConnection.OheConnectionType);
                            _userManager.ReconnectUser(client, reConnection.UserID);

                            if (client.Deserialize(reConnection.Logic, reConnection.UserID))
                                OnClientReConnect(client);
                            else
                                OnClientConnect(client);
                        }
                        else
                        {
                            _userManager.Connect(client);
                            OnClientConnect(client);
                        }

                        client.OnClientClose += OnClientDisconnect;

                        if (client.OheConnectionType == OheConnectionType.Web)
                        {
                            Monitor.Exit(client.WebSync);
                        }
                    }
                    catch (System.Exception exc) // catch any exception here to prevent exiting while loop, which lead to browser hang for web connections
                    {
                        Utils.Utils.WriteLog("DoListen, SocketException, AcceptSocket client: " + exc.Message + Environment.NewLine
                                 + "���� �������:" + exc.StackTrace);
                        continue;
                    }
                }
            }
            #region Exceptions
            catch (SocketException exc) 
            {
                Utils.Utils.WriteLog("DoListen, SocketException, Global: " + exc.Message + Environment.NewLine 
                                                 + "���� �������:" + exc.StackTrace);

            }
            catch (ConfigurationException ex)
            {
                Utils.Utils.WriteLog(ex.ToString());
                _isContinue = false;
                if (_userManager != null)
                    _userManager.Stop();
            }
            catch (System.Exception e)
            {
                Utils.Utils.WriteLog(e.Message);
                _isContinue = false;
                if(_userManager != null)
                    _userManager.Stop();
            }
            #endregion

        }
        #endregion


        #region OnClientReConnect - ����������� ����� ������ d������������ ����� ����������� ����
        // �������� �������� �� ���������� ������������� (������������ ����������)
        // � ����������� �������������
        // � ������ �������
        private void OnClientReConnect(UserTelnetConnection userConnection)
        {
            try
            {
                userConnection.ConnectedDate = DateTime.Now;
                userConnection.LastActivityDate = userConnection.ConnectedDate;

                userConnection.Logic.CurrentFormStatus = OheTelnetScreenManager.OheFormStatus.IsRestoreSession;

                //Utils.Utils.WriteLog("OnClientReConnect: " + userConnection.UserID + "Console: " + userConnection.UserConsole + "IP" + userConnection.ClientIP);


                userConnection.ForceReceiveString(Environment.NewLine);

            }
            catch (System.Exception e)
            {
                _isContinue = false;
                Utils.Utils.WriteLog("OHE.Telnet ������: ������ � ������ OnClientReConnect."
                                            + Environment.NewLine + e.Message);
            }
        }
        #endregion


        #region OnClientConnect - ����������� ����� ������ �����������
        // �������� �������� �� ���������� ������������� (������������ ����������)
        // � ����������� �������������
        // � ������ �������
        private void OnClientConnect(UserTelnetConnection userConnection)
        {
            try
            {
                userConnection.ConnectedDate = DateTime.Now;
                userConnection.LastActivityDate = userConnection.ConnectedDate;



        #if DEBUG


#else

#if REMOTINGVERSION
                // ��������� �� ������������ ���������� ����������� � �������
                if(OheLicenseObjectManagerClient.GetMaxTelnetUsers() <= 0)
                {
                        userConnection.SendData(
                                            OheConstant.TELNET_CLEAR_SEQ + Environment.NewLine 
                                                    + Environment.NewLine 
                                                    + CenterString("��������� ������������")
                                                    + Environment.NewLine 
                                                    + CenterString("���������� ��������")
                                                    + Environment.NewLine
                                                    + CenterString("������������ �����")
                                                    + Environment.NewLine
                                                    + CenterString("��������")
                                                );
                    Thread.Sleep(5000);
                    OnClientDisconnect(userConnection);
                }
                else
                {
#endif
                // ��������� �� ������������ ���������� ����������� � �������
                if(_userManager.MaxUsers() <= 0)
                {
                   userConnection.SendData(
                            (userConnection.OheConnectionType == OheConnectionType.HandHeld ?
                                    OheConstant.TELNET_CLEAR_SEQ + Environment.NewLine 
                                            + Environment.NewLine 
                                            + CenterString("��������� ������������")
                                            + Environment.NewLine 
                                            + CenterString("���������� ��������")
                                            + Environment.NewLine
                                            + CenterString("������������ �����")
                                            + Environment.NewLine
                                            + CenterString("��������") :
                                            "<div class=\"form\"><table><tr><td align=center>"
                                            + "��������� ������������"
                                            + "<BR>"
                                            + "���������� ��������"
                                            + "<BR>"
                                            + "������������ �����"
                                            + "<BR>"
                                            + "�������� </td></tr></table></div>")
                                 );
                    Thread.Sleep(5000);
                    OnClientDisconnect(userConnection);
                }
                else
                {
#endif
                if (userConnection.OheConnectionType == OheConnectionType.HandHeld)
                {
                    userConnection.SendData("���� ����������...");
                    Thread.Sleep(200);
                    userConnection.ForceReceiveString(Environment.NewLine);
                }
#if DEBUG                
#else
                } 
#endif
            }
#if DEBUG                
#else
            catch (TimeoutException timeOut)
            {
                _isContinue = false;
                Utils.Utils.WriteLog("�����: OnClientConnect()" + Environment.NewLine + "������ ������� ���������� �������� �� ������� OHE Platform"
                    + Environment.NewLine + "���������� ������:" + timeOut.Message);
            }
            catch (FaultException exc)
            {
                _isContinue = false;
                Utils.Utils.WriteLog("�����: OnClientConnect()" + Environment.NewLine + "����������� ���������� �� ������� ��������� OHE Platform"
                    + Environment.NewLine + "���������� ������:" + exc.Message);
            }
            catch (CommunicationObjectFaultedException exc)
            {
                _isContinue = false;
                string _massageError = "�����: OnClientConnect()" + Environment.NewLine + "������ ��� ���������� � ��������� OHE Platform"
                    + Environment.NewLine + "��������� ������ �������� � ������ Open Source HandHeld Platform"
                    + " � ������������� ������ ����." + Environment.NewLine
                    + "���������� ������: " + exc.Message;
                Utils.Utils.WriteLog(_massageError);
            }
#endif
            catch (System.Exception e)
            {
                _isContinue = false;
                Utils.Utils.WriteLog("OHE.Telnet ������: ������ � ������ OnClientConnect." 
                                            + Environment.NewLine + e.Message);
            }
        }
        #endregion


        #region OnClientDisconnect -����������� ����� ������ �����������
        private void OnClientDisconnect(UserTelnetConnection sender)
        {
            try
            {
                lock (_userManager.Connections.SyncRoot)
                {
                    OheTelnetScreenManager obj = sender.Logic;
                    var userScreen = sender.ClientIP.ToString() + " " + sender.MacAddress + " " + sender.UserConsole;
                    RfForm activeForm;
                    try
                    {
                        activeForm  = obj.Forms[obj.ActiveFormId];
                    }
                    catch 
                    {
                        activeForm = null;
                    }
                    // ����������� ������
                    // ��������� ������ ������, �� ������� ���� ����� �� ����������.
                    //if (!sender.LogOff && sender.UserID != string.Empty)
                    if (sender.UserID != string.Empty)
                    {
                        var fork = 0;
                        try
                        {
                            fork = ExecuteSqlQuery.ExecuteScalar("select count(*) from t_binlocat (nolock) where binlabel='@" + sender.UserID + "'");
                        }
                        catch { }

                        if (fork > 0 || (activeForm != null && activeForm.IsRestoreSession))
                        {
                            // ��������� ������, ��������� ����������� ������� �� ��������, �.�. ����� ���� �� ����������,
                            // ��� ������ ����� ������� �������������� ������.
                            using (FileStream stream = File.Create(Utils.Utils.assemblyPath + @"\Session\" + sender.UserID + ".user"))
                            {
                                BinaryFormatter format = new BinaryFormatter();
                                format.Serialize(stream, obj);
                            }
                            //Utils.Utils.WriteLog("��Server OnClientDisconnect() ���������� ������ � �����:" + sender.UserID + " " + sender.UserConsole + sender.MacAddress);
                        }
                        else
                        {
                            // ������������� ��������� ������, ���� � ��� ��� ������������� �� ���������.
                            // ������������� ���������� ������� �� �������� ������������.
                            OnClientDisconnectAutoConfirmSession(obj.ProcessValues, sender.UserID, sender.LastActivityDate, userScreen);
                        }

                        // ������� ��� ��� ������� ��������
                        // �� ��������� ��������� ������������
                        sender.LogOff = true;

#if DEBUG
#else
                        //Utils.Utils.WriteLog("TelnetSessionManager.RemoveUser:" + sender.UserID + " " + sender.UserConsole + sender.MacAddress);
                        // ������� ������������ �� ��������� ��������.
                        TelnetSessionManager.RemoveUser(sender.UserID);
#endif

                        //Utils.Utils.WriteLog("��Server OnClientDisconnect() ���-�� �������� �������������: " + _userManager.Connections.Count.ToString());
                        // ������� �������� ������������ c ������� ����������
                        _userManager.Connections.Remove(sender);
                        //Utils.Utils.WriteLog("��Server OnClientDisconnect() ���-�� �������� ������������� ����� ��������: " + _userManager.Connections.Count.ToString());

                    }
                    //�������� ��������� �������������� �������� �� ���������� ������.
                    //OnClientDisconnectAutoConfirmSession(obj.ProcessValues, sender.UserID, sender.LastActivityDate, userScreen);
                }
                sender.CloseClientConnection();
            }
            catch (System.Exception exc)
            {
                Utils.Utils.WriteLog("OnClientDisconnect " + exc.Message + Environment.NewLine + exc.InnerException); 
            }
        }
        #endregion


        #region OnClientDisconnectAutoConfirmSession - �������������� ���������� ������ ������������
        /// <summary>
        /// ������������� ��������� ������, ���������� ���������� � ���
        /// </summary>
        /// <param name="Values"></param>
        private void OnClientDisconnectAutoConfirmSession(Dictionary<string, object> Values, string UserId, DateTime LastActivityDate, string UserConsole)
        {
            string _query = string.Empty;
            if (Values != null)
            {
                try
                {
                    string _startDate = string.Empty;
                    string _startTime = string.Empty; 
                    if (Values.ContainsKey("SYS_VALUE"))
                    {
                        DataTable dt = (DataTable)Values["SYS_VALUE"];
                        _startDate = dt.Rows[0].ItemArray[dt.Columns["SYS_STARTDATE"].Ordinal].ToString();
                        _startTime = dt.Rows[0].ItemArray[dt.Columns["SYS_STARTTIME"].Ordinal].ToString();
                    }
                    else
                    {
                        _startDate = LastActivityDate.ToString("dd.MM.yyyy HH:mm:ss");
                        _startTime = "01.01.1900 " + LastActivityDate.ToString("HH:mm:ss");
                    }

                    string _lastDateActivity = LastActivityDate.ToString("dd.MM.yyyy HH:mm:ss");
                    _query = "exec dbo.[sp_wms_log_off_user_by_system] '" 
                            + UserId + "','" + _startDate + "','" + _startTime + "','" 
                            + _lastDateActivity + "','" + UserConsole + "' " ;

                    List<RfControlEventsParams> _params = new List<RfControlEventsParams>();
                    _params.Add(new RfControlEventsParams("@login","varchar",10,UserId));
                    _params.Add(new RfControlEventsParams("@start_date", "datetime", 0, _startDate));
                    _params.Add(new RfControlEventsParams("@start_time", "datetime", 0, _startTime));
                    _params.Add(new RfControlEventsParams("@activity", "datetime", 0, _lastDateActivity));
                    _params.Add(new RfControlEventsParams("@user_console", "varchar", 250, UserConsole));

                    ExecuteSqlQuery.ExecuteEvent("sp_wms_log_off_user_by_system", _params, Values, null);
                }
                catch (System.Exception exc)
                {
                    Utils.Utils.WriteLog("������ ��������������� ���������� ������" + Environment.NewLine
                                    + "������: " + _query + Environment.NewLine
                                    + "���������� ������:" + exc.Message);
                }
            }
        }
        #endregion


        #region OnClientRemoteClose
        /// <summary>
        /// ������������ ������� ������ ������ ������������� ��� ����� �������
        /// </summary>
        /// <param name="USER"></param>
        [Obsolete("������ ����� �� ������������")]
        private void OnClientRemoteClose(string USER, IPAddress IP)
        {
            UserTelnetConnection del = null;
            try
            {
                lock (_userManager.Connections.SyncRoot)
                {
                    foreach (UserTelnetConnection user in _userManager.Connections)
                        if (Utils.Utils.AllTrim(user.UserID.ToUpperInvariant()) == Utils.Utils.AllTrim(USER.ToUpperInvariant())
                                && user.ClientIP != IP)
                        {
                            del = user;
                            break;
                        }
                }

                if (del != null)
                {
                    
                    del.SendData(OheConstant.TELNET_CLEAR_SEQ 
                            + CenterString("�����������")
                            + Environment.NewLine
                            + CenterString("������ ������������")
                            + Environment.NewLine
                            + CenterString("���������� �����")
                            + Environment.NewLine
                            + CenterString("�������")
                            + OheConstant.SetCursorPosition(0, 0));
                    Thread.Sleep(5000);

                    del.CloseClientConnection();

                    _userManager.Disconnect(del);
                }
            }
            catch
            { }
        }
        #endregion


        #region Dispose
        public void Dispose()
        {

#if REMOTINGVERSION
                    lock (_userManager.Connections.SyncRoot)
                    {
                        foreach (UserTelnetConnection _connection in _userManager.Connections)
                        {
                            OheLicenseObjectManagerClient.RemoveTelnetUser(_connection.ClientId);
                        }

                    }
#endif
            try
            {
                // ������� ���� ������������� � �������
                _userManager.Stop();
            }
            catch (TimeoutException timeOut)
            {
                Utils.Utils.WriteLog("HHServer: �����: Dispose()" + Environment.NewLine + "������ ������� ���������� �������� �� ������� OHE Platform"
                    + Environment.NewLine + "���������� ������:" + timeOut.Message);
            }
            catch (FaultException exc)
            {
                Utils.Utils.WriteLog("HHServer: �����: Dispose()" + Environment.NewLine + "����������� ���������� �� ������� ��������� OHE Platform"
                    + Environment.NewLine + "���������� ������:" + exc.Message);
            }
            catch (CommunicationObjectFaultedException exc)
            {
                string _massageError = "HHServer: �����: Dispose()" + Environment.NewLine + "������ ��� ���������� � ��������� OHE Platform"
                    + Environment.NewLine + "��������� ������ �������� � ������ Open Source HandHeld Platform"
                    + "� ������������� ������ ����." + Environment.NewLine
                    + "���������� ������: " + exc.Message;
                Utils.Utils.WriteLog(_massageError);
            }
            catch (System.Exception exc)
            {
                Utils.Utils.WriteLog("HHServer: Dispose " + exc.Message);
            }
            StopServer();
        }
        #endregion


		#region CenterString - ����������� ������ �� �����������
		/// <summary>
		/// ����������� ������ �� ������
		/// </summary>
		/// <param name="userInput">������</param>
		/// <param name="width">������ ������</param>
		/// <param name="heigth"> ������ ������</param>
		/// <returns></returns>
		private string CenterString(string str)
		{
			int width = TlnConfigManager.TELNET_WIDTH;
			int heigth = TlnConfigManager.TELNET_HEIGHT;

			if (str.Length > width || str.Length == 0)
				return str;

			int center = (int)Math.Round((decimal)width / 2, 0);
			int width2 = (int)Math.Round((decimal)str.Length / 2, 0);

			string rez = string.Empty;
			for (int line = 1; line < center - width2; line++)
				rez += " ";

			rez += str + "                                      ".Substring(0, (width - (rez + str).Length > 0 ? width - (rez + str).Length : 1));

			return rez;
		}
		#endregion
    }
}

