﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Ohe.Telnet.Web
{
    class HtmlTemplater
    {
        public HtmlTemplater()
        {
            Variables = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Variables { get; private set; }

        /// <summary>
        /// Processes template and renders template variables in it
        /// </summary>
        /// <param name="template">Path to template relative to Web directory without extension</param>
        /// <returns>Html code where template variables substituted by corresponding values</returns>
        public string Process(string template, bool isFile = true)
        {
            if (string.IsNullOrWhiteSpace(template))
            {
                throw new ArgumentException("Parameter cannot be null, empty or contain only whitespace characters", "template");
            }

            string content = string.Empty;
            if (isFile)
            {
                var baseDir = Utils.Utils.assemblyPath + "Web" + Path.DirectorySeparatorChar;
                var templatePath = baseDir + template + ".html";
                content = File.ReadAllText(templatePath);
            }
            else
            {
                content = template;
            }

            content = Regex.Replace(content, @"\{(.+?)\}", m => Variables.ContainsKey(m.Groups[1].Value) ? Variables[m.Groups[1].Value] : m.Groups[0].Value);
            content = Regex.Replace(content, @"\r\n?|\n", " ");

            return content + Environment.NewLine;
        }

        public static string Process(string template, Dictionary<string, string> variables, bool isFile = true)
        {
            var t = new HtmlTemplater();
            foreach (var entry in variables)
            {
                t.Variables.Add(entry.Key, entry.Value);
            }
            return t.Process(template, isFile);
        }
    }
}
