using System;

namespace Ohe.Telnet.Exception
{
    #region CommonException - ����� ����������
    /// <summary>
    /// ����� ���������� 
    /// </summary>
    public class CommonException : System.Exception
    {
        public CommonException() { }

        public CommonException(string msg) : base(msg) { }

        public CommonException(string msg, System.Exception exc) : base(msg, exc) { }

        private static string StringException(System.Exception exception)
        {
            return Environment.NewLine + "��������:" + (exception.Source == null ? "�����������" : exception.Source) + Environment.NewLine
                + "Target:" + (exception.TargetSite == null ? "�����������" : exception.TargetSite.ToString()) + Environment.NewLine
                + "����������:" + exception.GetType() + Environment.NewLine
                + "���������:" + (exception.Message == null ? "�����������" : exception.Message) + Environment.NewLine
                + "���� �������:" + (exception.StackTrace == null ? "�����������" : exception.StackTrace.ToString()) + Environment.NewLine;
        }

        public override string ToString()
        {
            if (InnerException == null)
                return StringException(this);
            return StringException(this) + Environment.NewLine + Environment.NewLine + "���������� ����������::" + Environment.NewLine + Environment.NewLine
                + StringException(InnerException);
        }

    }
	#endregion

    #region AuthorizationException - ������ ��������� � ������������ �������������
    /// <summary>
    /// ��������� ������ ��������� � ������������ �������������
    /// </summary>
    public class AuthorizationException : System.Exception
    {
        private bool _isNeedPrint = true;
        public bool IsNeedPrint { get { return _isNeedPrint; } set { _isNeedPrint = value; } }

        private bool _isNeedMail = true;
        public bool IsNeedMail { get { return _isNeedMail; } set { _isNeedMail = value; } }

        private bool _isNeedRecover;
        public bool IsNeedRecover { get { return _isNeedRecover; } set { _isNeedRecover = value; } }

        private string _badValue = string.Empty;
        public string BadValue { get { return _badValue; } set { _badValue = Utils.Utils.AllTrim(value); } }

        private string _nextState = "dbConnectStrs";
        public string NextState { get { return _nextState; } set { _nextState = Utils.Utils.AllTrim(value); } }

        public AuthorizationException()
        { }

        public AuthorizationException(string message) : base(message) { }

        public AuthorizationException(string message, System.Exception e) : base(message, e) { }

        //protected AuthorizationException(SerializationInfo serialInfo, StreamingContext streamContext) : base(serialInfo, streamContext) { }

        public AuthorizationException(string message, bool isNeedRecover)
            : base(message)
        {
            _isNeedRecover = isNeedRecover;
        }
        /*
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
         */
    }
    #endregion

    #region LogicException - ������� ��������� ������
    /// <summary>
    /// ��������� ������ ��������� � ������� ��������� ������
    /// </summary>
    public class LogicException : System.Exception
    {
        private static string _query = "����������";
        public static string Query { get { return _query; } set { _query = Utils.Utils.AllTrim(value); } }

        private readonly object _object;
        public object Object { get { return _object; } }

        public LogicException() { }

        public LogicException(string msg) : base(msg) { }

        public LogicException(string msg, System.Exception exc) : base(msg, exc) { }

        public LogicException(System.Exception e, string message, string query)
            : base(message, e)
        {
            Query = query;
        }

        /// <summary>
        /// �����������. ������������ ����� ���������� ������
        /// ������� ������������ ��� ���������� ������ �� �������� ���������
        /// </summary>
        /// <param name="e"></param>
        /// <param name="message"></param>
        /// <param name="obj"></param>
        public LogicException(string message, object obj) : base(message)
        {
            _object = obj;
        }

        private static string StringException(System.Exception exception)
        {
            return Environment.NewLine + "��������: " + (Query == null ? "�����������" : Query) + Environment.NewLine
                + "Target: " + (exception.TargetSite == null ? "�����������" : exception.TargetSite.ToString()) + Environment.NewLine
                + "���������: " + (exception.Message == null ? "��� ��������" : exception.Message) + Environment.NewLine
                + "���� �������: " + (exception.StackTrace == null ? "�����������" : exception.StackTrace.ToString()) + Environment.NewLine;
        }

        public override string ToString()
        {
            if (InnerException == null)
                return StringException(this);
            return StringException(this) + Environment.NewLine + Environment.NewLine + "���������� ����������::" + Environment.NewLine + Environment.NewLine
                + StringException(InnerException);
        }

    }
    #endregion

    #region SqlDataException - ��������� ������ ��������� � ������� ���� ������
    /// <summary>
    /// ��������� ������ ��������� � ������� ���� ������
    /// </summary>
    public class SqlDataException : System.Exception
    {
        private string _query = "����������";
        public string Query { get { return _query; } set { _query = Utils.Utils.AllTrim(value); } }

        private string _param = string.Empty;
        public string Param { get { return _param; } set { _param = Utils.Utils.AllTrim(value).ToLowerInvariant(); } }
        
        public SqlDataException() { }

        public SqlDataException(string message, System.Exception exception) : base(message, exception) { }

        public SqlDataException(string message) : base(message) { }

        public SqlDataException(System.Exception e, string message) : base(message, e) { }

        public SqlDataException(string message, string query, string param)
            : base(message)
        {
            Query = query;
            _param = param;
        }

        public SqlDataException(System.Exception e, string message, string query)
            : base(message, e)
        {
            Query = query;
        }

        public override string ToString()
        {
            return base.ToString()
                + Environment.NewLine + "�������� ���������:" + _query + Environment.NewLine + Environment.NewLine
                + (Param != null ? "��������:" + Param : string.Empty)
                + (StackTrace != null ? StackTrace : string.Empty);
        }

    //public override void GetObjectData(SerializationInfo info, StreamingContext context) { base.GetObjectData(info, context); }
    //protected SqlDataException(SerializationInfo serialInfo, StreamingContext streamContext) : base(serialInfo, streamContext) { }
    }
    #endregion

    #region ConfigurationException - ��������� ������ ��������� c ����������� ������������
    /// <summary>
    /// ��������� ������ ��������� c ����������� ������������
    /// </summary>
    public class ConfigurationException : System.Exception
    {
        private string _obj = "����������";
        public string ErrObj { get { return _obj; } set { _obj = Utils.Utils.AllTrim(value); } }

        private string _param = string.Empty;
        public string User { get { return _param; } set { _param = Utils.Utils.AllTrim(value).ToLowerInvariant(); } }

        public ConfigurationException() { }

        public ConfigurationException(string message, System.Exception exception)
                        : base(message, exception) { }

        public ConfigurationException(string message) : base(message) { }

        public ConfigurationException(System.Exception e, string message) 
                        : base(message, e) { }

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="message">��������� ����������</param>
        /// <param name="query">��� ������� ���������� ����������</param>
        /// <param name="param">����� ������������ ���������� ����������</param>
        public ConfigurationException(string message, string param)
            : base(message)
        {
            ErrObj = _obj;
            _param = param;
        }

        /// <summary>
        /// ����������
        /// </summary>
        /// <param name="e">����������</param>
        /// <param name="message">��������� ����������</param>
        /// <param name="obj">��� ������� ���������� ����������</param>
        public ConfigurationException(System.Exception e, string message, string obj)
            : base(message, e)
        {
            ErrObj = obj;
        }

        public override string ToString()
        {
            return base.ToString()
                + Environment.NewLine + "��� �������:" + ErrObj
                + Environment.NewLine
                + (User != null ? "������������:" + User : string.Empty)
                + Environment.NewLine;
        }
    }
    #endregion
}
