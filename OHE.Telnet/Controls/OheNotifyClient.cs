﻿using System.Collections.Generic;
using Ohe.Telnet.Configuration;
using Ohe.Utils;
using Ohe.Data;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Контрол инцидентов
    /// </summary>
    public class OheTelnetNotifyClient : RfFormBase
    {

        private List<int> _notifies = new List<int>();
        private OheDataTable _dataSource;
        private int _messageId;
        private string _userId;
        private string _link;
        private RfForm _rfForm;
        private int _parent;


        public OheTelnetNotifyClient(string userId, RfForm form, int parent)
        {
            _userId = userId;
            _messageId = 0;
            _rfForm = form;
            _rfForm.Permission = "QWERTYUIOPASDFGHJKLZXCVBNM";
            _parent = parent;
            OnInit();
        }

        public OheTelnetNotifyClient(string userId)
        {
            _userId = userId;
            _messageId = 0;
            OnInit();
        }

        #region OnInit
        /// <summary>
        /// Заполняет данными контрол
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        private bool OnInit()
        {
            string initQuery = "execute sp_notify_get_message_by_protocol '" + _userId + "', 'telnet'";
            try
            {
                // Проверяем что на гриде нет списке параметров или грид надо показывать обязательно
                using (OheDataClass dc = new OheDataClass("sql", TlnConfigManager.ConnectionString))
                {
                    _dataSource = dc.GetTableByQuery(initQuery);
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                Utils.Utils.WriteLog(string.Format("Инициализация подтверждений, вызвало ошибку {0}", ex.Message));
                return false;
            }
        }
        #endregion


        #region OnSubmit
        /// <summary>
        /// Подтверждает что сообщение возвращено пользователю
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public override bool OnSubmit()
        {
            OheDataTable dt;
            string submitQuery = "execute sp_notify_confirm_receive_message '" + _userId + "', 0, {0}, 'C'";
            try
            {
                foreach (int i in _notifies)
                {
                    OheDataClass.GetTableFromQuery(string.Format(submitQuery, i), null);
                }
                _notifies = new List<int>();
                return true;
            }
            catch (System.Exception ex)
            {
                Utils.Utils.WriteLog(string.Format("Подтверждение получения сообщений вызвало ошибку {0}", ex.Message));
                return false;
            }
        }
        #endregion


        #region GetMessage
        /// <summary>
        ///Генерирует текущее сообщение. Находит следующий идентификатор сообщения.
        /// </summary>
        /// <returns></returns>
        public string GetMessage()
        {
            if (_dataSource == null || _dataSource.IsEmpty)
                return string.Empty;

            for (int i = 0; i < _dataSource.RowsCount; i++)
            {
                int id = _dataSource.GetInt(i, "notify_id");
                string type;
                string link;
                if (_messageId == 0)
                {
                    _link = _dataSource.GetString(i, "link");
                    //type = _dataSource.GetString(i, "type");
                    _notifies.Add(id);

                    _messageId = _dataSource.RowsCount > 1 ? _dataSource.GetInt(1, "notify_id") : id;

                    return _dataSource.GetString(i, "message");
                }

                if (id != _messageId) continue;

                _notifies.Add(id);
                _link = _dataSource.GetString(i, "link");

                _messageId = _dataSource.RowsCount >= i + 1 ? _dataSource.GetInt(i + 1, "notify_id") : _dataSource.GetInt(0, "notify_id");

                return _dataSource.GetString(i, "message");
            }

            return string.Empty;
        }

        #endregion


        #region GetNotifyForm
        /// <summary>
        /// Процедура генерирует дополнительную форму если при событии обновления
        /// хранимая процедура возвратила несколь строк
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public RfForm GetNotifyForm()
        {
           string rr = _rfForm.GetActiveControl().ValueID;
           _rfForm.GetControlById(2).Value = GetMessage();
           _rfForm.Parent = _parent;

           RfControlKey control = (RfControlKey)_rfForm.GetControlById(9);

            int target;
            control.Target = int.TryParse(_link, out target) ? target : _parent;

            if (control.Target != _parent)
                control.Description = OheConstant.d + "Выполнить: F3" + OheConstant.TELNET_ESC_SEQ + "0m";
            else
                control.Description = "ESC: ВЫХОД";

            return _rfForm;
        }
        #endregion

        public RfForm GetNextNotify()
        {
            return GetNotifyForm();
        }




        #region AllTask
        public string AllTask()
        {
            if (string.IsNullOrEmpty(_userId))
                return string.Empty;

            if (!OnInit())
                return TlnConfigManager.BEEP + OheConstant.d + "(E)" + OheConstant.TELNET_ESC_SEQ + "0m";

            if (_dataSource.RowsCount > 0)
                return TlnConfigManager.BEEP + OheConstant.d + "(" + _dataSource.RowsCount + ")" + OheConstant.TELNET_ESC_SEQ + "0m";

            return string.Empty;
        } 
        #endregion
    }
}
