﻿using System;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Text;
using Ohe.Telnet.Data;
using Ohe.Telnet.Exception;
using Ohe.Utils;
using Ohe.Telnet.Configuration;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Содержит список типа меню но для выбора данных из базы
    /// </summary>
    public class RfControlList : RfControl
    {
        #region Define Variables

        private int _page = 0; // показывает текущую страницу на которой находится пользователь

        private string _result; // определяет представление контрола 

        private string _pageDown; // значение для листания экрана вниз

        private string _pageUp;   // значение для листания экрана вверх

        private int _listmenuCount; // список позиций меню на экране

        private List<object[]> _source; // определяет параметры позиции списка меню

        private List<object[]> _static; // значения списка, которые не меняются.

        private DataTable _dt = new DataTable();

        private DataTable _current = new DataTable();

        #endregion


        #region Properties

        #region Page
        /// <summary>
        /// Возвращает текущий номер страницы
        /// </summary>
        public int Page { get { return _page; } set { _page = value; } }
        
        #endregion


        #region RowsByPage
        /// <summary>
        /// Возващае
        /// </summary>
        public int RowsByPage { get { return _listmenuCount; } }
        
        #endregion


        #region Value
        /// <summary>
        /// Возвращет содержимое контрола
        /// </summary>
        public new List<object[]> Value { get { return _source; } set { _source = value; } }
        
        #endregion


        #region ControlView
        // Возвращает значение которое ввел пользователь
        public string ControlView { get { return _result; } }
        
        #endregion


        #region CurrentRow
        // Возвращает строку которую ввел пользователь
        public DataTable CurrentRow { get { return _current; } }
        
        #endregion

        #endregion


        #region Constructor


        public RfControlList(RfForm parentForm, RfControl parentControl)
            : base(parentForm, parentControl)
        {
            _source = new List<object[]>();
            _static = new List<object[]>();

            // Стуктура таблицы для статических данных
            _dt.Columns.Add(new DataColumn("ID", Type.GetType("System.String")));
            _dt.Columns.Add(new DataColumn("VALUE", Type.GetType("System.String")));

            _listmenuCount = TlnConfigManager.MenuLinesCount;
            _pageDown = TlnConfigManager.TELNET_MENU_DOWN;
            _pageUp = TlnConfigManager.TELNET_MENU_UP;
        }

        #endregion


        #region Methods

        #region ExecuteOnInitResult возвращает таблицу с результатом события On_Init
        /// <summary>
        /// Возвращает результат после запуска определенного события принадлежащего контролу
        /// </summary>
        /// <param name="name">Имя события</param>
        public override bool ExecuteEventHandlerControl(string name, ref Dictionary<string, object> results)
        //public override bool ExecuteEventHandlerControl(string name, ref Hashtable Results)
        {
            object tbl = null;
            RfControlEvents rfEvent = null;
            try
            {
                // Определяем имя события для контрола
                rfEvent = GetEventByName(name, results);
                
                // Возвращаем объект
                try
                {
                    // Устанавливаем значения параметров в хранимую процедуру
                    rfEvent.Params = SetParamsForSp(rfEvent.Params, results);
                    tbl = ExecuteSqlQuery.ExecuteEvent(rfEvent.SpName, rfEvent.Params, results, ParentForm);
                }
                catch (SqlException ex)
                {
                    results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                    Utils.Utils.WriteLog("Ошибка выполнения sql комманд: " + Environment.NewLine
                        + "Хранимая процедура:" + rfEvent.SpName + Environment.NewLine
                        + "Форма: " + rfEvent.FormId.ToString() + Environment.NewLine
                        + "Сообщение:" + Environment.NewLine + ex.ToString());
                    return false;
                }
                // Обработка ошибок связанных с данными и запуском хранимых процедур
                catch (SqlDataException ex)
                {
                    results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                    Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.ToString());
                    return false;
                }
                catch (LogicException ex)
                {
                    results["SYS_ERROR"] = ex.Message;
                    return false;
                }
                catch (FormatException ex)
                {
                    results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                    Utils.Utils.WriteLog("Ошибка конвертирования формата данных: " + ex.ToString());
                    return false;
                }
                catch (NullReferenceException ex)
                {
                    results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                    Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.ToString());
                    return false;
                }

                // Проверка на то что хранимая процедура отработала нормально
                // Добавляем ключ к общему кешу
                if (results.ContainsKey(ValueID)) results[this.ValueID] = tbl;
                        else results.Add(ValueID, tbl);
                FillListSource(tbl as DataTable);

                return true;
            }
            catch (System.Exception ex)
            {
                Utils.Utils.WriteLog("Ошибка обработки данных возвращаемых хранимой процедурой: "
                                            + Environment.NewLine + "Экран: " + ParentForm.Id.ToString()
                                            + Environment.NewLine + "Контрол:" + Id
                                            + Environment.NewLine + "Ссылка на объект:" + ValueID
                                            + Environment.NewLine + "Хранимая процедура: " + rfEvent.SpName
                                            + Environment.NewLine + "Значение:" + Value
                                            + Environment.NewLine + "Описание: " + ex.Message
                                            + Environment.NewLine + "Внутренняя ошибка: " + ex.InnerException
                                            );
                results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                return false;
            }
        }

        /// <summary>
        /// Заливает список содержимым
        /// </summary>
        /// <param name="dt"></param>
        public void FillListSource(DataTable dt)
        {
            _source = new List<object[]>();

            if (dt.Columns.Count == 1)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                    _source.Add(new object[] { dt.Rows[i].ItemArray[0].ToString(), dt.Rows[i].ItemArray[0].ToString() });

                _current = dt.Clone();
                _current.Columns.Add(new DataColumn("SSS",dt.Columns[0].DataType));

            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                    _source.Add(dt.Rows[i].ItemArray);

                _current = dt.Clone();
            }
        }
        #endregion


        #region FillStaticValue
        /// <summary>
        /// Заливает список содержимым статических значений.
        /// </summary>
        /// <param name="dt"></param>
        public void FillStaticValue(DataTable dt)
        {
            _static = new List<object[]>();

            if (dt.Columns.Count == 1)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                    _static.Add(new object[] { dt.Rows[i].ItemArray[0].ToString(), dt.Rows[i].ItemArray[0].ToString() });

                _current = dt.Clone();
                _current.Columns.Add(new DataColumn("SSS", dt.Columns[0].DataType));


            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                    _static.Add(dt.Rows[i].ItemArray);


                _current = dt.Clone();
            }

            
        }
        
        #endregion


        #region GetValueById - возвращает данные выбранные пользователем
        /// <summary>
        /// Возвращает значения по числу которое ввел пользователь
        /// </summary>
        /// <param name="Id">Число которое ввел пользователь</param>
        /// <returns></returns>
        public string GetValueById(int Id)
        {
            int i = 0;
            int user = (_page)*_listmenuCount+(Id - 1);
            foreach (object[] currList in _source)
            {
                // Ищем то что пользователь ввел
                if (user == i)
                {
                    return Utils.Utils.AllTrim(currList[0].ToString());
                }
                i++;
            }
            return string.Empty;
        }
        #endregion


        #region GetCurrentDataTable - возвращает данные выбранные пользователем
        /// <summary>
        /// Возвращает значения по числу которое ввел пользователь
        /// </summary>
        /// <param name="Id">Число которое ввел пользователь</param>
        /// <returns></returns>
        private void GetCurrentDataTable(int Id)
        {
            int i = 0;

            int user = (_page) * _listmenuCount + (Id - 1);

            foreach (object[] currList in _source)
            {
                // Ищем то что пользователь ввел
                if (user == i)
                {
                    DataRow dr = _current.NewRow(); 
                    dr.ItemArray = currList;
                    _current.Rows.Add(dr);
                    return;
                }
                i++;
            }
        }
        #endregion


        #region Отображает следующую и предыдущую страницы
        /// <summary>
        /// Возвражает слежующий список
        /// </summary>
        /// <returns></returns>
        public string GetNextMenu() { return GetCurrentList(1); }

        /// <summary>
        /// Возвращает предыдущий список листа
        /// </summary>
        /// <returns></returns>
        public string GetPreviousMenu() { return GetCurrentList(-1); }
        #endregion


        #region Private GetCurrentList
        private string GetCurrentList(int next)
        {
            int j = 1;
            int currline = 0;
            // устанавливаем курсор на первую строку
            string rez = string.Empty;

            switch(next)
            {
                case 1:
                    _page++;
                    break;
                case -1:
                    _page--;
                    break;
            }

            if (_page > 0)
            {
                if (_source.Count / ((_page) * _listmenuCount) < 1)   // есть нет меню для листания вперед
                    _page = 0;
            }

            foreach (object[] currList in _source)
            {
                // Проверка на то что пользователь находится на первой странице
                if (_page <= 0 && currline < _listmenuCount)
                {
                    rez += (currline + 1).ToString() + "." + Utils.Utils.AllTrim(currList[1].ToString()) + Environment.NewLine;
                    _page = 0;
                    currline++;
                }
                else if (_page <= 0) break;

                // Пользователь находится на следующей странице
                if (
                    currline < (_listmenuCount)
                    && j > (_page * _listmenuCount)
                    && j < (_page * _listmenuCount + _listmenuCount + 2)
                    && _page > 0
                    )
                {
                    rez += (currline + 1).ToString() + "." + Utils.Utils.AllTrim(currList[1].ToString()) + Environment.NewLine;
                    currline++;
                }
                else if (_page > 0 && currline > _listmenuCount) break;
                j++;
            }

            // Определяем общее количество страниц в меню
            int linesal = (int)(Math.Ceiling(((double)_source.Count / (double)_listmenuCount)));

            rez += (linesal>1 ? Environment.NewLine + "       " + (_page + 1).ToString() + "/"+ linesal.ToString() : string.Empty);

            return rez;
        }
        #endregion


        #region ParseString - public
        /// <summary>
        /// Обрабатывает значения которые ввел пользователь
        /// </summary>
        /// <param name="userInput"></param>
        public string ParseString(string str)
        {
            if (Utils.Utils.AllTrim(_pageDown) == Utils.Utils.AllTrim(str)) // пользователь решил перейти на следующую страницу
            // возвращаем результат
            {
                _result = string.Empty;
                return GetNextMenu();
            }
            if (Utils.Utils.AllTrim(_pageUp) == Utils.Utils.AllTrim(str))
            {    // пользователь переходит на экран назад
                _result = string.Empty;
                return GetPreviousMenu();
            }
            
            // пользователь выбрал какой то из пунктов меню
            try
            {
                int.Parse(str);
            }
            catch
            {
                _result = string.Empty;
                return string.Empty;
            }

            if (int.Parse(str) >= 1 && int.Parse(str) <= _listmenuCount)
            {
                _result = GetValueById(int.Parse(str));

                // Определяем текущие параметры для глобального кеша
                GetCurrentDataTable(int.Parse(str));
                return _result;
            }
            
            _result = string.Empty;
            return string.Empty;
        }

        #endregion


        #region override OnChange
        public override bool OnChange(string str, ref Dictionary<string, object> results) 
        {
            if (!IsActivate) return true;

            #region Обработка контролов типа лист
            string rez = ParseString(str);
            // Пользователь ввел какую-то фигню
            if (ControlView == string.Empty && rez == string.Empty)  { ParentForm.CurrentState = FormState.InValid; }
            // Пользователь выбрал какой-то пункт, надо возвращать результат на форму
            else if (ControlView != string.Empty && str != string.Empty)
            {
                // Добавляем параметр в общую кучу параметров
                if (!results.ContainsKey(ValueID.ToUpperInvariant()))
                    results.Add(ValueID, CurrentRow);
                else
                    results[ValueID.ToUpperInvariant()] = CurrentRow;

                ParentForm.ResultFromChild = rez;

                ParentForm.CurrentState = FormState.Valid;
                return true;
            }
            // Пользователь пролиснул экран (нажал какой-то управляющий контрол)
            else
            {
                ParentForm.CurrentState = FormState.InValid;
                return false;
            }
            #endregion

            return true;
        }
        #endregion


        #region override Clear
        public override void Clear() { Value = new List<object[]>(); IsActivate = false; }
        #endregion


        #region override InitControlFromXML
        public override void InitControlFromXML(System.Xml.XmlNode node)
        {
            // Определяем параметры по умолчванию для контрола

            base.InitControlFromXML(node);

            // Добавляем параметры к событию
            if (node.HasChildNodes)
            {
                #region Цикл по параметрам
                foreach (XmlNode prms in node.ChildNodes)
                {
                    if (prms.NodeType == XmlNodeType.Element)
                    {
                        switch (Utils.Utils.AllTrim(prms.Name.ToUpperInvariant()))
                        {
                            case "OPTION":
                                DataRow dr = _dt.NewRow();
                                dr[0] = Utils.Utils.AllTrim(prms.Attributes["id"].Value);
                                dr[1] = Utils.Utils.AllTrim(prms.Attributes["value"].Value);
                                _dt.Rows.Add(dr);

                                break;
                        }
                    }
                }

                if (_dt.Rows.Count > 0)
                    FillStaticValue(_dt);

                #endregion
            }
        }
        #endregion


        #region override ToString
        /// <summary>
        /// Возвращает текущее содержимое контрола
        /// </summary>
        /// <returns></returns>
        public override string[] ToString(Dictionary<string, object> cache)
        {
            if(!IsVisible(cache))
                return new string[] { "" };

            // Проверка на то что нет события инициализации для 
            // список
            // ТОгда добавляем статичные данные если они есть
            if (_source.Count == 0)
                _source = _static;

            char[] str = Environment.NewLine.ToCharArray();
            return GetCurrentList(0).Split(str);
        }
    
        #endregion


        #region override ToHtml
        public override string[] ToHtml(Dictionary<string, object> cache)
        {
            if (!IsVisible(cache))
                return new string[] { "" };

            // Проверка на то что нет события инициализации для 
            // список
            // ТОгда добавляем статичные данные если они есть
            if (_source.Count == 0)
                _source = _static;


            int j = 1;
            int currline = 0;
            // устанавливаем курсор на первую строку
            string rez = string.Empty;

            StringBuilder html = new StringBuilder();

            foreach (object[] currList in _source)
            {
                // Проверка на то что пользователь находится на первой странице
                if (_page <= 0 && currline < _listmenuCount)
                {
                    html.Append("<div><input type=\"button\" class=\"list_menuitem\" onclick=\"Menu_Click('" + Utils.Utils.AllTrim((currline + 1).ToString()) + "');\" name=\"list" + Utils.Utils.AllTrim((currline + 1).ToString()) + "\" value=\"" + Utils.Utils.AllTrim((currline + 1).ToString()) + ". " + Utils.Utils.AllTrim(currList[1].ToString()) + "\"/></div>" + Environment.NewLine);
                    _page = 0;
                    currline++;
                }
                else if (_page <= 0) break;

                // Пользователь находится на следующей странице
                if (
                    currline < (_listmenuCount)
                    && j > (_page * _listmenuCount)
                    && j < (_page * _listmenuCount + _listmenuCount + 2)
                    && _page > 0
                    )
                {
                    html.Append("<div><input type=\"button\" class=\"list_menuitem\" onclick=\"Menu_Click('" + Utils.Utils.AllTrim((currline + 1).ToString()) + "');\" name=\"list" + Utils.Utils.AllTrim((currline + 1).ToString()) + "\" value=\"" + Utils.Utils.AllTrim((currline + 1).ToString()) + ". " + Utils.Utils.AllTrim(currList[1].ToString()) + "\"/></div>" + Environment.NewLine);
                    currline++;
                }
                else if (_page > 0 && currline > _listmenuCount) break;
                j++;
            }

            // Определяем общее количество страниц в меню
            int linesal = (int)(Math.Ceiling(((double)_source.Count / (double)_listmenuCount)));

            rez += (linesal > 1 ? Environment.NewLine + "       " + (_page + 1).ToString() + "/" + linesal.ToString() : string.Empty);

            html.Append("<div class='list_iteminput'><input type='text' value='' size='2' width='10' class='menu_input' name='user' " + OheConstant.WEB_ONCLICK_EVENT + "></div>");

            if (linesal > 1)
                html.Append("<div align='justify'>" + (_page + 1).ToString() + "/" + linesal.ToString() + "</div>");

            return html.ToString().Split(Environment.NewLine.ToCharArray()[0]);

        }
        #endregion

        #endregion
    }
}
