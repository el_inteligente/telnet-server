﻿using System;
using System.Collections.Generic;
using System.Text;
using Ohe.Telnet.Configuration;
using Ohe.Utils;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Описывает сообщения которые пришли на форму
    /// </summary>
    public class RfControlMessage : RfControl
    {

        #region Constructors

        public RfControlMessage(RfForm parentForm, RfControl parentControl)
            : base(parentForm, parentControl)
        {
            ValueID = "SYS_MSG";
        }
        
        #endregion


        #region override Methods

        #region override ToString

        public override string[] ToString(Dictionary<string, object> cache)
        {
            string rez = string.Format(Value, Environment.NewLine);

            char[] str = Environment.NewLine.ToCharArray();

            return rez.Split(str);
        }


        public string ToString(OheConnectionType connectionType)
        {
            
            if (connectionType != OheConnectionType.Web)
                return string.Format(OheConstant.TELNET_CLEAR_SEQ + Value + (string.IsNullOrEmpty(Value) || ParentForm.IsIf ?
                                                                            "" : TlnConfigManager.BEEP), Environment.NewLine);
            
            StringBuilder str = new StringBuilder();
            str.Append("<div class=\"form\">");
            str.Append("<input type=\"hidden\" name=\"ref\" value=\"" + Id.ToString() + "\">");
            str.Append("<table cellpadding=\"1\" cellspacing=\"0\">");
            str.AppendLine("<tr><td><input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='Выход'></td></tr>");
            str.AppendLine("<tr><td class='message'>" + string.Format(Value, "<br>") + "</td></tr>");
            str.AppendLine("<tr><td><input class=\"input\" type=\"text\"></td></tr>");
            str.AppendLine("</table>");
            str.AppendLine("</div>");

            return str.ToString();
        }

        #endregion


        #region override ToHtml
        /// <summary>
        /// Возвращает Html представление контрола
        /// </summary>
        /// <param name="cache"></param>
        /// <returns></returns>
        public override string[] ToHtml(Dictionary<string, object> cache)
        {
            string rez = string.Format(Value, Environment.NewLine);

            if (Utils.Utils.isEmpty(rez))
                return new string[] { "" };

            string [] result = rez.Split(Environment.NewLine.ToCharArray()[0]);

            for(int i =0;i<result.Length;i++)
            {
                result[i] = "<div class='message' ><b>" + result[i] + "</b></div>";
            }

            return result;
        }
        #endregion


        #endregion

    }
}
