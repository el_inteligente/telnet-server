﻿using System;
using System.Xml;
using System.Collections.Generic;
using Ohe.Telnet.Exception;
using Ohe.Utils;
using Ohe.Telnet.Configuration;
using System.ComponentModel;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Описывает контрол textbox 
    /// </summary>
    public abstract class RfFormBase : IDisposable, IComparable
    {
        #region Properties

        private bool _dispose = false; // переменная отслеживает что класс не существует
        private Component component = new Component();

        public int Id { get; set; }

        /// <summary>
        /// Взвращает идентификатор экрана верхнего уровня
        /// </summary>
        public int Parent { get; set; }

        private readonly RfForm _parentForm;
        public RfForm ParentForm { get { return _parentForm; } }

        private readonly RfControl _parentControl;
        public RfControl ParentControl { get { return _parentControl; } }

        /// <summary>
        /// Идентифактор формы перехода
        /// </summary>
        public int Target { get; set; }

        /// <summary>
        /// Описание формы
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Значение контрола
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Ширина экрана
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Привилегия пользователя
        /// </summary>
        public string Permission { get; set; }

        /// <summary>
        /// Лицензия на использование контрола
        /// </summary>
        public string License { get; set; }

        /// <summary>
        /// Ссылка на прверку глобального кеша
        /// </summary>
        public string CheckValue { get; set; }

        /// <summary>
        /// Активна ли форма или контрол
        /// </summary>
        public bool IsActivate { get; set; }

        private Dictionary<string, object> _cashe = new Dictionary<string, object>();
        public Dictionary<string, object> Cashe { get { return _cashe; } }

        /// <summary>
        /// Определяет тип контрола
        /// Поддерживает следующие типы - Text, InputBox, Param, ControlKey 
        /// </summary>
        public OheConstant.OheTelnetControlType OheControlType { get; set; }

        #region Event scripts

        private RfControlEvents _onInitEventScript;
        public RfControlEvents OnControlInitEvent { get { return _onInitEventScript; } set { _onInitEventScript = value; } }

        private RfControlEvents _onExitEventScript;
        public RfControlEvents OnControlExitEvent { get { return _onExitEventScript; } set { _onExitEventScript = value; } }

        private RfControlEvents _onChangeEventScript;
        public RfControlEvents OnControlChangeEvent { get { return _onChangeEventScript; } set { _onChangeEventScript = value; } }

        private RfControlEvents _onSubmitEventScript;
        public RfControlEvents OnConrolSubmitEvent { get { return _onSubmitEventScript; } set { _onSubmitEventScript = value; } }
        
        #endregion

        #endregion


        #region Events
        public virtual bool OnInit() { return true; }

        public virtual bool OnExit() { return true; }

        public virtual bool OnSubmit() { return true; }

        public virtual bool OnChange(string str, ref Dictionary<string,object> results) { return true; }
        #endregion


        #region Constructors
        protected RfFormBase()
        {
            CheckValue = string.Empty;
            Value = string.Empty;
            Width = 0;
            IsActivate = false;
            OheControlType = OheConstant.OheTelnetControlType.Form;
            Parent = 0;
        }

        public RfFormBase(RfForm parentFrom)
        {
            CheckValue = string.Empty;
            Value = string.Empty;
            Width = 0;
            IsActivate = false;
            OheControlType = OheConstant.OheTelnetControlType.Form;
            Parent = 0;
            _parentForm = parentFrom;
        }

        public RfFormBase(RfForm parentForm, RfControl parentControl) : this(parentForm) 
        { 
            _parentControl = parentControl; 
        }

        public RfFormBase(Dictionary<string, object> Cashe)
        {
            CheckValue = string.Empty;
            Value = string.Empty;
            Width = 0;
            IsActivate = false;
            OheControlType = OheConstant.OheTelnetControlType.Form;
            Parent = 0;
            _cashe = Cashe;
        }

        #endregion


        #region Methods
        public virtual string [] ToString(Dictionary<string, object> cache)
        {
            return new string[]{string.Empty};
        }

        /// <summary>
        /// Возвращает доступен ли контрол для показа
        /// </summary>
        /// <param name="cache"></param>
        /// <returns></returns>
        public bool IsVisible(Dictionary<string, object> cache)
        {
            return Utils.Utils.GetParamValue(Utils.Utils.GetObjName(CheckValue)
                                             , Utils.Utils.GetObjColumn(CheckValue)
                                             , cache) != "N";
        }

        #region CenterString - выравнивает строку по горизонтали
        /// <summary>
        /// Выравнивает строку по центру
        /// </summary>
        /// <returns></returns>
        public string CenterString(string str)
        {

            int width = TlnConfigManager.TELNET_WIDTH;
            int heigth = TlnConfigManager.TELNET_HEIGHT;
            if (str.Length > width || str.Length == 0)
                return str;

            int center = (int)Math.Round((decimal)width / 2, 0);
            int width2 = (int)Math.Round((decimal)str.Length / 2, 0);

            string rez = string.Empty;
            for (int line = 1; line < center - width2; line++)
                rez += " ";

            rez += str + "                                      ".Substring(0, (width - (rez + str).Length > 0 ? width - (rez + str).Length : 1));

            return rez;
        }
        #endregion


        public virtual void Clear() { Value = string.Empty; IsActivate = false; }

        /// <summary>
        /// Добавляет скрипт события с к контролу
        /// </summary>
        /// <param name="Event"></param>
        public virtual void AddEvent(RfControlEvents Event) 
        {
            switch (Event.Event)
            { 
                case "OnInit":
                    _onInitEventScript = Event;
                    break;
                case "OnExit":
                    _onExitEventScript = Event;
                    break;
                case "OnChange":
                    _onChangeEventScript = Event;
                    break;
                case "OnSubmit":
                    _onSubmitEventScript = Event;
                    break;

                default:
                    throw new System.Exception("неверное значение события.");
            }

        }

        //public virtual void AddControl(RfFormBase Control) { }

        public virtual void InitControlFromXML(XmlNode node)
        {
            #region парсинг атрибутов
            foreach (XmlAttribute attr in node.Attributes)
            {
                #region Switch
                switch (Utils.Utils.AllTrim(attr.Name.ToUpperInvariant()))
                {
                    case "ID":
                        #region line
                        try
                        {
                            if(this is RfForm)
                                Id = int.Parse(attr.Value);
                        }
                        catch (FormatException ex)
                        {
                            throw new ConfigurationException(ex,
                                              "Ошибка: неверный тип параметра"
                                              + Environment.NewLine
                                              + "Идентификатор формы: " + Parent
                                              + Environment.NewLine
                                              + (Id == 0 ? string.Empty : "Идентификатор Контрола: " + Id)
                                              );
                        }
                        #endregion
                        break;
                    case "TARGET":
                        try
                        {
                            Target = int.Parse(attr.Value);
                        }
                        catch (FormatException ex)
                        {
                            throw new ConfigurationException(ex, "Ошибка: Неверный формат параметра формы", attr.Value);
                            }
                        break;
                    case "PERMISS":
                        Permission = Utils.Utils.AllTrim(attr.Value.ToUpperInvariant());
                        break;
                    case "PARENT":
                        try
                        {
                            Parent = int.Parse(attr.Value);
                        }
                        catch (FormatException ex)
                        {
                            throw new ConfigurationException(ex, "Ошибка: Неверный формат параметра формы", attr.Value);
                        }
                        break;
                    case "TITLE":
                        Description = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "DESCRIPT":
                        Description = Utils.Utils.AllTrim(attr.Value.Replace("я", "Я"));
                        break;
                    case "CHECKVALUE":
                        CheckValue = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "VALUE":
                        Value = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "CONTROLTYPE":
                        #region Controltype
                        string controltype = Utils.Utils.AllTrim(attr.Value.ToUpperInvariant());

                        switch (controltype)
                        {
                            case "TEXT":
                                OheControlType = OheConstant.OheTelnetControlType.Text;
                                break;
                            case "INPUT":
                                OheControlType = OheConstant.OheTelnetControlType.InputBox;
                                break;
                            case "LIST":
                                OheControlType = OheConstant.OheTelnetControlType.List;
                                break;
                            case "FUNCTION":
                                OheControlType = OheConstant.OheTelnetControlType.ControlKey;
                                break;
                            case "HELP":
                                OheControlType = OheConstant.OheTelnetControlType.Help;
                                break;
                            case "MESSAGE":
                                OheControlType = OheConstant.OheTelnetControlType.Message;
                                break;
                        }
                        
                        #endregion
                        break;

                }
                #endregion
            }
            #endregion

            #region Добавление событий форм и контролов
            foreach (XmlNode opt in node.ChildNodes)
            {
                if (opt.NodeType == XmlNodeType.Element)
                {
                    // Форма поддерживает только два события
                    switch (Utils.Utils.AllTrim(opt.Name.ToUpperInvariant()))
                    {
                        case "INIT":
                            OnControlInitEvent = new RfControlEvents(ParentForm, this);
                            OnControlInitEvent.InitControlFromXML(opt);
                            break;
                        case "SUBMIT":
                            OnConrolSubmitEvent = new RfControlEvents(ParentForm,this);
                            OnConrolSubmitEvent.InitControlFromXML(opt);
                            break;
                        case "EXIT":
                            OnControlExitEvent = new RfControlEvents(ParentForm,this);
                            OnControlExitEvent.InitControlFromXML(opt);
                            break;
                        case "CHANGE":
                            OnControlChangeEvent = new RfControlEvents(ParentForm,this);
                            OnControlChangeEvent.InitControlFromXML(opt);
                            break;
                    }
                }
            }
            #endregion

        }


        #region IComparable.CompareTo(object o)
        /// <summary>
        /// Интерфейс сортировки контролов
        /// </summary>
        /// <param name="o">Объект для сравнения</param>
        /// <returns>int IComparable</returns>
        int IComparable.CompareTo(object o)
        {
            RfFormBase cnl = (RfFormBase)o;
            if (Id > cnl.Id)
                return 1;
            if (Id < cnl.Id)
                return -1;

            return 0;
        }

        /// <summary>
        /// Интерфейс сортировки контролов
        /// </summary>
        /// <param name="o">Объект для сравнения</param>
        /// <returns>int IComparable</returns>
        public int CompareTo(RfFormBase o)
        {
            if (Id > o.Id)
                return 1;
            if (Id < o.Id)
                return -1;
            else
                return 0;
        }
        #endregion


        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool Dispose(bool dis)
        {
            if (dis)
                component.Dispose();
            _dispose = true;

            return _dispose;
        }

        #endregion
        
        #endregion

    }
}
