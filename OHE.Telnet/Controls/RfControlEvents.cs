﻿using System;
using System.Linq;
using System.Xml;
using System.Collections.Generic;
using Ohe.Telnet.Exception;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Описывает события которые могут происходить с формой или контролом
    /// </summary>
    public class RfControlEvents
    {

        #region Define Variables
		private readonly int _formId;  // идентификатор формы
        private readonly int _cnlId;   // идентификатор контрола для которого происходит событие
        private readonly string _name; // имя события
        private string _spName; // имя хранимой процедуры которую надо вызвать
        private List<RfControlEventsParams> _lParams = new List<RfControlEventsParams>(); // список параметров для запуска хранимой процедуры
        private readonly RfFormBase _parentForm;
        private readonly RfFormBase _parentControl;
    	#endregion
    
        #region Properties
        /// <summary>
        /// Идентификатор формы
        /// </summary>
        public int FormId { get { return _formId; } }

        /// <summary>
        /// Идентификатор контрола для которого происходит событие
        /// </summary>
        public int ControlId { get { return _cnlId; } }

        /// <summary>
        /// Имя события
        /// </summary>
        public string Event { get { return _name; } }

        /// <summary>
        /// Возвращает список параметров для выполнения события
        /// </summary>
        public List<RfControlEventsParams> Params
        {
            get { return _lParams; }
            set { _lParams = value; }
        }

        /// <summary>
        /// Возвращает имя хранимой процедуры
        /// </summary>
        public string SpName { get { return _spName; } }

		private string _tableId = string.Empty;
		/// <summary>
		/// Возвращает идентификатор таблицы, в которую заливаются
		/// параметры при выполнении события
		/// </summary>
		public string TableId { get { return _tableId; } set { _tableId = value.ToUpperInvariant(); } }

        public RfFormBase ParentForm { get { return _parentForm; } }

        public RfFormBase ParentControl { get { return _parentControl; } }
        #endregion 


        #region Constructors

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="formId">Идентификатор формы</param>
        /// <param name="cnlId">Идентификатор контрола Id которому принадлежит событие</param>
        /// <param name="name">Название события OnInit,OnChange</param>
        /// <param name="spName">Имя хранимой процедуры</param>
        public RfControlEvents(int formId, int cnlId, string name, string spName)
        {
            _formId = formId;
            _cnlId = cnlId;
            _name = name;
            _spName = spName;
        }

        public RfControlEvents()   { _lParams = new List<RfControlEventsParams>(); }

        public RfControlEvents(RfFormBase parent, RfFormBase parentControl)
        {
            _parentForm = parent;
            _parentControl = parentControl;
        }

        #endregion


        #region Methods

        /// <summary>
        /// Добавляет параметр в класс
        /// </summary>
        /// <param name="rf"></param>
        public void AddParametr(RfControlEventsParams rf)
        {
            _lParams.Add(rf);
        }
        
        public void InitControlFromXML(XmlNode node)
        {
            string eventName = node.Name;

            foreach (XmlAttribute attr in node.Attributes)
            {
                switch (Utils.Utils.AllTrim(attr.Name.ToUpperInvariant()))
                {
                    case "SPNAME":
                        _spName = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "ID":
                        _tableId = Utils.Utils.AllTrim(attr.Value);
                        break;
                }
            }

            if(_spName == null)
                throw new ConfigurationException("Ошибка:Нет хранимой процедуры"
                                       + Environment.NewLine
                                       + "Идентификатор формы: " + ParentForm.Id.ToString()
                                       + Environment.NewLine
                                       + Environment.NewLine
                                       + "Событие: " + eventName
                                      );

            // параметр хранимой процедуры
            RfControlEventsParams pr;
            // Добавляем параметры к событию
            if (node.HasChildNodes)
                #region Цикл по параметрам
                foreach (XmlNode prms in node.ChildNodes.Cast<XmlNode>().Where(prms => Utils.Utils.AllTrim(prms.Name.ToUpperInvariant()) == "PARAMS"
                                                                                       && prms.NodeType == XmlNodeType.Element))
                {
                    pr = new RfControlEventsParams(this);
                    pr.InitControlFromXML(prms);
                    // Добавляем параметр к хранимой процедуре
                    _lParams.Add(pr);
                }
                #endregion

        }
        #endregion


    }
}
