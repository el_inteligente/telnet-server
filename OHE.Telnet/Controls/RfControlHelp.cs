﻿using System;
using System.Collections.Generic;
using System.Text;
using Ohe.Utils;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Описывает контрол для помощи на экране
    /// </summary>
    public class RfControlHelp : RfControl
    {

        #region Constructors

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id">Идентификатор контрола</param>
        /// <param name="parent">Принадлежность к форме</param>
        /// <param name="oheTelnetControlType">Тип контрола</param>
        /// <param name="value">Значение контрола</param>
        public RfControlHelp(int id, int parent, OheConstant.OheTelnetControlType oheTelnetControlType, string value)
        {
            Id = id;
            Parent = parent;
            OheControlType = oheTelnetControlType;
            Description = string.Empty;
            Value = value;
            X = 0;
            Y = 0;
            ReadOnly = true;
        }

        public RfControlHelp(RfForm parentForm, RfControl parentControl) : base(parentForm, parentControl) { }
    
        #endregion


        #region Methods override


        #region override ToString

        public string ToString(OheConnectionType connectionType) 
        {
            if (connectionType != OheConnectionType.Web)
                return string.Format(OheConstant.TELNET_CLEAR_SEQ + Value, Environment.NewLine);

            StringBuilder str = new StringBuilder();
            str.Append("<div class=\"form\">");
            str.Append("<input type=\"hidden\" name=\"ref\" value=\"" + Id.ToString() + "\">");
            str.Append("<table class=\"form\" cellpadding=\"1\" cellspacing=\"0\">");
            str.AppendLine("<tr><td><input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='Выход'></td></tr>");
            str.AppendLine("<tr><td class=\"help\">" + string.Format(Value, "<br>") + "</td></tr>");
            str.AppendLine("</table>");
            str.AppendLine("</div>");

            return str.ToString();
        }

        #endregion


        #region override ToHtml
        public override string[] ToHtml(Dictionary<string, object> cache)
        {
            return new string[] {"<div class=\"help\">" + string.Format(Value,"<br>") + "</div>"};
        }
        #endregion

        #endregion
    }
}
