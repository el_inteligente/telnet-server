﻿namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Описывает пункт меню пользователя терминала
    /// </summary>
    public class Menu
    {

        #region Define Variables
        private int _id;  // Имя меню которое отображается на экране
        private int _targetId; // Определяет идентификатор экрана (меню) куда надо идти
        private int _pаrentId; // Определяет вложенность меню для основного 0
        private string _name;  // Имя меню которое отображается на экране
        //private string _type;  // Определяет тип (меню или форма)
        private string _permiss; // привелегия для меню
        //private List<UsersMenu> _lhead = new List<UsersMenu>();

        #endregion


        #region Properties
        /// <summary>
        /// Возвращает название меню как будет видется на терминале
        /// </summary>
        public string Name {  get { return _name; } }
        
        /// <summary>
        /// Возвращает идентификатор меню
        /// </summary>
        public int MenuID { get { return _id; } }

        /// <summary>
        /// ВОзвращает тип меню (меню или форма)
        /// </summary>
        //public string Type { get { return _type; } }

        /// <summary>
        /// Возвращает идентификатор определяющий вложенность меню
        /// </summary>
        public int ParentId { get { return _pаrentId; } set { _pаrentId = value; } }

        /// <summary>
        /// Возвращает идентификатор куда надо перейти с этого меню
        /// </summary>
        public int TargetId { get { return _targetId; } }

        /// <summary>
        /// Возвращает привилегию меню
        /// </summary>
        public string Permiss { get { return _permiss; } }

        #endregion


        #region Constructors
        /// <summary>
        /// Тут генерируем все пункты меню которые возможны
        /// </summary>
        public Menu() {  }



        #region Params Constructor
        /// <summary>
        /// 
        /// Класс создает определенное меню
        /// </summary>
        /// <param name="i">Идентификатор меню</param>
        /// <param name="n">Название меню - отображается на экране</param>
        /// <param name="p">Привилегия для меню</param>
        /// <param name="tar">Определяет идентификатор экрана на который надо идти</param>
        /// <param name="parent">Определяет идентификатор родительского объекта</param>
        /// <param name="type">Тип меню</param>
        public Menu(int i, string n, string p, int tar, int parent)//, string type)
        {
            _id = i;
            _name = n;
            _permiss = p;
            _targetId = tar;
            _pаrentId = parent;
            //_type = type;
        }
        
        #endregion

        #endregion

    }
}
