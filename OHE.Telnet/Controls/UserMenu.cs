//#define NOTIFY
using System;
using System.Collections.Generic;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Text;
using Ohe.Telnet.Configuration;
using Ohe.Utils;
//using Ohe.Telnet.Utils;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// ��������� ������ ���� ��������� ������������ 
    /// ����� ��������� ������ � �������� ����
    /// </summary>
    public partial class UserMenu : Menu
    {
        #region Properties

        private readonly List<Menu> _lhead = new List<Menu>();
        private int _page;       // ������� �������� ���� �� ������� ��������� ������������
        private readonly int _linesMenu;  // ���������� ����� ���������� ��� ����
        private int _parent;     // ������� ����������� ����
        private RfControlHelp _help; // ���������� ������� � ���� ������
        private int _currentTarget; // ���������� ������� ������ �� ��������� ������� ��� �������
        private OheConnectionType _connectionType = OheConnectionType.HandHeld; // ��� ���������� ������������

        /// <summary>
        /// ���������� ������� ������ �� ��������� ������� ��� �������, ������� ������ ������������
        /// </summary>
        public int CurrentTarget { get { return _currentTarget; } set { _currentTarget = value; } }

        /// <summary>
        /// ���������� ������� �������� ����������� ����
        /// </summary>
        public int EscMenu { get { return _parent; } set { _parent = value; } }

        /// <summary>
        /// ������� ��� ���������� ������������
        /// </summary>
        public OheConnectionType OheConnectionType { get { return _connectionType; } set { _connectionType = value; } }


        /// <summary>
        /// ����� ������������, �������� ����������� ����
        /// </summary>
        public string UserId { get; set; }

        #endregion


        #region Constructor
        /// <summary>
        /// ��� ���������� ��� ������ ���� ������� �������� ������������ �� ����������
        /// </summary>
        public UserMenu(List<Menu> menu, int menuLines)
        {
            _lhead = menu;
            _page = 0;
            _linesMenu = menuLines;
            _parent = 0;
            _help = new RfControlHelp(0, 0, OheConstant.OheTelnetControlType.Text,
                         CenterString("������") + Environment.NewLine
                         + CenterString("��� ���������") + Environment.NewLine
                         + CenterString("����������� �������:") + Environment.NewLine
                         + "�������� ������: " + TlnConfigManager.TELNET_MENU_DOWN + " " + Environment.NewLine
                         + "�������� �����:  " 
                         + TlnConfigManager.TELNET_MENU_UP 
                         + Environment.NewLine
                         + Environment.NewLine
                         + "����� ����: ����.1-" + Utils.Utils.AllTrim(menuLines.ToString(CultureInfo.InvariantCulture))
                         );
        }

        /// <summary>
        /// ����������� ����
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="menuLines"></param>
        /// <param name="userId"></param>
        public UserMenu(List<Menu> menu, int menuLines, string userId) : this(menu, menuLines)
        {
            UserId = userId;
        }

        #region Not Use
        /// <summary>
        /// ����������� ��� ����� ������ ���� �� ������������
        /// </summary>
        /// <param name="menuLines">�������� ���������� ����� ����</param>
        /// 
        /*
        public UserMenu(int menuLines)
        {
            _linesMenu = menuLines;
            // �������������� �������� ���� ����� � ������ ����� 0
            // ��������� �������� ���������� ������������ ���� � ������
            // ��������� �� ������������� ���� - ������ ��������
            // ����������� ���� ����� ���� ��������������!!!!!!!!!
            // ��� �������� ���� �������� 4 (_parent) ������ ���� ����� -1
            AddControl(new Menu(0, "�����", "AB", 2, -1, "menu"));
            AddControl(new Menu(0, "C����", "AC", 1, -1, "menu"));
            AddControl(new Menu(0, "�������", "AC", 22, -1, "screen"));
            AddControl(new Menu(0, "��������������", "A", 2, -1, "screen"));
            #region Not Use

            AddControl(new Menu(1, "��� � ������", "AC", 30, 0, "screen"));
            AddControl(new Menu(1, "�����������", "AC", 13, 0, "menu"));
            AddControl(new Menu(1, "����������", "A", 13, 0, "menu"));
            AddControl(new Menu(1, "������", "A", 3, 0, "menu"));
            AddControl(new Menu(1, "���������� �����", "A", 15, 0, "menu"));

            AddControl(new Menu(2, "�� ������", "AB",5, 0, "menu"));
            AddControl(new Menu(2, "�� ��������", "A", 4, 0, "menu"));
            AddControl(new Menu(2, "BATCH �����", "A", 5, 0, "menu"));

            AddControl(new Menu(3, "�� ������", "A", 6, 0, "menu"));
            AddControl(new Menu(3, "�� ���������", "A", 7, 0, "menu"));
            AddControl(new Menu(3, "������", "A", 8, 0, "screen"));
            AddControl(new Menu(3, "�� ����", "A", 9, 0, "menu"));
            #endregion  

            //AddControl(new Menu(21, "������� ������", "A", 25, 1, "screen"));
            //AddControl(new Menu(23, "Bla", "A", 22, 0, "screen"));

            //AddControl(new RfControlHelp(0, 0, "Text", string.Empty, "�������� ���� ���������", 0, 0));
            //ArrayList a = new ArrayList();
            //a.Add(new RfControlHelp(0, 0, "Text", string.Empty, "�������� ���� ���������", 0, 0));
            //AddControl(a);
        }
        */

        #endregion
        #endregion

        #region Methods

        #region GetCurrentMenuPermiss2 - ���������� ������ ����(������� �� �����������)
        /// <summary>
        /// ���������� ������ �������� ���� ��� ������ ������������ �� ������� ��������
        /// </summary>
        /// <returns>String false - ������������ ����� �� �������� ����, 
        ///                 screen - ������������ ������� �� ���� �� ������,
        ///                          ���� ��������� �������
        /// </returns>
        public string GetCurrentMenuPermiss2()
        {
            // ���������� ��, ��� ������������ ������������
            if (_parent < 0)
                return "false";

            // �������� �� �� ��� ������������ ��� �� � ���� � �� ������ ��������
            if (GetBoolMenuPermiss() == 0)
                        return "screen";

            // ���������� ���� Html ���� � ��� ��� ���������� WEB
            if (_connectionType == OheConnectionType.Web)
                return GetXmlMenu(); // GetXmlMenu();


            // ������������� ������ �� ������ ������
            StringBuilder  result = new StringBuilder();
            result.Append(OheConstant.TELNET_CLEAR_SEQ);

#if NOTIFY
            OheTelnetNotifyClient notify = new OheTelnetNotifyClient(UserId);
            result.Append(notify.AllTask());
#endif

            result.AppendLine(Utils.Utils.TELNET_HEAD_MENUTOP(TlnConfigManager.TELNET_HELP_STR) != string.Empty ? Utils.Utils.TELNET_HEAD_MENUTOP(TlnConfigManager.TELNET_HELP_STR) : string.Empty);

            int i = 1;   // ����������� ������ ���� �� ������� ��������
            int j = 1; // 
            int countMenu = _lhead.Count(s => s.MenuID == _parent); // �������� ���������� ���� ��� ������������� ��������������

            // ��������� ����� ���������� ���� ����������� �����������

            #region cc ������� �������� ���� � ������ �� ��������
            // �� ����� ���������� ���� � ������
            if (_page > 0)
            {
                if (countMenu / ((_page) * _linesMenu) < 1)   // ���� ��� ���� ��� �������� ������
                    _page = 0;
            }
            // ��������� ���� ������������ ��������� 6 ��������� ����
            foreach (Menu s in _lhead)
            {
                if (i < (_linesMenu + 1) && j > (_page * _linesMenu) && j < (_page * _linesMenu + _linesMenu + 2) && s.MenuID == _parent)
                {
                    result.AppendLine(Utils.Utils.AllTrim(i.ToString()) + ". " + Utils.Utils.AllTrim(s.Name));
                    i++;
                }

                if (s.MenuID == _parent)
                    j++;
            }
            // ��������� ������� �������� ���� ���� �� �����
            while (i < (_linesMenu + 1))
            {
                result.AppendLine();// += Environment.NewLine;
                i++;
            }

            // ���������� ����� ���������� ������� � ����
            int linesal = (int)(Math.Ceiling(((double)countMenu / (double)_linesMenu)));

            string r = (_page + 1).ToString() + "/" + linesal.ToString();//(j-1).ToString();//_LheadMenu.Count.ToString();

            #endregion

            // ���������� ���� ��������� ������ ���� ���������� ������� ������ 1
            return result.ToString() + (linesal > 1 ? r + TlnConfigManager.TELNET_HEAD_MENUBOT : string.Empty) + GetCursorPosition();
        }
        #endregion

        /// <summary>
        /// ���������� ������������� ��� ���� � ������� html
        /// </summary>
        /// <returns></returns>
        private string GetXmlMenu()
        {
            StringBuilder html = new StringBuilder();

            /*
            <div data-theme="b" data-role="header" data-position="fixed">
                <a data-role="button" data-direction="reverse" href="#page1" data-icon="home"
                data-iconpos="left" class="ui-btn-right">
                    �����
                </a>
                <h3>
                    �������
                </h3>
            </div>
            */

            html.Append("<div class=\"form\">");
            html.Append("<table cellspacing='0' cellpadding='1' class='form' align='left'>");
            html.Append("<input name='ref' value='menu' type='hidden'>");
            // ������������� ������ �� ������ ������
            
            int i = 1;   // ����������� ������ ���� �� ������� ��������
            int j = 1; // 
            int linesal; // ���������� ���������� ����� ����

            html.Append(Utils.Utils.TELNET_HEAD_WEBMENUTOP(TlnConfigManager.TELNET_HELP_STR));
            

            // ��������� ����� ���������� ���� ������������ �����������
            int countMenu = _lhead.Count(s => s.MenuID == _parent); // �������� ���������� ���� ��� ������������� ��������������

            if (_page > 0)
            {
                if (countMenu / ((_page) * _linesMenu) < 1)   // ���� ��� ���� ��� �������� ������
                    _page = 0;
            }
            // ��������� ���� ������������ ��������� 6 ��������� ����
            foreach (Menu s in _lhead)
            {
                if (i < (_linesMenu + 1) && j > (_page * _linesMenu) && j < (_page * _linesMenu + _linesMenu + 2) && s.MenuID == _parent)
                {
                    html.Append("<tr><td><input type=\"button\" class='menu_button' onclick=\"Menu_Click('" + Utils.Utils.AllTrim(i.ToString()) + "');\" name=\"menu" + Utils.Utils.AllTrim(i.ToString()) + "\" value=\"" + Utils.Utils.AllTrim(i.ToString()) + ". " + Utils.Utils.AllTrim(s.Name) + "\"/></td></tr>");
                    i++;
                }
                if (s.MenuID == _parent)
                    j++;
            }
            // ��������� ������� �������� ���� ���� �� �����
            while (i < (_linesMenu + 1))
            {
                html.Append("<tr><td>&nbsp;</td></tr>");
                i++;
            }
            html.Append("<tr><td><input type='text' value='' size='2' width='10' class='menu_input' name='user' " + OheConstant.WEB_ONCLICK_EVENT + "></td></tr>");
            // ���������� ����� ���������� ������� � ����
            linesal = (int)(Math.Ceiling(((double)countMenu / (double)_linesMenu)));

            string r = (_page + 1).ToString() + "/" + linesal.ToString();//(j-1).ToString();//_LheadMenu.Count.ToString();

            if(linesal > 1)
                html.Append("<tr><td class='menu_pager'>" + r + TlnConfigManager.TELNET_HEAD_MENUBOT + "</td></tr>");

            html.Append("</table>");
            html.Append("</div>");

            return html.ToString();

        }


        /// <summary>
        /// ���������� ������������� ��� ���� � ������� html ����� ����
        /// </summary>
        /// <returns></returns>
        private string GetHtmlMenu()
        {
            StringBuilder html = new StringBuilder();

            /*
            <div data-theme="b" data-role="header" data-position="fixed">
                <a data-role="button" data-direction="reverse" href="#page1" data-icon="home"
                data-iconpos="left" class="ui-btn-right">
                    �����
                </a>
                <h3>
                    �������
                </h3>
            </div>
            */

            html.AppendLine(@"<div data-theme='b' data-role='header' data-position='fixed'>
                                <a data-role='button' data-direction='reverse' href='#page1' data-icon='home'
                data-iconpos='left' class='ui-btn-right'>
                    �����
                </a>
                <h3>
                    ����
                </h3>
            </div>");

            //html.Append("<table cellspacing='0' cellpadding='1' class='form' align='left'>");
            html.Append("<input name='ref' value='menu' type='hidden'>");
            // ������������� ������ �� ������ ������

            int i = 1;   // ����������� ������ ���� �� ������� ��������
            int j = 1; // 
            int linesal; // ���������� ���������� ����� ����

            //html.Append(Utils.Utils.TELNET_HEAD_WEBMENUTOP(TlnConfigManager.TELNET_HELP_STR));
            html.AppendLine(@"<ul data-role='listview' data-divider-theme='' data-inset='true'>");

            // ��������� ����� ���������� ���� ������������ �����������
            int countMenu = _lhead.Count(s => s.MenuID == _parent); // �������� ���������� ���� ��� ������������� ��������������

            if (_page > 0)
            {
                if (countMenu / ((_page) * _linesMenu) < 1)   // ���� ��� ���� ��� �������� ������
                    _page = 0;
            }
            // ��������� ���� ������������ ��������� 6 ��������� ����
            foreach (Menu s in _lhead)
            {
                if (i < (_linesMenu + 1) && j > (_page * _linesMenu) && j < (_page * _linesMenu + _linesMenu + 2) && s.MenuID == _parent)
                {
                    //html.Append("<tr><td><input type=\"button\" class='menu_button' onclick=\"Menu_Click('" + Utils.Utils.AllTrim(i.ToString()) + "');\" name=\"menu" + Utils.Utils.AllTrim(i.ToString()) + "\" value=\"" + Utils.Utils.AllTrim(i.ToString()) + ". " + Utils.Utils.AllTrim(s.Name) + "\"/></td></tr>");
                    html.AppendLine(@"<li data-theme='c'>
                        <a href='#page1' data-transition='none'>
                            "+ Utils.Utils.AllTrim(i.ToString()) + ". " + Utils.Utils.AllTrim(s.Name) + @"
                        </a>
                    </li>");

                    i++;
                }
                if (s.MenuID == _parent)
                    j++;
            }
            // ��������� ������� �������� ���� ���� �� �����
            /*
            while (i < (_linesMenu + 1))
            {
                html.Append("<tr><td>&nbsp;</td></tr>");
                i++;
            }
            */
            
            html.AppendLine("</ul>");

            html.AppendLine("<input type='text' value='' size='2' width='10' class='menu_input' name='user' " + OheConstant.WEB_ONCLICK_EVENT + ">");

            // ���������� ����� ���������� ������� � ����
            linesal = (int)(Math.Ceiling(((double)countMenu / (double)_linesMenu)));

            string r = (_page + 1).ToString() + "/" + linesal.ToString();//(j-1).ToString();//_LheadMenu.Count.ToString();

            /*
            if (linesal > 1)
                html.Append("<tr><td class='menu_pager'>" + r + TlnConfigManager.TELNET_HEAD_MENUBOT + "</td></tr>");
            */

	        html.AppendLine(@"<div data-theme='b' data-role='footer' data-position='fixed'>
	            <div data-role='navbar' data-iconpos='top'>
		              <ul>
			            <li><a href='a.html' class='ui-btn-active ui-state-persist' data-icon='arrow-l'>����</a></li>
			            <li><a href='a.html' class='ui-btn-active ui-state-persist' data-icon='arrow-r'>����</a></li>
		              </ul>
            	</div>");


            html.Append("</div>");

            return html.ToString();

        }



        #region GetSubMenuOrScreen - ���������� ������ ���� ��� ������
        /// <summary>
        /// ���������� ������������� ���� ������� ������ ������������
        /// </summary>
        /// <param name="Lh">������ ���� ���������� �� ����������</param>
        /// <param name="data">����� ������� ���� ������������ � �������� ���� 999 - ����� �� ����</param>
        /// <returns>������ ���� ��� ������ (������)</returns>
        public string GetSubMenuOrScreen(int data)
        {
            int j = 1;
            // ���� ������������ ����� �� ��������� ��� �� ���,
            // ����� ������� �� ������� ����
            if (!ExistsMenuById(_parent) && data == 999)
            {
                _parent = 0;
                _page = 0;
                return GetCurrentMenuPermiss2();
            }

            foreach (Menu hm in _lhead)
            {
                if (data == 999 && hm.MenuID == _parent)
                {
                    // �� ������������ ������ ������ �� ����� �� ����
                    _parent = hm.ParentId;
                    _page = 0;
                    break;
                }

                if (j == (_page * _linesMenu + data) && hm.MenuID == _parent)
                {
                    // ����� ���� � ���� ���� ���� �������
                    _parent = hm.TargetId;
                    // ���������� �� ����� ����� ��� ������� ���� ������� ������������
                    _currentTarget = hm.TargetId;
                    _page = 0;
                    break;
                }
                if (hm.MenuID == _parent) j++;
            }
                return GetCurrentMenuPermiss2();
        }
        #endregion


        #region GetBoolMenuPermiss - ���������� ��������� �� ������������ �� ������ ��� � ����
        /// <summary>
        /// ���������� ��������� �� ������������ �� ������ ��� � ����
        /// </summary>
        /// <returns>int 1 - ���� ��� ����
        ///              0 - ���� ��� ����� (��� ��������� � ��������)
        ///             -1 - �������� �� ���������� (���� ���� ���������� �����)
        /// </returns>
        public int GetBoolMenuPermiss()
        {
            if (ExistsMenuById(_parent))
                return 1;

            return 0;

            #region Not use
		            /*
            string type = string.Empty;
            // ��������� ����� ���������� ���� ����������� �����������
            foreach (Menu s in _lhead)
            {
                if (s.MenuID == this._parent)
                {
                    if (s.Type == "MENU")
                        return 1;
                    // �� ������� ��� �� ������
                    else if (s.Type == "SCREEN")
                    {
                        _currentTarget = s.TargetId;
                        count++;
                    }
                }
            }
            switch(count)
            {
                case 0:
                    return -1;
                case 1:
                    return 0;
                default: return -1;
            }
             */
 
	        #endregion
        }

        #endregion


        #region MovePriveousPage
        /// <summary>
        /// ��������� ���������� �������� ����
        /// </summary>
        /// <returns>������</returns>
        public string MovePriveousPage()
        {
            if (_page > 0) _page--; else _page = 0;
            return GetCurrentMenuPermiss2();
        }

        #endregion


        #region MoveNextPage
        /// <summary>
        /// ���������� ��������� �������� ����
        /// </summary>
        /// <returns>������</returns>
        public string MoveNextPage()
        {
            _page++;
            return GetCurrentMenuPermiss2();
        }
        #endregion


        #region HasHelpMenu
        /// <summary>
        /// ���������� ����� �� ����� ��������� ��� ������
        /// </summary>
        /// <returns></returns>
        public bool HasHelpMenu()
        {
            if (_help != null) return true;
            return false;
        }

        #endregion


        #region AddControl(object c)
        /// <summary>
        /// ��������� ������� ������ ��� ���� (������ ��� ���� ��� ����� ����)
        /// </summary>
        /// <param name="c"></param>
        public void AddControl(object c)
        {
            if (c is RfControlHelp)
                _help = (RfControlHelp)c;
            if (c is Menu)
                _lhead.Add((Menu)c);
        }

        /// <summary>
        /// ��������� ������ ��������� � ���� (������ ��� ���� ��� ����� ����)
        /// </summary>
        /// <param name="c"></param>
        public void AddControl(ArrayList c)
        {
          foreach (object o in c)
            switch (Utils.Utils.AllTrim(o.GetType().Name))
            {
                case "RfControlHelp":
                    _help = (RfControlHelp)o;
                    break;
                case "Menu":
                    _lhead.Add((Menu)o);
                    break;
            }
        }

        #endregion


        #region GetControlHelp
        /// <summary>
        /// ���������� ������� ������ ��� ����
        /// </summary>
        /// <returns></returns>
        public string GetControlHelp()
        {
            StringBuilder str = new StringBuilder();
            if(_connectionType == OheConnectionType.Web)
            {
            str.Append("<div class=\"form\">");
            str.Append("<input type=\"hidden\" name=\"ref\" value=\"" + this._currentTarget.ToString() + "\">");
            str.Append("<table class=\"form\" cellpadding=\"1\" cellspacing=\"0\">");
            str.AppendLine("<tr><td><input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='�����'></td></tr>");
            str.AppendLine("<tr><td>"+_help.Value+"</td></tr>");
            str.AppendLine("</table>");
            str.AppendLine("</div>");
            }
            else
                str.Append(OheConstant.TELNET_CLEAR_SEQ + _help.Value + OheConstant.SetCursorPosition(0,0));
            
            return str.ToString();
        }
        #endregion

        #region PermissHeadMenu - ���������� ������ ���� � ���� List ��� ������������ ����������
        /// <summary>
        /// ���������� ������ ���� � ���� List ��� ������������ ����������
        /// </summary>
        /// <param name="_perm">���������� ������������</param>
        /// <returns>List</returns>
        public List<Menu> PermissHeadMenu(string perm)
        {
            return _lhead.Where(hd => hd.Permiss.IndexOf(Utils.Utils.AllTrim(perm)) >= 0).ToList();
        }

        #endregion

        /// <summary>
        /// ���������� ���� �� ���� � ������ �� ��������������
        /// </summary>
        /// <param name="id">������������� ����</param>
        /// <returns></returns>
        public bool ExistsMenuById(int id)
        {
            return _lhead.Any(menu => menu.MenuID == id);

            /*
            foreach (Menu menu in _lhead)
            {
                if (menu.MenuID == id)
                    return true;
            }
             */
        }

      #region CenterString - ����������� ������ �� �����������
	  /// <summary>
	  /// ����������� ������ �� ������
	  /// </summary>
	  /// <param name="userInput">������</param>
	  /// <param name="width">������ ������</param>
	  /// <param name="heigth"> ������ ������</param>
	  /// <returns></returns>
	  public string CenterString(string str)
	  {
		  int width = TlnConfigManager.TELNET_WIDTH;
		  int heigth = TlnConfigManager.TELNET_HEIGHT;

		  if (str.Length > width || str.Length == 0)
			  return str;

		  int center = (int)Math.Round((decimal)width / 2, 0);
		  int width2 = (int)Math.Round((decimal)str.Length / 2, 0);

		  string rez = string.Empty;
		  for (int line = 1; line < center - width2; line++)
			  rez += " ";

		  rez += str + "                                      ".Substring(0, (width - (rez + str).Length > 0 ? width - (rez + str).Length : 1));

		  return rez;
	  }
	  #endregion

	  #region ������������� ������ � ������ �������
	  private static string _cursor = string.Empty;
	  /// <summary>
	  /// ���������� � ��������� ������� ������� �������
	  /// </summary>
	  /// <param name="X"></param>
	  /// <param name="Y"></param>
	  /// <param name="cursor"></param>
	  /// <returns></returns>
	  private string GetCursorPosition()
	  {
	      if (_cursor == string.Empty)
		  {
			  _cursor = OheConstant.TELNET_ESC_SEQ
						 + (TlnConfigManager.TELNET_CURSOR_Y != string.Empty ? TlnConfigManager.TELNET_CURSOR_Y.ToString() : TlnConfigManager.TELNET_HEIGHT.ToString())
						 + ";"
						 + (TlnConfigManager.TELNET_CURSOR_X != string.Empty ? TlnConfigManager.TELNET_CURSOR_X.ToString() : (TlnConfigManager.TELNET_WIDTH - 2).ToString()) + "f";
			  return _cursor;
		  }

	      return _cursor;
	  }

        #endregion

      #endregion
    }

}
