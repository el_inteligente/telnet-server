﻿using System.Xml;
using System.Collections.Generic;
using Ohe.Utils;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Описывает контрол - который является функциональной клавишей для
    /// вызова подчиненного экрана
    /// </summary>
    public class RfControlKey : RfControl
    {
        #region Define Variables

            private bool _child;      // определяет, является ли функциональная клавиша,
                                              // клавишей перехода, или результат возвращается в 
                                              // в вызвавшую его форму
        #endregion


        #region Properties
        
            /// <summary>
            /// Определят результат возвращать в родительскую форму или нет
            /// </summary>
            public bool IsChild { get { return _child; } set { _child = value; } }

            #region Old Properties
        //private int _target;      // определяет идентификатор меню куда надо перейти.
        //private string _descript; // строка которая будет показываться на экране
        /// <summary>
        /// Определяет меню куда надо перейти
        /// </summary>
        //public int Target { get { return _target; } set { _target = value; } }

        /// <summary>
        /// Возвращает описание которое будет показано на экране терминала
        /// для отображения управляющих контролов
        /// </summary>
        //public new string Descript { get { return _descript; } set { _descript = value; } }

        #endregion

        #endregion


        #region Constructors

        public RfControlKey(RfForm parentForm, RfControl parentControl)
            : base(parentForm, parentControl)
        {
            _child = false;
        }

           #region Old Constructors
           /*
            /// <summary>
            /// Конструктор класса
            /// </summary>
            /// <param name="Id">Идентификатор контрола (Уникальный) соответствует номеру строки на которой должен появиться</param>
            /// <param name="Parent">Ссылка на элемент верхнего уровня</param>
            /// <param name="Target">Показывает куда надо идти при нажатии клавиши</param>
            /// <param name="Value">Значение контрола</param>
            public RfControlKey(int Id,int Parent, int Target, string Value, string Desc) 
                : base (Id,Parent,Target,Value)
            {
                //base.Target = Target;
                base.Description = Desc;
            }

            public RfControlKey(int Id, int Parent, int Target, string Value, string Desc, bool Child)
                : base(Id, Parent, Target, Value)
            {
                base.Description = Desc;
                _child = Child;
            }

            public RfControlKey()  { }
            */
           #endregion
        #endregion


        #region Methods

          #region override Clear

          /// <summary>
          /// Ошищает контрол от данных
          /// </summary>
    	  public override void Clear() {
    	  }

          #endregion


          #region override InitControlFromXML
          /// <summary>
          /// Инициализация контрола из XML
          /// </summary>
          /// <param name="node"></param>
          public override void InitControlFromXML(XmlNode node)
          {
              base.InitControlFromXML(node);

              foreach (XmlAttribute attr in node.Attributes)
              {
                  switch (Utils.Utils.AllTrim(attr.Name.ToUpperInvariant()))
                  {
                      case "CHILD":
                          _child = Utils.Utils.StrToBool(attr.Value);
                          break;
                  }
              }
          } 
          #endregion


          #region override ToString
          /// <summary>
          /// Выводит контрол на экран
          /// </summary>
          /// <param name="cache"></param>
          /// <returns></returns>
          public override string[] ToString(Dictionary<string, object> cache)
          {
              if(!IsVisible(cache))
                  return new string[] { string.Empty };

              return new string[] {((IsMark) ? OheConstant.TELNET_ESC_SEQ + "7m" : "")
                    + Description  // + Environment.NewLine
                    + ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "0m" : "")};
          }
        
          #endregion


          public override string[] ToHtml(Dictionary<string, object> cache)
          {
              if(!IsVisible(cache))
                  return new string[] { string.Empty };

              string _class = "class=\"function" +(IsMark ? "_inverse" : "") + "\"";
              
              return new string[] {"<div><input type=\"button\" name=\"function" + base.Value + "\" " 
                                    + _class +" value=\"" + (Description == string.Empty ? Value : Description) + "\""
                                    + " onclick=\"Function_Click('" + Value + "');\"></div>"};
          }

	    #endregion    
    }
}
