﻿using System;
using System.Xml;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Ohe.Telnet.Data;
using Ohe.Telnet.Exception;
using Ohe.Utils;


namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Описывает весь экран для терминала
    /// </summary>
    public class RfForm : RfFormBase// IDisposable
    {
        #region Define Values
        private List<RfControl> _rfcontrol = new List<RfControl>(); // возвращает список контролов которые есть на форме
        private bool _autorefresh; // признак того что форма должна обнуляться при инициализации
        private RfControlMessage _message; // контрол для отслеживания ошибок или сообщений
        private RfControlHelp _helpMenu; // описывает контрол для меню
        private FormState _isValid;  // определяет, произошла на форме ошибка или нет
        private string _htmlSource = string.Empty;

        #endregion


        #region Properties
        /// <summary>
        /// Возвращает и определяет список контролов для формы
        /// </summary>
        public List<RfControl> Controls { get { return _rfcontrol; } set { _rfcontrol = value; } }


        /// <summary>
        /// Возвращает список управляющий контролов
        /// </summary>
        public List<RfControlKey> ContrrolKeys 
        { 
            get
            {
                return Controls.OfType<RfControlKey>().ToList();
            }
        }


        /// <summary>
        /// Текущее событие формы
        /// </summary>
        public Events CurrentEvent { get; set; }


        /// <summary>
        /// Определяет контрол сообщений для формы
        /// </summary>
        public RfControlMessage Message { get { return _message; } set { _message = value; } }


        /// <summary>
        /// Текущее состояние формы
        /// </summary>
        public FormState CurrentState { get { return _isValid; } set { _isValid = value; } }


        /// <summary>
        /// Содержит результат обоработки подчиненной формы
        /// </summary>
        public string ResultFromChild { get; set; }


        /// <summary>
		/// Признак обнуления кеша при инициализации формы
		/// </summary>
		public bool IsAutoRefresh { get { return _autorefresh; } set { _autorefresh = false; } }


        /// <summary>
        /// Тип формы
        /// </summary>
        public bool IsIf { get; set; }

        /// <summary>
        /// Признак проверки товаров на погрузчике
        /// </summary>
        public bool IsCheckFork { get; set; }

        /// <summary>
        /// Список объектов, которые необходимо удалить из глобального кеша
        /// </summary>
        public string RemoveObjects { get; set; }

        /// <summary>
        /// Признак восстановления сессии, если произошел обрыв.
        /// Практически на всех экранах этот параметр = true
        /// За исключением отбора, т.к. не нем происходило задвоение отбора
        /// </summary>
        bool _restoreSession = true;
        public bool IsRestoreSession { get { return _restoreSession; } }


        #region IsUseNotify
        /// <summary>
        /// Поддерживает ли форма отображение оповещений
        /// </summary>
        public bool IsUseNotify
        {
            get;
            private set;
        }

        #endregion


        #region Old Properties
        // может быть пустой
        //private int _parent;  // указывает на то куда надо вернутся если пользователь вышел
        //private string _checkparam; // список параметров, значение которых надо проверять на форме
        //private string _permiss; // определяет привилегию для формы
        //private bool _active;    // определяет активна ли форма в данный момент
        //private bool _dispose = false; // переменная отслеживает что класс не существует
        //private Component component = new Component();
        //private int _id; // идентификатор формы
        //private int _target; // указывает куда надо перейти при submit form
        //private RfControlEvents _onInitEvent; // описывает событие по инициализации формы
        //private RfControlEvents _onSubmitEvent; // описывает событие по submit
        //private RfControlEvents _onExitEvent; // описывает выхода из формы по управляющей клавиши
        /// <summary>
        /// Параметр, который надо проверять при входе на форму
        /// Используется для условных переходов с формы на форму
        /// </summary>
        //public string CheckValue { get { return _checkparam; } set { _checkparam = value; } }
        /// <summary>
        /// Возвращает идентификатор формы
        /// </summary>
        //public int Id { get { return _id; } }

        /// <summary>
        /// Возвращает родительскую форму
        /// </summary>
        //public int Parent { get { return _parent; } set { _parent = value; } }

        /// <summary>
        /// Возвращает идентификатор куда надо перейти
        /// </summary>
        //public int Target { get { return _target; } }

        /// <summary>
        /// Возвращает что форма активна или нет
        /// </summary>
        //public bool IsActivate { get { return _active; } set { _active = value; } }

        /// <summary>
        /// Возвращает привилегию для формы
        /// </summary>
        //public string Permiss { get { return _permiss; } set { _permiss = value; } }

        //private string _descript = string.Empty;
        //public string Description { get { return _descript; } set { _descript = value; } }

        #endregion

        #endregion


        #region Constructors
        public RfForm(RfForm parentForm) : base(parentForm)
        {
            _restoreSession = true;
            RemoveObjects = string.Empty;
            Id = 0;
            Target = 0;
        }

        public RfForm(RfForm parentForm, int id, int parent) : base(parentForm)
        {
            _restoreSession = true;
            RemoveObjects = string.Empty;
            Id = id;
            Parent = parent;
            Target = 0;
        }

        #endregion


        #region Methods

        #region InitControlFromXML - инициализация из XML
        public override void InitControlFromXML(XmlNode node)
        {
            base.InitControlFromXML(node);
            IsUseNotify = false;            
            RfControl _control = null;

            #region парсинг атрибутов формы
            foreach (XmlAttribute attr in node.Attributes)
            {
                #region Switch
                switch (Utils.Utils.AllTrim(attr.Name.ToUpperInvariant()))
                {
                        /*
                    case "CYCLE":
                        IsCycle = Utils.Utils.StrToBool(attr.Value);
                        break;*/
                    case "AUTOREFRESH":
                        _autorefresh = Utils.Utils.StrToBool(attr.Value);
                        break;
                    case "CHECKFORK":
                        IsCheckFork = Utils.Utils.Str2Bool(attr.Value);
                        break;
                    case "ISIF":
                        IsIf = Utils.Utils.Str2Bool(attr.Value);
                        break;
                    case "REMOVE":
                        RemoveObjects = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "RESTORE":
                        _restoreSession = Utils.Utils.Str2Bool(attr.Value);
                        break;
                    case "USENOTIFY":
                        IsUseNotify = Utils.Utils.Str2Bool(attr.Value);
                        break;
                }
                #endregion
            }
            #endregion

            // Проверка на наличие обязательных атрибутов
            #region Проверка обязательных параметров
            if (Id == 0 || Target < 0 || Permission == string.Empty)
                throw new ConfigurationException("Ошибка: Не определены основные параметры формы"
                                                         + Environment.NewLine
                                                         + (Id == 0 ? "Не определен идентификатор формы" : "Идентификатор формы:" + Id.ToString())
                                                         + Environment.NewLine
                                                         + (Target < 0 ? "Не определена форма перехода" : string.Empty)
                                                         + Environment.NewLine
                                                         + (Permission == string.Empty ? "Не задана привилегия для формы" : string.Empty)
                                                        );

            if (IsIf && Parent == 0)
                throw new ConfigurationException("Ошибка: Не определены основные параметры формы"
                                                         + Environment.NewLine
                                                         + (Id <= 0 ? "Не определен идентификатор формы" : "Идентификатор формы:" + Id.ToString())
                                                         + Environment.NewLine
                                                         + (Parent < 0 ? "Не определена форма перехода для ветки условия" : string.Empty)
                                                        );
            #endregion

            // Поиск подчиненных элементов
            if (!node.HasChildNodes)
                throw new ConfigurationException("Нет определения основных параметров формы");

            #region Добавление событий и контролов формы

            foreach (XmlNode opt in node.ChildNodes.Cast<XmlNode>().Where(opt => opt.NodeType == XmlNodeType.Element))
            {
                // Форма поддерживает только два события
                switch (Utils.Utils.AllTrim(opt.Name.ToUpperInvariant()))
                {
                        #region События определены в базовом классе

                        // Определены в базовом классе
                        /*
                            case "INIT":
                                //form.AddEvent(GetEvent(formId, -1, "OnInit", opt));
                                OnControlInitEvent = new RfControlEvents(this, null);
                                OnControlInitEvent.InitControlFromXML(opt);
                                break;
                            case "SUBMIT":
                                //form.AddEvent(GetEvent(formId, -1, "OnSubmit", opt));
                                OnConrolSubmitEvent = new RfControlEvents(this, null);
                                OnConrolSubmitEvent.InitControlFromXML(opt);
                                break;
                            case "EXIT":
                                //form.AddEvent(GetEvent(formId, -1, "OnExit", opt));
                                OnControlExitEvent = new RfControlEvents(this, null);
                                OnControlExitEvent.InitControlFromXML(opt);
                                break;
                            */

                        #endregion

                    case "HTML":
                        _htmlSource = opt.InnerXml;
                        break;
                    case "CONTROLS":

                        #region Добавление контролов

                        if (opt.HasChildNodes)
                        {
                            XmlNodeList elem = opt.ChildNodes;
                            foreach (XmlNode cntls in from XmlNode cntls in elem where cntls.NodeType == XmlNodeType.Element 
                                                      where Utils.Utils.AllTrim(cntls.Name.ToUpperInvariant()) == "CONTROL" select cntls)
                            {
                                switch (cntls.Attributes["controltype"].Value.ToUpperInvariant())
                                {
                                    case "TEXT":
                                    case "INPUT":
                                        _control = new RfControl(this, null);
                                        _control.InitControlFromXML(cntls);
                                        break;
                                    case "LIST":
                                        _control = new RfControlList(this, null);
                                        _control.InitControlFromXML(cntls);
                                        break;
                                    case "FUNCTION":
                                        _control = new RfControlKey(this, null);
                                        _control.InitControlFromXML(cntls);
                                        break;
                                    case "HELP":
                                        _control = new RfControlHelp(this, null);
                                        _control.InitControlFromXML(cntls);
                                        break;
                                    case "MESSAGE":
                                        _control = new RfControlMessage(this, null);
                                        _control.InitControlFromXML(cntls);
                                        break;

                                    default:
                                        throw new ConfigurationException(
                                            "Ошибка: неверный тип контрола"
                                            + Environment.NewLine
                                            + "Идентификатор формы: " + Id
                                            + Environment.NewLine
                                            + (Id == 0 ? string.Empty : "Идентификатор Контрола: " + Id)
                                            + Environment.NewLine
                                            + "Параметр: controltype неопределен");
                                }
                                AddControl(_control);
                            }
                        }
                        else
                            throw new ConfigurationException("Ошибка: Нет определения контролов для формы"
                                                             + Environment.NewLine
                                                             + "Идентификатор формы: " + Id.ToString()
                                );

                        #endregion

                        break;
                }
            }

            #endregion
        }

        #endregion


        #region AddControl
        /// <summary>
        /// ДОбавляет контрол в форму
        /// </summary>
        /// <param name="c">Контрол</param>
        public void AddControl(RfControl c)
        {
            if (c is RfControlList)
                _rfcontrol.Add(c);
            else if (c is RfControlMessage)
            {
                _message = (RfControlMessage)c;
                _rfcontrol.Add(c);
            }
            else if (c is RfControlHelp)
                _helpMenu = (RfControlHelp)c;
            else
                _rfcontrol.Add(c);
            
            /*
            if (c is RfControlKey)
                _rfformkeys.Add((RfControlKey)c);
            else if (c is RfControlList)
            {
                _rfcontrol.Add(c);
            }
            else if (c is RfControlMessage)
            {
                _message = (RfControlMessage)c;
                _rfcontrol.Add(c);
            }
            else if (c is RfControlHelp)
                _helpMenu = (RfControlHelp)c;
            else
            {
                _rfcontrol.Add(c);
            }
            */
        }


        #region Not Use
        /// <summary>
        /// Добавляет контролы на форму из списка
        /// </summary>
        /// <param name="c"></param>
        /*
        public void AddControl(ArrayList c)
        {
            foreach (object o in c)
                switch (Utils.Utils.AllTrim(o.GetType().Name))
                {
                    case "RfControlKey":
                        _rfformkeys.Add((RfControlKey)o);
                        break;
                    case "RfControlList":
                        _rflistcntl.Add((RfControlList)o);
                        break;
                    case "RfControlMessage":
                        _message = ((RfControlMessage)o);
                        break;
                    case "RfControlHelp":
                        _helpMenu = ((RfControlHelp)o);
                        break;
                    case "RfControl":
                        _rfcontrol.Add((RfControl)o);
                        break;
                }
        }
        */
        
        #endregion       
        #endregion


        #region AddEvent(RfControlEvents events) (comment)
        /// <summary>
        /// Добавляет событие для формы
        /// </summary>
        /// <param name="events"></param>
        /*
        public void AddEvent(RfControlEvents events)
        {
            if (Utils.Utils.AllTrim(events.Event) == Utils.Utils.AllTrim(Events.OnInit.ToString()))
                _onInitEvent = events;
            if (Utils.Utils.AllTrim(events.Event) == Utils.Utils.AllTrim(Events.OnSubmit.ToString()))
                _onSubmitEvent = events;
            if (Utils.Utils.AllTrim(events.Event) == Utils.Utils.AllTrim(Events.OnExit.ToString()))
                _onExitEvent = events;
        }
        */
        // not use
        /*
        public void AddEvent(ArrayList events)
        {
            foreach (RfControlEvents e in events)
            {
                if (Utils.Utils.AllTrim(e.Event) == Utils.Utils.AllTrim(Events.OnInit.ToString()))
                    OnControlInitEvent = e;
                if (Utils.Utils.AllTrim(e.Event) == Utils.Utils.AllTrim(Events.OnSubmit.ToString()))
                    OnConrolSubmitEvent = e;
                if (Utils.Utils.AllTrim(e.Event) == Utils.Utils.AllTrim(Events.OnExit.ToString()))
                    OnControlExitEvent = e;
            }
        }
        */
        #endregion


        #region HasActiveControls
        /// <summary>
        /// Возвращает имеет ли форма активные контролы в текущий момент
        /// </summary>
        /// <returns></returns>
        public bool HasActiveControls()
        {
            return (Controls.Count(n => n.IsActivate == true) > 0);
        }
        #endregion


        #region SetCurrentEvent - определяет логику обработки событий
        /// <summary>
        /// Обрабатывает события формы
        /// </summary>
        /// <param name="str">Строка введенная пользователем</param>
        /// <param name="child">Параметр определяет эта форма подчиненная или нет</param>
        public void SetCurrentEvent(string str, bool child, ref Dictionary<string,object> result)
        {
            string[] split;
            // Форма уже обрабатывается
            // Определяем текущее событие формы
            if (IsActivate)
            {
                switch (Utils.Utils.AllTrim(Enum.GetName(typeof(Events), this.CurrentEvent).ToUpperInvariant()))
                {
                    case "UNBOUNDED":
                        #region UnBounded
                        if (this.HasActiveControls())
                            this.CurrentEvent = Events.OnChange;
                        break;
                        #endregion
                    case "ONINIT":
                        #region OnInit

                        if (!string.IsNullOrEmpty(RemoveObjects))
                        {
                            split = RemoveObjects.Split("'".ToCharArray()[0]);
                            foreach (string remove in split)
                            {
                                if (result.ContainsKey(remove))
                                    result.Remove(remove);
                            }
                        }

                        // Вызываем событие инициализации формы
                        if(!OnInit(child, result))
                            return;

                        SetNextActiveControl(ref result);
                        break;
                        #endregion
                    case "ONCHANGE":
                        #region OnChange

                        if (child)
                            ResultFromChild = string.Empty;

                        // Обнуляем значение ошибки
                        result["SYS_ERROR"] = string.Empty;
                            OnChange(str, ref result);

                        // Произошла какая-то ошибка
                        if (_isValid != FormState.Valid)   return;

                        // Отправляем дальше форму для активации следующего контрола
                        // Генерируем результат текущего представления формы
                        // если есть следующий контрол
                        SetNextActiveControl(ref result);
                        _isValid = FormState.Valid;

                        // Все контролы у нас обработаны надо делать submit формы
                        // и возвращать результат
                        if (!HasActiveControls())
                        {
                            CurrentEvent = Events.OnSubmit;
                            _isValid = FormState.Valid;
                            // Вызываем выполнение события OnSubmit
                            SetCurrentEvent(str, child, ref result);
                        }
                        break;
                        #endregion
                    case "ONSUBMIT":
                        #region OnSubmit
                        if (OnSubmit(child, result))
                            CurrentEvent = Events.OnEnd;
                        #endregion  
                        break;
                    case "ONEND":
                        #region OnEnd
                        ClearForm();
                        //_result = this.ToString() + this.GetCursorPosition();
                        break;
                        #endregion
                    case "ONEXIT":
                        #region OnExit
                        if (OnExit(child, result))
                        {
                            CurrentEvent = Events.OnEnd;
                            ClearForm();
                        }
                        break;    
                        #endregion
                }
            }
            else
            {
                // Мы пришли на форму в первый раз надо что-то делать
                #region Первый раз на форме

                // Активируем форму
                IsActivate = true;

                if (!string.IsNullOrEmpty(RemoveObjects))
                {
                    split = RemoveObjects.Split("'".ToCharArray()[0]);
                    foreach (string remove in split)
                    {
                        if (result.ContainsKey(remove))
                            result.Remove(remove);
                    }
                }


                if (OnControlInitEvent != null)
                {
                    CurrentEvent = Events.OnInit;
                    SetCurrentEvent(str, child, ref result);
                }
                else
                {
                    CurrentEvent = Events.UnBounded;
                    // Устанавливаем фокус на первый контрол формы
                    SetNextActiveControl(ref result);
                }
                #endregion
            }
        }
        #endregion


        #region События OnInit,OnExit,OnSubmit,OnChange
        /// <summary>
        /// Вызывает событие обработки инициализации формы
        /// </summary>
        /// <param name="childForm"></param>
        private bool OnInit(bool childForm, Dictionary<string, object> cache)
        {
            // Проверяем все прошло нормально или нет
            if (ExecuteEventHandler(childForm, cache))
            {
                _isValid = FormState.Valid;
                return true;
            }

            // Проверка на то, что у нас произошла системная ошибка
            _isValid = cache["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА" ? FormState.Exception : FormState.InValid;

            return false;
        }

        /// <summary>
        /// Вызывает событие Выхода с формы
        /// </summary>
        /// <param name="childForm"></param>
        private bool OnExit(bool childForm, Dictionary<string, object> cache)
        {
            // Проверяем все прошло нормально или нет
            if (ExecuteEventHandler(childForm, cache))
            {
                _isValid = FormState.Valid;
                return true;
            }

            // Проверка на то, что у нас произошла системная ошибка
            _isValid = cache["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА" ? FormState.Exception : FormState.InValid;

            return false;
        }

        /// <summary>
        /// Вызывает событие обновления формы, добавляет параметр в глобальный кеш
        /// Надо переписать этот метод
        /// </summary>
        /// <param name="userInput">Строка введенная пользователем</param>
        /// <param name="child">Параметр определяет эта форма подчиненная или нет</param>
        private void OnChange(string str, ref Dictionary<string, object> results)
        {
            _isValid = FormState.InValid;
            SetMessage(string.Empty);
            try
            {
                foreach (RfControl control in Controls)
                {
                    if (!control.OnChange(str, ref results))
                        return;
                }

                #region Обработка контролов формы (comment)
                /*
                foreach (RfControl cnl in this.Controls)
                {
                    if (cnl is RfControlKey) { }
                    else
                    {
                        if (cnl.IsActivate && cnl is RfControlList)
                        {
                            #region Обработка контролов типа лист
                            string rez = ((RfControlList)cnl).ParseString(userInput);
                            // Пользователь ввел какую-то фигню
                            if (((RfControlList)cnl).ControlView == string.Empty
                                            && (rez == string.Empty))
                            { _isValid = FormState.InValid; }
                            // Пользователь выбрал какой-то пункт, надо возвращать результат на форму
                            else if (((RfControlList)cnl).ControlView != string.Empty
                                        && userInput != string.Empty
                                    )
                            {
                                // Добавляем параметр в общую кучу параметров
                                if (!Results.ContainsKey(Utils.Utils.AllTrim(((RfControlList)cnl).ValueID.ToUpper())))
                                    //Results.Add(((RfControlList)cnl).ValueID
                                    //           ,Utils.Utils.AllTrim(((RfControlList)cnl).ControlView.ToUpper()));
                                    Results.Add(((RfControlList)cnl).ValueID
                                              , ((RfControlList)cnl).CurrentRow);
                                else
                                    Results[Utils.Utils.AllTrim(((RfControlList)cnl).ValueID.ToUpper())] = ((RfControlList)cnl).CurrentRow;

                                this._resultChild = rez;

                                _isValid = FormState.Valid;
                                return;
                            }
                            // Пользователь пролиснул экран (нажал какой-то управляющий контрол)
                            else
                            {
                                _isValid = FormState.InValid;
                                return;
                            }
                            #endregion
                        }
                        // Мы пришли на контрол первый раз
                        // у контрола нет процедуры инициализации или просто 
                        // она вернула пустое значение, т.е. значения у контрола нет
                        // Пользователь ввел какое-то значение или нет
                        else if (cnl.IsActivate
                                    && cnl is RfControl
                                        && cnl.Value == string.Empty)
                        {
                            #region Проверка на возможность пустого значения контрола
                            if ((cnl.Mandatory) && Utils.Utils.AllTrim(userInput) == string.Empty)
                            {
                                #region Проверка на то что пользователь ввел пустое значение
                                setMessage("ВВЕДИ ДАННЫЕ");
                                //_result = this.ToString(true) + GetCursorPosition();
                                this.CurrentEvent = Events.OnChange;
                                _isValid = FormState.InValid;
                                #endregion
                            }
                            else
                            {
                                #region Использовать пустые значения можно или пользователь что-то ввел
                                // ПРоверка на корректность значения контрола
                                if (cnl.ValidateValues(userInput))
                                {
                                    #region Данные введены корректные
                                    this.CurrentEvent = Events.OnChange;

                                    // Добавляем параметр в общую кучу параметров
                                    if (!Results.ContainsKey(((RfControl)cnl).ValueID))
                                        Results.Add(((RfControl)cnl).ValueID, Utils.Utils.AllTrim(userInput));
                                    else
                                        Results[((RfControl)cnl).ValueID] = Utils.Utils.AllTrim(userInput);

                                    // Обнуляем ошибки
                                    Results["SYS_ERROR"] = string.Empty;

                                    // Проверка на наличие события обновления в базе
                                    if (cnl.GetExistsEventByName("OnChange"))
                                    {
                                        if (!cnl.ExecuteEventHandlerControl("OnChange", ref Results))
                                        {
                                            // Событие по каким-то причинам не выполнилось
                                            setMessage(Results["SYS_ERROR"].ToString());
                                            // Проверка, на то что возникла системная ошибка
                                            if (Results["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА")
                                            {
                                                _isValid = FormState.Exception;
                                            }
                                            return;
                                        }
                                        // Хранимая процедура возвратила несколько строк в результате
                                        // надо выбирать один
                                        else if (((DataTable)Results[cnl.ValueID]).Rows.Count > 1)
                                        {
                                            _isValid = FormState.MultiResult;
                                            return;
                                        }
                                    }

                                    #endregion

                                    // Обработали текущий контрол надо возвращать данные пользователю
                                    _isValid = FormState.Valid;
                                    cnl.Value = Utils.Utils.AllTrim(userInput);
                                    return;
                                }
                                else
                                {
                                    #region Неверный формат данных от пользователя
                                    setMessage("НЕВЕРНЫЙ ТИП ДАННЫХ");
                                    //_result = this.ToString(true) + GetCursorPosition();
                                    // Обработали текущий контрол надо возвращать данные пользователю
                                    _isValid = FormState.InValid;
                                    return;
                                    #endregion
                                }
                                #endregion
                            }
                            #endregion
                        }
                        // теперь у контрола есть параметр который
                        // возможно пришел с процедуры инициализации
                        // но пользователь изменил значение
                        else if (cnl.IsActivate
                                  && cnl is RfControl
                                    && cnl.Value != string.Empty
                                        && cnl.Value != Utils.Utils.AllTrim(userInput)
                                            && Utils.Utils.AllTrim(userInput) != string.Empty)
                        {
                            #region Проверка на значения
                            // ПРоверка на корректность значения контрола
                            if (cnl.ValidateValues(userInput))
                            {

                                // Добавляем параметр в общую кучу параметров
                                if (!Results.ContainsKey(((RfControl)cnl).ValueID))
                                    Results.Add(((RfControl)cnl).ValueID, Utils.Utils.AllTrim(userInput));
                                else
                                    Results[((RfControl)cnl).ValueID] = Utils.Utils.AllTrim(userInput);

                                this.CurrentEvent = Events.OnChange;
                                cnl.Value = Utils.Utils.AllTrim(userInput);

                                // Проверка на наличие события обновления в базе
                                if (cnl.GetExistsEventByName("OnChange"))
                                    if (!cnl.ExecuteEventHandlerControl("OnChange", ref Results))
                                    {
                                        setMessage(Results["SYS_ERROR"].ToString());
                                        // Проверка, на то что возникла системная ошибка
                                        if (Results["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА")
                                        {
                                            _isValid = FormState.Exception;
                                        }
                                        return;
                                    }
                                    // Хранимая процедура возвратила несколько строк в результате
                                    // надо выбирать один
                                    else if (((DataTable)Results[cnl.ValueID]).Rows.Count > 1)
                                    {
                                        _isValid = FormState.MultiResult;
                                        return;
                                    }


                                // Обработали текущий контрол надо возвращать данные пользователю
                                _isValid = FormState.Valid;
                                return;
                            }
                            else
                            {
                                setMessage("НЕВЕРНЫЙ ТИП");
                                //_result = this.ToString(true);
                                // Обработали текущий контрол надо возвращать данные пользователю
                                _isValid = FormState.InValid;
                                return;
                            }
                            #endregion
                        }
                        // теперь у контрола есть параметр который
                        // возможно пришел с процедуры инициализации
                        // пользователь ввел пустое значение, т.е. подтвердил текущее значение
                        else if (cnl.IsActivate
                                    && cnl is RfControl
                                        && cnl.Value != string.Empty
                                            && (userInput == string.Empty ||
                                                cnl.Value == Utils.Utils.AllTrim(userInput)))
                        {
                            #region ПРоверка значений
                            // ПРоверка на корректность значения контрола
                            if (cnl.ValidateValues(((RfControl)cnl).Value))
                            {
                                this.CurrentEvent = Events.OnChange;

                                // Добавляем параметр в общую кучу параметров
                                if (!Results.ContainsKey(((RfControl)cnl).ValueID))
                                    Results.Add(((RfControl)cnl).ValueID, ((RfControl)cnl).Value);
                                else
                                    Results[((RfControl)cnl).ValueID] = ((RfControl)cnl).Value;

                                // Проверка на наличие события обновления в базе
                                if (cnl.GetExistsEventByName("OnChange"))
                                    if (!cnl.ExecuteEventHandlerControl("OnChange", ref Results))
                                    {
                                        // Событие по каким-то причинам не выполнилось
                                        setMessage(Results["SYS_ERROR"].ToString());
                                        // Проверка, на то что возникла системная ошибка
                                        if (Results["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА")
                                        {
                                            _isValid = FormState.Exception;
                                        }
                                        return;
                                    }
                                    // Хранимая процедура возвратила несколько строк в результате
                                    // надо выбирать один
                                    else if (((DataTable)Results[cnl.ValueID]).Rows.Count > 1)
                                    {
                                        _isValid = FormState.MultiResult;
                                        return;
                                    }

                                // Обработали текущий контрол надо возвращать данные пользователю
                                _isValid = FormState.Valid;
                                return;
                            }
                            else
                            {
                                setMessage("НЕВЕРНЫЙ ТИП");
                                // Обработали текущий контрол надо возвращать данные пользователю
                                _isValid = FormState.InValid;
                                return;
                            }
                            #endregion
                        }
                    }
                }
                */

                #endregion

            }
            catch (System.Exception ex)
            {
                throw new System.Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Вызывает событие обработки окончания формы
        /// </summary>
        /// <param name="childForm"></param>
        private bool OnSubmit(bool childForm, Dictionary<string, object> cache)
        {
            // Проверяем все прошло нормально или нет
            if (ExecuteEventHandler(childForm, cache))
            {
                _isValid = FormState.Valid;
                return true;
            }

            // Проверка на то, что у нас произошла системная ошибка
            _isValid = cache["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА" ? FormState.Exception : FormState.InValid;

            return false;
        } 
        #endregion
      
  
        #region SetNextActiveControl - активация текущего контрола
        /// <summary>
        /// Возвращает следующий контрол который должен быть активирован
        /// </summary>
        public void SetNextActiveControl(ref Dictionary<string, object> results)
        {
            int id = 0;
            // Проверка на то что пришла только первый раз и надо
            // сделать инициализацию контролов если она есть у них
            try
            {
                if ((CurrentEvent == Events.OnInit || CurrentEvent == Events.UnBounded) && !HasActiveControls())
                {
                    CurrentEvent = Events.OnChange;
                    if (ActivateControl(ref results))
                        return;
                }
                // Если пользователь НЕ имеет активный контрол 
                // на текущий момент (нет события инициализации)
                // И форма находится в состоянии обновления
                if (CurrentEvent == Events.OnChange && !HasActiveControls())
                {
                    if (ActivateControl(ref results)) // вспомогательная процедура активации контрола
                        return;
                }
                // Форма уже находится в обновлении и имеет один из активных контролов
                if (CurrentEvent != Events.OnChange || !HasActiveControls()) return;

                DisableControl(out id); // дезактивируем контрол вспомогательной процедурой
                // Определяем следующий активный контрол
                foreach (RfControl control in Controls)
                {
                    if (control.Id > id && (control.OheControlType == OheConstant.OheTelnetControlType.InputBox || control.OheControlType == OheConstant.OheTelnetControlType.List))
                    {
                        if (!ActivateControl(ref results, control.Id))
                        {
                            control.IsActivate = false;
                            continue;
                        }
                        break;
                    }
                }
            }
            catch (System.Exception exc)
            {
                throw new System.Exception(exc.Message, exc.InnerException);
            }
        }

        /// <summary>
        /// Активирует контрол с определенным идентификатором
        /// </summary>
        /// <param name="results">Глобальные параметры</param>
        /// <param name="Id">Идентификатор контрола</param>
        private bool ActivateControl(ref Dictionary<string, object> results, int Id)
        {
            bool rez = false;

            try
            {
                RfControl rf = GetControlById(Id);
                if (rf == null) return false;
                // проверка на то что существует событие инициализации для контрола
                #region проверка на то что существует событие инициализации для контрола
                if (rf.GetExistsEventByName("OnInit"))
                {
                    if (rf.ExecuteEventHandlerControl("OnInit", ref results))
                        _isValid = FormState.Valid;
                    else
                    {
                        // Произошла ошибка пока неизвестно какая
                        SetMessage(results["SYS_ERROR"].ToString());
                        // Проверка, на то что возникла системная ошибка
                        if (results["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА")
                        {
                            _isValid = FormState.Exception;
                        }
                        else
                            _isValid = FormState.InValid;


                        // ТОлько для контрола List делаем событие инициализации
                        // Exception при пустом списке
                        if (rf is RfControlList)
                            _isValid = FormState.Exception;
                    }
                }
                #endregion

                // активируем контрол если он не только для чтения
                if (!rf.ReadOnly)
                {
                    rf.IsActivate = true;
                }
                else
                {
                    // активируем следующий контрол
                    rf.IsActivate = true;
                    //SetNextActiveControl(ref Results);
                    return rez;
                }
            }
            catch (System.Exception exc)
            {

                throw new System.Exception(exc.Message, exc.InnerException);
            }
            return !rez;
        }


        // Активирует первый контрол который не Text
        /// <summary>
        /// Активирует первый контрол который не Text, или текущий контрол,
        /// который надо активировать
        /// </summary>
        /// <param name="results">Глобальные параметры</param>
        private bool ActivateControl(ref Dictionary<string, object> results)
        {
            bool rez = false;

            try
            {
                var rf = GetTopNonTextControl();
                if (rf == null) return false;
                // проверка на то что существует событие инициализации для контрола
                #region проверка на то что существует событие инициализации для контрола
                if (rf.GetExistsEventByName("OnInit"))
                {
                    if (rf.ExecuteEventHandlerControl("OnInit", ref results))
                        _isValid = FormState.Valid;
                    else
                    {
                        // Произошла ошибка пока неизвестно какая
                        SetMessage(results["SYS_ERROR"].ToString());
                        // Проверка, на то что возникла системная ошибка
                        _isValid = results["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА" ? FormState.Exception : FormState.InValid;

                        // ТОлько для контрола List делаем событие инициализации
                        // Exception при пустом списке
                        if (rf is RfControlList)
                            _isValid = FormState.Exception;
                    }
                }
                #endregion

                // активируем контрол если он не только для чтения
                if (!rf.ReadOnly)
                {
                    rf.IsActivate = true;
                }
                else
                {
                    // активируем следующий контрол
                    rf.IsActivate = true;
                    //SetNextActiveControl(ref Results);
                    return rez;
                }
            }
            catch (System.Exception exc)
            {
                throw new System.Exception(exc.Message, exc.InnerException);
            }
            return !rez;
        }

        // Дезактивирует активный контрол
        private void DisableControl(out int id)
        {
            id = 0; // Начальное значение идентификатора

            RfControl rf = Controls.FirstOrDefault(s => s.IsActivate == true);
            if (rf == null) return;
            rf.IsActivate = false;
            id = rf.Id;
        }
        #endregion

        
        #region HasHelpMenu
        /// <summary>
        /// Возвращает если для этого меню help
        /// </summary>
        /// <returns></returns>
        public bool HasHelpMenu()
        {
            return (_helpMenu != null);
        }
        
        #endregion


        #region GetCursorPosition
        /// <summary>
        /// ВОзвращает текущую позицию курсора
        /// </summary>
        /// <returns></returns>
        public string GetCursorPosition()
        {
            RfControl cnl = GetActiveControl();
            return OheConstant.TELNET_ESC_SEQ + (cnl.Id + cnl.Y).ToString() + ";" + cnl.X.ToString() + "f";
        }
        #endregion

        
        #region GetControlHelp
        /// <summary>
        /// Возвращает контрол помощи для меню
        /// </summary>
        /// <returns></returns>
        public RfControlHelp GetControlHelp() { return _helpMenu; }
        
        #endregion


        #region GetControlById(int Id)
        /// <summary>
        /// Возвращает контрол по идентификатору 
        /// </summary>
        /// <param name="id">Идентификатор контрола в форме (число)</param>
        /// <returns></returns>
        public RfControl GetControlById(int id)
        {
            return Controls.FirstOrDefault(n => n.Id == id);
        }
        #endregion


	    #region GetTopNonTextControl
		  /// <summary>
        /// Возвращает первый не текстовый контрол
        /// </summary>
		  /// <returns>RfControl</returns>
        public RfControl GetTopNonTextControl()
        {

            return Controls.FirstOrDefault(n => (n.OheControlType == OheConstant.OheTelnetControlType.InputBox
                                                 || n.OheControlType == OheConstant.OheTelnetControlType.List)
                                                && string.IsNullOrEmpty(n.Value));


        }
        #endregion


        #region GetActiveControl
        /// <summary>
        /// Возвращает текущий активный контрол
        /// </summary>
        /// <returns></returns>
        public RfControl GetActiveControl()
        {
            RfControl rf =Controls.FirstOrDefault(n => n.IsActivate == true);

            return (rf == null ? new RfControl() : rf);
        }
        #endregion


        #region ExecuteEventHandler - Выполняет текущее событие
        /// <summary>
        /// Выполняет текущее событие формы (инициализация или submit)
        /// Процедуры инициализации и submit ФОРМЫ ничего не записывают в глобальное
        /// хранилище, только возвращают результат.
        /// В отличие от событий инициализации и change контролов, которые скидывают значения в
        /// глобальный кеш.
        /// </summary>
        /// <param name="child">Определяет подчиненная эта форма или нет</param>
        /// <returns>String[] параметров</returns>
        private bool ExecuteEventHandler(bool child, Dictionary<string,object> values)
        {
            //object obj = null;

            #region Определяем текущее событие и параметры
            switch (Utils.Utils.AllTrim(Enum.GetName(typeof(Events), CurrentEvent).ToUpperInvariant()))
            {
                case "ONINIT":
                    #region OnInit
                    return ExecuteEventByName(OnControlInitEvent, values);
                    #endregion
                case "ONSUBMIT":
                    #region OnSubmit
                    return ExecuteEventByName(OnConrolSubmitEvent, values);
                    #endregion
                case "ONEXIT":
                    #region OnExit
                        return ExecuteEventByName(OnControlExitEvent, values);
                    #endregion

                #region Old Code (comment)
                /*
                case "ONINIT":
                    #region OnInit
                    if (HasEventByName("oninit"))
                    {
                        SetParamsForSp(OnControlInitEvent.Params, Values);
                        try
                        {
                            obj = ExecuteSqlQuery.ExecuteEvent(OnControlInitEvent.SpName, OnControlInitEvent.Params);

							 if (OnControlInitEvent.TableId != string.Empty)
							 {
								 if (!Values.ContainsKey(OnControlInitEvent.TableId))
									 Values.Add(OnControlInitEvent.TableId, obj);
								 else
									 Values[OnControlInitEvent.TableId] = obj;
							 }
                            return true;
                        }
                        // Обработка ошибок связанных с данными и запуском хранимых процедур
                        catch (SqlDataException ex)
                        {
                            Values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                            Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.ToString());
                            return false;
                        }
                        catch (LogicException ex) 
                            {
                                setMessage(ex.Message);
                                // Записываем все равно результат в глобальный 
                                // кеш, т.к. может он будет использоваться дальше
                                if (OnControlInitEvent.TableId != string.Empty)
                                {
                                    if (!Values.ContainsKey(OnControlInitEvent.TableId))
                                        Values.Add(OnControlInitEvent.TableId, ex.Object);
                                    else
                                        Values[OnControlInitEvent.TableId] = ex.Object;
                                }
                                return false; 
                            }
                        catch (FormatException ex)
                        {
                            Values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                            Utils.Utils.WriteLog("Ошибка конвертирования формата данных: " + ex.ToString());
                            return false;
                        }
                    }
                    else
                        return true;
                    
                    #endregion    
                case "ONSUBMIT":
                    #region OnSubmit
		        if (HasEventByName("onsubmit"))
                    {
                        SetParamsForSp(OnConrolSubmitEvent.Params, Values);
                        try
                        {
                            obj = ExecuteSqlQuery.ExecuteEvent(OnConrolSubmitEvent.SpName, OnConrolSubmitEvent.Params);


									 if (OnConrolSubmitEvent.TableId != string.Empty)
									 {
										 if (!Values.ContainsKey(OnConrolSubmitEvent.TableId))
                                             Values.Add(OnConrolSubmitEvent.TableId, obj);
										 else
                                             Values[OnConrolSubmitEvent.TableId] = obj;
									 }

                            return true;
                        }
                        // Обработка ошибок связанных с данными и запуском хранимых процедур
                        catch (SqlDataException ex) 
                        {
                            Values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                            Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.ToString()); 
                        }
                        catch (LogicException ex)
                        {
                            setMessage(ex.Message);
                            // Записываем все равно результат в глобальный 
                            // кеш, т.к. может он будет использоваться дальше
                            if (OnConrolSubmitEvent.TableId != string.Empty)
                            {
                                if (!Values.ContainsKey(OnConrolSubmitEvent.TableId))
                                    Values.Add(OnConrolSubmitEvent.TableId, ex.Object);
                                else
                                    Values[OnConrolSubmitEvent.TableId] = ex.Object;
                            }
                            return false;
                        }
                        catch (FormatException ex)
                        {
                            Values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                            Utils.Utils.WriteLog("Ошибка конвертирования формата данных: " + ex.ToString());
                        }
                    }
                    else
                        return true;
                    break;
	            #endregion  
                case "ONEXIT":
                    #region OnExit
                    if (HasEventByName("onexit"))
                    {
                        SetParamsForSp(OnControlExitEvent.Params, Values);
                        try
                        {
                            obj = ExecuteSqlQuery.ExecuteEvent(OnControlExitEvent.SpName, OnControlExitEvent.Params);


									 if (OnControlExitEvent.TableId != string.Empty)
									 {
										 if (!Values.ContainsKey(OnControlExitEvent.TableId))
											 Values.Add(OnControlExitEvent.TableId, obj);
										 else
											 Values[OnControlExitEvent.TableId] = obj;
									 }

                            return true;
                        }
                        // Обработка ошибок связанных с данными и запуском хранимых процедур
                        catch (SqlDataException ex)
                        {
                            Values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                            Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.ToString());
                        }
                        catch (LogicException ex)
                        {
                            setMessage(ex.Message);
                            // Записываем все равно результат в глобальный 
                            // кеш, т.к. может он будет использоваться дальше
                            if (OnControlExitEvent.TableId != string.Empty)
                            {
                                if (!Values.ContainsKey(OnControlExitEvent.TableId))
                                    Values.Add(OnControlExitEvent.TableId, ex.Object);
                                else
                                    Values[OnControlExitEvent.TableId] = ex.Object;
                            }
                            return false;
                        }
                        catch (FormatException ex)
                        {
                            Values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                            Utils.Utils.WriteLog("Ошибка конвертирования формата данных: " + ex.ToString());
                        }
                    }
                    else
                        return true;
                    break;
                    #endregion  
                    */
                
                #endregion
            }
            #endregion

            // Проверка на то что результат таблица
            #region DataTable
            /*
            if (obj is DataTable)
            {
                #region Обработка результатов вывода процедуры
                // Выполняем событие для формы и записываем событие выполнения результата
                if (!child)
                {
                    // Выполняем результат submit и вывыдим в сообщение на форме
                    try
                    {
                        setMessage(((DataTable)obj).Rows[0].ItemArray[1].ToString());
                    }
                    catch
                    {
                        setMessage(((DataTable)obj).Rows[0].ItemArray[0].ToString());
                    }
                }
                else
                {
                    // У нас подчиненная форма, все действия на ней выполнены, 
                    // надо вернуть результат на основную форму
                    try
                    {
                        ResultFromChild = ((DataTable)obj).Rows[0].ItemArray[0].ToString();
                    }
                    catch
                    {
                        ResultFromChild = ((DataTable)obj).Rows[0].ItemArray[1].ToString();
                    }
                }
                #endregion
            }
            */
            #endregion
            return true;
        }


        /// <summary>
        /// Private method Execute event
        /// </summary>
        /// <param name="Event"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        private bool ExecuteEventByName(RfControlEvents Event, Dictionary<string, object> values)
        {
            if (Event == null) return true;

            SetParamsForSp(Event.Params, values);

            try
            {
                var obj = ExecuteSqlQuery.ExecuteEvent(Event.SpName, Event.Params, values, this);

                if (Event.TableId != string.Empty)
                {
                    if (!values.ContainsKey(Event.TableId))
                        values.Add(Event.TableId, obj);
                    else
                        values[Event.TableId] = obj;
                }

                return true;
            }
            // Обработка ошибок связанных с данными и запуском хранимых процедур
            catch (SqlException ex)
            {
                values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                Utils.Utils.WriteLog("Ошибка выполнения sql комманд: " + Environment.NewLine
                    + "Хранимая процедура:" + Event.SpName + Environment.NewLine
                    + "Форма: " +  Event.FormId.ToString() + Environment.NewLine
                    + "Сообщение:" + Environment.NewLine + ex.ToString());
                return false;
            }
            catch (SqlDataException ex)
            {
                values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.ToString());
                return false;
            }
            catch (LogicException ex)
            {
                SetMessage(ex.Message);
                // Записываем все равно результат в глобальный 
                // кеш, т.к. может он будет использоваться дальше
                if (Event.TableId != string.Empty)
                {
                    if (!values.ContainsKey(Event.TableId))
                        values.Add(Event.TableId, ex.Object);
                    else
                        values[Event.TableId] = ex.Object;
                }
                return false;
            }
            catch (FormatException ex)
            {
                values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                Utils.Utils.WriteLog("Ошибка конвертирования формата данных: " + ex.ToString());
                return false;
            }
            catch (System.Exception ex)
            {
                values["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                Utils.Utils.WriteLog("Ошибка обработки данных возвращаемых хранимой процедурой: "
                                            + Environment.NewLine + "Экран: " + Event.FormId.ToString()
                                            + Environment.NewLine + "Контрол:" + Event.ControlId.ToString()
                                            + Environment.NewLine + "Ссылка на объект:" + Event.TableId

                                            + Environment.NewLine + "Хранимая процедура:" + Event.SpName
                                            + Environment.NewLine + "Описание: " + ex.Message
                                            + Environment.NewLine + "Внутренняя ошибка: " + ex.InnerException
                                            + Environment.NewLine + "Стек вызова: " + (ex.StackTrace == null ? "" : ex.StackTrace)
                                            );
                return false;
            }

        }

        /// <summary>
        /// Устанавливает параметры для хранимой процедуры
        /// </summary>
        /// <param name="Params"></param>
        /// <param name="cache"></param>
        private void SetParamsForSp(List<RfControlEventsParams> Params, Dictionary<string,object> cache)
        {
            foreach (RfControlEventsParams prm in Params)
            {
                object obj;
                // Определяем пустое значение параметра если записи
                // нет в глобальном кеше
                if (prm.ControlId != string.Empty && !cache.ContainsKey(prm.ControlId))
                {
                    prm.Value = string.Empty;
                    continue;
                }

                // Тут забираем параметры у других контролов
                if (prm.ControlId != string.Empty)
                {
                    int    col   = -99;
                    string colstr = "-99";

                    // Опередяет название колонок записи если они есть
                    if (prm.Column != string.Empty)
                    {
                        try
                        {
                            col = int.Parse(prm.Column);
                        }
                        catch { colstr = prm.Column; }
                    }

                    obj = cache[prm.ControlId];
                    var table = obj as DataTable;
                    if (table != null)
                    {
                        try
                        {
                            // Используем колонки для записи если их нашли
                            if (colstr != "-99")
                            {
                                col = table.Columns[colstr].Ordinal;
                            }
                            else if (col == -99)
                                col = 0;

                            prm.Value = table.Rows[0].ItemArray[col];//.ToString();
                        }
                        catch
                        {
                            prm.Value = string.Empty; 
                        }
                    }
                    else if (obj is string)
                    {
                        prm.Value = Utils.Utils.AllTrim(((string)obj));
                    }
                }
            }
        }

        #endregion


        #region setMessage(string msg)
        /// <summary>
        /// Устанавливает сообщение для формы
        /// </summary>
        /// <param name="msg">сообщение</param>
        public void SetMessage(string msg)
        {
            if (_message != null)
                _message.Value = msg;
        }
        #endregion


        #region ToString
        /// <summary>
        /// Возвращает содержимое экрана в виде строки
        /// </summary>
        /// <returns></returns>
        public string ToString(Dictionary<string, object> cache, OheConnectionType connectionType)
        {
            try
            {
                // ВОзвращаем html если тип соединения web
                if (connectionType == OheConnectionType.Web)
                    return ToHtml(cache);

                var rez = new Dictionary<int, string>();

                Controls.Sort();
                int _rows = 0;
                foreach (RfControl cnl in this.Controls)
                {
                    string[] list;
                    #region Заполнение данными
                    list = cnl.ToString(cache);

                    _rows = cnl.Id;

                    // Обработка массива если есть контрол список
                    if (list != null)
                        for (int j = 0; j < list.Length; j++)
                        {
                            if (list[j] != string.Empty || list[j] != "")
                            {
                                if (!rez.ContainsKey(_rows))
                                {
                                    rez.Add(_rows, list[j]);
                                    _rows++;
                                }
                                else
                                {
                                    rez[_rows] = list[j];
                                    _rows++;
                                }
                            }
                        }
                    #endregion
                }


                StringBuilder str = new StringBuilder();

                int maxId = GetMaxIdFromDictionary(rez);
                // Выстраиваем все контролы по списку
                for (int i = 1; i <= maxId; i++)
                {
                    if (rez.ContainsKey(i))
                        str.AppendLine(rez[i]);
                    else
                        str.AppendLine();//(Environment.NewLine);
                }

                return str.ToString() + GetCursorPosition();
            }

            catch (System.Exception ex)
            {
                var exx = new ConfigurationException(ex.Message, ex);
                exx.ErrObj = Id.ToString();
                Utils.Utils.WriteLog(exx.ToString());
                return (connectionType == OheConnectionType.HandHeld ? OheConstant.TELNET_CLEAR_SEQ
                        + CenterString("ОШИБКА:")
                        + Environment.NewLine
                        + CenterString("В КОНФИГУРАЦИИ") + Environment.NewLine
                        + CenterString("обратитесь к") + Environment.NewLine
                        + CenterString("АДМИНИСТРАТОРУ") + Environment.NewLine
                        + OheConstant.SetCursorPosition(0, 0) : OheConstant.TELNET_CONFIGURATION_EXCEPTON_WEBCONNECTION);
            }

        }



        #region ToString old (comment)
        /// <summary>
        /// Возвращает содержимое экрана в виде строки
        /// </summary>
        /// <returns></returns>
        /*
        public string ToString(Dictionary<string, object> Cache)
        {

            try
            {
                Dictionary<int, string> rez = new Dictionary<int, string>();
                int _rows = 0;
                foreach (RfControl cnl in this.Controls)
                {
                    string[] list;
                    #region Заполнение данными
                    if (cnl is RfControlList)
                        list = ((RfControlList)cnl).ToString(Cache);
                    else if (cnl is RfControlHelp)
                        list = ((RfControlHelp)cnl).ToString(Cache);
                    else if (cnl is RfControlMessage)
                        list = ((RfControlMessage)cnl).ToString();
                    else
                        list = cnl.ToString(Cache);

                    _rows = cnl.Id;

                    if (list != null)
                        for (int j = 0; j < list.Length; j++)
                        {
                            if (list[j] != string.Empty || list[j] != "")
                            {
                                if (!rez.ContainsKey(_rows))
                                {
                                    rez.Add(_rows, list[j]);
                                    _rows++;
                                }
                                else
                                {
                                    rez[_rows] = list[j];
                                    _rows++;
                                }
                            }
                        }
                    #endregion
                }

                #region Добавление функциональных клавиш
                foreach (RfControlKey l in _rfformkeys)
                {
                    if (!rez.ContainsKey(l.Id))
                        rez.Add(l.Id, l.ToString(Cache));
                    else
                    {
                        rez[l.Id] = l.ToString(Cache);
                    }
                }
                #endregion

                StringBuilder userInput = new StringBuilder();

                for (int i = 1; i <= GetMaxIdFromDictionary(rez); i++)
                {
                    if (rez.ContainsKey(i))
                    {
                        userInput.Append(GetValueFromDictionary(rez, i) + Environment.NewLine);
                    }
                    else
                        userInput.Append(Environment.NewLine);
                }

                return userInput.ToString() + GetCursorPosition();
            }

            catch (Exception ex)
            {
                ConfigurationException exx = new Ohe.Telnet.Exceptions.ConfigurationException(ex.Message, ex);
                exx.ErrObj = this.Id.ToString();
                Utils.Utils.WriteLog(exx.ToString());
                return OheConstant.TELNET_CLEAR_SEQ
                        + CenterString("ОШИБКА:")
                        + Environment.NewLine
                        + CenterString("В КОНФИГУРАЦИИ") + Environment.NewLine
                        + CenterString("обратитесь к") + Environment.NewLine
                        + CenterString("АДМИНИСТРАТОРУ") + Environment.NewLine
                        + OheConstant.SetCursorPosition(0, 0);
            }

        }
        */
        #endregion


		#region CenterString - выравнивает строку по горизонтали
		  /// <summary>
		  /// Выравнивает строку по центру
		  /// </summary>
		  /// <param name="userInput">Строка</param>
		  /// <param name="width">ширина экрана</param>
		  /// <param name="heigth"> высота экрана</param>
		  /// <returns></returns>
        /*
		  private string CenterString(string userInput)
		  {
			  int width = TlnConfigManager.TELNET_WIDTH;
			  int heigth = TlnConfigManager.TELNET_HEIGHT;

			  if (userInput.Length > width || userInput.Length == 0)
				  return userInput;

			  int center = (int)Math.Round((decimal)width / 2, 0);
			  int width2 = (int)Math.Round((decimal)userInput.Length / 2, 0);

			  string rez = string.Empty;
			  for (int line = 1; line < center - width2; line++)
				  rez += " ";

			  rez += userInput + "                                      ".Substring(0, (width - (rez + userInput).Length > 0 ? width - (rez + userInput).Length : 1));

			  return rez;
		  }
         */
		  #endregion

        /// <summary>
        /// Возвращает текстовое представление формы
        /// </summary>
        /// <param name="clear">Определяет очистить экран или нет</param>
        /// <returns></returns>
        public string ToString(bool clear, Dictionary<string, object> cache, OheConnectionType connectionType)
        {
            if (connectionType == OheConnectionType.Web)
                return ToHtml(cache);

            if (clear)
                return OheConstant.TELNET_CLEAR_SEQ + ToString(cache,connectionType);

            return ToString(cache,connectionType);
        }


        private string ToHtml(Dictionary<string, object> cache)
        {
            try
            {
                var rez = new Dictionary<int, string>();

                Controls.Sort();
                int rows;
                foreach (RfControl cnl in this.Controls)
                {
                    string[] list;
                    #region Заполнение данными
                    list = cnl.ToHtml(cache);

                    rows = cnl.Id;

                    // Обработка массива если есть контрол список
                    if (list != null)
                        for (int j = 0; j < list.Length; j++)
                        {
                            if (list[j] != string.Empty || list[j] != "")
                            {
                                if (!rez.ContainsKey(rows))
                                {
                                    rez.Add(rows, list[j]);
                                    rows++;
                                }
                                else
                                {
                                    rez[rows] = list[j];
                                    rows++;
                                }
                            }
                        }
                    #endregion
                }


                StringBuilder str = new StringBuilder();

                str.Append("<form action='SendData' method='post' enctype='application/x-www-form-urlencoded'>");

                string controlsResult = _htmlSource;

                int maxId = GetMaxIdFromDictionary(rez);
                // Выстраиваем все контролы по списку
                if (_htmlSource == string.Empty)
                {
                    for (int i = 1; i <= maxId; i++)
                    {
                        if (rez.ContainsKey(i))
                        {
                            if (i == 1)
                            {
                                str.Append(
                                    rez[i].Replace(
                                        "</div>",
                                        "</div><input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='x'>"
                                    )
                                );
                            }
                            else
                            {
                                str.Append(rez[i]);
                            }
                        }
                        else
                        {
                            str.Append("<div class='separator'></div>");
                        }
                    }
                }
                else
                {
                    for (int i = 1; i <= maxId; i++)
                    {
                        controlsResult = rez.ContainsKey(i) ? controlsResult.Replace("~~"+i.ToString()+"~~", i == 1 ? rez[i].Replace("</div>", "<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='x'></div>") : rez[i]) : controlsResult.Replace("~~" + i.ToString() + "~~", "&nbsp;");
                    }

                    str.Append(controlsResult);
                }
                
                str.Append("<input type='hidden' name='ref' value='" + Id.ToString() + "'>");
                str.Append("<button type='submit' class='ui-btn-hidden' aria-disabled='false'>Отправить</button>");
                str.Append("</form>");

                return str.ToString();
            }

            catch (System.Exception ex)
            {
                var exx = new ConfigurationException(ex.Message, ex) {ErrObj = Id.ToString()};
                Utils.Utils.WriteLog(exx.ToString());
                return OheConstant.TELNET_CONFIGURATION_EXCEPTON_WEBCONNECTION;
            }
        }

        private int GetMaxIdFromDictionary(Dictionary<int, string> rez)
        {
            return rez.Max(n => n.Key);
        }

        #endregion


        #region ClearForm
        /// <summary>
        /// Очищает форму
        /// </summary>
        public void ClearForm()
        {
            foreach (RfControl cnl in Controls)
                cnl.Clear();

            SetMessage(string.Empty);
            //this._result = string.Empty;
            IsActivate = false;
            ResultFromChild = string.Empty;
            CurrentEvent = Events.UnBounded;
        }       
        #endregion


        #region Dispose (comment)
        /*
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool Dispose(bool dis)
        {
            if (dis)
                component.Dispose();
            _dispose = true;

            return _dispose;
        }
        */
        #endregion


        #region HasEventByName (comment)
        /// <summary>
        /// Возвращает переменную о наличие события по имени
        /// </summary>
        /// <param name="name">Имя события</param>
        /// <returns>bool</returns>
        /*
        private bool HasEventByName(string name)
        {
            switch (Utils.Utils.AllTrim(name.ToUpperInvariant()))
            {
                case "ONINIT":
                    if (OnControlInitEvent != null)
                        return true;
                    else
                        return false;

                case "ONSUBMIT":
                    if (OnConrolSubmitEvent != null)
                        return true;
                    else
                        return false;
                case "ONEXIT":
                    if (OnControlExitEvent != null)
                        return true;
                    else
                        return false;
            }
            return false;
        }
        */
        #endregion


        #region GetValueFromDictionary (comment)
        /*
        private string GetValueFromDictionary(Dictionary<int, string> rez, int id)
        {

            foreach (KeyValuePair<int, string> kvp in rez)
            {
                if (kvp.Key == id)
                    return kvp.Value;
            }

            return string.Empty;
        }
        */
        #endregion


        #region GetControlKeyById(int Id) (comment)
        /// <summary>
        /// Возвращает управляющий контрол по идентификатору
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>RfControlKey</returns>
        /*
        public RfControlKey GetControlKeyById(int Id)
        {
            foreach (RfControlKey cnl in _rfformkeys)
            {
                if (cnl.Id == Id)
                        return cnl;
            }
            return new RfControlKey();
        }
        */
        #endregion

		#endregion

        #region Methods Use for Development
		#region ToString
        /*
		  /// <summary>
        /// Возвращает содержимое экрана в виде строки
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            try
            {
                //string[,] rez = new string[100,2];//this.Controls.Count + _rfformkeys.Count];
                Dictionary<int, string> rez = new Dictionary<int, string>();

                for (int i = 1; i < 99; i++)
                {
                    foreach (RfControl cnl in this.Controls)
                    {
                        #region Заполнение данными
                        if (cnl.Id == i && !rez.ContainsKey(i))
                        {
                            if (cnl is RfControlList)
                            {
                                string[] list = ((RfControlList)cnl).ToString();
                                for (int j = 0; j < list.Length; j++)
                                {
                                    if (list[j] != string.Empty || list[j] != "")
                                    {
                                        rez.Add(i, list[j] + Environment.NewLine);
                                        i++;
                                    }
                                }
                                i--;
                            }
                            else if (cnl is RfControlHelp)
                                rez.Add(i, ((RfControlHelp)cnl).ToString() + Environment.NewLine);
                            else if (cnl is RfControlMessage)
                                rez.Add(i, ((RfControlMessage)cnl).ToString() + Environment.NewLine);
                            else
                                rez.Add(i, cnl.ToString() + Environment.NewLine);

                            break;
                        }
                        #endregion
                    }
                }
                #region Добавление функциональных клавиш
                for (int i = 1; i < 99; i++)
                {
                    if (_rfformkeys.Count == 0)
                        break;

                    foreach (RfControlKey l in _rfformkeys)
                    {
                        if (l.Id == i)
                        {
                            if(!rez.ContainsKey(i))
                                rez.Add(i, l.ToString());
                            break;
                        }
                    }
                }
                #endregion


                StringBuilder userInput = new StringBuilder();
                for (int i = 1; i <= GetMaxIdFromDictionary(rez); i++)
                {
                    if (rez.ContainsKey(i))
                        userInput.Append(GetValueFromDictionary(rez, i));
                    else
                        userInput.Append(Environment.NewLine);
                }

                return userInput.ToString();
            }
            catch (Exception ex)
            {
                ConfigurationException exx = new Ohe.Telnet.Exceptions.ConfigurationException(ex.Message,ex);
                exx.ErrObj = this.Id.ToString();
                Utils.Utils.WriteLog(exx.ToString());
                return OheConstant.TELNET_CLEAR_SEQ
                        + CenterString("ОШИБКА:")
                        + Environment.NewLine
                        + CenterString("В КОНФИГУРАЦИИ") + Environment.NewLine
                        + CenterString("обратитесь к") + Environment.NewLine
                        + CenterString("АДМИНИСТРАТОРУ") + Environment.NewLine
                        + OheConstant.SetCursorPosition(0, 0);
            }

        }
        */
        #endregion        
        #endregion

    }
}
