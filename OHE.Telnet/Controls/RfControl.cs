﻿using System;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Text;
using Ohe.Telnet.Data;
using Ohe.Telnet.Exception;
using Ohe.Utils;
using Ohe.Telnet.Configuration;
using Ohe.Utils.Interface;


namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Описывает контрол ввода данных 
    /// </summary>
    public class RfControl : RfFormBase, IOheControlInterface
    {

        #region Define Variables
        
            private string _idValue = string.Empty;   // определяет идентификатор переменной которая используется
                                                      // как общая переменная в процессе (обязателен для input контролов)

            private bool _readOnly;   // определяет контрол только для чтения или нет

            private string _valueType = "string"; // определяет тип значения контрола

            private string _align;     // определяет положение по горизонтали контрола только 
                                       // для текстового контрола

            private int _x = -1;            // координата по Х курсора

            private int _y;        // координата по Y курсора

            private bool _require;  // определяет обязательный параметр для ввода или нет

            private string _column = "-99";  // определяет порядковый номер в колонке или название колонки

            private bool _ismark;

            private bool _isValid = true;

            private bool _isPassword;

            private bool _scanOnly = false;  // Параметр определяет, что значение можно только сканировать

        #endregion


        #region Properties

            #region X
            /// <summary>
            /// Возвращает координату Х
            /// </summary>
            public int X { get { return _x; } set { _x = value; } }
            
            #endregion


            #region Y
            /// <summary>
            /// Возвращает координату Y
            /// </summary>
            public int Y { get { return _y; } set { _y = value; } }
            
            #endregion


            #region Mandatory
            /// <summary>
            /// Определяет обязателен параметр для ввода или нет
            /// </summary>
            public bool Mandatory { get { return _require; } set { _require = value; } }
            
            #endregion


            #region typeValue
            /// <summary>
            /// Возвращает тип значения для контрола
            /// </summary>
            public string typeValue { get { return _valueType; } set { _valueType = value; } }
            
            #endregion


            #region ReadOnly
            /// <summary>
            /// Определяет можно или нельзя редактировать контрол
            /// </summary>
            public bool ReadOnly { get { return _readOnly; } set { _readOnly = value; } }
            
            #endregion


            #region ValueID
            /// <summary>
            /// Возвращает идентификатор значения контрола, используется 
            /// другими контролами в системе
            /// </summary>
            public string ValueID { get { return Utils.Utils.AllTrim(_idValue.ToUpper()); } set { _idValue = value; } }
            
            #endregion


            #region Column
            /// <summary>
            /// Определяет название колонки в таблице данных
            /// </summary>
            public string Column { get { return _column; } set { _column = Utils.Utils.AllTrim(value); } }
            
            #endregion


            #region Align
            /// <summary>
            /// Возвращает и устанавливает горизонтальное положение контрола на экране
            /// </summary>
            public string Align { get { return _align; } set { _align = Utils.Utils.AllTrim(value.ToLowerInvariant()); } }
            
            #endregion


            #region IsMark
            /// <summary>
            /// Возвращает и устанавливает значение признака маркировки 
            /// контрола (фон другим цветом)
            /// </summary>
            public bool IsMark { get { return _ismark; } set { _ismark = value; } }
            
            #endregion


            #region IsValid
            /// <summary>
            /// Определяет корректность контрола
            /// Т.е. корректность ввода пользователем данных
            /// Если конфигурация контрола не верная - сама форма не верная
            /// </summary>
            public bool IsValid { get { return _isValid; } set { _isValid = value; } }
            
            #endregion


            #region Old Properties (comment)
        //private int _telnet_width = 0;
        //private bool _isActive; // возвращает идентификатор определяющий соотояния контрола
        //private List<RfControlEvents> _events;
        //private OheConstant.Type _type; // опеределяет тип контрола
        //private string _descript; // описание которое будет написано для контрола
        //private string _value;    // определяет значение контрола

        //private int _parent;      // определяет идентификатор экрана на который надо перейти при возврате
        //private int _id;          // определяет Id контрола, также определяет строку
        // находится контрол
        /// <summary>
        /// Возвращает идентификатор контрола
        /// </summary>
        //public int Id { get { return _id; }}

        /// <summary>
        /// Взвращает идентификатор экрана верхнего уровня
        /// </summary>
        //public int Parent { get { return _parent; } set { _parent = value; } }

        /// <summary>
        /// Определяет активен ли контрол или нет
        /// </summary>
        //public bool IsActivate { get { return _isActive; }  set { _isActive = value; } }

        /// <summary>
        /// Определяет значение контрола
        /// </summary>
        //public string Value { get { return _value; } set { _value = value; } }

        /// <summary>
        /// Определяет тип контрола
        /// Поддерживает следующие типы - Text, InputBox, Param, ControlKey 
        /// </summary>
        //public OheConstant.Type ControlType { get { return _type; } set { _type = value; } }

        //private string _checkValue = string.Empty;
        /// <summary>
        /// Параметр, который определяет видимость контрола от
        /// взависимости от глобальных значений кеша
        /// </summary>
        //public string Checkvalue { get { return _checkValue; } set { _checkValue = value; } } 
        /// <summary>
        /// Возвращает статический текст для контрола
        /// </summary>
        //public string Descript { get { return _descript; } set { _descript = value; } }
        /// <summary>
        /// Возвращает список событий определенных для контрола
        /// </summary>
        //public List<RfControlEvents> Events  {  get { return _events; } set { _events = value; } }

        #endregion

            public bool IsPassword { get { return _isPassword; } set { _isPassword = value; } }


            public bool ScanOnly { get { return _scanOnly; } }

        #endregion

        
        #region Constructors


            public RfControl() { Width = TlnConfigManager.TELNET_WIDTH;  _isPassword = false;}


            public RfControl(RfForm parentForm, RfControl parentControl)
                : base(parentForm, parentControl)
            {
                Width = TlnConfigManager.TELNET_WIDTH;
            }

        #endregion


        #region Methods

        #region override InitControlFromXML
        /// <summary>
        /// Генерирует событие для форм, или контролов
        /// </summary>
        public override void InitControlFromXML(XmlNode node)
        {
            Id = 0;

            base.InitControlFromXML(node);
            string id = string.Empty;
            foreach (XmlAttribute attr in node.Attributes)
            {
                #region Switch
                switch (Utils.Utils.AllTrim(attr.Name.ToUpperInvariant()))
                {
                    case "ID":
                        id = Utils.Utils.AllTrim(attr.Value.ToUpperInvariant());
                        if (id.Contains("(") && id.Contains(")"))
                        {
                            int start = id.IndexOf("(");
                            _idValue = id.Substring(0, start);
                            _column = id.Substring(start + 1, (id.Length - start) - 2);
                        }
                        else
                            _idValue = id;

                        break;
                    case "LINE":
                        #region line
                        try
                        {
                            Id = int.Parse(attr.Value);
                        }
                        catch (FormatException ex)
                        {
                            throw new ConfigurationException(ex,
                                              "Ошибка: неверный тип параметра"
                                              + Environment.NewLine
                                              + "Идентификатор формы: " + Parent
                                              + Environment.NewLine
                                              + "Идентификатор Контрола: " + attr.Value
                                              );
                        }
                        #endregion
                        break;
                    case "TYPE":
                        _valueType = Utils.Utils.AllTrim(attr.Value.ToLowerInvariant());
                        break;
                    case "READONLY":
                        _readOnly = Utils.Utils.StrToBool(Utils.Utils.AllTrim(attr.Value));
                        break;
                    case "MANDATORY":
                        _require = Utils.Utils.StrToBool(Utils.Utils.AllTrim(attr.Value));
                        break;
                    case "CURSOR_X":
                        #region cursor_x
                        try
                        {
                            _x = int.Parse(attr.Value);
                        }
                        catch (FormatException ex)
                        {
                            throw new ConfigurationException(ex,
                                              "Ошибка: неверный тип параметра"
                                              + Environment.NewLine
                                              + "Идентификатор формы: " + ParentForm.Id.ToString()
                                              + Environment.NewLine
                                              + "Идентификатор Контрола: " + Id.ToString()
                                              + Environment.NewLine
                                              + "Параметр: cursor_x = " + attr.Value);
                        }

                        #endregion
                        break;
                    case "CURSOR_Y":
                        #region cursor_y
                        try
                        {
                            _y = int.Parse(attr.Value);
                        }
                        catch (FormatException ex)
                        {
                            throw new ConfigurationException(ex,
                                              "Ошибка: неверный тип параметра"
                                              + Environment.NewLine
                                              + "Идентификатор формы: " + ParentForm.Id.ToString()
                                              + Environment.NewLine
                                              + "Идентификатор Контрола: " + Id.ToString()
                                              + Environment.NewLine
                                              + "Параметр: cursor_y = " + attr.Value);
                        }

                        #endregion
                        break;
                    case "ALIGN":
                        _align = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "ISMARK":
                        _ismark = Utils.Utils.StrToBool(attr.Value);
                        break;
                    case "PASSWORD":
                        _isPassword = Utils.Utils.StrToBool(attr.Value);
                        break;

                    case "SCANONLY":
                        _scanOnly = Utils.Utils.StrToBool(attr.Value);
                        break;
                }
                #endregion
            }

            if (Id == 0)
                throw new ConfigurationException(
                                          "Ошибка: неверный параметр контрола"
                                          + Environment.NewLine
                                          + "Идентификатор формы: " + ParentForm.Id.ToString()
                                          + Environment.NewLine
                    //+ "Тип контрола: " + controltype
                                          );

            _x = (_x == -1 ? Description.Length + 1 : _x);
        }
        #endregion


        #region override OnChange
        public override bool OnChange(string str, ref Dictionary<string, object> results)
        {
            if (!IsActivate) return true;

            // Проверка что у нас контрол требует только сканирования
            // И пришли не пустые данные
            if (ScanOnly && !string.IsNullOrEmpty(str))
            {
                if (!str.EndsWith("\\u007F", StringComparison.OrdinalIgnoreCase))
                {

                    ParentForm.SetMessage("СКАНИРУЙ ЗНАЧЕНИЕ");
                    // Обработали текущий контрол надо возвращать данные пользователю
                    ParentForm.CurrentState = FormState.InValid;
                    return false;
                }
                else
                    str = str.Replace("\\U007F", "");
            }
            str = str.Replace("\\U007F", "");


            // Мы пришли на контрол первый раз
            // у контрола нет процедуры инициализации или просто 
            // она вернула пустое значение, т.е. значения у контрола нет
            // Пользователь ввел какое-то значение или нет
            if (Value == string.Empty)
            {
                #region Проверка на возможность пустого значения контрола
                if ((Mandatory) && Utils.Utils.AllTrim(str) == string.Empty)
                {
                    #region Проверка на то что пользователь ввел пустое значение
                    ParentForm.SetMessage("ВВЕДИ ДАННЫЕ");
                    //_result = this.ToString(true) + GetCursorPosition();
                    ParentForm.CurrentEvent = Events.OnChange;
                    ParentForm.CurrentState = FormState.InValid;
                    #endregion
                }
                else
                {
                    #region Использовать пустые значения можно или пользователь что-то ввел
                    // ПРоверка на корректность значения контрола
                    if (ValidateValues(str))
                    {
                        #region Данные введены корректные
                        ParentForm.CurrentEvent = Events.OnChange;

                        // Добавляем параметр в общую кучу параметров
                        if (!results.ContainsKey(ValueID))
                            results.Add(ValueID, Utils.Utils.AllTrim(str));
                        else
                            results[ValueID] = Utils.Utils.AllTrim(str);

                        // Обнуляем ошибки
                        results["SYS_ERROR"] = string.Empty;

                        // Проверка на наличие события обновления в базе
                        if (GetExistsEventByName("OnChange"))
                        {
                            if (!ExecuteEventHandlerControl("OnChange", ref results))
                            {
                                // Событие по каким-то причинам не выполнилось
                                ParentForm.SetMessage(results["SYS_ERROR"].ToString());
                                // Проверка, на то что возникла системная ошибка
                                if (results["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА")
                                {
                                    ParentForm.CurrentState = FormState.Exception;
                                }
                                return false;
                            }
                            // Хранимая процедура возвратила несколько строк в результате
                            // надо выбирать один
                            
                            if (((DataTable)results[ValueID]).Rows.Count > 1)
                            {
                                ParentForm.CurrentState = FormState.MultiResult;
                                return false;
                            }
                        }

                        #endregion

                        // Обработали текущий контрол надо возвращать данные пользователю
                        ParentForm.CurrentState = FormState.Valid;
                        Value = Utils.Utils.AllTrim(str);
                        return true;
                    }

                    #region Неверный формат данных от пользователя
                    ParentForm.SetMessage("НЕВЕРНЫЙ ТИП ДАННЫХ");
                    //_result = this.ToString(true) + GetCursorPosition();
                    // Обработали текущий контрол надо возвращать данные пользователю
                    ParentForm.CurrentState = FormState.InValid;
                    return false;
                    #endregion

                    #endregion
                }
                #endregion
            }
            // теперь у контрола есть параметр который
            // возможно пришел с процедуры инициализации
            // но пользователь изменил значение
            else if (Value != string.Empty && Value != Utils.Utils.AllTrim(str) && Utils.Utils.AllTrim(str) != string.Empty)
            {
                #region Проверка на значения
                // ПРоверка на корректность значения контрола
                if (ValidateValues(str))
                {
                    // Добавляем параметр в общую кучу параметров
                    if (!results.ContainsKey(ValueID))
                        results.Add(ValueID, Utils.Utils.AllTrim(str));
                    else
                        results[ValueID] = Utils.Utils.AllTrim(str);

                    ParentForm.CurrentEvent = Events.OnChange;
                    Value = Utils.Utils.AllTrim(str);

                    // Проверка на наличие события обновления в базе
                    if (GetExistsEventByName("OnChange"))
                        if (!ExecuteEventHandlerControl("OnChange", ref results))
                        {
                            ParentForm.SetMessage(results["SYS_ERROR"].ToString());
                            // Проверка, на то что возникла системная ошибка
                            if (results["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА")
                            {
                                ParentForm.CurrentState = FormState.Exception;
                            }
                            return false;
                        }
                        // Хранимая процедура возвратила несколько строк в результате
                        // надо выбирать один
                        else if (((DataTable)results[ValueID]).Rows.Count > 1)
                        {
                            ParentForm.CurrentState = FormState.MultiResult;
                            return false;
                        }


                    // Обработали текущий контрол надо возвращать данные пользователю
                    ParentForm.CurrentState = FormState.Valid;
                    return true;
                }
                
                ParentForm.SetMessage("НЕВЕРНЫЙ ТИП");
                //_result = this.ToString(true);
                // Обработали текущий контрол надо возвращать данные пользователю
                ParentForm.CurrentState = FormState.InValid;
                return false;

                #endregion
            }
            // теперь у контрола есть параметр который
            // возможно пришел с процедуры инициализации
            // пользователь ввел пустое значение, т.е. подтвердил текущее значение
            else if (Value != string.Empty && (str == string.Empty || Value == Utils.Utils.AllTrim(str)))
            {
                #region ПРоверка значений
                // ПРоверка на корректность значения контрола
                if (ValidateValues(Value))
                {
                    ParentForm.CurrentEvent = Events.OnChange;

                    // Добавляем параметр в общую кучу параметров
                    if (!results.ContainsKey(ValueID))
                        results.Add(ValueID, Value);
                    else
                        results[ValueID] = Value;

                    // Проверка на наличие события обновления в базе
                    if (GetExistsEventByName("OnChange"))
                        if (!ExecuteEventHandlerControl("OnChange", ref results))
                        {
                            // Событие по каким-то причинам не выполнилось
                            ParentForm.SetMessage(results["SYS_ERROR"].ToString());
                            // Проверка, на то что возникла системная ошибка
                            if (results["SYS_ERROR"].ToString() == "СИСТЕМНАЯ ОШИБКА")
                            {
                                ParentForm.CurrentState = FormState.Exception;
                            }
                            return false;
                        }
                        // Хранимая процедура возвратила несколько строк в результате
                        // надо выбирать один
                        else if (((DataTable)results[ValueID]).Rows.Count > 1)
                        {
                            ParentForm.CurrentState = FormState.MultiResult;
                            return false;
                        }

                    // Обработали текущий контрол надо возвращать данные пользователю
                    ParentForm.CurrentState = FormState.Valid;
                    return true;
                }
                
                
                ParentForm.SetMessage("НЕВЕРНЫЙ ТИП");
                // Обработали текущий контрол надо возвращать данные пользователю
                ParentForm.CurrentState = FormState.InValid;
                return false;

                #endregion
            }

            return true;
        }
        
        #endregion


        #region GetEventByName(string name)
        /// <summary>
        /// Возвращает событие для контрола по имени
        /// </summary>
        /// <param name="name">Имя события</param>
        /// <param name="cache">Список параметров из глобального кеша</param>
        /// <returns>RfControlEvents</returns>
        protected RfControlEvents GetEventByName(string name, Dictionary<string,object> cache)
        {
            switch (Utils.Utils.AllTrim(name))
            {
                case "OnInit":
                    return OnControlInitEvent;
                case "OnExit":
                    return OnControlExitEvent;
                case "OnChange":
                    return OnControlChangeEvent;
                case "OnSubmit":
                    return OnConrolSubmitEvent;
                default:
                    return new RfControlEvents();
            }
        }
        #endregion


        #region ExecuteEventHandlerControl(string name) - выполняет событие по имени
        /// <summary>
        /// Возвращает результат после запуска определенного события принадлежащего контролу
        /// Контрол поддерживает два события OnInit, OnChange   
        /// </summary>
        /// <param name="name">Имя события</param>
        /// <param name="results">Список параметров из глобального кеша</param>
        /// <returns>bool</returns>
        public virtual bool ExecuteEventHandlerControl(string name, ref Dictionary<string, object> results)
        {
            object tbl = null;
            string rez = string.Empty;
            RfControlEvents rfEvent = null;

            try
            {
                // Определяем имя события для контрола
                rfEvent = GetEventByName(name, results);

                // Возвращаем объект
                try
                {
                    // Устанавливаем значения параметров в хранимую процедуру
                    rfEvent.Params = SetParamsForSp(rfEvent.Params, results);
                    tbl = ExecuteSqlQuery.ExecuteEvent(rfEvent.SpName, rfEvent.Params, results, ParentForm);
                }
                // Обработка ошибок связанных с данными и запуском хранимых процедур
                #region Exception
                catch (SqlException ex)
                {
                    results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                    Utils.Utils.WriteLog("Ошибка выполнения sql комманд: " + Environment.NewLine
                        + "Хранимая процедура:" + rfEvent.SpName + Environment.NewLine
                        + "Форма: " + rfEvent.FormId.ToString() + Environment.NewLine
                        + "Сообщение:" + Environment.NewLine +  ex.ToString());
                    return false;
                }
                catch (SqlDataException ex)
                {
                    results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                    Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.ToString());
                    return false;
                }
                catch (LogicException ex)
                {
                    results["SYS_ERROR"] = ex.Message;
                    return false;
                }
                catch (FormatException ex)
                {
                    //Results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                    results["SYS_ERROR"] = "НЕВЕРНЫЙ ТИП";
                    Utils.Utils.WriteLog("Ошибка конвертирования формата данных: " + ex.Message);
                    return false;
                }
                catch (NullReferenceException ex)
                {
                    results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                    Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.Message);
                    return false;
                }

                
                #endregion                
                // Проверка на то что хранимая процедура отработала нормально
                // Добавляем ключ к общему кешу
                if (results.ContainsKey(this.ValueID)) results[this.ValueID] = tbl; 
                    else results.Add(this.ValueID, tbl);


                // Определяем значение параметра только в том случае если это не 
                // контрол ввода (или контрол ввода имеет атрибут только для чтения)
                if (tbl is DataTable)
                {
                    #region Обработка таблицы
                    DataTable dt = (DataTable)tbl;
                    int column = -99;
                    string strcolumn = string.Empty;
                    try
                    {
                        column = int.Parse(Column);
                    }
                    catch
                    {
                        strcolumn = Column;
                    }

                    if (strcolumn != string.Empty && strcolumn != "-99")
                    {
                        column = dt.Columns[strcolumn].Ordinal;
                    }
                    else if (column == -99)
                        column = 0;

                    try
                    {
                        switch (_valueType)
                        {
                            case "datetime":
                                object date = dt.Rows[0].ItemArray[column];
                                if (date is DateTime)
                                {
                                    rez = Utils.Utils.DateTimeToStr(((DateTime)date),string.Empty,TlnConfigManager.ShortDateTime);
                                }
                                rez = "01.01.1900";

                                //rez = Utils.Utils.Substring(dt.Rows[0].ItemArray[column].ToString(),0,TlnConfigManager.ShortDateTime.Length);
                                break;
                            default :
                                rez = Utils.Utils.AllTrim(dt.Rows[0].ItemArray[column].ToString());
                                break;
                        }
                        
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                        Utils.Utils.WriteLog("Ошибка обработки данных:"
                                            + Environment.NewLine + "Экран: " + ParentForm.Id.ToString()
                                            + Environment.NewLine + "Объект: " + Id.ToString()
                                            + Environment.NewLine + "Контрол:" + ValueID
                                            + Environment.NewLine  + "Хранимая процедура:" + rfEvent.SpName
                                            + Environment.NewLine  + "Не вернула результат запроса."
                                            + Environment.NewLine  + "Ошибка: " + ex.Message
                                                    );
                    }
                    catch (System.Exception ex)
                    {
                        results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                        Utils.Utils.WriteLog("Ошибка обработки данных:"
                                            + Environment.NewLine + "Экран: " + ParentForm.Id.ToString()
                                            + Environment.NewLine + "Название колонки или идентификатор не существует"
                                            + Environment.NewLine + "Контрол:" + ValueID
                                            + Environment.NewLine + "Ошибка: " + ex.Message);

                        //Utils.Utils.WriteLog("Ошибка обработки данных: " + ex.ToString());
                        return false;
                    }
                    #endregion                
                }
                
                // Если значение пустое, то присваиваем текущее
                if (Value == string.Empty)
                        Value = rez;

                return true;
            }
            catch (NullReferenceException ex)
            {
                results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                Utils.Utils.WriteLog("Ошибка обработки данных: "
                                            + Environment.NewLine + "Экран: " + ParentForm.Id.ToString()
                                            + Environment.NewLine + "Контрол:" + Id
                                            + Environment.NewLine + "Ссылка на объект:" + ValueID
                                            + Environment.NewLine + "Хранимая процедура: [" + rfEvent.SpName +"] нет возвращаемого значения контрола. ["  + Id.ToString() + "(" + ValueID+ ")] "
                                            + Environment.NewLine + "Значение:" + Value
                                            + Environment.NewLine + "Внутренняя ошибка: "
                                            + Environment.NewLine + "Описание: " + ex.Message + ex.InnerException.Message
                                            );

                return false;
            }
            catch (System.Exception ex) 
            {
                Utils.Utils.WriteLog("Ошибка обработки данных возвращаемых хранимой процедурой: "
                                            + Environment.NewLine + "Экран: " + ParentForm.Id.ToString()
                                            + Environment.NewLine + "Контрол:" + Id
                                            + Environment.NewLine + "Ссылка на объект:" + ValueID
                                            + Environment.NewLine + "Хранимая процедура: " + rfEvent.SpName
                                            + Environment.NewLine + "Значение:" + Value
                                            + Environment.NewLine + "Описание: " + ex.Message
                                            + Environment.NewLine + "Внутренняя ошибка: " + ex.InnerException
                                            + Environment.NewLine + "Стек вызова: " + (ex.StackTrace == null ? "" : ex.StackTrace)
                                            );
                results["SYS_ERROR"] = "СИСТЕМНАЯ ОШИБКА";
                return false; 
            }
            // ВОзвращаем результат
        } 
        #endregion


        #region SetParamsForSp - устанавливает текущие значения параметров
        /// <summary>
        /// Устанавливает параметры для хранимой процедуры из глобального кеша
        /// </summary>
        /// <param name="Params">Список параметров для хранимой процедуры</param>
        /// <param name="cache">Список параметров из глобального кеша</param>
        protected List<RfControlEventsParams> SetParamsForSp(List<RfControlEventsParams> Params, Dictionary<string, object> cache)
        {
            var rez = new List<RfControlEventsParams>();

            foreach (RfControlEventsParams prm in Params)
            {
                // Тут забираем параметры у других контролов
                if (prm.ControlId != string.Empty)
                {
                    #region Обработка контрола с параметрами из глобального кеша
                    // Проверка на то что используются системные переменные
                    #region Глобальные переменные
                    switch (prm.ControlId)
                    {
                        case "SYS_DATE":
							    prm.Value = Utils.Utils.DateTimeToStr(DateTime.Now, TlnConfigManager.ShortDateTime, TlnConfigManager.ShortDateTime);
                                rez.Add(prm);
                            continue;
                        case "SYS_DATETIME":
								prm.Value = Utils.Utils.DateTimeToStr(DateTime.Now, "dd.MM.yyyy HH:mm", TlnConfigManager.ShortDateTime);
                                rez.Add(prm);
                            continue;
                        case "SYS_TIME":
								prm.Value = Utils.Utils.DateTimeToStr(DateTime.Now, "HH:mm", TlnConfigManager.ShortDateTime);
                                rez.Add(prm);
                            continue;
                        case "SYS_YEAR":
								prm.Value = Utils.Utils.DateTimeToStr(DateTime.Now, "yyyy", TlnConfigManager.ShortDateTime);
                                rez.Add(prm);
                            continue;
                        case "SYS_MONTH":
								prm.Value = Utils.Utils.DateTimeToStr(DateTime.Now, "MM", TlnConfigManager.ShortDateTime);
                                rez.Add(prm);
                            continue;
                        case "SYS_DAY":
								prm.Value = Utils.Utils.DateTimeToStr(DateTime.Now, "dd", TlnConfigManager.ShortDateTime); ;
                                rez.Add(prm);
                            break;
                    }
                    #endregion

                    // Определяем пустое значение параметра если записи
                    // нет в глобальном кеше
                    if (!cache.ContainsKey(prm.ControlId))
                    {
                        prm.Value = string.Empty;
                        rez.Add(prm);
                        continue;
                    }

                    var obj = cache[prm.ControlId];

                    if (obj is DataTable)
                    {
                        #region Обработка таблицы
                        DataTable dt = (DataTable)obj;
                        int column = -99;
                        string strcolumn = "-99";
                        try
                        {
                            column = int.Parse(prm.Column);
                        }
                        catch
                        {
                            strcolumn = prm.Column;
                        }

                        try
                        {
                            if (strcolumn != "-99")
                            {
                                column = dt.Columns[strcolumn].Ordinal;
                            }
                            else if (column == -99)
                                column = 0;
                        }
                        catch (System.Exception ex)
                        {
                            /*
                             Utils.Utils.WriteLog("Сообщение: Нет значения параметра:"
                                                    + Environment.NewLine + "Экран: " + ParentForm.Id.ToString()
                                                    + Environment.NewLine + "Параметр: " + prm.Name + Environment.NewLine 
                                                    + "Значение: " + prm.ControlId + "(" + prm.Column + ")");*/
                             prm.Value = string.Empty;
                             rez.Add(prm);
                             continue;
                        }

                        prm.Value = dt.Rows[0].ItemArray[column]; // Utils.Utils.ConvertStrToDbObject(dt.Rows[0].ItemArray[column].ToString(), prm.Type, string.Empty);
                        
                        #endregion
                    }
                    else if (obj is string)
                    {
                        prm.Value = Utils.Utils.ConvertStrToDbObject((string)obj, prm.Type, string.Empty);
                    } 
                    #endregion
                }

                rez.Add(prm);
            }

            return rez;
        }
        #endregion
        

        #region GetExistsEventByName(string id)
        /// <summary>
        /// Возвращает bool есть или нет событие по имени
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Bool</returns>
        public bool GetExistsEventByName(string name)
        {
            switch (Utils.Utils.AllTrim(name))
            {
                case "OnInit":
                    return OnControlInitEvent!=null;
                case "OnExit":
                    return OnControlExitEvent != null;
                case "OnChange":
                    return OnControlChangeEvent != null;
                case "OnSubmit":
                    return OnConrolSubmitEvent != null;
                default:
                    return false;
            }
        }
        #endregion


        #region ValidateValues
        /// <summary>
        /// Процедура проверяет корректность данный введенный пользователем в соответсвтие с типом контролов
        /// 20.10.2011 Дополнительная проверка округления нечелых типов
        /// </summary>
        /// <returns></returns>
        public bool ValidateValues(string data)
        {
            if (data == string.Empty)
                    return true;
            try
            {
                Utils.Utils.ConvertStrToDbObject(data, _valueType,string.Empty);
                return true;
            }
            catch { return false; }
        } 
        #endregion


        #region GetValueFromTable
        /// <summary>
        /// Возвращает значение контрола
        /// </summary>
        /// <param name="cache"></param>
        /// <returns></returns>
        private string GetValueFromTable(Dictionary<string, object> cache)
        {
            object rez = null;
            // Определяет что контрол типа текст и он днамический
            // Контрол имеет название или порядковый номер колонки
            // как параметр вывода
            if (
                (!string.IsNullOrEmpty(ValueID) && OheControlType == OheConstant.OheTelnetControlType.Text)
                ||
                (Column != "-99" && OheControlType == OheConstant.OheTelnetControlType.InputBox)
               )
            {
                if (!cache.ContainsKey(ValueID))
                    return string.Empty;

                object o = cache[ValueID];
                DataTable dt = o as DataTable;

                #region Поиск значения контрола
                try
                {
                    if (dt is DataTable)
                    {
                        if (Column != "-99")
                        {
                            try
                            {
                                rez = dt.Rows[0].ItemArray[dt.Columns[Column].Ordinal];
                            }
                            catch
                            {
                                rez = dt.Rows[0].ItemArray[int.Parse(Column)];
                            }

                            if(rez is DateTime)
                            {
                                return Utils.Utils.DateTimeToStr((DateTime)rez, TlnConfigManager.ShortDateTime, TlnConfigManager.ShortDateTime);
                            }

                            return Utils.Utils.Str2Telnet(rez.ToString());
                        }

                        return Utils.Utils.Str2Telnet(dt.Rows[0].ItemArray[1].ToString());
                    }

                    if (Column != "-99")
                        return string.Empty;

                    return Utils.Utils.Str2Telnet((string)o);
                }
                catch
                {
                    return string.Empty;
                }

                #endregion
            }

            return Utils.Utils.Str2Telnet(Value);
        }
        #endregion


        #region virtual ToString()
        /// <summary>
        /// Процедура возвращает строку которая должна отобразиться на терминале
        /// </summary>
        /// <returns></returns>
        public virtual string[] ToString(Dictionary<string, object> cache)
        {
            string rez = string.Empty;
            if(!IsVisible(cache))
                return null;// string.Empty;

            char[] str = Environment.NewLine.ToCharArray();


            switch (_align)
            {
                case "left":
                    rez = Description + GetValueFromTable(cache);
                    if (rez.Trim().Length > Width)
                    {
                        if (IsMark)
                            rez = OheConstant.TELNET_ESC_SEQ + "7m" + rez.Insert(Width, OheConstant.TELNET_ESC_SEQ + "0m" + "\r\n" + OheConstant.TELNET_ESC_SEQ + "7m") + OheConstant.TELNET_ESC_SEQ + "0m";
                        else
                            rez = rez.Insert(Width, "\r\n");
                        rez = string.Format(rez, Environment.NewLine);
                    }
                    else
                    {
                        rez = string.Format
                            (((IsMark) ? OheConstant.TELNET_ESC_SEQ + "7m" : "") +
                            rez + ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "0m" : "")
                            , Environment.NewLine
                            );
                    }
                    break;
                case "right":
                    rez = Description + GetValueFromTable(cache);
                    if (rez.Trim().Length <= Width)
                        for (int i = rez.Length; i < Width; i++)
                            rez = rez.Insert(0, " ");
                    else
                    {
                        if (IsMark)
                            rez = OheConstant.TELNET_ESC_SEQ + "7m" + rez.Insert(Width, OheConstant.TELNET_ESC_SEQ + "0m" + "\r\n" + OheConstant.TELNET_ESC_SEQ + "7m") + OheConstant.TELNET_ESC_SEQ + "0m";
                        else
                            rez = rez.Insert(Width, "\r\n");
                        rez = string.Format(rez, Environment.NewLine);

                        break;
                    }

                    rez = ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "7m" : "") + rez + ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "0m" : "");
                    break;
                case "center":
                    rez = CenterString(Description + GetValueFromTable(cache));

                    if (rez.Trim().Length > Width)
                    {
                        if (IsMark)
                            rez = OheConstant.TELNET_ESC_SEQ + "7m" + rez.Insert(Width, OheConstant.TELNET_ESC_SEQ + "0m" + "\r\n" + OheConstant.TELNET_ESC_SEQ + "7m") + OheConstant.TELNET_ESC_SEQ + "0m";
                        else
                            rez = rez.Insert(Width, "\r\n");

                        rez = string.Format(rez, Environment.NewLine);
                    }
                    else
                    {
                        rez = string.Format
                            (
                            ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "7m" : "")
                            + rez
                            + ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "0m" : "")
                            , Environment.NewLine);
                    }
                    break;

                case "justify":
                    rez = Description;
                    string rez2 = GetValueFromTable(cache);
                    if ((rez + rez2).Length < Width)
                    {
                        for (int i = rez.Length; i < Width; i++)
                        {
                            rez = rez.Insert(rez.Length, " ");
                            if ((rez + rez2).Length < Width)
                            {
                                int ss = Width - (rez + rez2).Length;

                                rez = string.Format
                                    (
                                    ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "7m" : "")
                                    +
                                    rez + rez2
                                    + "                                                ".Substring(0, ss)
                                    + ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "0m" : "")
                                    , Environment.NewLine);

                                return rez.Split(str);
                            }
                        }
                    }
                    else
                    {
                        rez = rez + rez2;
                        if (rez.Trim().Length > Width)
                        {
                            if (IsMark)
                                rez = OheConstant.TELNET_ESC_SEQ + "7m" + rez.Insert(Width, OheConstant.TELNET_ESC_SEQ + "0m" + "\r\n" + OheConstant.TELNET_ESC_SEQ + "7m") + OheConstant.TELNET_ESC_SEQ + "0m";
                            else
                                rez = rez.Insert(Width, "\r\n");
                            rez = string.Format(rez, Environment.NewLine);
                        }
                        else
                        {
                            rez = string.Format
                                (
                                ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "7m" : "") +
                                rez
                                + ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "0m" : "")
                                , Environment.NewLine);
                        }
                    }
                    break;
                default:
                    rez = Description + GetValueFromTable(cache);

                    if (rez.Trim().Length > Width)
                    {
                        if (IsMark)
                            rez = OheConstant.TELNET_ESC_SEQ + "7m" + rez.Insert(Width, OheConstant.TELNET_ESC_SEQ + "0m" + "\r\n" + OheConstant.TELNET_ESC_SEQ + "7m") + OheConstant.TELNET_ESC_SEQ + "0m";
                        else
                            rez = rez.Insert(Width, "\r\n");
                        rez = string.Format(rez, Environment.NewLine);

                    }
                    else
                    {
                        rez = string.Format
                            (((IsMark) ? OheConstant.TELNET_ESC_SEQ + "7m" : "") +
                               rez
                              + ((IsMark) ? OheConstant.TELNET_ESC_SEQ + "0m" : "")
                            , Environment.NewLine
                            );
                    }
                    break;
            }

            return rez.Split(str);

        }
        #endregion


        #region virtual ToHtml()
        /// <summary>
        /// Процедура возвращает строку которая должна отобразиться на терминале
        /// </summary>
        /// <returns></returns>
        public virtual string[] ToHtml(Dictionary<string, object> cache)
        {
            if (!IsVisible(cache))
                return null;

            var htmlBuffer = new StringBuilder();
            var text = Description + " " + GetValueFromTable(cache);

            if (OheControlType == OheConstant.OheTelnetControlType.InputBox)
            {
                var label = string.Format(
                    "<label for='{0}'>{1}</label>",
                    ValueID,
                    text
                );

                string type = IsPassword ? "password" : "text";
                string enabled = IsActivate ? string.Empty : "disabled='disabled'";
                string editable = ReadOnly ? "readonly='readonly'" : string.Empty;

                var input = string.Format(
                    "<input type='{0}' name='{1}' value='{2}' {3} {4} />",
                    type,
                    ValueID,
                    Value,
                    enabled,
                    editable
                );

                htmlBuffer.Append("<div data-role='fieldcontain'>");
                htmlBuffer.Append(label);
                htmlBuffer.Append(input);
                htmlBuffer.Append("</div>");
            }
            else
            {
                htmlBuffer.Append(IsMark ? "<div data-role='header'><h1>" : "<div class='line'>");
                htmlBuffer.Append(text);
                htmlBuffer.Append(IsMark ? "</div></h1>" : "</div>");
            }

            return new string[] { htmlBuffer.ToString() };
            /*var rez = new StringBuilder();
            if (!IsVisible(cache))
                return null;

            char[] str = Environment.NewLine.ToCharArray();
            string newId = "cntr" + Id.ToString();

            rez.Append("<div align='" + _align + "'");


            string val = string.Empty;
            string _class = " class='{0}" + (IsMark ? "_inverse" : "") + "'>";

            switch (OheControlType)
            {
                case OheConstant.OheTelnetControlType.InputBox:
                    _class = string.Format(_class, "input");
                    break;

                case OheConstant.OheTelnetControlType.Text:
                    _class = string.Format(_class, "text");
                    break;
            }

            rez.Append(_class);
            string currentValue = GetValueFromTable(cache);
            val = Description + " " + currentValue;

            int inte = val.LastIndexOf(currentValue);
            if (Utils.Utils.AllTrim(val).Length > Width)
            {
                if (inte > Width)
                {
                    val = val.Insert(Width, "<br>");
                    val = val.Insert(inte, "<span class=\"value\">");
                    val += "</span>";
                }
                else
                {
                    val = val.Insert(inte, "<span class=\"value\">");
                    val = val.Insert(Width + 20, "<br>");
                    val += "</span>";
                }
                val = string.Format(val, Environment.NewLine);
            }
            else
            {
                val = val.Insert(inte, "<span class=\"value\">");
                val += "</span>";
                val = string.Format(val, Environment.NewLine);
            }

            rez.Append(val);

            if (OheControlType == OheConstant.OheTelnetControlType.InputBox)
            {
                if (!ReadOnly)
                    _class = "class=\"input" + (IsActivate ? "_active" : "_disable") + "\"";
                else
                    _class = "class=\"input_readonly\"";

                if (IsActivate)
                {
                    rez.Append("&nbsp;<input type=\"" +
                               (IsPassword ? "password" : "text") + "\" name=\"user\" id=\"user\" value=\"" + Value +
                               "\" " + _class + OheConstant.WEB_ONCLICK_EVENT + ">");
                }
            }
            rez.Append("</div>");


            return rez.ToString().Split(str);*/
        }
        #endregion


        #endregion


    }
}
