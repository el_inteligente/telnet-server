﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Ohe.Telnet.Exception;
using Ohe.Utils;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// Определяет список параметров для события
    /// </summary>
    public class RfControlEventsParams
    {
        #region Define Variables
        private string _name; // определяет имя параметра
        private object _value; // определяет значение параметра
        private int _size;     // определяет размер параметра для текстовых значений
        private string _type; // определяет тип параметра
        private string _cntlId; // определяет контрол (ValueId) с которого надо взять значение для 
                                // для вызова хранимой процедуры
        private RfControlEvents _parentEvent;

        #endregion

        #region Properties
        /// <summary>
        /// Название параметра
        /// </summary>
        public string Name { get { return _name; } }

        /// <summary>
        /// Значение параметра
        /// </summary>
        public object Value { get { return _value; } set { _value = value; } }

        /// <summary>
        /// Тип параметра
        /// </summary>
        public string Type { get { return _type; } set { _type = value; } }

        /// <summary>
        /// Размер параметра для текстовых параметров
        /// </summary>
        public int Size { get { return _size; } }

        /// <summary>
        /// Определяет идентификатор контрола с которого надо взять значение для хранимой процедуры
        /// </summary>
        public string ControlId
        {
            get
				{
					return Utils.Utils.GetObjName(_cntlId);
            }
            set { _cntlId = Utils.Utils.AllTrim(value.ToUpperInvariant()); }
        }

        /// <summary>
        /// Возвращает имя колонки в записи
        /// </summary>
		public string Column { get { return Utils.Utils.GetObjColumn(_cntlId); } }

        public RfControlEvents ParentEvent { get { return _parentEvent; } }
        #endregion

        #region Constructors
        public RfControlEventsParams() { }

        /// <summary>
        /// КОнструктор
        /// </summary>
        /// <param name="name">Название парметра</param>
        /// <param name="type">Тип параметра</param>
        /// <param name="size">Размер параметра только для теста</param>
        /// <param name="value">Значение параметра</param>
        public RfControlEventsParams(string name, string type, int size, string value)
        {
            _name = name;
            _type = type;
            _size = size;
            _value = value;
            _cntlId = string.Empty;
        }

        /// <summary>
        /// КОнструктор
        /// </summary>
        /// <param name="name">Название парметра</param>
        /// <param name="type">Тип параметра</param>
        /// <param name="size">Размер параметра только для теста</param>
        /// <param name="value">Значение параметра</param>
        /// <param name="cntlId">Определяет с какого контрола надо брать значение параметра</param>
        public RfControlEventsParams(string name, string type, int size, string value, string cntlId)
        {
            _name = name;
            _type = type;
            _size = size;
            _value = value;
            _cntlId = Utils.Utils.AllTrim(cntlId.ToUpper());
        }


        public RfControlEventsParams(RfControlEvents parentEvent)
        {
            _parentEvent = parentEvent;
        }



        /// <summary>
        /// Возвращает текстовое значение параметра.
        /// Используется для постороения строки запроса.
        /// </summary>
        /// <returns>стринг</returns>
        public string ToString()
        {
            return _name +"="+
                    (Utils.Utils.StringIsNumber(_type) ? (_value == null ? "null" : _value.ToString()) : (_value == null ? "null" : "'" + _value.ToString() + "'"));
            //return _name +"="+ (_value == null ? "null" : _value.ToString());;
        }

        public void InitControlFromXML(XmlNode node)
        {
            foreach (XmlAttribute attr in node.Attributes)
            {
                #region Switch
                switch (Utils.Utils.AllTrim(attr.Name.ToUpperInvariant()))
                {
                    case "NAME":
                        _name = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "TYPE":
                        _type = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "VALUE":
                        _value = Utils.Utils.AllTrim(attr.Value);
                        break;
                    case "SIZE":
                        try
                        {
                            _size = int.Parse((attr.Value != string.Empty ? attr.Value : "0"));
                        }
                        catch (FormatException ex)
                        {
                            throw new ConfigurationException(ex,
                                              "Ошибка: неверный тип параметра"
                                              + Environment.NewLine
                                              + "Идентификатор формы: " + _parentEvent.ParentForm.Id.ToString()
                                              + Environment.NewLine
                                              + (_parentEvent.ParentControl == null ? string.Empty : "Идентификатор Контрола:" + ((RfControl)_parentEvent.ParentControl).ValueID)
                                              + Environment.NewLine
                                              + "Событие:  " + _parentEvent.SpName + Environment.NewLine
                                              + "Параметр: " + _name + Environment.NewLine
                                              + "Значение: " + _value + Environment.NewLine
                                              + "Размер: " + attr.Value + Environment.NewLine
                                              );
                        }
                        break;
                    case "ID":
                        _cntlId = Utils.Utils.AllTrim(attr.Value);
                        break;
                }
                #endregion
            }

            // Генерируем ошибку при недостаточном количестве параметров
            if (_name == null || _type == null)
                throw new ConfigurationException("Не заданы основные значения для"
                                              + Environment.NewLine
                                              + "параметров хранимой процедуры"
                                              + Environment.NewLine
                                              + "Идентификатор формы: " + _parentEvent.ParentForm.Id.ToString()
                                              + Environment.NewLine
                                              + (_parentEvent.ParentControl == null ? string.Empty : "Идентификатор Контрола:" + ((RfControl)_parentEvent.ParentControl).ValueID)
                                              + Environment.NewLine
                                              + "Событие: " + _parentEvent.Event
                                              );
            if(_cntlId == null && _value == null)
                throw new ConfigurationException("Не заданы основные значения для"
                                              + Environment.NewLine
                                              + "параметров хранимой процедуры"
                                              + Environment.NewLine
                                              + "Идентификатор формы: " + _parentEvent.ParentForm.Id.ToString()
                                              + Environment.NewLine
                                              + (_parentEvent.ParentControl == null ? string.Empty : "Идентификатор Контрола:" + ((RfControl)_parentEvent.ParentControl).ValueID)
                                              + Environment.NewLine
                                              + "Событие: " + _parentEvent.Event
                                              );

        }

        #endregion
    }
}
