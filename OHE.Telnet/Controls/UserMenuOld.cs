using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Ohe.Telnet.Configuration;
using Ohe.Utils;

namespace Ohe.Telnet.Controls
{
    /// <summary>
    /// ��������� ������ ���� ��������� ������������ 
    /// ����� ��������� ������ � �������� ����
    /// </summary>
    public partial class UserMenu
    {

        #region GetCurrentMenuPermiss - ���������� ������ ����(������� �� �����������)
        /// <summary>
        /// ���������� ������ �������� ���� ��� ������ ������������ �� ������� ��������
        /// </summary>
        /// <returns>String false - ������������ ����� �� �������� ����, 
        ///                 screen - ������������ ������� �� ���� �� ������,
        ///                          ���� ��������� �������
        /// </returns>
        public string GetCurrentMenuPermiss()
        {
            // ���������� ��, ��� ������������ ������������
            if (_parent < 0)
                return "false";

            // �������� �� �� ��� ������������ ��� �� � ���� � �� ������ ��������
            if (GetBoolMenuPermiss() == 0)
                return "screen";

            // ���������� ���� Html ���� � ��� ��� ���������� WEB
            if (_connectionType == OheConnectionType.Web)
                return GetXmlMenu();//GetXmlMenu();

            /*
            if (rezlt < 0)
                throw new ConfigurationException("������ ����������:"
                                                + Environment.NewLine
                                                + "������ MENU " 
                                                + " �� ��������� � ������������",_parent.ToString());
            */

            // ������������� ������ �� ������ ������
            StringBuilder result = new StringBuilder();
            string rez = OheConstant.TELNET_CLEAR_SEQ;  //+ OheConstant.TELNET_ESC_SEQ + "1;2f";
            rez += (Utils.Utils.TELNET_HEAD_MENUTOP(TlnConfigManager.TELNET_HELP_STR) != string.Empty ? Utils.Utils.TELNET_HEAD_MENUTOP(TlnConfigManager.TELNET_HELP_STR) + Environment.NewLine : string.Empty);
            int i = 1;   // ����������� ������ ���� �� ������� ��������
            int j = 1; // 
            int countMenu = _lhead.Count(s => s.MenuID == _parent); // �������� ���������� ���� ��� ������������� ��������������

            // ��������� ����� ���������� ���� ����������� �����������

            #region cc ������� �������� ���� � ������ �� ��������
            // �� ����� ���������� ���� � ������
            #region Not Use
            /*
            if (countMenu == 0)
            {
                _parent = 0;
                _page = 0;
                return rez + "���� �� ����������" + Environment.NewLine + " � ������������";
            }
            */
            #endregion
            if (_page > 0)
            {
                if (countMenu / ((_page) * _linesMenu) < 1)   // ���� ��� ���� ��� �������� ������
                    _page = 0;
            }
            // ��������� ���� ������������ ��������� 6 ��������� ����
            foreach (Menu s in _lhead)
            {
                if (i < (_linesMenu + 1) && j > (_page * _linesMenu) && j < (_page * _linesMenu + _linesMenu + 2) && s.MenuID == _parent)
                {
                    rez += Utils.Utils.AllTrim(i.ToString()) + ". " + Utils.Utils.AllTrim(s.Name) + Environment.NewLine;
                    i++;
                }
                if (s.MenuID == _parent)
                    j++;
            }
            // ��������� ������� �������� ���� ���� �� �����
            while (i < (_linesMenu + 1))
            {
                rez += Environment.NewLine;
                i++;
            }

            // ���������� ����� ���������� ������� � ����
            int linesal = (int)(Math.Ceiling(((double)countMenu / (double)_linesMenu)));

            string r = (_page + 1).ToString() + "/" + linesal.ToString();//(j-1).ToString();//_LheadMenu.Count.ToString();

            #endregion

            // ���������� ���� ��������� ������ ���� ���������� ������� ������ 1
            return rez + (linesal > 1 ? r + TlnConfigManager.TELNET_HEAD_MENUBOT : string.Empty) + GetCursorPosition();
        }
        #endregion


    }
}