﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.18444
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ohe.Telnet.Properties {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Ohe.Telnet.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;handheld&gt;
        ///  &lt;logon&gt;
        ///    &lt;form&gt;
        ///      &lt;controls&gt;
        ///        &lt;control line=&quot;1&quot; ismark=&quot;1&quot; align=&quot;center&quot; checkvalue=&quot;&quot; controltype=&quot;text&quot; descript=&quot;OHE СЕРВЕР&quot; id=&quot;&quot; /&gt;
        ///        &lt;control line=&quot;2&quot; ismark=&quot;0&quot; align=&quot;center&quot; checkvalue=&quot;&quot; controltype=&quot;text&quot; descript=&quot;УПРАВЛЕНИЕ СКЛАДОМ&quot; id=&quot;&quot; /&gt;
        ///        &lt;control line=&quot;3&quot; ismark=&quot;0&quot; align=&quot;center&quot; checkvalue=&quot;&quot; controltype=&quot;text&quot; descript=&quot;OPEN HANDHELD.WMS&quot; id=&quot;&quot; /&gt;
        ///        &lt;control line=&quot;4&quot; ismark=&quot;0&quot; align=&quot;center&quot; c [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string config {
            get {
                return ResourceManager.GetString("config", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;handheld&gt;
        ///  &lt;logon&gt;
        ///    &lt;form&gt;
        ///      &lt;controls&gt;
        ///        &lt;control line=&quot;1&quot; ismark=&quot;1&quot; align=&quot;center&quot; checkvalue=&quot;&quot; controltype=&quot;text&quot; descript=&quot;OHE СЕРВЕР&quot; id=&quot;&quot; /&gt;
        ///        &lt;control line=&quot;2&quot; ismark=&quot;0&quot; align=&quot;center&quot; checkvalue=&quot;&quot; controltype=&quot;text&quot; descript=&quot;УПРАВЛЕНИЕ СКЛАДОМ&quot; id=&quot;&quot; /&gt;
        ///        &lt;control line=&quot;3&quot; ismark=&quot;0&quot; align=&quot;center&quot; checkvalue=&quot;&quot; controltype=&quot;text&quot; descript=&quot;OHE.WMS&quot; id=&quot;&quot; /&gt;
        ///        &lt;control line=&quot;4&quot; ismark=&quot;0&quot; align=&quot;center&quot; checkvalue= [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string login {
            get {
                return ResourceManager.GetString("login", resourceCulture);
            }
        }
    }
}
