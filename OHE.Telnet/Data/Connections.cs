using System;
using System.Data;
using System.Data.SqlClient;
using Ohe.Utils;

namespace Ohe.Telnet.Data
{
    /// <summary>
    /// ����� ��������� ���������� � ����� ������
    /// </summary>
	public class VsSqlConnection: IDisposable
	{
        #region Properties
        private SqlConnection _connect;
        public SqlConnection Connection { get { return _connect; } set { _connect = value; } }

        private SqlTransaction _tran;
        public SqlTransaction Transaction { get { return _tran; } set { _tran = value; } }

        private ConnectionState _connState = ConnectionState.Closed;
        public ConnectionState ConnectionState { get { return _connState; } set { _connState = value; } }

        private string _tranName;
        public string TransactionName { get { return _tranName; } set { _tranName = value; } }
        
        #endregion

		#region Constructor
		/// <summary>
		///		�����������
		/// </summary>
		/// <param name="connection">string, database connect string</param>
		public VsSqlConnection(string connection)
		{
			try
			{
				//creating SqlConnection and open it
				Connection = new SqlConnection(connection);
				Connection.Open();
				ConnectionState = ConnectionState.Open;
				//this.Transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
			}
			catch(System.Exception ex)
			{
				ConnectionState = ConnectionState.Closed;
				throw(new System.Exception(ex.Message));
			}
		}
		#endregion

		#region Commit

		/// <summary>
		///		commit the transactions
		/// </summary>
		public void Commit()
		{
			try
			{
				this.Transaction.Commit();
			}
            catch (System.Exception e)
			{
				//first, rollback the transaction
				this.Rollback();
                throw (new System.Exception(e.Message));
			}
		}

		#endregion

		#region Rollback
		/// <summary>
		///		rollback the transactions
		/// </summary>
		public void Rollback()
		{
			try
			{
				Transaction.Rollback();
			}
            catch (System.Exception e)
			{
                throw (new System.Exception(e.Message));
			}			
		}
		#endregion

		#region Close
		/// <summary>
		///		close the connection
		/// </summary>
        public void Close() { this.Connection.Close(); }
		#endregion

		#region IDisposable
		public void Dispose()
		{
            if (Connection.State != ConnectionState.Closed)
                Close();
            if (Transaction != null)
                Transaction.Dispose();
		}

		#endregion
	}
}



