using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Ohe.Telnet.Controls;
using Ohe.Telnet.Configuration;
using Ohe.Exceptions;
using Ohe.Telnet.Exception;


namespace Ohe.Telnet.Data
{
    /// <summary>
	/// Summary description for Execute.
	/// </summary>
	internal static class ExecuteSqlQuery
	{
        private static int  _tranCount = 0;

        private static int TranCount
        {
            get
            {
                try
                {
                    _tranCount++; return _tranCount;
                }
                catch
                {
                    _tranCount = 0; return _tranCount;
                }
            }
        }

		#region Execute returns DataTable
		/// <summary>
		///		���������� ������� � ������������ ������ ���� ���� ��� ���������
		/// </summary>
		/// <param name="query">��� �������� ���������</param>
        /// <param name="prms">������ ���������� ��� �������� ���������</param>
        /*
        public static DataTable sp_Execute(string query,List<RfControlEventsParams> prms)
		{
			try
			{
         
                SqlDataAdapter da = new SqlDataAdapter();
                VsSqlConnection d = new VsSqlConnection(Utils.Utils.GetConnectionString());
                da.SelectCommand = new SqlCommand(query, d.Connection);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Transaction = d.Transaction;

                #region ParseParametrs
                foreach (RfControlEventsParams par in prms)
                {
                    if (par.Type != SqlDbType.Int)
                    {
                        SqlParameter prm = new SqlParameter(par.Name, par.Type, par.Size);
                        prm.Value = par.Value;
                        da.SelectCommand.Parameters.Add(prm);
                    }
                    else
                    {
                        SqlParameter prm = new SqlParameter(par.Name, par.Type);
                        prm.Value = par.Value;
                        da.SelectCommand.Parameters.Add(prm);
                    }
                }
                #endregion

				DataSet ds = new DataSet();
				da.Fill(ds);
				d.Commit();
				d.Close();
				return ds.Tables[0];
			}
			catch
            { 
                #region Catch
		        DataTable d = new DataTable("diman");
				DataColumn c = new DataColumn();
				c.DataType = System.Type.GetType("System.String");
				c.ColumnName = "test";
				d.Columns.Add(c);

				DataRow dr = d.NewRow();
				dr["test"] = "error";
				//dr.ItemArray[0] = "error";
				d.Rows.Add(dr);
				return d;
 	            #endregion  
            }
		}
        */
		#endregion


        #region Execute returns ������ � �������� ������
        /// <summary>
        ///	���������� ������� � ������������ ������ ���� ���� ��� ���������
        /// </summary>
        /// <param name="query">��� �������� ���������</param>
        /// <param name="prms">������ ���������� ��� �������� ���������</param>
        public static object ExecuteEvent(string query, List<RfControlEventsParams> prms, Dictionary<string, object> globalCache, RfForm parentForm)
        {
            StringBuilder select = new StringBuilder();
            string userId = string.Empty;
            string _trannum = Utils.Utils.AllTrim(TranCount.ToString());

            if (parentForm != null)
            {
                select.AppendLine("form id = " + parentForm.Id);
                select.AppendLine("event =" + parentForm.CurrentEvent.ToString());
            }

            if(!globalCache.ContainsKey("SYS_USER_DATABASE_LOG"))
                globalCache.Add("SYS_USER_DATABASE_LOG", new List<string>(100));

            try
            {
                using (VsSqlConnection connection = new VsSqlConnection(TlnConfigManager.ConnectionString))
                {
                    //#if DEBUG
                    select.AppendLine("query:");
                    select.Append("declare @p" + _trannum + " bit set @p" + _trannum + "=0 exec " + query + " ");
                    //#endif
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        #region �������� �������� ��������� ��� ��������� �������

                        da.SelectCommand = new SqlCommand(query, connection.Connection);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        //da.SelectCommand.Transaction = d.Transaction;
                        da.SelectCommand.CommandTimeout = 180;

                        #region ParseParametrs

                        foreach (RfControlEventsParams par in prms)
                        {
                            try
                            {
                                da.SelectCommand.Parameters.AddWithValue(par.Name, par.Value);
                                //#if DEBUG
                                select.Append(par.ToString() + ",");
                                //#endif
                            }
                            catch (FormatException ex)
                            {
                                //#if DEBUG
                                Utils.Utils.WriteDebugLog("debug", select.ToString() + Environment.NewLine + ex.Message);
                                //#endif
                                throw new FormatException("������ ��������� ������:"
                                                          + Environment.NewLine
                                                          + "�������� ���������:"
                                                          + query + Environment.NewLine
                                                          + "��������: " + par.Name + Environment.NewLine
                                                          + "��������:" +
                                                          (par.Value == null ? "" : par.Value.ToString()) +
                                                          Environment.NewLine
                                                          + "�������� �������������� ���������� �������� ���������"
                                                          + Environment.NewLine
                                                          + "������: " + ex.Message);
                            }
                        }

                        #endregion

                        // ��������� �������� �������� ������������
                        SqlParameter rezult = new SqlParameter();
                        rezult.ParameterName = "result";
                        rezult.SqlDbType = SqlDbType.Bit;
                        rezult.Direction = ParameterDirection.Output;

                        select.Append("@result=@p" + _trannum  + " output select @p" + _trannum);

                        // ��������� �������� ������� ���������� ����������
                        da.SelectCommand.Parameters.Add(rezult);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        Ohe.Data.OheDataTable dt = new Ohe.Data.OheDataTable((DataTable) ds.Tables[0]);
                        select.Append(Environment.NewLine + dt.ToCsvDataSource() + Environment.NewLine);

                        // �������� �� ��, ��� ��������� ���������� 0 � ����������
                        // ������ �.�. ��������� �����-�� ���������� ������
                        string rr = Utils.Utils.AllTrim(da.SelectCommand.Parameters["result"].Value.ToString().ToUpper());

                        select.AppendLine("result=" + rr);

                        if (rr != string.Empty)
                        {
                            if (rr != "TRUE")
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    throw new LogicException(ds.Tables[0].Rows[0].ItemArray[0].ToString(), ds.Tables[0]);
                                }
                                else
                                    throw new LogicException("@result=0");
                            }
                        }
                        else
                        {
                            OheDataException exception = new OheDataException("� �������� ���������"
                                                                              + select.ToString()
                                                                              + Environment.NewLine
                                                                              +
                                                                              "�� ���������� �������� ������������� ��������� @result=[" +
                                                                              rr + "]", query, string.Empty);
                            Utils.Utils.WriteLog(exception.ToString());

                            //��������� ������ ��� ������� � ����
                            Utils.Utils.WriteDebugLog(userId + "_trace_logic_"
                                                      ,
                                                      Utils.Utils.GetLast100Operations(
                                                          (List<string>) globalCache["SYS_USER_DATABASE_LOG"]
                                                          , select.ToString(), exception.ToString()));


                            throw new SqlDataException(exception.ToString());
                        }
                        return ds.Tables[0];

                        #endregion
                    }
                }
            }
            #region Catch
            catch (System.Exception exc)
            {
                if (exc is LogicException) { }
                else
                {
                    if (!globalCache.ContainsKey("SYS_VALUE"))
                        throw exc;

                    DataTable dd = (DataTable)globalCache["SYS_VALUE"];
                    userId = dd.Rows[0].ItemArray[dd.Columns["SYS_ID"].Ordinal].ToString();

                    Utils.Utils.WriteDebugLog(userId + "_exception"
                                        , Utils.Utils.GetLast100Operations((List<string>)globalCache["SYS_USER_DATABASE_LOG"]
                                        , select.ToString(), exc.Message + Environment.NewLine + (exc.InnerException == null ? "" : exc.InnerException.Message))
                                         );
                }
                throw exc;
            }
            #endregion
            #region Finally
            finally
            {
                // ��������� � ���������� ��� ����������
                List<string> ld = ((List<string>)globalCache["SYS_USER_DATABASE_LOG"]);
                if (ld.Count > 100)
                    ld.RemoveAt(0);

                ld.Add(select.ToString());
                globalCache["SYS_USER_DATABASE_LOG"] = ld;

#if DEBUG
                try
                {
                    if (globalCache.ContainsKey("SYS_VALUE"))
                    {
                        DataTable dd = (DataTable)globalCache["SYS_VALUE"];
                        userId = dd.Rows[0].ItemArray[dd.Columns["SYS_ID"].Ordinal].ToString();

                        Utils.Utils.WriteDebugLog(userId + "_debug", select.ToString());
                    }
                }
                catch { }
#endif


            }
            #endregion
        }
        #endregion


		#region Execute Select returns Int
		/// <summary>
		///		overloaded Execute, this one could be commit/rollback by SqlConnection transaction control
		/// </summary>
		/// <param name="query">������ ������� ������ �����������</param>
		public static int ExecuteScalar(string query)
		{
            using (VsSqlConnection connection = new VsSqlConnection(TlnConfigManager.ConnectionString))
            {
                SqlCommand ExCommand = new SqlCommand(query, connection.Connection);
                try
                {
                    //ExCommand.Transaction = connection.Transaction;
                    ExCommand.CommandTimeout = 180;
                    int nAffected = (Int32)ExCommand.ExecuteScalar();
                    return nAffected;
                }
                catch (System.Exception e)
                {
                    string eMessage = "SQLEXECUTE: An error occurred processing the statement: " + Environment.NewLine 
                        + query + Environment.NewLine 
                        + e.Message.ToString();
                    throw new System.Exception(Utils.Utils.AllTrim(eMessage));
                }
            }

            /*
			VsSqlConnection connection = new VsSqlConnection(TlnConfigManager.ConnectionString);
			SqlCommand ExCommand = new SqlCommand(query, connection.Connection);
            try
            {
                //ExCommand.Transaction = connection.Transaction;
                ExCommand.CommandTimeout = 180;
                int nAffected = (Int32)ExCommand.ExecuteScalar();
                return nAffected;
            }
            catch (Exception e)
            {
                string eMessage = "SQLEXECUTE: An error occurred processing the statement: " + @"\r\n" + query + @"\r\n" + e.Message.ToString();
                throw new Exception(Utils.Utils.AllTrim(eMessage));
            }
            finally
            {
                //ExCommand.Transaction.Commit();
                connection.Connection.Close();
            }
             */
		}
		#endregion
	}
}
