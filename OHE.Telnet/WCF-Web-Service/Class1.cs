﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WebProgrammingModel.Samples;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.ServiceModel.Description;

namespace Ohe.Telnet.WCF_Web_Service
{
    class Class1
    {
        public void Start()
        {
            ServiceHost host = new ServiceHost(typeof(OheWebTelnetService), new Uri("http://localhost:8000"));

            ServiceEndpoint endpoint = host.AddServiceEndpoint(typeof(OheWebTelnetService), new WebHttpBinding(), "");
            endpoint.Behaviors.Add(new FormProcessingBehavior());

            ServiceMetadataBehavior smb = host.Description.Behaviors.Find<ServiceMetadataBehavior>();

            if (smb != null)
            {
                smb.HttpGetEnabled = false;
                smb.HttpsGetEnabled = false;
            }

            ServiceDebugBehavior sdb = host.Description.Behaviors.Find<ServiceDebugBehavior>();
            if (sdb != null)
            {
                sdb.HttpHelpPageEnabled = false;
            }

            host.Open();
        }

    }
}
