//  Copyright (c) Microsoft Corporation. All rights reserved.

using System;
using System.Threading;
using System.Collections.Specialized;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using OpenSourceHHWebClient.Controls;
using Ohe.Telnet.Configuration;
using Ohe.Utils;
//using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Microsoft.WebProgrammingModel.Samples
{
    [ServiceContract()]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,//PerSession, 
    ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    /*
       � �������� ��������.
     * �������� ������: ��� ���������� ������� ����� � ������������� html ����
     * ��� ��������� ���������� �������� ������ �������
     */
    class OheWebTelnetService
    {
        private string _result;
        //private AutoResetEvent _eEvent;

        [OperationContract]
        [WebGet(UriTemplate = "webterm/")]
        public Message WebGetMethod()
        {
            string _sessionId = Guid.NewGuid().ToString();
            var sessionTicket = TlnWebSessionManager.GetSessionTicket(_sessionId);
            string message = string.Empty;
            if (sessionTicket == null)
            {

                sessionTicket = new OheTerminalClient();
                sessionTicket.CreateClient("localhost", 5400, _sessionId);

                //Task<string> tsResponse = SendRequest("localhost", 5400, string.Empty);
                //message = tsResponse.Result;
            }

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/html",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(sessionTicket.CurrentMessage);
                        writer.Flush();
                    });
        }


        [OperationContract]
        [WebGet(UriTemplate = "style.css")]
        public Message GetCss()
        {

            string text = string.Empty;
            using (TextReader reader = File.OpenText(Utils.assemblyPath + "style.css"))
            //using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\jquery.mobile-1.3.1.min.css"))
            {
                text = reader.ReadToEnd();
            }

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/css",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(text);
                        writer.Flush();
                    });
        }


        [OperationContract]
        [WebGet(UriTemplate = "js/func.js")]
        public Message GetJavascript()
        {

            //jquery.mobile-1.3.1.min.js jquery-1.9.1.min.js 
            string text = string.Empty;
            //using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\func.js"))
            /*
            using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\jquery-1.9.1.min.js "))
	        {
	            text = reader.ReadToEnd();
	        }
            */
            string text2 = string.Empty;
            /*
            using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\jquery.mobile-1.3.1.min.js"))
	        {
	            text2 = reader.ReadToEnd();
	        }
            */



            string text3 = string.Empty;
            using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\func.js"))
            {
                text3 = reader.ReadToEnd();
            }

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/javascript",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(text + text2 + text3);
                        writer.Flush();
                    });
        }


        private void ReturnResult(string rez)
        {
            _result = rez;
            //_eEvent.Set();
        }

        [OperationContract]
        [WebInvoke(UriTemplate = "SendData/")]
        //[WebInvoke(UriTemplate = "ProcessForm/{templateParam1}/{templateParam2}")]
        public Message SendData(NameValueCollection formData)
        {
            //_eEvent = new AutoResetEvent(false);
            string _sessionId = formData["key"];
            string _value = formData["user"];

            //Utils.WriteLog(string.Format("Send data sesion={0}, data={1}",_sessionId,_value));
            OheTerminalClient sessionTicket = TlnWebSessionManager.GetSessionTicket(_sessionId);

            if (sessionTicket == null)
            {
                _sessionId = Guid.NewGuid().ToString();
                sessionTicket = new OheTerminalClient("localhost", 5400, _sessionId);
                sessionTicket.OnClientCallBack += ReturnResult;
                TlnWebSessionManager.PutSessionTicket(_sessionId, sessionTicket);

                //_eEvent.WaitOne();

                return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/html",
                        delegate(Stream output)
                        {
                            TextWriter writer = new StreamWriter(output);
                            writer.WriteLine(_result);
                            writer.Flush();
                        });

            }

            sessionTicket.SendString(_value + "\r\n");
            //_eEvent.WaitOne();

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/html",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(_result);
                        writer.Flush();
                    });
        }


        [OperationContract]
        [WebInvoke(UriTemplate = "ProcessForm/{templateParam1}/{templateParam2}")]
        public Message ProcessForm(string templateParam1, string templateParam2, NameValueCollection formData)
        {
            DumpValues(Console.Out, templateParam1, templateParam2, formData);

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/plain",
                delegate(Stream output)
            {
                TextWriter writer = new StreamWriter(output);
                DumpValues(writer, templateParam1, templateParam2, formData);
            });
        }

        private void DumpValues(TextWriter writer, string templateParam1, string templateParam2, NameValueCollection formData)
        {
            writer.WriteLine("Form processed!");
            writer.WriteLine("templateParam1: {0}", templateParam1);
            writer.WriteLine("templateParam2: {0}", templateParam2);

            foreach (string key in formData.Keys)
            {
                writer.WriteLine("{0}: {1}", key, formData[key]);
            }


            writer.Flush();
        }


    }
}
