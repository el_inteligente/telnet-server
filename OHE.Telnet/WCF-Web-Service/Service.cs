//  Copyright (c) Microsoft Corporation. All rights reserved.

using System;
using System.Threading;
using System.Collections.Specialized;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using OpenSourceHHWebClient.Controls;
using Ohe.Telnet.Configuration;
using Ohe.Utils;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Microsoft.WebProgrammingModel.Samples
{
    [ServiceContract()]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,//PerSession, 
    ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    /*
       � �������� ��������.
     * �������� ������: ��� ���������� ������� ����� � ������������� html ����
     * ��� ��������� ���������� �������� ������ �������
     */
    class OheWebTelnetService
    {
        private string _result;
        //private AutoResetEvent _eEvent;

        [OperationContract]
        [WebGet(UriTemplate = "webterm/")]
        public Message WebGetMethod()
        {
            string _sessionId = Guid.NewGuid().ToString();
            var sessionTicket = TlnWebSessionManager.GetSessionTicket(_sessionId);
            string message = string.Empty;
            if (sessionTicket == null)
            {
                Task<string> tsResponse = SendRequest("localhost", 5400, string.Empty);
                message = tsResponse.Result;
            }

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/html",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(message);
                        writer.Flush();
                    });
        }


        private async Task<string> SendRequest(string server, int port, string data)
        {
            try
            {
                // set up IP address of server
                IPAddress ipAddress = null;
                IPHostEntry ipHostInfo = Dns.GetHostEntry(server);
                for (int i = 0; i < ipHostInfo.AddressList.Length; ++i)
                {
                    if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[i];
                        break;
                    }
                }
                if (ipAddress == null)
                    throw new Exception("Unable to find an IPv4 address for server");

                TcpClient client = new TcpClient();
                await client.ConnectAsync(ipAddress, port); // connect to the server

                NetworkStream networkStream = client.GetStream();
                StreamWriter writer = new StreamWriter(networkStream);
                StreamReader reader = new StreamReader(networkStream);

                writer.AutoFlush = false;
                string requestData = "<web>"; // 'end-of-requet'
                await writer.WriteLineAsync(requestData);
                string response = await reader.ReadLineAsync();

                client.Close();

                return response;

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        } // SendRequest

        [OperationContract]
        [WebGet(UriTemplate = "style.css")]
        public Message GetCss()
        {

            string text = string.Empty;
            using (TextReader reader = File.OpenText(Utils.assemblyPath + "style.css"))
            //using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\jquery.mobile-1.3.1.min.css"))
            {
                text = reader.ReadToEnd();
            }

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/css",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(text);
                        writer.Flush();
                    });
        }


        [OperationContract]
        [WebGet(UriTemplate = "js/func.js")]
        public Message GetJavascript()
        {

            //jquery.mobile-1.3.1.min.js jquery-1.9.1.min.js 
            string text = string.Empty;
            //using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\func.js"))
            /*
            using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\jquery-1.9.1.min.js "))
	        {
	            text = reader.ReadToEnd();
	        }
            */
            string text2 = string.Empty;
            /*
            using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\jquery.mobile-1.3.1.min.js"))
	        {
	            text2 = reader.ReadToEnd();
	        }
            */



            string text3 = string.Empty;
            using (TextReader reader = File.OpenText(Utils.assemblyPath + "js\\func.js"))
            {
                text3 = reader.ReadToEnd();
            }

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/javascript",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(text + text2 + text3);
                        writer.Flush();
                    });
        }


        private void ReturnResult(string rez)
        {
            _result = rez;
            //_eEvent.Set();
        }

        [OperationContract]
        [WebInvoke(UriTemplate = "SendData/")]
        //[WebInvoke(UriTemplate = "ProcessForm/{templateParam1}/{templateParam2}")]
        public Message SendData(NameValueCollection formData)
        {
            //_eEvent = new AutoResetEvent(false);
            string _sessionId = formData["key"];
            string _value = formData["user"];
            string message = string.Empty;

            //Utils.WriteLog(string.Format("Send data sesion={0}, data={1}",_sessionId,_value));
            OheTerminalClient sessionTicket = TlnWebSessionManager.GetSessionTicket(_sessionId);

            if (sessionTicket == null)
            {
                Task<string> tsResponse = SendRequest("localhost", 5400, string.Empty);
                message = tsResponse.Result;
            }
            else
            {

            }

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/html",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(_result);
                        writer.Flush();
                    });



            sessionTicket.SendString(_value + "\r\n");
            //_eEvent.WaitOne();

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/html",
                    delegate(Stream output)
                    {
                        TextWriter writer = new StreamWriter(output);
                        writer.WriteLine(_result);
                        writer.Flush();
                    });
        }


        [OperationContract]
        [WebInvoke(UriTemplate = "ProcessForm/{templateParam1}/{templateParam2}")]
        public Message ProcessForm(string templateParam1, string templateParam2, NameValueCollection formData)
        {
            DumpValues(Console.Out, templateParam1, templateParam2, formData);

            return StreamMessageHelper.CreateMessage(MessageVersion.None, "", "text/plain",
                delegate(Stream output)
            {
                TextWriter writer = new StreamWriter(output);
                DumpValues(writer, templateParam1, templateParam2, formData);
            });
        }

        private void DumpValues(TextWriter writer, string templateParam1, string templateParam2, NameValueCollection formData)
        {
            writer.WriteLine("Form processed!");
            writer.WriteLine("templateParam1: {0}", templateParam1);
            writer.WriteLine("templateParam2: {0}", templateParam2);

            foreach (string key in formData.Keys)
            {
                writer.WriteLine("{0}: {1}", key, formData[key]);
            }


            writer.Flush();
        }


    }
}
