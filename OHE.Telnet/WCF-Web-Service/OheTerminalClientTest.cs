﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel.Channels;
using System.Threading;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using OpenSourceHHWebClient;
using Ohe.Telnet.Configuration;
using Ohe.Utils;

namespace OpenSourceHHWebClient.Controls
{

    public class MessageReceivedEventArgs : EventArgs
    {
        private string _message; 
        public MessageReceivedEventArgs(string message)
        {
            _message = message;
        }

        public string Message { get { return _message; } }
    }

    public class DataReceivedEventArgs : EventArgs
    {
        private byte[] _data;
        public DataReceivedEventArgs(byte[] data)
        {
            _data = data;
        }


        public byte[] Data { get { return _data; } }
    }


    /// <summary>
    /// Класс описывает подключение к серверу Телнет
    /// </summary>
    public class OheTerminalClient : IDisposable
    {

        protected AutoResetEvent MessageReceiveEvent = new AutoResetEvent(false);
        protected AutoResetEvent DataReceiveEvent = new AutoResetEvent(false);
        protected AutoResetEvent OpenedEvent = new AutoResetEvent(false);
        protected AutoResetEvent CloseEvent = new AutoResetEvent(false);

        public event EventHandler Closed;
        public event EventHandler<DataReceivedEventArgs> DataReceived;
        //public event EventHandler<ErrorEventArgs> Error;
        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        public event EventHandler Opened;

        public string CurrentMessage { get; private set; }
        public byte[] CurrentData { get; private set; }

        private OheTerminalClient client;

        public delegate void OnRbTerminalClientDispatchHandler(string cmd);
        public event OnRbTerminalClientDispatchHandler OnClientCallBack;
        //private OnRbTerminalClientDispatchHandler _onRbTerminalClientDispatch;
        //public OnRbTerminalClientDispatchHandler OnRbTerminalClientDispatch { get { return _onRbTerminalClientDispatch; } set { _onRbTerminalClientDispatch = value; } }

        #region Private Properties

        private IPEndPoint iep;
        private Socket s;
        private Byte[] m_byBuff = new Byte[32767];
        private string host = "localhost";
        private int port = 23;
        private string _errorStringMessage = string.Empty;

        private StringBuilder _extraData = new StringBuilder();
        private Hashtable key2dispatch = new Hashtable();
        private string _session;

        #endregion

        public string SessionId { get { return _session; } }

        private string _result = string.Empty;
        public void Clear() { _result = string.Empty; }

        public string Result { get { return _result; } }


        #region Constructors

        public OheTerminalClient()
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="Session">Идентификатор Сессии</param>
        public OheTerminalClient(string Session)
        {
            this._session = Session;
            Init();
            CreateConnection();
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="host">IP адрес хоста</param>
        /// <param name="port">Порт хоста</param>
        /// <param name="Session">Идентификатор Сессии</param>
        public OheTerminalClient(string host, int port, string Session)
        {
            _session = Session;
            this.host = host;
            this.port = port;
            this.Opened += new EventHandler(webSocketClient_Opened);
            this.Closed += new EventHandler(webSocketClient_Closed);
            this.DataReceived += new EventHandler<DataReceivedEventArgs>(webSocketClient_DataReceived);
            this.MessageReceived += new EventHandler<MessageReceivedEventArgs>(webSocketClient_MessageReceived);


            Init();
            CreateConnection();

        }
        #endregion


        #region IDisposable Members
        public void Dispose()
        {
            StopConnection();
        }
        #endregion


        #region Init - инициализация
        private void Init()
        {
            key2dispatch.Add("F1", "\u001bOP");
            key2dispatch.Add("F2", "\u001bOQ");
            key2dispatch.Add("F3", "\u001bOR");
            key2dispatch.Add("F4", "\u001bOS");
            key2dispatch.Add("F5", "\u001bOT");
            key2dispatch.Add("F6", "\u001bOU");
            key2dispatch.Add("F7", "\u001bOV");
            key2dispatch.Add("F8", "\u001bOW");
            key2dispatch.Add("F9", "\u001bOX");
        }
        #endregion


        #region Create / Stop Connection - открытие закрытие соединения


        public OheTerminalClient CreateClient(string host, int port, string Session)
        {
            
            client = new OheTerminalClient(host, port, Session);


            return client;
        }

        /// <summary>
        /// Создает соединение
        /// </summary>
        private void CreateConnection()
        {
            try
            {
                IPHostEntry IPHost = Dns.Resolve(host);
                IPAddress[] addr = IPHost.AddressList;

                s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                iep = new IPEndPoint(addr[0], port);
                s.Blocking = false;

                if (!OpenedEvent.WaitOne(1000))
                    throw new Exception("Нет соединения");

                //s.ReceiveTimeout = 10000;
                s.BeginConnect(iep, ConnectCallback, s);

            }
            catch (Exception e)
            {
                _errorStringMessage = e.Message;
            }
        }

        /// <summary>
        /// Закрывает соединение
        /// </summary>
        private void StopConnection()
        {
            try
            {
                if (s != null)
                {
                    s.Shutdown(SocketShutdown.Both);
                    s.Close();

                    /*
                    if (s.Connected && _errorStringMessage != string.Empty)
                        _onRbTerminalClientError(new Exception(Marshal.GetLastWin32Error().ToString()));
                    */
                }
            }
            catch { }

            s = null;
        }


        #endregion


        #region ConnectCallback
        /// <summary>
        /// Читает в асинхронном режиме данные от клиента
        /// </summary>
        /// <param name="asyncResult"></param>
        public void ConnectCallback(IAsyncResult asyncResult)
        {
            try
            {
                if (s.Connected)
                {
                    _result = string.Empty;
                    //s.Send(Encoding.GetEncoding(TlnConfigManager.ServerEncoding).GetBytes("<web>"));
                    s.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, OnConnectClose, s);
                    //s.Receive(m_byBuff.Length);
                }
            }
            catch (Exception e)
            {
                _errorStringMessage = e.Message;
            }
        }
        #endregion

        public void OnConnectClose(IAsyncResult asyncResult)
        {
            _errorStringMessage = string.Empty;

            int nBytesRec = 0;

            try
            {
                nBytesRec = s.EndReceive(asyncResult);

                if (s.Connected)
                {
                    s.Send(Encoding.GetEncoding(TlnConfigManager.ServerEncoding).GetBytes("<web>"));
                    s.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, OnReceivedData, s);
                }
            }
            catch (Exception exc)
            {
                _result = exc.Message;
                _errorStringMessage = exc.Message;
                //OnReturnResult(this);
            }
        }


        #region OnRecievedData - Обрабатывает строку клиента и возвращает результат клиенту
        /// <summary>
        /// Обрабатывает строку клиента и возвращает результат
        /// </summary>
        /// <param name="asyncResult"></param>
        /// 
        /*
        public void OnReceivedData(IAsyncResult asyncResult)
        {
            _errorStringMessage = string.Empty;

            int nBytesRec = 0;

            try
            {
                nBytesRec = s.EndReceive(asyncResult);
            }
            catch (Exception exc)
            {
                _result = exc.Message;
                _errorStringMessage = exc.Message;
                OnReturnResult(this);
                return;
            }

            if (nBytesRec > 0)
            {
                #region обработка результатов
                StringBuilder m_strLine = new StringBuilder();

                for (int i = 0; i < nBytesRec; i++)
                {
                    Char ch = Convert.ToChar(m_byBuff[i]);
                    m_strLine.Append(Convert.ToString(ch));
                }

                try
                {
                    int strLinelen = m_strLine.Length;
                    if (strLinelen == 0)
                        m_strLine.Append(Convert.ToString("<BR>"));

                    Byte[] mToProcess = new Byte[strLinelen];

                    for (int i = 0; i < strLinelen; i++)
                        mToProcess[i] = Convert.ToByte(m_strLine[i]);

                    string mOutText = Encoding.GetEncoding(OheConfigWebClient.ClientEncoding).GetString(mToProcess, 0, strLinelen);


                    if (mOutText.Length > 0)
                    {
                        _extraData.Append(mOutText);

                        if (_extraData.ToString().IndexOf("</div>") == -1)
                        {
                            if (s != null)
                                s.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, new AsyncCallback(OnReceivedData), s);
                        }
                        else
                        {
                            _result = _extraData.ToString();

                            try
                            {
                                _result = _result.Substring(_result.LastIndexOf("<div class=\"form\""));
                            }
                            catch { }
                            _extraData = new StringBuilder();
                            OnReturnResult(this);
                            //Utils.WriteWebLog("OnReceiveData " + _result);
                        }

                    }
                }
                catch (Exception e)
                {
                    _errorStringMessage = e.Message;
                    throw;
                }

                #endregion
            }
            else
            {
                if (!s.Connected)
                {
                    _result = "<div class='error'>Утеряно соединение с сервером<BR>Перезапустите приложение или<BR>обратитесь к системному<BR>администратору</div>";
                    s.Shutdown(SocketShutdown.Both);
                    s.Close();
                    OnReturnResult(this);
                }
                else
                {
                    if (s != null)
                    {
                        _result = "<div class='error'>Утеряно соединение с сервером<BR>Перезапустите приложение или<BR>обратитесь к системному<BR>администратору</div>";
                        OnReturnResult(this);
                    }
                }
            }
        }
        */
        #endregion


        #region OnRecievedData - Обрабатывает строку клиента и возвращает результат клиенту
        /// <summary>
        /// Обрабатывает строку клиента и возвращает результат
        /// </summary>
        /// <param name="asyncResult"></param>
        public void OnReceivedData(IAsyncResult asyncResult)
        {
            _errorStringMessage = string.Empty;
            int nBytesRec = 0;

            try
            {
                nBytesRec = s.EndReceive(asyncResult);
            }
            catch (Exception exc)
            {
                _result = exc.Message;
                _errorStringMessage = exc.Message;
                return;
            }

            if (nBytesRec > 0)
            {
                StringBuilder m_strLine = new StringBuilder();

                for (int i = 0; i < nBytesRec; i++)
                {
                    Char ch = Convert.ToChar(m_byBuff[i]);
                    m_strLine.Append(Convert.ToString(ch));
                }

                try
                {
                    int strLinelen = m_strLine.Length;
                    if (strLinelen == 0)
                        m_strLine.Append(Convert.ToString("<BR>"));

                    Byte[] mToProcess = new Byte[strLinelen];

                    for (int i = 0; i < strLinelen; i++)
                        mToProcess[i] = Convert.ToByte(m_strLine[i]);

                    string mOutText = Encoding.GetEncoding(TlnConfigManager.ClientEncoding).GetString(mToProcess, 0, strLinelen);


                    if (mOutText.Length > 0)
                    {
                        _extraData.Append(mOutText);

                        if (!_extraData.ToString().EndsWith("</div>"))
                        {
                            if (s != null)
                                s.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, new AsyncCallback(OnReceivedData), s);

                        }
                        else
                        {
                            //_result = _extraData.ToString()

                            try
                            {
                                _result = ParseString(_extraData.ToString().Substring(_extraData.ToString().LastIndexOf("<div class=\"form\"")));

                                OnClientCallBack(_result);
                            }
                            catch { }
                            _extraData = new StringBuilder();
                            //Utils.WriteWebLog("OnReceiveData " + _result);
                            //if (_onRbTerminalClientDispatch != null)
                            //    _onRbTerminalClientDispatch(_result);
                        }

                        #region Comment
                        /*
                    if (mOutText.IndexOf("</table>") == -1
                        //&& mOutText.IndexOf("...") == 1
                        )
                    {
                        _extraData.Append(mOutText);

                        if (s != null)
                            s.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, new AsyncCallback(OnReceivedData), s);
                    }
                    else
                    {
                        //_result = _extraData.ToString() + mOutText;
                        _result = mOutText;
                        _extraData = new StringBuilder();
                    }
                    */
                        #endregion
                    }
                }
                catch (Exception e)
                {
                    _errorStringMessage = e.Message;
                    throw;
                }
            }
            else
            {
                if (!s.Connected)
                {
                    _result = "<div class='error'>Утеряно соединение с сервером<BR>Перезапустите приложение или<BR>обратитесь к системному<BR>администратору</div>";
                    s.Shutdown(SocketShutdown.Both);
                    s.Close();
                }
                else
                {
                    if (s != null)
                    {
                        _result = "<div class='error'>Утеряно соединение с сервером<BR>Перезапустите приложение или<BR>обратитесь к системному<BR>администратору</div>";
                        //    s.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, new AsyncCallback(OnReceivedData), s);
                    }

                }
                //throw new NullReferenceException("Соединение с сервером утеряно<BR>Необходима авторизация");
            }
        }
        #endregion


        #region ReturnString - возвращает результат
        /// <summary>
        /// Парсит результат
        /// </summary>
        /// <returns></returns>
        private string ParseString(string Str)
        {
            //Utils.WriteWebLog("ReturnString" + _result);

            /*
            int num = 0;
            while ((_result == string.Empty) && this.s.Connected)
            {
                num++;
                if (_errorStringMessage != string.Empty)
                {
                    _result = this._errorStringMessage;
                    break;
                }
                Thread.Sleep(200);
                if (this.s.ReceiveTimeout < (num * 200))
                {
                    this._result = "<div class='error'>Утеряно соединение с сервером<BR>Перезапустите приложение или<BR>обратитесь к системному<BR>администратору</div>";
                    break;
                }
            }
            */
            string rez = Str;

            rez = rez.Replace(OheConstant.TELNET_CLEAR_SEQ, "");
            rez = rez.Replace(TlnConfigManager.BEEP, "");
            rez = rez.Replace(OheConstant.TELNET_ESC_SEQ, "");

            #region Comment Code
            ////Utils.WriteWebLog("ReturnString After Fill _result:" + _result);
            /*
            char ch = char.Parse("~");
            string[] _rez = _result.Split(ch);
            for (int i = 0; i < _rez.Length; i++)
            {
                _rez[i] = _rez[i].Replace("+![2J+![1;0H", "");
                _rez[i] = _rez[i].Replace("+![7m", "<b>");
                _rez[i] = _rez[i].Replace("+![0m", "</b>");

                for (int h = 0; h < 100; h++)
                {
                    for (int t = 0; t < _rez.Length; t++)
                    {
                        if (_rez[i].IndexOf("+![" + t.ToString() + ";" + h.ToString() + "f") >= 0)
                        {
                            _rez[(t == 0 ? 1 : t) - 1] += "<input type='text' value='' size='5' width='10' name='user'>";
                            break;
                        }
                    }
                }
            }
            _result = string.Empty;
            for (int i = 0; i < _rez.Length; i++)
            {
                _result += _rez[i] + "<BR>";
            }
            */

            #endregion

            //string onkeydown = " onkeydown=\"javascript: document.onkeydown = ToggleShortcut;\" onChange=\"showCommentDialog();\" ";
            if (rez.IndexOf("<input") < 0)
            {
                rez += "<div align=\"center\"><input type='submit' value='Авторизация' name='user'><input type=\"hidden\" name=\"ref\" value=\"newsession\"></div>";
                //Utils.WriteWebLog("Return Strin Empty" + _result);
            }

            //return JqueryMobileHtmlStyle();
            /*
            return @"<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8'>
  <meta name='viewport' content='initial-scale=1.0, user-scalable=no'>
  <meta name='apple-mobile-web-app-capable' content='yes'>
  <meta name='apple-mobile-web-app-status-bar-style' content='black'>
  <title></title>
 
  <link rel='stylesheet' href='http://"+ host +@":81/style.css' type='text/css'>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
  <script language='javascript' src='http://" + host + @":81/js/func.js'></script>
  <bgsound src='sounds/" + ""//TlnConfigManager. T.GetSoundsEffect(rez)
                                       + @"' />
  </head>
  <body>
  <div data-role='page' id='page1'>
    <form action='/SendData' name='form1' method='post' ID='form1'>"
                    + rez
                    + "<input type='hidden' name='esc' value='0'>"
                    + "<input type='hidden' name='key' value='" + _session + "'>"

                    + @"</form>
            <script language='javascript'>
                if(document.form1.user != null)
                    document.form1.user.focus();
            </script>
  </body>
  </div>
</html>";
             */


            #region Old Code
            return @"<html>
 <head>
  <title>Терминал "+ host + @"</title> 
    <link rel='stylesheet' href='http://" + host + @":81/style.css' type='text/css'>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
      <script language='javascript'> var needComment = false;

/*document.onkeydown = ToggleShortcut;  */
function ToggleShortcut(event) {
    if (!document.getElementById) return;
    if (window.event) event = window.event;

    if (event.keyCode == 27) {
        document.getElementById('esc').value = '1';
        needComment = true;
    }
    else if (event.keyCode == 13) {
        document.form1.submit();
    }
}
function showCommentDialog() {
    if (needComment) {
        needComment = false;
        document.form1.submit();
    }
}
function ExitEvent() {
    document.getElementById('esc').value = '1';
    document.form1.submit();
}

function Menu_Click(val) {
    document.getElementById('user').value = encodeURIComponent(val);
    document.form1.submit();
}
function Function_Click(val) {
    document.getElementById('user').value = encodeURIComponent(val);
    document.form1.submit();
}</script>
      <bgsound src='sounds/" + ""//TlnConfigManager. T.GetSoundsEffect(rez)
                                       + @"' />
</head>
 <body>
    <form action='/SendData/' name='form1' method='post' ID='form1'>"
        + rez
        + "<input type='hidden' name='esc' id='esc' value='0'>"
        + "<input type='hidden' name='key' id='key' value='" + _session + "'>"
          
        + @"</form>
            <script language='javascript'>
                if(document.form1.user != null)
                    document.form1.user.focus();
            </script>
  </body>
</html>";
            #endregion
        }

        private string JqueryMobileHtmlStyle()
        {
            string rez = @"<!DOCTYPE html>
            <html>
            <head>
              <meta charset='utf-8'>
              <meta name='viewport content='initial-scale=1.0, user-scalable=no'>
              <meta name='apple-mobile-web-app-capable' content='yes'>
              <meta name='apple-mobile-web-app-status-bar-style' content='black'>
              <title></title>
              <link rel='stylesheet' href='http://192.168.0.3:81/webterm/js/jquery.mobile-1.3.1.min.css'>
              <!-- Extra Codiqa features -->
              <link rel='stylesheet' href='codiqa.ext.css'>
              <!-- jQuery and jQuery Mobile -->
              <script src='http://192.168.0.3:81/webterm/js/jquery-1.9.1.min.js'></script>
              <script src='http://192.168.0.3:81/webterm/js/jquery.mobile-1.3.1.min.js'></script>
              <!-- Extra Codiqa features -->
              <script src='http://192.168.0.3:81/webterm/js/func.js'></script>
            </head>
            <body>
                <!-- Home -->
                <div data-role='page' id='page1'>
                    <div data-theme='b' data-role='header' data-position='fixed'>
                        <a data-role='button' data-direction='reverse' href='#page1' data-icon='home'
                        data-iconpos='left' class='ui-btn-right'>
                            Выход
                        </a>
                        <h3>
                            OHE.WMS
                        </h3>
                    </div>
                    <div data-role='content'>
                        <form action=''>
                            <div data-role='fieldcontain'>
                                <label for='textinput1'>Пользователь:</label>
                                <input name='' id='textinput1' placeholder='' value='' type='text' data-mini='true'>
                            </div>
                            <div data-role='fieldcontain'>
                                <label for='textinput2'>Пароль:</label>
                                <input name='' id='textinput2' placeholder='' value='' type='password' data-mini='true'>
                            </div>
                            <div data-role='fieldcontain'>
                                <label for='textinput3'>Оборудование:</label>
                                <input name='' id='textinput3' placeholder='' value='' type='text' data-mini='true'>
                            </div>
                        </form>
                    </div>
	                <div data-theme='b' data-role='footer' data-position='fixed'>
	                <div data-role='navbar'>
		                  <ul>
			                <li><a href='a.html' class='ui-btn-active ui-state-persist'>Войти</a></li>
		                  </ul>
	                </div><!-- /navbar -->
	                <!--
		                <div data-role='fieldcontain'>
			                <fieldset data-role='controlgroup' data-type='horizontal' data-mini='true'>
				                <input type='radio' name='radio-action' id='remove' value='remove'>
				                <label for='remove'>F2: Список</label>
				                <input type='radio' name='radio-action' id='hide' value='hide'>
				                <label for='hide'>F2: Список</label>
				                <input type='radio' name='radio-action' id='width' value='width'>
				                <label for='width'>F2: Список</label>
			                </fieldset>
		                </div>
	                -->
	                </div>

                </div>
                </body>
                </html>";

            return rez;

        }


        private string HtmlStyle()
        {
            return string.Empty;
            return @"body
{
  margin:0px;
  font: bold 16px arial;
  font-weight:bold;
  background-color:white;
}

.form { height: 100%; width: 100%; overflow:visible;
background-color:#FFFFFF; font: bold 16px arial;}
.menu { /*background-color:#b3d4ff; border: solid #6593cf 1px; border-bottom:solid  #6593cf 1px;*/
text-transform:uppercase; font-weight: bold; padding: 2px;   font: bold 16px arial;
}
.menu_head { color:#15428b;   font: bold 16px arial;}
/* Класс для текстовых данных*/
.text {color: #15429f;    font: bold 16px arial;}
/* Класс для текстовых данных - выделение*/
.text_inverse {background-color:#d6e7ff; color:#15428b;   font: bold 16px arial;}
/* Класс для контролов ввода данных */
.input { width: 200px;   font: bold 16px arial;}
/* Класс для контролов ввода данных - выделение*/
.input_inverse {background-color:#eeeeee;border: dashed #7eacb1 1px; width: 200px;   font: bold 16px arial;}
/* Класс для контролов ввода данных - активный */
.input_active {border: solid #7eacb1 1px; width: 200px;   font: bold 16px arial;}
/* Класс для контролов ввода данных - неактивный*/
.input_disable {background-color:#eeeeee;border: dashed #7eacb1 1px;   font: bold 16px arial;}
/* Класс для контролов ввода данных - только для чтения */
.input_readonly {background-color:#eeeeee;border: dashed #7eacb1 1px;   font: bold 16px arial;}
/* Класс для кнопок списка (list) */
.list_menuitem { text-align:left; font-weight: bold; text-transform:uppercase; width: 300px;   font: bold 16px arial; /*background-color:#b3d4ff; border: solid #6593cf 1px; border-bottom:solid  #6593cf 1px; text-transform:uppercase;   font: bold 16px arial; */}
/* Класс для контрола данных выбора из списка (list) */
.list_iteminput { /*background-color:#eeeeee;border: #7eacb1 1px;   font: bold 16px arial; */}
/* Класс для кнопок меню */
.menu_button { text-align:left; font-weight: bold; text-transform:uppercase; width: 300px; font: bold 16px arial; width:300px;}
/* Класс для меню ввода данных */
.menu_input { width: 200px;   font: bold 16px arial;}
/* Класс для меню вывод количества страниц  */
.menu_pager {  font: bold 16px arial;}
/* Класс для функциональных клавиш  */
.function {   font: bold 16px arial; width: 100px; /*background-color:#b3d4ff; border: solid #6593cf 1px; border-bottom:solid  #6593cf 1px; text-transform:uppercase;*/}
/* Класс для функциональных клавиш - выделение */
.function_inverse {background-color:#b3d4ff; border: solid #6593cf 1px; border-bottom:solid  #6593cf 1px; text-transform:uppercase;   font: bold 16px arial;}
/* Класс для помощи */
.help {color:#FF0000;   font: bold 16px arial;}
/* Класс для отображения ошибок ввода */
.message {color:#FF0000;   font: bold 16px arial;}
/* Класс для кнопки выхода */
.exit_button {font: bold; width:60px;   font: bold 16px arial;}
.error {color:#FF0000;   font: bold 16px arial;}
/* Класс для значений */
.value {color:#FF0000;   font: bold 16px arial;}";
        }

        #endregion


        #region DispatchMessage
        /// <summary>
        /// Отсылает строку серверу и запускает возврат клиенту обратно
        /// </summary>
        /// <param name="strText"></param>
        void DispatchMessage(string strText)
        {
            if (strText == null)
                return;

            try
            {
                s.Send(Encoding.GetEncoding(TlnConfigManager.ServerEncoding).GetBytes(strText));
                s.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, new AsyncCallback(OnReceivedData), s);
            }
            catch (Exception e)
            {
                _errorStringMessage = e.Message;
                _result = "<div class='error'>Утеряно соединение с сервером<BR>Перезапустите приложение или<BR>обратитесь к системному<BR>администратору</div>";
            }
        }
        #endregion


        #region SendString - посылают строку серверу
        public void SendString(string query)
        {
            if (!s.Connected)
            {
                //DispatchMessage("Утеряно соединение с сервером");
                _result = "<div class='error'>Утеряно соединение с сервером<BR>Перезапустите приложение или<BR>обратитесь к системному<BR>администратору</div>";
            }
            else
                DispatchMessage(query);
        }
        #endregion


        #region SendFKey
        /// <summary>
        /// Парсит функциональные клавиши и отправляет серверу
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool SendFKey(string key)
        {
            string str = (string)key2dispatch[key];
            if (str != null)
                DispatchMessage(str);
            else
                return false;

            return true;
        }
        #endregion


        void webSocketClient_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            CurrentMessage = e.Message;
            MessageReceiveEvent.Set();
        }

        void webSocketClient_DataReceived(object sender, DataReceivedEventArgs e)
        {
            CurrentData = e.Data;
            DataReceiveEvent.Set();
        }

        void webSocketClient_Closed(object sender, EventArgs e)
        {
            CloseEvent.Set();
        }

        void webSocketClient_Opened(object sender, EventArgs e)
        {
            OpenedEvent.Set();
        }
    }

}