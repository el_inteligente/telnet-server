﻿//#define RBINTERFACE
#if RBINTERFACE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Services.Protocols;
using System.Runtime.Remoting.Messaging;

namespace Ohe.Telnet.RbTelnetServer
{
    [ServiceContract(Namespace = "http://xb2.net/soap" /*, Name = "RBWMSsoapService"*/)
    , XmlSerializerFormat(/*Use = OperationFormatUse.Encoded
        , Style = OperationFormatStyle.Rpc*/)
    ]
    public interface IRBWMS_SoapService
    {
        [OperationContract(Action="http://xb2.net/soap/RB_HH_Request"
                          , ReplyAction = "RB_RB_HH_RequestResponse", Name = "RB_RB_HH_RequestResponse")]
        string RB_HH_Request(string SID, string REQUEST, string OUTPUTTYPE, string IFACEVERSION, out string RESPONSE, out string STATE_MAJOR, out string STATE_MINOR, out string USERID);

        /*
        [OperationContract(//Action = "http://xb2.net/soap/RB_HH_Request"
            //, ReplyAction = "RB_RB_HH_RequestResponse"
            )]
        string RequestHandhelds(string sessionId, string request, string outputType, string interfaceVersion, out string stateMajor, out string stateMinor, out string userId);
        */
    }
}
#endif