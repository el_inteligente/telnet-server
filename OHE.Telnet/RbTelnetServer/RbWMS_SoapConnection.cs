﻿//#define RBINTERFACE
#if RBINTERFACE

using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;


namespace Ohe.Telnet.RbTelnetServer
{

    #region RbWebConnectionClient
    public class RbWebConnectionClient : IDisposable
    {
        private RBWMS_SoapServiceAdapter _service;

        private Exception _lastException;

        public Exception LastException { get { return _lastException; } }

        #region Constructor
        public RbWebConnectionClient(string host)
        {
            _service = new RBWMS_SoapServiceAdapter(host);
        }

        public RbWebConnectionClient()
        {
            _service = new RBWMS_SoapServiceAdapter("http://localhost:8080");
        }
        #endregion

        #region RequestHandhelds
        public string RequestHandhelds(string sessionId, string request, string outputType, string interfaceVersion, out string stateMajor, out string stateMinor, out string userId)
        {
            string response;
        //    string stateMajor;
        //    string stateMinor;
        //    string userId;

            _service.RB_HH_Request(sessionId, request, outputType, interfaceVersion, out response
                , out stateMajor, out stateMinor, out userId);

            return response;
        }

        #endregion


        #region RequestHandheldsTest
        public string RequestHandheldsTest(string sessionId, string request, string outputType, string interfaceVersion, out string loadRabbitResponse)
        {
            string response;
            string stateMajor;
            string stateMinor;
            string userId;

            //_service.RB_LoadRabbit_HH_Request(sessionId, request, outputType, interfaceVersion, out response, out stateMajor, out stateMinor
            //    , out userId, out loadRabbitResponse);
            response = "<?xml version=\"1.0\"?><message>RbDispatch</message>";
            loadRabbitResponse = "<?xml version=\"1.0\"?><message>RbDispatch</message>";

            return response;
        }
        #endregion
        


        #region IDisposable Members
        public void Dispose()
        {
            if (_service != null)
            {
                _service.Dispose();
                _service = null;
            }
        }
        #endregion
    }
    #endregion

    
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [WebServiceBinding(Location = "", Name = "RBWMSsoapService", Namespace = "http://xb2.net/soap")]
    public sealed class RBWMS_SoapServiceAdapter : SoapHttpClientProtocol
    {
        public RBWMS_SoapServiceAdapter(string host)
        {
            this.Url = host;
            this.RequestEncoding = Encoding.GetEncoding("windows-1251");
            this.Timeout = 120000;
        }

        public string GetURL()
        {
            return Url;
        }

        [SoapDocumentMethod(
             "http://xb2.net/soap/RB_HH_Request",
             RequestNamespace = "http://xb2.net/soap",
             ResponseNamespace = "http://xb2.net/soap",
             ResponseElementName = "RB_RB_HH_RequestResponse",
             Use = SoapBindingUse.Encoded,
             ParameterStyle = SoapParameterStyle.Wrapped)
        ]
        public void RB_HH_Request(string SID, string REQUEST, string OUTPUTTYPE, string IFACEVERSION, out string RESPONSE, out string STATE_MAJOR, out string STATE_MINOR, out string USERID)
        {

            //object[] results = this.Invoke("RB_HH_Request", new object[] { SID, REQUEST, OUTPUTTYPE, IFACEVERSION });
            //RESPONSE = ((string)results[0]);
            //object[] results = new object[4];

            RESPONSE = ("<?xml version=\"1.0\"?><message>RbDispatchfwerweфывакацук</message>");
            STATE_MAJOR = "1";// ((string)results[1]);
            STATE_MINOR = "1";// ((string)results[2]);
            USERID = "1";// ((string)results[3]);
        }

    }


    //[DebuggerStepThrough()]
    //[DesignerCategory("code")]
    //[WebServiceBinding(Location = "", Name = "RBWMSsoapService", Namespace = "http://xb2.net/soap")]
    /*
    [ServiceContract(Namespace = "http://xb2.net/soap", Name = "RBWMSsoapService"), XmlSerializerFormat]

    public class RBWMS_SoapService : WebService
    {
        [SoapDocumentMethod(
             "http://xb2.net/soap/RB_HH_Request",
             RequestNamespace = "http://xb2.net/soap",
             ResponseNamespace = "http://xb2.net/soap",
             ResponseElementName = "RB_RB_HH_RequestResponse",
             Use = SoapBindingUse.Encoded,
             ParameterStyle = SoapParameterStyle.Wrapped)

        [WebMethod]
        public void RB_HH_Request(string SID, string REQUEST, string OUTPUTTYPE, string IFACEVERSION, out string RESPONSE, out string STATE_MAJOR, out string STATE_MINOR, out string USERID)
        {

            object[] results = new object[] { "<?xml version=\"1.0\"?><message>RbDispatch</message>", "1", "1","UER1"};
            RESPONSE = ((string)results[0]);
            //object[] results = new object[4];
            RESPONSE = ("<?xml version=\"1.0\"?><message>RbDispatch</message>");
            STATE_MAJOR = ((string)results[1]);
            STATE_MINOR = ((string)results[2]);
            USERID = ((string)results[3]);
        }
    }   
    */




}
#endif