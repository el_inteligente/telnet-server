﻿//#define RBINTERFACE
#if RBINTERFACE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Ohe.Telnet.RbTelnetServer
{
    // NOTE: If you change the class name "RBWMS_SoapService" here, you must also update the reference to "RBWMS_SoapService" in App.config.
    public class RBWMS_SoapService : IRBWMS_SoapService
    {

        private RBWMS_SoapServiceAdapter _service;
        public RBWMS_SoapService()
        {
            _service = new RBWMS_SoapServiceAdapter("http://localhost:8080");
        }
        //[DebuggerStepThrough()]
        //[DesignerCategory("code")]
        //[WebServiceBinding(Location = "", Name = "RBWMSsoapService", Namespace = "http://xb2.net/soap")]
        public string RB_HH_Request(string SID, string REQUEST, string OUTPUTTYPE, string IFACEVERSION, out string RESPONSE, out string STATE_MAJOR, out string STATE_MINOR, out string USERID)
        {

            _service.RB_HH_Request(SID, REQUEST, OUTPUTTYPE, IFACEVERSION, out RESPONSE, out STATE_MAJOR, out STATE_MINOR, out USERID);

            /*
            object[] results = new object[] { "<?xml version=\"1.0\"  encoding=\"windows-1251\"?><message>dsfdsRbDisdfdfывпмвапавпspatch</message>", "1", "1", "UER1" };
            RESPONSE = ((string)results[0]);
            //object[] results = new object[4];
            RESPONSE = "";// ("<?xml version=\"1.0\"  encoding=\"utf-8\"?><message>RbDispatch</message>");
            STATE_MAJOR = ((string)results[1]);
            STATE_MINOR = ((string)results[2]);
            USERID = ((string)results[3]);
            */

            return RESPONSE;
        }
    }

}
#endif