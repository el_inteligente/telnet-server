﻿using System;
using System.ServiceModel;
using Ohe.Interfaces;
using Ohe.Utils.WCF;

namespace Ohe.Telnet.Server
{
    /// <summary>
    /// Класс обработки удаленных событий модуля телнет сервера
    /// </summary>
    [ServiceBehavior(AutomaticSessionShutdown=false)]
    public class OheTelnetModuleEventSubscriber : OheModuleEventSubscriberBase,IOheEvents
    {
        string _message = string.Empty;

        public event RemoveUsersEventHandler RemoveUsers;
        public delegate void RemoveUsersEventHandler(string[] logins);


        public OheTelnetModuleEventSubscriber(string ModuleId) : base(ModuleId) { }

        #region Events

            #region OnStart
            public override void OnStart()
            {
                try
                {
                    TelnetServer.RemoteStart();
                }

                catch (FaultException ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                                   + "Общая ошибка выполнения удаленного события OnStart()" + Environment.NewLine
                                   + "Описание Ошибки:" + ex.Message;
                    Utils.Utils.WriteLog(_message);
                    Dispose();
                }
                catch (CommunicationException ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                                    + "Ошибка связи при выполнении события OnStart()" + Environment.NewLine
                                    + "Описание Ошибки:" + ex.Message;
                    Utils.Utils.WriteLog(_message);
                    Dispose();
                }
                catch (System.Exception ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                            + "Ошибка связи при выполнении события OnStart()" + Environment.NewLine
                            + "Описание Ошибки:" + ex.Message;
                    Utils.Utils.WriteLog(_message);
                    Dispose();
                }
            }
            #endregion


            #region OnStop
            public override void OnStop()
            {
                try
                {
                    TelnetServer.Stop();
                }
                catch (FaultException ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                                   + "Общая ошибка выполнения удаленного события OnStop()" + Environment.NewLine
                                   + "Описание Ошибки:" + ex.Message;
                    Dispose();
                    Utils.Utils.WriteLog(_message);
                }
                catch (CommunicationException ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                                    + "Ошибка связи при выполнении события OnStop()" + Environment.NewLine
                                    + "Описание Ошибки:" + ex.Message;
                    Utils.Utils.WriteLog(_message);
                    Dispose();
                }
                catch (System.Exception ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                            + "Ошибка связи при выполнении события OnStop()" + Environment.NewLine
                            + "Описание Ошибки:" + ex.Message;
                    Utils.Utils.WriteLog(_message);
                    Dispose();
                }

            }
            #endregion


            #region OnRemove
            public override void OnRemove(string [] Login)
            {
                try
                {
                    if(RemoveUsers != null)
                    {
                        RemoveUsers(Login);
                        //TelnetServer.RemoteRemoveUser(Login);
                    } 
                    /*
                    foreach(string usr in Login)
                        TelnetServer.RemoteRemoveUser(usr);
                     */
                }
                catch (FaultException ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                                   + "Общая ошибка выполнения удаленного события OnRemove()" + Environment.NewLine
                                   + "Описание Ошибки:" + ex.Message;
                    Utils.Utils.WriteLog(_message);
                    Dispose();
                }
                catch (CommunicationException ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                                    + "Ошибка связи при выполнении события OnRemove()" + Environment.NewLine
                                    + "Описание Ошибки:" + ex.Message;
                    Utils.Utils.WriteLog(_message);
                    Dispose();
                }
                catch (System.Exception ex)
                {
                    _message = "Модуль: OHE.Telnet" + Environment.NewLine
                            + "Ошибка связи при выполнении события OnRemove()" + Environment.NewLine
                            + "Описание Ошибки:" + ex.Message;
                    Utils.Utils.WriteLog(_message);

                    Dispose();
                }

            }
            #endregion

        #endregion

    }
}
