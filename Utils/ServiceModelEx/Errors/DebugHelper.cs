// � 2008 IDesign Inc. All rights reserved 
//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.ServiceModel;
using System.Diagnostics;
using System.Reflection;

namespace ServiceModelEx
{
   public static class DebugHelper
   {
      public const bool IncludeExceptionDetailInFaults = 
      #if DEBUG
            true;
      #else
            false;
      #endif

      public static Exception ExtractException(this FaultException<ExceptionDetail> fault)
      {
         return ExtractException(fault.Detail);
      }
      static Exception ExtractException(ExceptionDetail detail)
      {
         Exception innerException = null;
         if(detail.InnerException != null)
         {
            innerException = ExtractException(detail.InnerException);
         }

         Type type = Type.GetType(detail.Type);
         Debug.Assert(type.IsSubclassOf(typeof(Exception)));
         Exception exception = Activator.CreateInstance(type,detail.Message,innerException) as Exception;
         Debug.Assert(exception != null);
         SetExceptionField(exception,"_stackTraceString",detail.StackTrace);
         SetExceptionField(exception,"_helpURL",detail.HelpLink);
         return exception;
      }
      static void SetExceptionField(Exception exception,string field,string value)
      {
         FieldInfo fieldInfo = typeof(Exception).GetField(field,BindingFlags.Instance|BindingFlags.NonPublic);
         fieldInfo.SetValue(exception,value);
      }
   }
}