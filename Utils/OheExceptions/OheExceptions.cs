﻿using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Text;
using Ohe.Utils;


namespace Ohe.Exceptions
{

	#region OheException
	[Serializable]
	public class OheException : Exception
	{
		public OheException()
		{
		}

		public OheException(string message)
			: base(message)
		{
		}

		public OheException(string message, Exception exception)
			: base(message, exception)
		{ }

		protected OheException(SerializationInfo serialInfo, StreamingContext streamContext)
			: base(serialInfo, streamContext)
		{
		}

		private static string Exception2String(Exception exception)
		{
			return Environment.NewLine + "Source:" + (exception.Source == null ? "Unknown" : exception.Source) + Environment.NewLine
				+ "Target:" + (exception.TargetSite == null ? "Unknown" : exception.TargetSite.ToString()) + Environment.NewLine
				+ "Exception:" + exception.GetType() + Environment.NewLine
				+ "Message:" + (exception.Message == null ? "Unknown" : exception.Message) + Environment.NewLine
				+ "Stack trace:" + (exception.StackTrace == null ? "Unknown" : exception.StackTrace.ToString()) + Environment.NewLine;
		}

		public override string ToString()
		{
			if (this.InnerException == null)
				return Exception2String(this);
			return Exception2String(this) + Environment.NewLine + Environment.NewLine + "Inner Exception::" + Environment.NewLine + Environment.NewLine
				+ Exception2String(this.InnerException);
		}
	}
	#endregion

	#region RbCompileError
	[Serializable]
	public class RbCompileError
	{
		#region Properties
		private bool _isError = true;
		public bool IsError { get { return _isError; } }

		private int _line = 0;
		public int Line { get { return _line; } }

		private int _column = 0;
		public int Column { get { return _column; } }

		private string _message = string.Empty;
		public string Message { get { return _message; } }
		#endregion

		#region Constructor
		public RbCompileError(bool isError, int line, int column, string message)
		{
			_isError = isError;
			_line = line;
			_column = column;
			_message = Utils.Utils.AllTrim(message);
		}
		#endregion

		#region ToString()
		public override string ToString()
		{
			return ((_isError ? "Error: " : "Warning: ") + "(" + _line + "," + _column + ") " + _message);
		}
		#endregion

	}
	#endregion

	#region OheCompileException
	/// <summary>
	/// Ошибки связанные с компиляцией отчетов
	/// </summary>
	[Serializable]
	public class OheCompileException : Exception
	{
		#region properties
		#region SourceCode
		private string _sourceCode = string.Empty;
		public string SourceCode
		{
			get
			{
				Hashtable ht = new Hashtable(_errors.Count);

				foreach (RbCompileError err in _errors)
					ht[err.Line] = null;

				StringBuilder src = new StringBuilder();

				string srcCopy = _sourceCode.Replace("\r", string.Empty).Replace("\n", Environment.NewLine);

				int line = 1;

				int pos = 0;

				while (pos != -1)
				{
					int lastPos = pos;
					pos = srcCopy.IndexOf(Environment.NewLine, pos);

					string str;

					if (pos != -1)
					{
						str = (pos == 0 ? string.Empty : srcCopy.Substring(lastPos, pos - lastPos));
						pos += 2;
					}
					else
					{
						if (lastPos >= srcCopy.Length)
							break;
						str = (pos == 0 ? string.Empty : srcCopy.Substring(lastPos));
					}

					src.Append(string.Format("{0:0000} {2} | {1}", line, str,
						(ht.ContainsKey(line) ? "+" : " ")) + Environment.NewLine);
					line++;
				}

				return src.ToString();
			}
		}
		#endregion

		#region IEnumerator Errors
		private ArrayList _errors = new ArrayList();
		public ArrayList Errors { get { return _errors; } }
		#endregion

		#region private GetMessagesString
		private string GetMessagesString(bool isErrors)
		{
			StringBuilder rez = new StringBuilder();

			foreach (RbCompileError err in _errors)
			{
				if (err.IsError == isErrors)
					rez.Append("(" + err.Line + "," + err.Column + ") " + err.Message + Environment.NewLine);
			}

			return rez.ToString();
		}
		#endregion

		public string ErrorsString { get { return GetMessagesString(true); } }

		public string WarningsString { get { return GetMessagesString(false); } }

		#region StatusMessage
		public string StatusMessage
		{
			get
			{
				int errorsCount = 0;
				int warnCount = 0;

				foreach (RbCompileError err in _errors)
				{
					if (err.IsError)
						errorsCount++;
					else
						warnCount++;
				}

				return (errorsCount == 0
					? "Compiled successfully."
					: errorsCount.ToString() + " Erorrs & " + warnCount.ToString() + " Warnings found.");
			}
		}
		#endregion
		#endregion

		#region Constructors
		public OheCompileException()
		{ }

		public OheCompileException(string message, string sourceCode, ArrayList errors)
			: base(message)
		{
			_sourceCode = Utils.Utils.AllTrim(sourceCode);
			_errors = errors;
		}
		#endregion

		#region Serialization
		protected OheCompileException(SerializationInfo serialInfo, StreamingContext streamContext)
			: base(serialInfo, streamContext)
		{
			_sourceCode = serialInfo.GetString("rb_sourceCode");
			_errors = (ArrayList)serialInfo.GetValue("rb_errors", typeof(ArrayList));
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("rb_sourceCode", _sourceCode);
			info.AddValue("rb_errors", _errors);
		}
		#endregion

		#region ToString()
		public override string ToString()
		{
			return Message
				+ Environment.NewLine + Environment.NewLine + "Source:" + Environment.NewLine + SourceCode
				+ Environment.NewLine + Environment.NewLine + "Errors:" + Environment.NewLine + ErrorsString
				+ Environment.NewLine + Environment.NewLine + "Warnings:" + Environment.NewLine + WarningsString;
		}
		#endregion
	}
	#endregion

	#region OheDataException
	/// <summary>
	/// Ошибки связанные с получением данных и т.п.
	/// </summary>
	[Serializable]
	public class OheDataException : Exception
	{
		private string _query = "Unknown";
		public string Query { get { return _query; } set { _query = Utils.Utils.AllTrim(value); } }

		private string _queryId = string.Empty;
		public string QueryId { get { return _queryId; } set { _queryId = Utils.Utils.AllTrim(value).ToLower(); } }


		public OheDataException()
		{
		}

		public OheDataException(string message, Exception exception)
			: base(message, exception)
		{ }

		public OheDataException(string message)
			: base(message)
		{
		}

		public OheDataException(Exception e, string message)
			: base(message, e)
		{
		}

		public OheDataException(string message, string query, string queryId)
			: base(message)
		{
			Query = query;
			QueryId = queryId;
		}

		public OheDataException(Exception e, string message, string query, string queryId)
			: base(message, e)
		{
			Query = query;
			QueryId = queryId;
		}

        public OheDataException(Exception e, string message, string query)
            : base(message, e)
        {
            Query = query;
        }


		protected OheDataException(SerializationInfo serialInfo, StreamingContext streamContext)
			: base(serialInfo, streamContext)
		{
		}

		public override string ToString()
		{
			return base.ToString()
				+ Environment.NewLine + "Query:" + _query + Environment.NewLine + Environment.NewLine
				+ (StackTrace != null ? StackTrace : string.Empty);
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}
	}
	#endregion

	#region OheReportException
	/// <summary>
	/// Описывает ошибки, связанные с генератором отчетов
	/// </summary>
	[Serializable]
	public class OheReportException : Exception
	{
        private string _formId;
        private string _controlId;
        private string _event;
        private string _select;
        private string _message;


		public OheReportException() { }

		public OheReportException(string message) : base(message) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FormId">Идентификатор формы</param>
        /// <param name="ControlId">Идентификатор контрола</param>
        /// <param name="Event">Событие</param>
        /// <param name="Select">Запрос</param>
        /// <param name="Message">Сообщение</param>
        /// <param name="InnerException">Внутреннее исключение</param>
        public OheReportException(string FormId, string ControlId, string Event, string Select, string Message, Exception InnerException):base (Message,InnerException)
        {
            _formId = FormId; _controlId = ControlId; _event = Event; _select = Select;
        }

        public OheReportException(string message, Exception exception) : base(message, exception) { }

        public OheReportException(Exception e, string message) : base(message, e) { }

        public override string ToString()
        {
            StringBuilder rez = new StringBuilder();

            rez.Append("<table>");
            rez.Append("<tr><td>Идентификатор формы: <b>" + (_formId != null ? _formId : "Неизвестно"));
            rez.Append("</b></td></tr>");
            rez.Append("<tr><td>Идентификатор контрола: <b>" + (_controlId != null ? _controlId : "Неизвестно"));
            rez.Append("</b></td></tr>");
            rez.Append("<tr><td>Событие: <b>" + (_event != null ? _event : "Неизвестно"));
            rez.Append("</b></td></tr>");
            rez.Append("<tr><td>Запрос: <b>" + (_select != null ? _select : "Неизвестно"));
            rez.Append("</b></td></tr>");
            rez.Append("<tr><td>Ошибка: " + (_message != null ? _message : "Неизвестно"));
            rez.Append("</td></tr>");
            rez.Append("<tr><td>Внутренняя ошибка: " + (InnerException != null ? InnerException.Message : "Неизвестно"));
            rez.Append("</td></tr>");
            rez.Append("</table>");

            return rez.ToString();
            
        }

	}
	#endregion

	#region OheLicenceException
	/// <summary>
	/// Описывает ошибки вязанные с лицензированием приложения
	/// </summary>
	[Serializable]
	public class OheLicenceException : Exception
	{
		private string _formId = string.Empty;
		public string FormId { get { return _formId; } }

		public OheLicenceException() { }

		public OheLicenceException(string formId)
			: base("Form id [" + formId + "] disabled, because, of licence restriction")
		{
			_formId = Utils.Utils.AllTrim(formId);
		}

		protected OheLicenceException(SerializationInfo serialInfo, StreamingContext streamContext) : base(serialInfo, streamContext) { }
	}
	#endregion

	#region DBException
	/// <summary>
	/// Ошибки, связанные с использованием базы данных
	/// </summary>
	[Serializable]
	public class DBException : Exception
	{
		public DBException()
		{
		}

		public DBException(string message)
			: base(message)
		{
		}

		public DBException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		protected DBException(SerializationInfo serialInfo, StreamingContext streamContext)
			: base(serialInfo, streamContext)
		{
		}
	}
	#endregion

	#region OheExceptionMessage
	[Serializable]
	public sealed class OheExceptionMessage
	{
		private OheExceptionMessage()
		{
		}

		public static string Message(Exception e)
		{
			string eMessage = "Exception:" + e.GetType().ToString() + Environment.NewLine +
				"Message:" + e.Message + Environment.NewLine +
				"Stack Trace:" + e.StackTrace + Environment.NewLine;
			return eMessage;
		}
	}
	#endregion

	#region OheLoginException
	/// <summary>
	/// Описывает ошибки авторизации пользователей
	/// </summary>
	[Serializable]
	public class OheLoginException : Exception
	{
		private bool _isNeedPrint = true;
		public bool IsNeedPrint { get { return _isNeedPrint; } set { _isNeedPrint = value; } }

		private bool _isNeedMail = true;
		public bool IsNeedMail { get { return _isNeedMail; } set { _isNeedMail = value; } }

		private bool _isNeedRecover;
		public bool IsNeedRecover { get { return _isNeedRecover; } set { _isNeedRecover = value; } }

		private string _badValue = string.Empty;
		public string BadValue { get { return _badValue; } set { _badValue = Utils.Utils.AllTrim(value); } }

		private string _nextState = "dbConnectStrs";
		public string NextState { get { return _nextState; } set { _nextState = Utils.Utils.AllTrim(value); } }

		public OheLoginException() : base() { }

		public OheLoginException(string message) : base(message) { }

		public OheLoginException(string message, Exception e) : base(message, e) { }

		protected OheLoginException(SerializationInfo serialInfo, StreamingContext streamContext) : base(serialInfo, streamContext) { }

		public OheLoginException(string message, bool isNeedRecover)
			: base(message)
		{
			_isNeedRecover = isNeedRecover;
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}
	}
	#endregion
}
