using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web;
using System.Xml;
using Ohe.Utils;
using Ohe.Web.Configuration;

namespace Ohe.Data
{
	public sealed class OheDataTable : IDisposable, IListSource
	{
		#region Class data 

		private DataTable _table;

		internal OheDataClass DataSession;

		public DataTable GetDataTable { get { return _table; } }

		private string _select = string.Empty;

		public string Select { get { return _select; } }

		#endregion


		#region Constructor / Dispose 
		public OheDataTable(OheDataClass dataSession, DataTable table, string query)
		{
			_select = query;
			DataSession = dataSession;
			_table = table;

			if (Name == string.Empty)
			{
				string _name = query.ToLowerInvariant();
				int start = _name.IndexOf("from")+5;

				_name = Utils.Utils.Substring(_name, start, _name.IndexOf(" ", start) - start);
				Name = _name.Replace("dbo.","").Replace("[","").Replace("]","");
			}

		}

        [Obsolete("����������� ������ ��� �������� �����")]
        public OheDataTable(DataTable table)
        {
            _select = "select ''";
            DataSession = null;
            _table = table;

            if (Name == string.Empty)
                Name = "test";
        }


		public OheDataTable()
		{
			_table = new DataTable();
			_table.Locale = CultureInfo.CurrentCulture;
		}

		public void Dispose()
		{
			DataSession = null;
		}
		#endregion


		#region Calculate specific row count from aggregate
		public int RbSpecificRowCount
		{
			get
			{
				try
				{
					if (_table.Rows.Count == 0)
						return 0;
					return GetInt(0, 0);
				}
				catch
				{
					return 0;
				}
			}
		}
		#endregion


		#region rowsCount
		public int RowsCount { get { return _table.Rows.Count; } }
		#endregion


		#region colsCount
		public int ColumnsCount { get { return _table.Columns.Count; } }
		#endregion


		#region isEmpty
		public bool IsEmpty { get { return (_table == null || _table.Rows.Count == 0); } }
		#endregion

        public void Clear() { _table.Clear(); }

        public void AcceptChanges() { _table.AcceptChanges(); }


		#region Columns operations

		#region ColumnId2Pos
		/// <summary>
		/// ���������� ������� ������� �� ��������
		/// </summary>
		/// <param name="colId">��� �������</param>
		/// <returns></returns>
		public int ColumnId2Pos(string colId)
		{
			DataColumn dc = _table.Columns[colId];
			return (dc == null ? -1 : dc.Ordinal);
		}
		#endregion


		#region Add / Del Column
		public DataColumn AddColumn(string columnId)
		{
			return _table.Columns.Add(columnId);
		}

		public DataColumn AddColumn(string columnId, Type type)
		{
			return _table.Columns.Add(columnId, type);
		}

		public DataColumn AddColumn(string columnId, Type type, string expression)
		{
			return _table.Columns.Add(columnId, type, expression);
		}

		public DataColumn AddColumn(string columnId, Type type, string expression, string title)
		{
			DataColumn rez = _table.Columns.Add(columnId, type, expression);
			rez.Caption = Utils.Utils.AllTrim(title);
			return rez;
		}

		public void DelColumn(string columnId)
		{
			_table.Columns.Remove(columnId);
		}
		#endregion


		#region Get / Set column Type / Name / Caption
		public Type GetColumnType(int col)
		{
			return _table.Columns[col].DataType;
		}

		public Type GetColumnType(string col)
		{
			return _table.Columns[col].DataType;
		}

		public string GetColumnName(int col)
		{
			return _table.Columns[col].ColumnName;
		}

		public void SetColumnCaption(int col, string name)
		{
			_table.Columns[col].Caption = name;
		}

		public string GetColumnCaption(int col)
		{
			return _table.Columns[col].Caption;
		}

		public void SetColumnName(int col, string name)
		{
			_table.Columns[col].ColumnName = Utils.Utils.AllTrim(name).ToLower(CultureInfo.CurrentCulture);
		}

		public void SetColumnName(string col, string name)
		{
			_table.Columns[col].ColumnName = Utils.Utils.AllTrim(name).ToLower(CultureInfo.CurrentCulture);
		}
		#endregion
		#endregion


		#region test row,col for null
		public bool IsNull(int row, int col)
		{
			return _table.Rows[row].IsNull(col);
		}

		public bool IsNull(int row, string col)
		{
			return _table.Rows[row].IsNull(col);
		}
		#endregion


		#region Name property
		/// <summary>
		/// ��� �������
		/// </summary>
		public string Name
		{
			get
			{
				return _table.TableName;
			}

			set
			{
				_table.TableName = value;
			}
		}
		#endregion


		#region Data access methods

		#region Set row / col to object
		/// <summary>
		/// ���������� ������ � ����������� ������, �������
		/// </summary>
		/// <param name="row">������</param>
		/// <param name="col">�������</param>
		/// <returns></returns>
		public object this[int row, int col]
		{
			set
			{
				_table.Rows[row][col] = (value == null ? DBNull.Value : value);
			}
			get
			{
				return _table.Rows[row][col];
			}
		}

		/// <summary>
		/// ���������� ������ � ����������� ������, �������
		/// </summary>
		/// <param name="row">������</param>
		/// <param name="col">��� �������</param>
		/// <returns></returns>
		public object this[int row, string col]
		{
			set
			{
				_table.Rows[row][col] = (value == null ? DBNull.Value : value);
			}
			get
			{
				return _table.Rows[row][col];
			}
		}
		#endregion


        #region get string
		public string GetString(int row, int col)
		{
			return Utils.Utils.Obj2Str(_table.Rows[row][col]);
		}

		public string GetString(int row, string col)
		{
			return Utils.Utils.Obj2Str(_table.Rows[row][col]);
		}
		#endregion


        public int GetSizeColumn(int col)
        {
            int rez = 0;
            switch (GetColumnName(col).ToLowerInvariant())
            {
                case "binlabel":
                    return 60;
                case "descript":
                    return 250;
                case "product":
                    return 70;
                case "po_num":
                case "so_num":
                    return 90;

            }

            if (GetColumnType(col).Name.ToLowerInvariant() == "datetime")
            {
                return 50;
            }

            for(int i=0;i<_table.Rows.Count; i++)
            {
                int len = Utils.Utils.AllTrim(_table.Rows[i][col].ToString()).Length * 10;
                if (len > rez)
                    rez = len;
            }
            if (rez > 200) rez = 200;
            return rez;
        }


		#region get int
		public int GetInt(int row, int col)
		{
			return Utils.Utils.Obj2Int(_table.Rows[row][col]);
		}

		public int GetInt(int row, string col)
		{
			return Utils.Utils.Obj2Int(_table.Rows[row][col]);
		}
		#endregion


		#region get decimal
		public decimal GetDecimal(int row, int col)
		{
			return Utils.Utils.Obj2Decimal(_table.Rows[row][col]);
		}

		public decimal GetDecimal(int row, string col)
		{
			return Utils.Utils.Obj2Decimal(_table.Rows[row][col]);
		}
		#endregion


		#region get datetime
		public DateTime GetDateTime(int row, int col)
		{
			return Utils.Utils.Obj2DateTime(_table.Rows[row][col]);
		}

		public DateTime GetDateTime(int row, string col)
		{
			return Utils.Utils.Obj2DateTime(_table.Rows[row][col]);
		}

		public string GetDateTimeString(int row, int col)
		{
			return Utils.Utils.Obj2DateTime(_table.Rows[row][col]).ToString("d", DateTimeFormatInfo.InvariantInfo);
		}

		public string GetDateTimeString(int row, string col)
		{
			return Utils.Utils.Obj2DateTime(_table.Rows[row][col]).ToString("d", DateTimeFormatInfo.InvariantInfo);
		}
		#endregion


        #region get time
        public TimeSpan GetTime(int row, int col)
        {
            return Utils.Utils.Obj2Time(_table.Rows[row][col]);
        }

        public TimeSpan GetTime(int row, string col)
        {
            return Utils.Utils.Obj2Time(_table.Rows[row][col]);
        }

        public string GetTimeString(int row, int col)
        {
            return Utils.Utils.Obj2Time(_table.Rows[row][col]).ToString();
        }
        #endregion



		#region get double
		public double GetDouble(int row, int col)
		{
			return Utils.Utils.Obj2Double(_table.Rows[row][col]);
		}

		public double GetDouble(int row, string col)
		{
			return Utils.Utils.Obj2Double(_table.Rows[row][col]);
		}
		#endregion


		#region get bool
		public bool GetBool(int row, int col)
		{
			return Utils.Utils.Obj2Bool(_table.Rows[row][col]);
		}

		public bool GetBool(int row, string col)
		{
			return Utils.Utils.Obj2Bool(_table.Rows[row][col]);
		}
		#endregion


        #region get float
        public float GetFloat(int row, string col)
        {
            return Utils.Utils.Obj2Float(_table.Rows[row][col]);
        }
        #endregion


        public object GetObject(int row, int col)
		{
			return _table.Rows[row][col];
		}


		public object GetObject(int row, string col)
		{
			return _table.Rows[row][col];
		}

		public object[] ItemArray(int row)
		{
			return _table.Rows[row].ItemArray;
		}
		#endregion


		#region AddRow
		public DataRow AddRow()
		{
			DataRow row = _table.NewRow();
			_table.Rows.Add(row);
			return row;
		}

		public DataRow AddRow(object[] obj)
		{
			return _table.Rows.Add(obj);
		}
		#endregion


		#region InsertRow
		public DataRow InsertRow(int pos)
		{
			DataRow row = _table.NewRow();
			_table.Rows.InsertAt(row, pos);
			return row;
		}

		public DataRow InsertRow(object[] obj, int pos)
		{
			DataRow row = _table.NewRow();
			row.ItemArray = obj;
			_table.Rows.InsertAt(row, pos);
			return row;
		}
		#endregion


		#region DeleteRow
		public void DeleteRow(int row)
		{
			_table.Rows[row].Delete();
		}
		#endregion



		#region isEmptyColumn
		public bool IsEmptyColumn(int col)
		{
			bool isEmpty = true;
			for (int row = 0; row < RowsCount; row++)
				if (!Utils.Utils.isEmpty(_table.Rows[row][col]))
				{
					isEmpty = false;
					break;
				}
			return isEmpty;
		}

		public bool IsEmptyColumn(string col)
		{
			bool isEmpty = true;
			for (int row = 0; row < RowsCount; row++)
				if (!Utils.Utils.isEmpty(_table.Rows[row][col]))
				{
					isEmpty = false;
					break;
				}
			return isEmpty;
		}
		#endregion


		#region Translate column names
		public void TranslateColNames()
		{
			foreach (DataColumn dc in _table.Columns)
			{
				if (dc.Caption != null && dc.Caption.StartsWith("<td"))
					continue;

				if (Utils.Utils.AllTrim(dc.Caption).Length == 0)
				{
					if (dc.ColumnName != null && dc.ColumnName.StartsWith("<td"))
						dc.Caption = dc.ColumnName;
					else
						dc.Caption = OheWebConfig.Translate(dc.ColumnName);
				}
				else
					dc.Caption = OheWebConfig.Translate(dc.Caption);
			}
		}

		public void TranslateColNames(OheLanguage language)
		{
			foreach (DataColumn dc in _table.Columns)
			{
				if (dc.Caption != null && dc.Caption.StartsWith("<td"))
					continue;

				if (Utils.Utils.AllTrim(dc.Caption).Length == 0)
				{
					if (dc.ColumnName != null && dc.ColumnName.StartsWith("<td"))
						dc.Caption = dc.ColumnName;
					else
						dc.Caption = OheWebConfig.Translate(dc.ColumnName, language);
				}
				else
					dc.Caption = OheWebConfig.Translate(dc.Caption, language);
			}
		}
		#endregion


        #region GetKeyValuesByColumn
        public List<string> GetKeyValuesByColumn(string Column)
        {
            List<string>  rez = new List<string>();

            List<object> values = ToColumnStringArray(Column);

            foreach (object ss in values)
            {
                string rex = Utils.Utils.Obj2Str(ss);
                if (!rez.Contains(rex))
                    rez.Add(rex);
            }

            return rez;
        }
        #endregion


        #region GetValuesByColumnKey
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnX">������� X</param>
        /// <param name="ColumnY">������� Y</param>
        /// <param name="Value">�������� ��� ��� X</param>
        /// <returns></returns>
        public string GetValuesByColumnKey(string ColumnX, string Str, string keyColumn,  string Category, string ColumnY)
        {
            for (int i = 0; i < _table.Rows.Count; i++)
            {
                if (GetString(i, keyColumn) == Category)
                {
                    if(GetString(i,ColumnX) == Str)
                         return GetString(i, ColumnY);
                }
            }

            return string.Empty;
        }
        #endregion


        #region GetValuesByColumnKey
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnX">������� X</param>
        /// <param name="ColumnY">������� Y</param>
        /// <param name="Value">�������� ��� ��� X</param>
        /// <returns></returns>
        public string GetValueByColumnKeyPie(string ColumnX, string Str, string keyColumn)
        {

            for (int i = 0; i < _table.Rows.Count; i++)
            {
                if (GetString(i, ColumnX) == Str)
                {
                     return GetString(i, keyColumn);
                }
            }
            return string.Empty;
        }
        #endregion




		#region ToStringArray 
		public string[][] ToStringArray()
		{
			string[][] rez = new string[_table.Rows.Count][];

			for (int i = 0; i < _table.Rows.Count; i++)
			{
				rez[i] = new string[_table.Columns.Count];
				for (int j = 0; j < _table.Columns.Count; j++)
					rez[i][j] = GetString(i, j);
			}

			return rez;
		}
		#endregion


		#region To1StringArray
		public string[] To1StringArray()
		{
			string[] rez = new string[_table.Rows.Count];

			for (int i = 0; i < _table.Rows.Count; i++)
				rez[i] = GetString(i, 0);

			return rez;
		}
		#endregion


        #region ToColumnStringArray
        public List<object> ToColumnStringArray(string Column)
        {
            List<object> rez = new List<object>();

            for (int i = 0; i < _table.Rows.Count; i++)
                rez.Add(GetObject(i, Column));

            rez.Sort();

            return rez;
        }
        #endregion


        #region ToColumnFloatArray
        public float[] ToColumnFloatArray(string Column)
        {
            float[] rez = new float[_table.Rows.Count];

            for (int i = 0; i < _table.Rows.Count; i++)
                rez[i] = GetFloat(i, Column);

            return rez;
        }
        #endregion
        

		#region ToCaptionArray
		public string[] ToCaptionArray()
		{
			string[] rez = new string[_table.Columns.Count];
			for (int i = 0; i < _table.Columns.Count; i++)
				rez[i] = _table.Columns[i].Caption;

			return rez;
		}
		#endregion


		#region ToColumnNameArray
		public string[] ToColumnNameArray()
		{
			string[] rez = new string[_table.Columns.Count];
			for (int i = 0; i < _table.Columns.Count; i++)
				rez[i] = _table.Columns[i].ColumnName;

			return rez;
		}
		#endregion


		#region locate a row
		public int Locate(string expression)
		{
			if (_table.Rows.Count == 0)
				return -1;

			DataRow[] foundRows = _table.Select(expression);

			if (foundRows == null || foundRows.Length <= 0)
				return -1;

			for (int i = 0; i < _table.Rows.Count; i++)
				if (_table.Rows[i].Equals(foundRows[0]))
					return i;

			return -1;
		}
		#endregion


		#region sort data
		public bool Sort(string sortQuery)
		{
			return Sort(sortQuery, 0);
		}

		public bool Sort(string sortQuery, int top)
		{
			//don't bother do anything if nothing is there
			if (_table.Rows.Count == 0)
				return false;

			DataRow[] foundRows = _table.Select("", sortQuery);


			if (foundRows.Length > 0)
			{
				DataTable dt = new DataTable(_table.TableName);
				dt.Locale = CultureInfo.CurrentCulture;

				foreach (DataColumn dc in _table.Columns)
					dt.Columns.Add(dc.ColumnName, dc.DataType);

				for (int i = 0; i < foundRows.Length && (top == 0 || i < top); i++)
					dt.Rows.Add(foundRows[i].ItemArray);

				_table = dt;
				return true;
			}
			else
				return false;
		}
		#endregion


		#region filter data
		public bool Filter(string expression, string sort, int top)
		{
			//don't bother do anything if nothing is there
			if (_table.Rows.Count == 0)
				return false;

			DataRow[] foundRows = (sort == null || sort.Length == 0 ? _table.Select(expression) : _table.Select(expression, sort));

			if (foundRows.Length > 0)
			{
				//because DataTable.Clear will also clear the data in foundRow
				//let's make a copy before clear
				DataRow[] newRows = new DataRow[foundRows.Length];

				for (int i = 0; i < foundRows.Length; i++)
				{
					DataRow r1 = _table.NewRow();
					r1.ItemArray = foundRows[i].ItemArray;
					newRows[i] = r1;
				}

				//clear all existing rows
				_table.Rows.Clear();

				int nLen = (top <= 0 ? newRows.Length : Math.Min(newRows.Length, top));

				//add back the found rows
				for (int i = 0; i < nLen; i++)
					_table.Rows.Add(newRows[i]);

				return true;
			}
			else
				return false;
		}

		public bool Filter(string expression, string sort)
		{
			return Filter(expression, sort, 0);
		}

		public bool Filter(string expression, int top)
		{
			return Filter(expression, null, top);
		}

		public bool Filter(string expression)
		{
			return Filter(expression, null, 0);
		}
		#endregion


		#region filter data
		public OheDataTable GetByFilter(string expression, string sort, int top)
		{
			OheDataTable rez = new OheDataTable(this.DataSession, new DataTable(), this._select);
			rez._table.TableName = _table.TableName;
			rez._table.Locale = _table.Locale;

			//don't bother do anything if nothing is there
			if (_table.Rows.Count == 0)
				return rez;

			DataRow[] foundRows = (sort == null || sort.Length == 0 ? _table.Select(expression) : _table.Select(expression, sort));

			int row = 1;

			foreach (DataColumn dc in _table.Columns)
			{
				rez._table.Columns.Add(dc.ColumnName, dc.DataType);
				if (row == top)
					break;
				row++;
			}

			foreach (DataRow dr in foundRows)
				rez._table.Rows.Add(dr.ItemArray);

			return rez;
		}

		public OheDataTable GetByFilter(string expression, string sort)
		{
			return GetByFilter(expression, sort, 0);
		}

		public OheDataTable GetByFilter(string expression, int top)
		{
			return GetByFilter(expression, null, top);
		}

		public OheDataTable GetByFilter(string expression)
		{
			return GetByFilter(expression, null, 0);
		}
		#endregion


		#region IListSource Members

		public IList GetList()
		{
			if (_table != null)
				return new DataView(_table);
			return null;
		}

		public bool ContainsListCollection
		{
			get
			{
				return true;
			}
		}

		#endregion


		#region GetByFilterColumns
		public OheDataTable GetByFilterColumns(ArrayList visibleColumnList)
		{
			OheDataTable rez = new OheDataTable(this.DataSession, new DataTable(), this._select);
			rez._table.TableName = _table.TableName;
			rez._table.Locale = _table.Locale;

			//don't bother do anything if nothing is there
			if (_table.Rows.Count == 0)
				return rez;

			foreach (DataColumn dc in _table.Columns)
				rez._table.Columns.Add(dc.ColumnName, dc.DataType);

			foreach (DataRow dr in _table.Rows)
				rez._table.Rows.Add(dr.ItemArray);

			ArrayList id2remove = new ArrayList();

			foreach (DataColumn dc in rez._table.Columns)
				if (visibleColumnList != null
					&& visibleColumnList.Count > 0
					&& visibleColumnList.IndexOf(dc.ColumnName.ToLower(CultureInfo.CurrentCulture)) == -1)
					id2remove.Add(dc.ColumnName);

			foreach (string colId in id2remove)
				rez.DelColumn(colId);

			return rez;
		}
		#endregion


		#region ToXmlDataSource
		[Obsolete("������������ �������� Anchor ��� �����")]
		public XmlNode ToXmlDataSource(XmlDocument doc, ArrayList visibleColumnList)
		{
			XmlNode dsNode = doc.CreateElement("datasource");

			ArrayList realColumnsList = new ArrayList();

			foreach (string colName in visibleColumnList)
				if (ColumnId2Pos(colName) != -1)
					realColumnsList.Add(colName);

			double[] doubleSum = new double[realColumnsList.Count];
			string[] colTypesNames = new string[realColumnsList.Count];

			#region append headers
			XmlNode headersNode = doc.CreateElement("headers");

			int realColPos = 0;

			foreach (string colName in realColumnsList)
			{
				DataColumn dc = _table.Columns[colName];

				if (dc == null)
					continue;

				colTypesNames[realColPos] = dc.DataType.Name;

				XmlNode headerNode = doc.CreateElement("header");

				XmlAttribute attr = doc.CreateAttribute("id");
				attr.Value = dc.ColumnName;
				headerNode.Attributes.Append(attr);

				attr = doc.CreateAttribute("value");
				attr.Value = Utils.Utils.AllTrim(dc.Caption);
				headerNode.Attributes.Append(attr);

				attr = doc.CreateAttribute("datatype");
				attr.Value = dc.DataType.Name;
				headerNode.Attributes.Append(attr);

				headersNode.AppendChild(headerNode);

				realColPos++;
			}

			dsNode.AppendChild(headersNode);
			#endregion

			#region append rows
			XmlNode rowsNode = doc.CreateElement("rows");

			int row = 0;
			foreach (DataRow dr in _table.Rows)
			{
				XmlNode rowNode = doc.CreateElement("row");

				int realPos = 0;

				foreach (string colId in realColumnsList)
				{
					DataColumn dc = _table.Columns[colId];

					if (dc == null)
						continue;

					if (dr.ItemArray[dc.Ordinal] == null || dr.IsNull(dc.Ordinal))
					{
						XmlNode fieldNode = doc.CreateElement("field");

						XmlAttribute attr = doc.CreateAttribute("value");
						attr.Value = string.Empty;
						fieldNode.Attributes.Append(attr);

						attr = doc.CreateAttribute("link");
						attr.Value = string.Empty;
						fieldNode.Attributes.Append(attr);

						rowNode.AppendChild(fieldNode);
					}
					else
					{
						string val = string.Empty;

						#region count for footer
						switch (colId.ToLower(CultureInfo.CurrentCulture))
						{
							case "product":
							case "packslip":
							case "po_num":
								if (string.Compare(_table.TableName, "asmbl_info", true) != 0)
									doubleSum[realPos]++;
								break;

							/*
							case "procstep":
								val = RbProcStepConfig.ProcStep2String((Int16)dr[dc.Ordinal], OheLanguage.English);
								break;
							*/
						}

						if (val.Length == 0)
							switch (colTypesNames[realPos])
							{
								case "Int16":
									if (colId != "PROCSTEP")
										doubleSum[realPos] += (Int16)dr[dc.Ordinal];
									val = dr[dc.Ordinal].ToString();
									break;

								case "Int32":
									if (colId != "PACKSIZE")
										doubleSum[realPos] += (Int32)dr[dc.Ordinal];
									val = dr[dc.Ordinal].ToString();
									break;

								case "Decimal":
									doubleSum[realPos] += Convert.ToDouble((Decimal)dr[dc.Ordinal]);
									val = dr[dc.Ordinal].ToString();
									break;

								case "Single":
									doubleSum[realPos] += (Single)dr[dc.Ordinal];
									val = dr[dc.Ordinal].ToString();
									break;

								case "Double":
									doubleSum[realPos] += (double)dr[dc.Ordinal];
									val = dr[dc.Ordinal].ToString();
									break;

								case "DateTime":
									val = ((DateTime)dr[dc.Ordinal]).ToString("yyyyMMdd", CultureInfo.CurrentCulture);
									break;

								default:
									val = dr[dc.Ordinal].ToString();
									break;
							}
						#endregion

						RbColumn2AnchorConfig cellAnchor = RbColumn2AnchorConfig.Column2Anchor(colId);

						string cellLink = string.Empty;
						string cellHeader = val;

						if (cellAnchor.IsExists)
						{
							string val2anchor = string.Empty;

							if (cellAnchor.Header.Length > 0)
								cellHeader = cellAnchor.Header;

							/*
							if (cellAnchor.ColId.Length > 0 && _table != null)
							{
								int pos = this.ColumnId2Pos(cellAnchor.ColId.ToLower());
								if (pos != -1)
									val2anchor = this.GetString(row, pos);
							}
							else
								val2anchor = val;
							*/

							if (val2anchor.Length > 0)
							{
								if (cellAnchor.IsPopup)
                                    cellLink = WebConstant.WEB_REF + cellAnchor.Link + Uri.EscapeDataString(val2anchor) + "&popup=1";
								else
                                    cellLink = WebConstant.WEB_REF + cellAnchor.Link + Uri.EscapeDataString(val2anchor);
							}
						}

						XmlNode fieldNode = doc.CreateElement("field");

						XmlAttribute attr = doc.CreateAttribute("value");
						attr.Value = cellHeader;
						fieldNode.Attributes.Append(attr);

						attr = doc.CreateAttribute("link");
						attr.Value = cellLink;
						fieldNode.Attributes.Append(attr);

						rowNode.AppendChild(fieldNode);
					}

					realPos++;
				}

				rowsNode.AppendChild(rowNode);

				row++;
			}

			dsNode.AppendChild(rowsNode);
			#endregion

			#region append footers
			XmlNode footersNode = doc.CreateElement("footers");

			for (int col = 0; col < doubleSum.Length; col++)
			{
				string strSum = string.Empty;

				if (doubleSum[col] != 0.0)
				{
					switch (((string)realColumnsList[col]).ToLower())
					{
						case "product":
							if (_table.TableName.ToLower() != "ASMBL_INFO")
								strSum = doubleSum[col].ToString() + " Products";
							break;

						case "packslip":
							if (_table.TableName.ToLower() != "ASMBL_INFO")
								strSum = doubleSum[col].ToString() + " Packslips";
							break;

						case "po_num":
							if (_table.TableName.ToLower() != "ASMBL_INFO")
								strSum = doubleSum[col].ToString() + " POs";
							break;

						default:
							strSum = doubleSum[col].ToString();
							break;
					}
				}

				XmlNode footerNode = doc.CreateElement("footer");

				XmlAttribute attr = doc.CreateAttribute("val");
				attr.Value = strSum;
				footerNode.Attributes.Append(attr);

				footersNode.AppendChild(footerNode);
			}

			dsNode.AppendChild(footersNode);
			#endregion

			return dsNode;
		}
		#endregion


		#region ToCsvDataSource
		public string ToCsvDataSource()
		{
			if (_table == null || _table.Rows.Count == 0)
				return string.Empty;

			StringBuilder csvRez = new StringBuilder();

			double[] doubleSum = new double[_table.Columns.Count];
			string[] colTypesNames = new string[_table.Columns.Count];

			#region append headers
			foreach (DataColumn dc in _table.Columns)
			{
				colTypesNames[dc.Ordinal] = dc.DataType.Name;

				csvRez.Append("\"");
				csvRez.Append(dc.Caption.Replace("\"", "\"\""));
				csvRez.Append("\",");
			}

			csvRez.Append(Environment.NewLine);
			#endregion

			#region append rows
			foreach (DataRow dr in _table.Rows)
			{
				for (int col = 0; col < dr.ItemArray.Length; col++)
				{
					if (dr.ItemArray[col] == null || dr.IsNull(col))
						csvRez.Append(",");
					else
					{
						string colId = _table.Columns[col].ColumnName.ToLower(CultureInfo.CurrentCulture);
						string val = string.Empty;

						#region count for footer
						switch (colId)
						{
							case "product":
							case "packslip":
							case "po_num":
								if (string.Compare(_table.TableName, "asmbl_info", true, CultureInfo.CurrentCulture) != 0)
									doubleSum[col]++;
								break;
						}

						if (val.Length == 0)
							switch (colTypesNames[col])
							{
								case "Int16":
									if (colId != "procstep")
										doubleSum[col] += (Int16)dr[col];
									val = dr[col].ToString();
									break;

								case "Int32":
									if (colId != "packsize")
										doubleSum[col] += (Int32)dr[col];
									val = dr[col].ToString();
									break;

								case "Decimal":
									doubleSum[col] += Convert.ToDouble((Decimal)dr[col]);
									val = dr[col].ToString();
									break;

								case "Single":
									doubleSum[col] += (Single)dr[col];
									val = dr[col].ToString();
									break;

								case "Double":
									doubleSum[col] += (double)dr[col];
									val = dr[col].ToString();
									break;

								case "DateTime":
									val = ((DateTime)dr[col]).ToString("yyyyMMdd");
									break;

								default:
									val = dr[col].ToString();
									break;
							}
						#endregion

						csvRez.Append("\"");
						csvRez.Append(val.Replace("\"", "\"\""));
						csvRez.Append("\",");
					}
				}

				csvRez.Append(Environment.NewLine);
			}
			#endregion

			#region append footers
            /*
			for (int col = 0; col < doubleSum.Length; col++)
			{
				string strSum = string.Empty;

				if (doubleSum[col] != 0.0)
				{
					switch (_table.Columns[col].ColumnName.ToLower(CultureInfo.CurrentCulture))
					{
						default:
							strSum = doubleSum[col].ToString();
							break;
					}
				}

				csvRez.Append("\"");
				csvRez.Append(strSum.Replace("\"", "\"\""));
				csvRez.Append("\",");
			}

			csvRez.Append(Environment.NewLine);
            */
			#endregion

			return csvRez.ToString();
		}
		#endregion


        #region ToXlsDataSource()
        public string ToXlsDataSource()
        {
            if (_table == null || _table.Rows.Count == 0)
                return string.Empty;

            StringBuilder csvRez = new StringBuilder();

            double[] doubleSum = new double[_table.Columns.Count];
            string[] colTypesNames = new string[_table.Columns.Count];

            #region append rows
            foreach (DataRow dr in _table.Rows)
            {
                csvRez.Append("<tr>");
                for (int col = 0; col < dr.ItemArray.Length; col++)
                {
                    if (dr.ItemArray[col] == null || dr.IsNull(col))
                        csvRez.Append("<td colspan=\"" + dr.ItemArray.Length.ToString() +"\"></td>");
                    else
                    {
                        string colId = _table.Columns[col].ColumnName.ToLower(CultureInfo.CurrentCulture);
                        string val = string.Empty;

                        #region count for footer
                        if (val.Length == 0)
                            switch (colTypesNames[col])
                            {
                                case "Int16":
                                    doubleSum[col] += (Int16)dr[col];
                                    val = dr[col].ToString();
                                    break;

                                case "Int32":
                                    doubleSum[col] += (Int32)dr[col];
                                    val = dr[col].ToString();
                                    break;

                                case "Decimal":
                                    doubleSum[col] += Convert.ToDouble((Decimal)dr[col]);
                                    val = dr[col].ToString();
                                    break;

                                case "Single":
                                    doubleSum[col] += (Single)dr[col];
                                    val = dr[col].ToString();
                                    break;

                                case "Double":
                                    doubleSum[col] += (double)dr[col];
                                    val = dr[col].ToString();
                                    break;

                                case "DateTime":
                                    val = ((DateTime)dr[col]).ToString("dd.MM.yyyy");
                                    break;

                                default:
                                    val = dr[col].ToString();
                                    break;
                            }
                        #endregion

                        csvRez.Append("<td>");
                        csvRez.Append(val);
                        csvRez.Append("</td>");
                    }
                }
                csvRez.Append("</tr>");
            }
            #endregion

            #region append footers
            csvRez.Append("<tr>");
            for (int col = 0; col < doubleSum.Length; col++)
            {
                string strSum = string.Empty;

                if (doubleSum[col] != 0.0)
                {
                    switch (_table.Columns[col].ColumnName.ToLower(CultureInfo.CurrentCulture))
                    {
                        default:
                            strSum = doubleSum[col].ToString();
                            break;
                    }
                }

                csvRez.Append("<td>");
                csvRez.Append(strSum);
                csvRez.Append("</td>");
            }
            csvRez.Append("</tr>");
            #endregion
            return csvRez.ToString();
        }
        #endregion

	}
}
