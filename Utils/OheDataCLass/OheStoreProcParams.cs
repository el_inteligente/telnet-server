using System;
using System.Collections.Generic;
using System.Text;
using Ohe.Utils;

namespace Ohe.Data
{
	/// <summary>
	/// ���������� ������ ���������� ��� �������
	/// </summary>
	public class RfControlEventsParams
	{
		#region Define Variables
		private string _name; // ���������� ��� ���������
		private string _value; // ���������� �������� ���������
		private int _size;     // ���������� ������ ��������� ��� ��������� ��������
		private string _type; // ���������� ��� ���������
		private string _cntlId; // ���������� ������� (ValueId) � �������� ���� ����� �������� ��� 
		// ��� ������ �������� ���������
		#endregion


		#region Properties
		/// <summary>
		/// �������� ���������
		/// </summary>
		public string Name { get { return _name; } }

		/// <summary>
		/// �������� ���������
		/// </summary>
		public string Value { get { return _value; } set { _value = value; } }

		/// <summary>
		/// ��� ���������
		/// </summary>
		public string Type { get { return _type; } set { _type = value; } }

		/// <summary>
		/// ������ ��������� ��� ��������� ����������
		/// </summary>
		public int Size { get { return _size; } }

		/// <summary>
		/// ���������� ������������� �������� � �������� ���� ����� �������� ��� �������� ���������
		/// </summary>
		public string ControlId
		{
			get
			{
				if (_cntlId.Length > 0)
				{
					// ���������� ������ �������� ������
					if (_cntlId.Contains("(") && _cntlId.Contains(")"))
						return Utils.Utils.AllTrim(_cntlId.Substring(0, _cntlId.IndexOf("(")));
					else
						return Utils.Utils.AllTrim(_cntlId);
				}
				else
					return string.Empty;
			}
			set { _cntlId = Utils.Utils.AllTrim(value.ToUpperInvariant()); }
		}

		/// <summary>
		/// ���������� ��� ������� � ������
		/// </summary>
		public string Column
		{
			get
			{
				if (_cntlId.Contains("(") && _cntlId.Contains(")"))
				{
					string d = _cntlId.Substring(_cntlId.IndexOf("(") + 1, _cntlId.Length - _cntlId.IndexOf("(") - 2);
					if (d.Length > 0)
						return d;
					else
						//return string.Empty;
						return "0";
				}
				else
					//return string.Empty;
					return "0";
			}
		}
		#endregion


		#region Constructors
		public RfControlEventsParams() { }

		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="Name">�������� ��������</param>
		/// <param name="Type">��� ���������</param>
		/// <param name="Size">������ ��������� ������ ��� �����</param>
		/// <param name="Value">�������� ���������</param>
		public RfControlEventsParams(string Name, string Type, int Size, string Value)
		{
			_name = Name;
			_type = Type;
			_size = Size;
			_value = Value;
			_cntlId = string.Empty;
		}

		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="Name">�������� ��������</param>
		/// <param name="Type">��� ���������</param>
		/// <param name="Size">������ ��������� ������ ��� �����</param>
		/// <param name="Value">�������� ���������</param>
		/// <param name="CntlId">���������� � ������ �������� ���� ����� �������� ���������</param>
		public RfControlEventsParams(string Name, string Type, int Size, string Value, string CntlId)
		{
			_name = Name;
			_type = Type;
			_size = Size;
			_value = Value;
			_cntlId = Utils.Utils.AllTrim(CntlId.ToUpper());
		}

		#endregion

	}
}
