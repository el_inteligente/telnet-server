using System;
using System.Data;
using System.Data.Common;

namespace Ohe.Data
{
	#region OheDataProviderType
	public enum OheDataProviderType
	{
		Undefined = 0,
		MicrosoftSqlServer = 1,
		Oracle = 2,
		OleDB = 3,
		Odbc = 4,
	}
	#endregion

	public interface IRbDataProvider
	{
		// Create a new instance of database connetion
		IDbConnection CreateConnectionObject();

		// Create a new instance of database data adapter
		DbDataAdapter CreateDataAdapterObject();

		// Type of actual connection object used
		Type ConnectionType { get; }

		// �������� ��� �������� ���������
		IDbDataParameter CreateStoreProcParameter();

        // �������� ��� �������� ���������
        IDataParameter CreateNewParameter(string name, SqlDbType dbType);

        // �������� ��� �������� ���������
        IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size);

        // �������� ��� �������� ���������
        IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size, byte precision, byte scale);


		// Data Provider type
		OheDataProviderType DataProviderType { get;}

        bool IsNamedParametersSupported { get; }
	}
}
