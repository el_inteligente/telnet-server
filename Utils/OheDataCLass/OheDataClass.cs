﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Text;
using Ohe.Utils;
using Ohe.Exceptions;
using Ohe.Web.Configuration;
using System.Data.SqlClient;


namespace Ohe.Data
{
	public sealed class OheDataClass : IDisposable
	{
		#region Class Data 

		    private IRbDataProvider dataProvider;

		    private IDbConnection dbConnection;

		    private IDbTransaction dbTransaction;

		    public ConnectionState State { get { return (dbConnection == null ? ConnectionState.Broken : dbConnection.State); } }

		    public string Database { get { return (dbConnection == null ? string.Empty : dbConnection.Database); } }

		#endregion


		#region Constructor / Dispose
            public OheDataClass() : this(OheWebConfig.DbType, OheWebConfig.ConnectionString) { }

		    /// <summary>
		    /// Конструктор, сразу открывает соединения
		    /// </summary>
		    /// <param name="dataProviderName">Имя провайдера данных</param>
		    /// <param name="connectionString">Строка подсоединения</param>
            public OheDataClass(string dataProviderName, string connectionString)
            {
                try
                {
                    dataProvider = DataProviderFromName(dataProviderName);
                    dbConnection = dataProvider.CreateConnectionObject();
                    dbConnection.ConnectionString = connectionString;

                    dbConnection.Open();
                }
                catch (SqlException sql)
                {
                    throw sql;
                }
                catch (Exception e)
                {
                    throw new OheDataException(e, e.Message);
                }


                if (dbConnection.State != ConnectionState.Open)
                    throw new OheDataException("Не могу создать соединение с базой данных.<br>Пользователь ASPNET не создан на сервере, или неверная строка подключения к базе данных:<br><br>"
                        + connectionString + ". <br><br>Пожалуйста проверьте строку соединения в конфигурации OheWebConfig.xml. Строка соединения: [" + connectionString + "]");
            }


            #region Old Dispose
            public void Dispose()
            {
                try
                {
                    if (dbConnection != null && dbConnection.State != ConnectionState.Closed)
                    {
                        if (dbTransaction != null)
                        {
                            try { dbTransaction.Rollback(); }
                            catch (OheDataException)
                            {
                            }
                            catch (InvalidOperationException)
                            {
                            }
                        }
                        dbConnection.Close();
                    }
                }
                catch (OheDataException) { }
            }
            #endregion
            /*
            public void Dispose() { Dispose(true); }
            private bool disposed = false;
            private bool commited = false;
            private void Dispose(bool disposing)
            {
                if (!disposed)
                {
                    if (disposing)
                    {
                        if (!commited) 
                        { 
                            if (dbTransaction != null) dbTransaction.Rollback();
                            if (dbConnection != null && dbConnection.State != ConnectionState.Closed) 
                                dbConnection.Close();
                        }
                    }
                    else
                    {
                        Utils.Utils.WriteWebLog("Сбой при вызове Dispose() в классе OheDataClass");
                    }
                }
            }

            ~OheDataClass() { Dispose(false);}
            */
		#endregion


		#region GetCurrentProvider
		public static OheDataProviderType GetCurrentProvider()
		{
			return DataProviderName2Type(OheWebConfig.DbType);
		}
		#endregion


		#region DataProviderName2Type
		public static OheDataProviderType DataProviderName2Type(string dataProviderName)
		{
			switch (Utils.Utils.AllTrim(dataProviderName).ToLower())
			{
				case "oracle":
					return OheDataProviderType.Oracle;

				case "oledb":
					return OheDataProviderType.OleDB;

				case "odbc":
					return OheDataProviderType.Odbc;

				default:
					return OheDataProviderType.MicrosoftSqlServer;
			}
		}
		#endregion


		#region DataProviderFromName - имя провайдера данных
		/// <summary>
		/// Возвращает интерфейс провайдера данных по идентификаторы
		/// </summary>
		/// <param name="dataProviderName"></param>
		/// <returns></returns>
		public static IRbDataProvider DataProviderFromName(string dataProviderName)
		{
			switch (Utils.Utils.AllTrim(dataProviderName).ToLower())
			{
				case "oracle":
					return new OheOracleDataProvider();

				case "oledb":
					return new OheOleDBDataProvider();

				case "odbc":
					return new OheOdbcDataProvider();

				default:
					return new OheSqlDataProvider();
			}
		}
		#endregion


		#region GetUserTablesName - Массив таблиц базы данных
		/// <summary>
		/// Возвращает Массив таблиц базы данных
		/// </summary>
		/// <returns></returns>
		public string[] GetUserTablesName()
		{
			switch (dataProvider.DataProviderType)
			{
				case OheDataProviderType.MicrosoftSqlServer:
					{
						OheDataTable dt = _GetTable("select * from sysobjects where xtype='U'", string.Empty);

						string[] rez = new string[dt.RowsCount];

						for (int row = 0; row < dt.RowsCount; row++)
							rez[row] = dt.GetString(row, 0);

						return rez;
					}

				default:
					return new string[0];
			}
		}

		public static string[] GetUserTables()
		{
			using (OheDataClass ds = new OheDataClass())
			{
				return ds.GetUserTablesName();
			}
		}

		#endregion


		#region DatabaseEncoding - Кодировка базы данных
		/// <summary>
		/// Кодировка базы данных
		/// </summary>
		public Encoding DatabaseEncoding
		{
			get
			{
				IDbCommand cmd = dbConnection.CreateCommand();
				cmd.CommandText = "select collationproperty(convert(nvarchar, databasepropertyex('" + dbConnection.Database + "', 'Collation')), 'CodePage')";
				cmd.CommandTimeout = 600;
				return Encoding.GetEncoding((int)cmd.ExecuteScalar());
			}
		}
		#endregion


		#region Transaction - поддержка транзакций
		/// <summary>
		/// Начало транзакции
		/// </summary>
		public void BeginTransaction()
		{
			if (dbTransaction != null)
				throw new OheDataException("Поддержка вложенных транзакций запрещена. Закройте предыдущие транзакции и повторите действие еще раз.");
			if (dbConnection.State != ConnectionState.Open)
				throw new OheDataException("Нет соединения с базой данных");

			try
			{
				dbTransaction = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
			}
			catch (Exception e)
			{
				dbTransaction = null;
				throw new OheDataException(e, "Не могу начать транзакцию.");
			}
		}

		/// <summary>
		/// Окончание транзакции
		/// </summary>
		public void CommitTransaction()
		{
			if (dbTransaction == null)
				throw new OheDataException("Предыдущая транзакция не завершена. Попробуйте еще раз.");
			if (dbConnection.State != ConnectionState.Open)
				throw new OheDataException("Нет соединения с базой данных");

			try
			{
				dbTransaction.Commit();
			}
			catch (Exception e)
			{
				throw new OheDataException(e, "Не могу завершить транзакцию");
			}
		}

		/// <summary>
		/// Откат транзакции
		/// </summary>
		public void RollbackTransaction()
		{
			if (dbTransaction == null)
				throw new OheDataException("Не могу откатить транзакцию, пожалуйста завершите предыдущую операцию");
			if (dbConnection.State != ConnectionState.Open)
				throw new OheDataException("Нет соединения с базой данных");

			try
			{
				dbTransaction.Rollback();
			}
			catch (Exception e)
			{
				throw new OheDataException(e, "Не могу откатить транзакцию");
			}
		}
		#endregion


		#region GetTableByQueryId - not static
		/// <summary>
		/// Возвращает запрос по идентификатору из конфигурации (с присвоенными параетрами)
        /// ИСпользуется только для Web приложений.
		/// </summary>
		/// <param name="queryId"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public string GetQueryByQueryId(string queryId, List<string[]> args)
		{
			string query2exec = string.Empty;

			try
			{
				string qd = OheWebConfig.GetQueryById(queryId, (int)dataProvider.DataProviderType);

				if (qd == null)
					return string.Empty;

				if (args.Count > 0)
				{
					foreach (string[] param in args)
						qd = qd.Replace("##" + param[0] + "##", param[1]);

					query2exec = qd;			
				}
				else
					query2exec = qd;
			}
			catch (Exception e)
			{
				throw new OheDataException(e, e.Message, "Id запроса '" + queryId + "'", queryId);
			}
			return query2exec;
		}

		/// <summary>
		/// Возвращает таблицу по идентификатору запроса
		/// </summary>
		/// <param name="queryId">Идентификатор запроса</param>
		/// <param name="args">Массив параметров</param>
		/// <returns></returns>
		public OheDataTable GetTableByQueryId(string queryId, List<string[]> args)
		{
			string query2exec = string.Empty;

			try
			{
				string qd = OheWebConfig.GetQueryById(queryId, (int)dataProvider.DataProviderType);

				if (qd == null)
					return  null;

				if (args.Count > 0)
				{
					foreach (string[] param in args)
						qd = qd.Replace("##" + param[0] + "##", param[1]);

					query2exec = qd;
				}
				else
					query2exec = qd;

				return _GetTable(query2exec, queryId);
			}
			catch (OheDataException)
			{
				throw;
			}
			catch (Exception e)
			{
				throw new OheDataException(e, e.Message, "ID запроса '" + queryId + "'", queryId);
			}
		}
		#endregion


        #region GetTableByQuery - public метод возвращает таблицу по запросу УНИВЕРСАЛЬНАЯ
        
        /// <summary>
        /// Возвращает таблицу по запросу
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <param name="queryId">Идентфикатор запроса</param>
        /// <returns></returns>
        public OheDataTable GetTableByQuery(string query)
        {
            query = Utils.Utils.AllTrim(query);
            string query2exec = query;

            try
            {
                IDbCommand cmd = dbConnection.CreateCommand();
                if (dbTransaction != null)
                    cmd.Transaction = dbTransaction;

                if (dataProvider.DataProviderType != OheDataProviderType.Oracle)
                {
                    cmd.CommandText = "set transaction isolation level read uncommitted";
                    cmd.ExecuteNonQuery();
                }


                cmd.CommandText = query2exec;
                cmd.CommandTimeout = 600;

                DbDataAdapter da = dataProvider.CreateDataAdapterObject();
                ((IDbDataAdapter)da).SelectCommand = cmd;

                //Запись в лог приложения
                //RbLog.Write2DebugSqlLog(cmd.CommandText);

                DataTable table = new DataTable();
                table.Locale = CultureInfo.CurrentCulture;

                da.Fill(table);

                return new OheDataTable(null, table, query2exec);
            }
            catch (SqlException sql)
            {
                throw sql;
            }
            catch (Exception e)
            {
                throw new OheDataException(e, e.Message, query2exec);
            }
        }

        #endregion


		#region _GetTable - private метод возвращает таблицу по запросу
		/// <summary>
		/// Возвращает таблицу по запросу
		/// </summary>
		/// <param name="query">Запрос</param>
		/// <param name="queryId">Идентфикатор запроса</param>
		/// <returns></returns>
		private OheDataTable _GetTable(string query, string queryId)
		{
			query = Utils.Utils.AllTrim(query);
			string query2exec = query;

			try
			{
				IDbCommand cmd = dbConnection.CreateCommand();
				if (dbTransaction != null)
					cmd.Transaction = dbTransaction;

				if (dataProvider.DataProviderType != OheDataProviderType.Oracle)
				{
					cmd.CommandText = "set transaction isolation level read uncommitted";
					cmd.ExecuteNonQuery();
				}


				cmd.CommandText = query2exec;
				cmd.CommandTimeout = 600;

				DbDataAdapter da = dataProvider.CreateDataAdapterObject();
				((IDbDataAdapter)da).SelectCommand = cmd;

				//Запись в лог приложения
				//RbLog.Write2DebugSqlLog(cmd.CommandText);

				DataTable table = new DataTable();
				table.Locale = CultureInfo.CurrentCulture;

                da.Fill(table);


				return new OheDataTable(null, table, query2exec);
			}
			catch (Exception e)
			{
				throw new OheDataException(e, e.Message, query2exec, queryId);
			}
		}

		#endregion
        

		#region _GetTableFromSp - private метод возвращает таблицу по запросу
		/// <summary>
		/// Возвращает таблицу по запросу
		/// </summary>
		/// <param name="query">Запрос</param>
		/// <param name="queryId">Идентфикатор запроса</param>
		/// <returns></returns>
		private OheDataTable _GetTableFromSp(string query, List<RfControlEventsParams> prms)
		{
			query = Utils.Utils.AllTrim(query);
			string query2exec = query;

			try
			{
				IDbCommand cmd = dbConnection.CreateCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				if (dbTransaction != null)
					cmd.Transaction = dbTransaction;

				if (dataProvider.DataProviderType != OheDataProviderType.Oracle)
				{
					cmd.ExecuteNonQuery();
				}

				cmd.CommandText = query2exec;
				cmd.CommandTimeout = 600;

				DbDataAdapter da = dataProvider.CreateDataAdapterObject();
				((IDbDataAdapter)da).SelectCommand = cmd;

				#region ParseParametrs
				foreach (RfControlEventsParams par in prms)
				{
					try
					{
						IDbDataParameter prm = dataProvider.CreateStoreProcParameter();

						if (par.Size > 0)
						{
							((IDbDataParameter)prm).ParameterName = par.Name;
							((IDbDataParameter)prm).Size = par.Size;
							((IDbDataParameter)prm).DbType = Utils.Utils.ConvertStrToDbTypeSame(par.Type);
						}
						else
						{
							((IDbDataParameter)prm).ParameterName = par.Name;
							((IDbDataParameter)prm).DbType = Utils.Utils.ConvertStrToDbTypeSame(par.Type);
						}

						((IDbDataParameter)prm).Value = Utils.Utils.ConvertStrToDbObject(par.Value, par.Type, string.Empty);

						((IDbDataAdapter)cmd).SelectCommand.Parameters.Add(prm);
					}
					catch (FormatException ex)
					{
						throw new FormatException("Ошибка обработки данных:"
											 + Environment.NewLine
											 + "Хранимая процедура:"
											 + query2exec + Environment.NewLine
											 + "Параметр: " + par.Name + Environment.NewLine
											 + "Значение:" + par.Value + Environment.NewLine
											 + "Неверное преобразование параметров хранимой процедуры"
											 + Environment.NewLine
											 + "Ошибка: " + ex.Message);
					}
				}
				#endregion

				//Запись в лог приложения
				//RbLog.Write2DebugSqlLog(cmd.CommandText);

				DataTable table = new DataTable();
				table.Locale = CultureInfo.CurrentCulture;

				da.Fill(table);

				return new OheDataTable(null, table, query2exec);
			}
			catch (Exception e)
			{
				throw new OheDataException(e, e.Message, query2exec, string.Empty);
			}
		}

		#endregion
	
	
		#region Execute
		/// <summary>
		/// Возвращает количество обработанных записей
		/// </summary>
		/// <param name="query"></param>
		/// <returns></returns>
		public int Execute(string query)
		{
			if (dbConnection.State != ConnectionState.Open)
				throw new OheDataException("Нет соединения с базой данных");

			try
			{
				IDbCommand cmd = dbConnection.CreateCommand();
				if (dbTransaction != null)
					cmd.Transaction = dbTransaction;
				cmd.CommandTimeout = 600;

				if (dataProvider.DataProviderType != OheDataProviderType.Oracle)
				{
					cmd.CommandText = "set ansi_warnings off";
					cmd.ExecuteNonQuery();
				}

				//RbLog.Write2DebugSqlLog(query);
				cmd.CommandText = query;
				return cmd.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				throw new OheDataException(e, e.Message + Environment.NewLine + query, query, string.Empty);
			}
		}
		#endregion


        /// <summary>
        /// Статический метод, результат работы хранимой процедуры
        /// </summary>
        /// <param name="queryId">Идентификатор</param>
        /// <param name="args">Параметры</param>
        /// <returns></returns>
        public static OheDataTable ExecuteStoreProcedure(string queryId, List<RfControlEventsParams> parameters)
        {
            using (OheDataClass session = new OheDataClass())
            {
                return session.ExecuteProcedure(queryId, parameters);
            }
        }


        public OheDataTable ExecuteProcedure(string procedureName, List<RfControlEventsParams> parameters)
        {
            //Hashtable CS$1$0000;
            if (dbConnection.State != ConnectionState.Open)
            {
                throw new OheDataException("Connection is not opened or busy");
            }

            IDbCommand spCommand = dbConnection.CreateCommand();
            spCommand.CommandType = CommandType.StoredProcedure;
            spCommand.CommandText = procedureName;
            if (dbTransaction != null)
            {
                spCommand.Transaction = dbTransaction;
            }
            foreach (RfControlEventsParams spParameter in parameters)
            {
                spCommand.Parameters.Add(this.CreateDataParameter(spParameter));
            }
            try
            {
                /*
                IDbCommand cmd = dbConnection.CreateCommand();
                if (dbTransaction != null)
                {
                    cmd.Transaction = dbTransaction;
                }
                cmd.CommandTimeout = 600;
                if (!(this.dataProvider.DataProviderType != OheDataProviderType.MicrosoftSqlServer))
                {
                    cmd.CommandText = "set ansi_warnings off";
                    cmd.ExecuteNonQuery();
                }
                */
                //DateTime startDate = DateTime.Now;
                //spCommand.Prepare();

                DbDataAdapter da = dataProvider.CreateDataAdapterObject();
                ((IDbDataAdapter)da).SelectCommand = spCommand;

                DataTable table = new DataTable();
                table.Locale = CultureInfo.CurrentCulture;


                da.Fill(table);

                return new OheDataTable(null,table, procedureName);
            }
            catch (Exception e)
            {
                throw new OheDataException(e, e.Message + Environment.NewLine + "SP:" + spCommand.CommandText, "SP:" + spCommand.CommandText, string.Empty);
            }
        }

        private IDataParameter CreateDataParameter(RfControlEventsParams spParameter)
        {
            IDataParameter dataParameter;
            SqlDbType sqlType = Utils.Utils.ConvertStrToDbType(spParameter.Type);
            if (sqlType != SqlDbType.Decimal)
            {
                if (sqlType != SqlDbType.Text)
                {
                    if (!dataProvider.IsNamedParametersSupported)
                    {
                        dataParameter = dataProvider.CreateNewParameter(string.Empty, sqlType, spParameter.Size);
                    }
                    else
                    {
                        dataParameter = dataProvider.CreateNewParameter("@" + spParameter.Name, sqlType, spParameter.Size);
                    }
                }
                else if (!dataProvider.IsNamedParametersSupported)
                {
                    dataParameter = dataProvider.CreateNewParameter(string.Empty, sqlType, 0x7fffffff);
                }
                else
                {
                    dataParameter = dataProvider.CreateNewParameter("@" + spParameter.Name, sqlType, 0x7fffffff);
                }
            }
            else if (!dataProvider.IsNamedParametersSupported)
            {
                dataParameter = dataProvider.CreateNewParameter(string.Empty, sqlType, spParameter.Size, (byte)spParameter.Size, 0);// (byte)spParameter.Dec);
            }
            else
            {
                dataParameter = dataProvider.CreateNewParameter("@" + spParameter.Name, sqlType, spParameter.Size, (byte)spParameter.Size, 0);// (byte)spParameter.Dec);
            }
            dataParameter.Value = spParameter.Value;
            //dataParameter.Direction = spParameter.;
            return dataParameter;
        }


		#region static ExecuteCommand статический метод
		/// <summary>
		/// Возвращает количество обработанных записей - статический метов
		/// </summary>
		/// <param name="query"></param>
		/// <returns></returns>
		public static int ExecuteCommand(string query)
		{
			using (OheDataClass session = new OheDataClass())
			{
				return session.Execute(query);
			}
		}
		#endregion


		#region static GetTableFromQuery 
		/// <summary>
		/// Статический метод, возвращает запрос по идентификатору
		/// </summary>
		/// <param name="queryId">Идентификатор</param>
		/// <param name="args">Параметры</param>
		/// <returns></returns>
		public static string GetQueryFromQueryId(string queryId, List<string[]> args)
		{
			using (OheDataClass session = new OheDataClass())
			{
				return session.GetQueryByQueryId(queryId, args);
			}
		}

		/// <summary>
		/// Статический метод, возвращает таблицу по идентификатору запроса
		/// </summary>
		/// <param name="queryId">Идентификатор</param>
		/// <param name="args">Параметры</param>
		/// <returns></returns>
		public static OheDataTable GetTableFromQueryId(string queryId, List<string[]> args)
		{
			using (OheDataClass session = new OheDataClass())
			{
				return session.GetTableByQueryId(queryId, args);
			}
		}

		/// <summary>
		/// Выполняет запрос и возвращает данные
		/// Версия 2. Побавил конвертацию sql паарметров. Теперь автоматическо добавляются кавычки при оперелении
		/// параметров.
		/// </summary>
		/// <param name="query"></param>
		/// <returns></returns>
		[Obsolete("Пожалуйста используйте другой метод GetTableByQueryId")]
		public static OheDataTable GetTableFromQuery(string query, List<string[]> args)
		{
		    if (args != null && args.Count > 0)
		    {
		        foreach (var param in args)
		        {
		            var prm = Utils.Utils.Prm2Sql(param[1]);

		            if (query.IndexOf("'##" + param[0] + "##'") < 0 &&
		                (param[1] == string.Empty || param[1] == "null"))
		            {

		                query = query.Replace("##" + param[0] + "##", (param[1] == "null" ? prm : "'" + prm + "'"));
		            }
		            else
		                query = query.Replace("##" + param[0] + "##", (param[1] == "null" ? "" : prm));
		        }
		    }

		    using (OheDataClass session = new OheDataClass())
			{
				return session._GetTable(query,string.Empty);
			}
		}
		#endregion


		#region static Create Check / WhereCheck
		/*
		static public string CreateWhereCheck(string wherePart, string fieldName, string fieldVal)
		{
			return CreateWhereCheck(wherePart, fieldName, "=", fieldVal);
		}

		static public string CreateWhereCheck(string wherePart, string fieldName, string wildcard, string fieldVal)
		{
			string str = CreateCheck(fieldName, wildcard, fieldVal);
			if (wherePart == null)
				wherePart = string.Empty;
			return (str.Length > 0 ? (wherePart.Length > 0 ? wherePart + " and " + str : " where " + str) : wherePart);
		}

		static public string CreateCheck(string fieldName, string wildcard, string fieldVal)
		{
			if (fieldName == null || fieldName.Length == 0)
				return string.Empty;
			return (fieldVal == null || fieldVal.Length == 0) ? string.Empty : " " + fieldName + " " + wildcard + " '" + fieldVal + "'";
		}

		static public string CreateCheck(string fieldName, string fieldVal)
		{
			return CreateCheck(fieldName, "=", fieldVal);
		}
		*/ 
		#endregion

	}
}
