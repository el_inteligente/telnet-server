using System;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace Ohe.Data
{

	public sealed class OheOracleDataProvider : IRbDataProvider
	{
        public bool IsNamedParametersSupported { get { return true; } }

        public IDbConnection CreateConnectionObject() { return new OracleConnection(); }

        public DbDataAdapter CreateDataAdapterObject() { return new OracleDataAdapter(); }

        public IDbDataParameter CreateStoreProcParameter() { return new OracleParameter(); }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType) { return new OracleParameter(name, (OracleType)dbType); }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size)
        {
            OracleParameter rez = new OracleParameter(name, (OracleType)dbType);
            rez.Size = size;
            return rez;
        }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size, byte precision, byte scale)
        {
            OracleParameter rez = new OracleParameter(name, (OracleType)dbType);
            rez.Size = size;
            rez.Precision = precision;
            rez.Scale = scale;
            return rez;
        }
        
		public OheDataProviderType DataProviderType { get { return OheDataProviderType.Oracle; } }

		public Type ConnectionType { get { return typeof(OracleConnection); } }
	}
    
	
    public sealed class OheOdbcDataProvider: IRbDataProvider
	{
        public IDbConnection CreateConnectionObject() { return new OdbcConnection(); }

        public DbDataAdapter CreateDataAdapterObject() { return new OdbcDataAdapter(); }

        public IDbDataParameter CreateStoreProcParameter() { return new OdbcParameter(); }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType)
        {
            return new OdbcParameter(name, (OdbcType)dbType);
        }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size)
        {
            OdbcParameter par = new OdbcParameter(name, (OdbcType)dbType);
            par.Size = size;
            return par;
        }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size, byte precision, byte scale)
        {
            OdbcParameter rez = new OdbcParameter(name, (OdbcType)dbType);
            rez.Size = size;
            rez.Precision = precision;
            rez.Scale = scale;
            return rez;
        }
        
		public OheDataProviderType DataProviderType {get {return OheDataProviderType.Odbc;}}

		public Type ConnectionType {get {return typeof(OdbcConnection);}}

        public bool IsNamedParametersSupported { get { return false; } }

	}
    

	public sealed class OheOleDBDataProvider: IRbDataProvider
	{
        public IDbConnection CreateConnectionObject() { return new OleDbConnection(); }

        public DbDataAdapter CreateDataAdapterObject() { return new OleDbDataAdapter(); }

        public IDbDataParameter CreateStoreProcParameter() { return new OleDbParameter(); }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType) { return new OleDbParameter(name, (OleDbType)dbType); }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size)
        {
            OleDbParameter par = new OleDbParameter(name, (OleDbType)dbType);
            par.Size = size;
            return par;
        }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size, byte precision, byte scale)
        {
            OleDbParameter rez = new OleDbParameter(name, (OleDbType)dbType);
            rez.Size = size;
            rez.Precision = precision;
            rez.Scale = scale;
            return rez;
        }

        public OheDataProviderType DataProviderType {get {return OheDataProviderType.OleDB;}}

		public Type ConnectionType {get {return typeof(OleDbConnection);}}

        public bool IsNamedParametersSupported { get { return false; } }

	}

	
    public sealed class OheSqlDataProvider: IRbDataProvider
	{
        public IDbConnection CreateConnectionObject() { return new SqlConnection(); }

        public DbDataAdapter CreateDataAdapterObject() { return new SqlDataAdapter(); }

        public IDbDataParameter CreateStoreProcParameter() { return new SqlParameter(); }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType) { return new SqlParameter(name, dbType); }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size)
        {
            SqlParameter rez = new SqlParameter(name, dbType);
            rez.Size = size;
            return rez;
        }

        // �������� ��� �������� ���������
        public IDataParameter CreateNewParameter(string name, SqlDbType dbType, int size, byte precision, byte scale)
        {
            SqlParameter rez = new SqlParameter(name, dbType);
            rez.Size = size;
            rez.Precision = precision;
            rez.Scale = scale;
            return rez;
        }

		public OheDataProviderType DataProviderType {get {return OheDataProviderType.MicrosoftSqlServer;}}

		public Type ConnectionType {get {return typeof(SqlConnection);}}

        public bool IsNamedParametersSupported { get { return true; } }

	}
}
