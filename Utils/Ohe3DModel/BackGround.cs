﻿using System;
using System.Collections.Generic;
using System.Text;
using Ohe.Web.Model3D.Controls;

namespace Ohe.Web.Model3D.Controls
{
    /// <summary>
    /// Цвет фона сцены
    /// </summary>
    public class BackGround : Model3dAbstract
    {
        #region Properties
        private string _backcolor;
        /// <summary>
        /// Цвет фона
        /// </summary>
        public string Color { get { return _backcolor; } set { _backcolor = value; } }

        private string _backImg;
        /// <summary>
        /// Картинка пола
        /// </summary>
        public string BackImg { get { return _backImg; } set { _backImg = value; } }

        private string _topImg;
        /// <summary>
        /// Стены пола
        /// </summary>
        public string TopImg { get { return _topImg; } set { _topImg = value; } }
        
        #endregion


        public BackGround() { }
        public BackGround(string Color) { _backcolor = Color; }


        public override string GetVrml()
        {
            return @"Background {
                	" + (_backcolor != null && _backcolor.Length >0 ? "groundColor [" + _backcolor + "]" : "")
                      + (_backImg != null && _backImg.Length > 0 ? "bottomUrl " + _backImg : "")
                      + (_topImg  != null && _topImg.Length > 0 ? "frontUrl \"" + _topImg + "\""
                                             + Environment.NewLine
                                             + "topUrl \"" + _topImg + "\""
                                             + Environment.NewLine
                                             + "leftUrl \"" + _topImg + "\""
                                             + Environment.NewLine
                                             + "rightUrl \"" + _topImg + "\"" : "");
        }


    }

}
