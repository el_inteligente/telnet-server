﻿using System;
using System.Collections.Generic;
using System.Text;
using Ohe.Web.Model3D;

namespace Ohe.Web.Model3D.Controls
{
    public enum EnumViewOrientation
    {
        X,Y,Z
    }
    public class ViewPoint : Model3dAbstract
    {
        
        #region Properties

        private double _rotate;
        /// <summary>
        /// Угол поворота
        /// </summary>
        public double Rotate { get { return _rotate; } set { _rotate = value; } }

        private string _description;
        /// <summary>
        /// Название точки обзора
        /// </summary>
        public string Description { get { return _description; } set { _description = value; } }

        private EnumViewOrientation _orientation;
        /// <summary>
        /// Ориентация вида в пространстве
        /// </summary>
        public EnumViewOrientation Orientation {get {return _orientation;} set {_orientation = value;}}

        #endregion


        #region Constructors

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="X">координата X</param>
        public ViewPoint(double X) : this(X, 0, 0, 0) { }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="X">координата X</param>
        /// <param name="Y">координата Y</param>
        public ViewPoint(double X, double Y) : this(X, Y, 0, 0) { }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="X">координата X</param>
        /// <param name="Y">координата Y</param>
        /// <param name="Z">координата Z</param>
        public ViewPoint(double X, double Y, double Z) : this(X, Y, Z, 0) { }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="X">координата X</param>
        /// <param name="Y">координата Y</param>
        /// <param name="Z">координата Z</param>
        /// <param name="Rotate">угол поворота</param>
        public ViewPoint(double X, double Y, double Z, double Rotate) { base.X = X; base.Y = Y; base.Z = Z; _rotate = Rotate; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="X">координата X</param>
        /// <param name="Y">координата Y</param>
        /// <param name="Z">координата Z</param>
        /// <param name="Rotate">угол поворота</param>
        /// <param name="Description">Название вида</param>
        public ViewPoint(double X, double Y, double Z, double Rotate, string Description) { base.X = X; base.Y = Y; base.Z = Z; _rotate = Rotate; _description = Description; }

        #endregion


        /// <summary>
        /// Генерация 3D представления
        /// </summary>
        /// <returns></returns>
        public override string GetVrml()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("DEF topview Viewpoint {");
	        str.AppendLine("position " + Utils.Utils.DoubleTo3DStr(X) +" " + Utils.Utils.DoubleTo3DStr(Y) + " " +Utils.Utils.DoubleTo3DStr(Z));
	        str.Append ("orientation ");

            switch (_orientation)
            {
                case EnumViewOrientation.X:
                    str.Append ("1 0 0 ");
                    break;
                case EnumViewOrientation.Y:
                    str.Append ("0 1 0 ");
                    break;
                case EnumViewOrientation.Z:
                    str.Append ("0 0 1 ");
                    break;
                default:
                    str.Append("1 0 0");
                    break;

            }

            str.AppendLine(Utils.Utils.DoubleTo3DStr(_rotate));
            str.AppendLine("description \"" + _description +"\" }");

            return str.ToString();
        }


    }
}
