﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ohe.Web.Model3D.Controls
{
    /// <summary>
    /// Описывает поверхность на которой находиться объект
    /// </summary>
    public class Pol : Model3dAbstract
    {

        #region Properties

        private string _color;
        /// <summary>
        /// Цвет пола
        /// </summary>
        public string Color { get { return _color; } set { _color = value; } }


        private string _pol = string.Empty;
        /// <summary>
        /// Исходный код пола
        /// </summary>
        public string Source { get { return _pol; } set { _pol = value; } }
        
        #endregion


        #region Constructors
        /// <summary>
        /// Конструктор
        /// </summary>
        public Pol() { }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="Length">Длина</param>
        /// <param name="Width">Ширина</param>
        /// <param name="Source">Содержимое слоя пол</param>
        public Pol(string Source) { _pol = Source; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="Length">Длина</param>
        /// <param name="Width">Ширина</param>
        public Pol(double Length, double Width) { base.Length = Length; base.Width = Width; }
        
        #endregion


        /// <summary>
        /// Генерация подложки (пола)
        /// </summary>
        /// <returns></returns>
        public override string GetVrml()
        {
            if (_pol != string.Empty)
                return _pol;
            else
            return @"Group{
children[
Pol {
pos " + Utils.Utils.DoubleTo3DStr(Width/2 - 0.5) + @" -0.01 " + Utils.Utils.DoubleTo3DStr(Length/2 - 0.5) + 
@" tran 0
size " + Utils.Utils.DoubleTo3DStr(Width+1) + " 0 " + Utils.Utils.DoubleTo3DStr(Length+1)
+Environment.NewLine
+(_color != null && _color.Length>0 ? "col " +_color : "" )
+@"}
]
}";
        }
    }
}
