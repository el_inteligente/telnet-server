﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ohe.Web.Model3D.Constant
{
    /// <summary>
    /// Статический класс с константами 
    /// Служит для определения основных параметров,
    /// которые переопределяются в 3D сцене
    /// </summary>
    static class Constants
    {
        public const string Header = "#VRML V2.0 utf8"; // Заголовок файла

        #region Pol - подложка
        /// <summary>
        /// Пол объекта
        /// </summary>
        public const string Pol = @"PROTO Pol [
field SFVec3f pos 0 0 0
field SFFloat tran 0 
field SFVec3f size   0 0 0
field SFColor col 0.8 0.8 0.8
]
{   Transform {
translation IS pos
children [               
Shape {
appearance Appearance {
material Material {
diffuseColor IS col
transparency  IS tran
}
}
geometry Box { size IS size } } ] }
} ";
        
        #endregion

        #region StandartBox - класс ячейка
        /// <summary>
        /// Ячейка склада
        /// </summary>
        public const string StandartBox = @"PROTO PBox [
field SFVec3f pos 0 0 0
field SFFloat tran 0.25 
field SFVec3f size   2 2 2
field SFColor clr   1 0.5 0.5
"
+ "field SFString language \"204\" "
+ @"]
{
Transform {
translation IS pos
children [   
Shape {
appearance Appearance {
material Material {
diffuseColor IS clr
transparency  IS tran } }
geometry Box { size IS size }
}
]
}
} 
";

        /// <summary>
        /// Старое значение константы
        /// </summary>
        public const string _StandartBox = @"PROTO Positioned_Box [
field SFVec3f position 0 0 0
field SFVec3f pos_text1 0 0 0
field SFVec3f pos_text2 0 0 0
field MFString reference []
field MFString descript []
field SFRotation turn 0 1 0 0
field SFFloat transparency 0.25 
field SFVec3f size   2 2 2
field SFColor box_color   1 0.5 0.5
field SFFloat size_text 0.25 
"
+ "field SFString language \"204\" "
+ @"]
{
Transform {
translation IS position
children [   
Shape {
appearance Appearance {
material Material {
diffuseColor IS box_color
transparency  IS transparency
}
}
geometry Box { size IS size
}
}
Transform {
translation IS pos_text1
children [ 
Shape {
appearance Appearance {
material Material {
diffuseColor 1 1 1
}
}
geometry DEF TEXT1 Text  {   string IS descript
fontStyle FontStyle{"
+ "	style \"BOLDITALIC\""
+ "	justify [\"MIDDLE\",\"MIDDLE\"]"
+ "	size 0"
+ " language \"204\""
+ "	family [ \"Verdana\", \"Arial\", \"Helvetica\" ]"
+ @"}
}
} 
Shape {
appearance Appearance {
material Material {
diffuseColor 1 1 1
}
}
geometry DEF TEXT Text  {   string IS reference
fontStyle FontStyle{"
+ "	style \"BOLDITALIC\""
+ "	justify [\"MIDDLE\",\"MIDDLE\"]"
+ "	size IS size_text"
+ " language \"204\""
+ "	family [ \"Verdana\", \"Arial\", \"Helvetica\" ]"
+ @"}
}
}     
]
}
Transform {
translation IS pos_text2
rotation IS turn
children [ 
Shape {
appearance Appearance {
material Material {
diffuseColor 1 1 1
}
}
geometry DEF TEXT Text  {   string IS reference
fontStyle FontStyle{"
+ " style \"BOLDITALIC\""
+ " justify [\"MIDDLE\",\"MIDDLE\"]"
+ " size IS size_text"
+ " language \"204\""
+ " family [ \"Verdana\", \"Arial\", \"Helvetica\" ]"
+ @"}
}
}     
]
}
DEF SCRIPT Script {
   eventIn  SFTime  run
   field SFNode node USE TEXT
   field SFNode node1 USE TEXT1"
+ "   url \"javascript:"
+ @"      function run () {
         print(node.string+' : '+node1.string);
      }"
+ "\""
+ @"} 
DEF TS TouchSensor{ }
]
}
ROUTE TS.touchTime TO SCRIPT.run
} ";

        
        #endregion

        #region RowText - описание текста ряда
        /// <summary>
        /// Текстовое описание ячейки
        /// </summary>
        public const string RowText = @"PROTO RText [
field SFVec3f pos 0 0 0
field MFString ref []
field SFRotation turn 0 1 0 0
field SFColor clr 1 0 0
field SFFloat size 0.25 
]
{   
Transform {
translation IS pos
rotation IS turn 
children [   
Shape {
appearance Appearance {
material Material {
diffuseColor IS clr
}
}
geometry Text {   string IS ref
fontStyle FontStyle{
style " +"\"BOLDITALIC\""
        +"justify [\"BEGIN\",\"END\"]"
        +"size IS size"
+@"}
}
}  
]
}
} 
";

        /// <summary>
        /// Старое определение текста
        /// </summary>
        public const string _TextBox = @"PROTO TBox [
field SFVec3f pos_text1 0 0 0
field SFVec3f pos_text2 0 0 0
field MFString reference []
field SFRotation turn 0 1 0 0
field SFColor color_text 1 0 0
field SFFloat size_text 0.25 
]
{   
Transform {
translation IS pos_text1
children [   
Shape {
appearance Appearance {
material Material {
diffuseColor IS color_text
}
}
geometry Text {   string IS reference
fontStyle FontStyle{"
+ "style \"BOLDITALIC\""
+ "	justify [\"BEGIN\",\"END\"]"
+ "	size IS size_text"
+ "	language \"204\""
+ "	family [ \"Verdana\", \"Arial\", \"Helvetica\" ]"
+ @"}
}
}  
Transform {
#translation IS pos_text2
rotation IS turn
children [   
Shape {
appearance Appearance {
material Material {
diffuseColor IS color_text
}
}
geometry Text {   string IS reference
fontStyle FontStyle{"
+ "	style \"BOLDITALIC\""
+ "	justify [\"BEGIN\",\"END\"]"
+ "	size IS size_text"
+ "	language \"204\""
+ "	family [ \"Verdana\", \"Arial\", \"Helvetica\" ]"
+ @"}
}
}     
]
}   
]
}
} 
";

        #endregion

        #region LText - текст ячейки
		public const string BinText = @"PROTO LText [
field SFVec3f pos 0 0 0
field MFString ref []
field SFRotation turn 0 1 0 0
field SFColor clr 1 1 1
field SFFloat size 0.25 
]
{   
Transform {
translation IS pos
rotation IS turn 
children [   
Shape {
appearance Appearance {
material Material {
diffuseColor IS clr
}
}
geometry Text {   string IS ref
fontStyle FontStyle{
style "
+ "\"BOLDITALIC\""
+ "justify [\"MIDDLE\",\"MIDDLE\"]"
+ @"size IS size
}
}
}  
]
}
} 
";
 
	#endregion 
    }
}
