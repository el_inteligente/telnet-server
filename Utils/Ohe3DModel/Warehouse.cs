﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Ohe.Web.Model3D.Constant;
using Ohe.Data;
using Ohe.Utils;


namespace Ohe.Web.Model3D.Controls
{
    /// <summary>
    /// Основной класс описывает 3D модель 
    /// </summary>
    public class Warehouse : Model3dAbstract
    {

        #region Properties

        private Hashtable _adress = new Hashtable(new OheCultureComparer());
        /// <summary>
        /// Возвращает список всех ячеек склада
        /// </summary>
        public Hashtable AdressList { get { return _adress; } }

        /// <summary>
        /// Инициализированная таблица, содержит данные инициализации
        /// </summary>
        private Hashtable _casheAddress = new Hashtable(new OheCultureComparer());


        private Pol _pol; // подложка для 3D модели
        /// <summary>
        /// подложка для 3D модели
        /// </summary>
        public Pol Pol { get { return _pol; } set { _pol = value; } }

        private BackGround _backObj;
        /// <summary>
        /// Объект подложка
        /// </summary>
        public BackGround BackObject { get { return _backObj; } set { _backObj = value; } }


        private string _viewpoints = string.Empty;
        /// <summary>
        /// Возвращает виды объектов
        /// </summary>
        /// <returns></returns>
        public string GetViewPoint
        {
            get
            {

                if (_viewpoints == string.Empty)
                {

                    ViewPoint _v1 = new ViewPoint(0, Width / 2, 0, -1.5707963267948966192313216916398, "Top View 2D");
                    _v1.Orientation = EnumViewOrientation.X;

                    ViewPoint _v2 = new ViewPoint(Width / 2.0, Width, - Length / 2, -0.75, "Top View 3D");
                    _v2.Orientation = EnumViewOrientation.X;


                    ViewPoint _v3 = new ViewPoint(-3, 1, -1, -1.5707963267948966192313216916398, "Top View Work");
                    _v3.Orientation = EnumViewOrientation.Y;

                    return _v1.GetVrml() + _v2.GetVrml() + _v3.GetVrml();
                }

                else return _viewpoints;
            }
            set { _viewpoints = value; }
        }

        
        #endregion

        /// <summary>
        /// Конструктор
        /// <param name="dataSource">Таблица с 3В моделью (по умолчанию)</param>
        /// </summary>
        public Warehouse() 
        {
            _backObj = new BackGround();
        }


        #region Private Methods

        /// <summary>
        /// Добавляет товар к объекту
        /// </summary>
        /// <param name="bin"></param>
        private void AddAdress(Binlabel bin)
        {
            if (_adress.ContainsKey(bin.ObjectId))
                _adress.Remove(bin.ObjectId);

            _adress.Add(bin.ObjectId, bin);
        }


        /// <summary>
        /// Заполняет данными объект. 
        /// Используется для добавления результатов запроса
        /// Событие submit
        /// </summary>
        /// <returns></returns>
        private void FillDataObject(OheDataTable dt)
        {
            //StringBuilder _vrml = new StringBuilder();

            // Проходим по всем рядам.
            for (int i = 0; i < dt.RowsCount; i++)
            {
                string _bin = dt.GetString(i, "binlabel");
                string _descript = dt.GetString(i, "descript");
                double _length = dt.GetDouble(i, "length");
                double _width = dt.GetDouble(i, "width");
                double _height = dt.GetDouble(i, "height");
                double _x = dt.GetDouble(i, "x");
                double _y = dt.GetDouble(i, "y");
                double _z = dt.GetDouble(i, "z");
                string _color = dt.GetString(i, "color");
                string _first = dt.GetString(i, "isfirst");
                int _whse = dt.GetInt(i, "whse_id");
                int _owner = dt.GetInt(i, "owner_id");
                double _transporent = dt.GetDouble(i, "transporent");
                string _coord_isfirst = dt.GetString(i, "coord_isfirst");

                AddAdress(new Binlabel(_bin, _x, _y, _z, _width, _height, _length, _first, _whse, _owner, _transporent, _descript, _color,_coord_isfirst));
            }

        }

        #endregion

        /// <summary>
        /// Заполняет данными объект. При событии инициализации
        /// Событие Init
        /// </summary>
        /// <returns></returns>
        public void BindDataObject(OheDataTable _dataSource)
        {
            _adress = new Hashtable(new OheCultureComparer());
            double _aded = 0; double _aded2 = 0;

            FillDataObject(_dataSource);

            // Вычисляем значение для подложки если это необходимо
            if (_pol == null || _pol.Source == string.Empty)
            {
                foreach (int keys in _adress.Keys)
                {
                    Binlabel bin = (Binlabel)_adress[keys];
                    // Вычисляем максимальные значения
                    // Ширины и длины
                    if (Length < bin.X)
                        Length = bin.X;

                    if (Width < bin.Z)
                        Width = bin.Z;

                    _aded = bin.Width;   // Ширина ячейки
                    _aded2 = bin.Length; // Длина ячейки
                }

                Length += _aded2 * 2; Width += _aded * 2;

                _pol = new Pol(Length, Width);
            }
        }

        /// <summary>
        /// Заполняет данными объект. 
        /// Используется для добавления результатов запроса
        /// Событие submit
        /// </summary>
        /// <returns></returns>
        public void BindDataObject(OheDataTable dt, Hashtable InitDataSource)
        {
            _adress = new Hashtable(new OheCultureComparer());

            foreach (int obj in InitDataSource.Keys)
                _adress.Add(((Binlabel)InitDataSource[obj]).ObjectId, InitDataSource[obj]);

           // Проходим по всем рядам.
            for (int i = 0; i < dt.RowsCount; i++)
            {
                string _bin  = dt.GetString(i, "binlabel");
                string _descript = dt.GetString(i, "descript");
                double _length = dt.GetDouble(i, "length");
                double _width = dt.GetDouble(i, "width");
                double _height = dt.GetDouble(i, "height");
                double _x = dt.GetDouble(i, "x");
                double _y = dt.GetDouble(i, "y");
                double _z = dt.GetDouble(i, "z");
                string _color = dt.GetString(i, "color");
                bool _first = Utils.Utils.Obj2Bool(dt.GetString(i, "isfirst"));
                int _whse = dt.GetInt(i, "whse_id");
                int _owner = dt.GetInt(i, "owner_id");
                double _transporent = dt.GetDouble(i, "transporent");

                int _id = (_bin + _owner.ToString() + _whse.ToString()).GetHashCode();
                if (_adress.ContainsKey(_id))
                {
                    //Binlabel Binlbl = (Binlabel)_adress[_id];
                    ((Binlabel)_adress[_id]).Description = _descript;
                    ((Binlabel)_adress[_id]).Color = _color;
                    ((Binlabel)_adress[_id]).Transporent = _transporent;
                }
            }
        }

        /// <summary>
        /// Генерация адресного пространства склада
        /// </summary>
        public override string GetVrml()
        {
            StringBuilder _rez = new StringBuilder();

            #region Header
            _rez.AppendLine(Constants.Header);
            _rez.AppendLine(Constants.Pol);
            _rez.AppendLine(Constants.StandartBox);
            _rez.AppendLine(Constants.RowText);
            _rez.AppendLine(Pol.GetVrml());
            _rez.AppendLine(GetViewPoint);
            _rez.AppendLine(Constants.BinText);

            _rez.AppendLine("	Group{  children[");

            #endregion

            // Генерируем содержимое склада
            foreach (int _bin in _adress.Keys)
            {
                _rez.AppendLine(((Binlabel)_adress[_bin]).GetVrml());
            }

            #region Footer
            _rez.AppendLine(@"DirectionalLight {     
     direction  1 -0.8 1     
     intensity   0.9
     color 1 1 1
     on TRUE
}
]
}");

            #endregion

            return _rez.ToString();
        }
        
    }

}
