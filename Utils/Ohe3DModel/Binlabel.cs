﻿using System;
using System.Collections.Generic;
using System.Text;
//using Ohe.Utils;

namespace Ohe.Web.Model3D.Controls
{
    /// <summary>
    /// Описывает параметры ячейки склада, контейнерного терминала
    /// </summary>
    public class Binlabel : Model3dAbstract
    {
        #region Const
        /// <summary>
        /// Описывает константу, которая заполняется массивом
        /// Значения массива
        /// {0} = X
        /// {1} = Y
        /// {2} = Z
        /// {3} = Color
        /// {4} = Transporent
        /// {5} = Width
        /// {6} = Heigth
        /// {7} = Length
        /// </summary>
        private const string Params = @"pos {0} {1} {2} clr {3} tran {4} size {5} {6} {7} ";

        private const string NodeName = "PBox ";

        #endregion


        #region Properties
        private string _binlabel;
        public string LocationId { get { return _binlabel; } set { _binlabel = value; } }
        
        private string _color;
        /// <summary>
        /// Цвет ячейки
        /// </summary>
        public string Color { get { return _color; } set { _color = value; } }

        private double _height;
        public double Height { get { return _height; } set { _height = value; } }

        private string _izFirst;
        public string FirstRow { get { return _izFirst; } set { _izFirst = value; } }

        private string _coord_isfirst;
        public string CoordinateFirst { get { return _coord_isfirst; } set { _coord_isfirst = value; } }

        private int _whseId = 0;
        public int WhseId { get { return _whseId; } set { _whseId = value; } }

        private int _ownerId = 0;
        public int OwnerId { get { return _ownerId; } set { _ownerId = value; } }

        private double _transporent = 0;
        public double Transporent { get { return _transporent; } set { _transporent = value; } }

        private string _description;
        public string Description { get { return _description; } set { _description = value; } }

        private int _object;
        public int ObjectId { get { return _object; } }
        
        #endregion


        #region Constructors
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="Binlabel">Ячейка</param>
        /// <param name="X">Координата Х</param>
        /// <param name="Y">Координата Y</param>
        /// <param name="Z">Координата Z</param>
        /// <param name="Width">Ширина</param>
        /// <param name="Height">Высота</param>
        /// <param name="Length">Длина</param>
        /// <param name="IzFirst">Название ряда. Используется только у первой ячейки ряда</param>
        /// <param name="OwnerId">Владелец</param>
        /// <param name="WhseId">Склад</param>
        /// <param name="Transporent">Прозрачность</param>
        /// <param name="Description">Содержимое ячейки</param>
        /// <param name="Color">Цвет ячейки</param>
        /// <param name="CoordIsFirst">Координаты первой ячейки ряда</param>
        public Binlabel(string Binlabel, double X, double Y, double Z, double Width, double Height, double Length
                , string IzFirst, int WhseId, int OwnerId, double Transporent, string Description
                , string Color, string CoordIsFirst)
        {
            base.X = X; base.Y = Y; base.Z = Z; base.Width = Width; _height = Height;
            base.Length = Length; _izFirst = IzFirst; _whseId = WhseId;
            _ownerId = OwnerId; _transporent = Transporent;
            _description = Description;
            _binlabel = Binlabel;
            _color = Color;
            _coord_isfirst = CoordIsFirst;

            GenHashCode();
        }
       
        #endregion


        #region Methods

        #region GenHashCode
        /// <summary>
        /// Генерирует уникальный идентификатор объекта
        /// </summary>
        private void GenHashCode()
        {
            _object = (_binlabel + _ownerId.ToString() + _whseId.ToString()).GetHashCode();
        }
        
        #endregion


        #region GetBinlabel
        /// <summary>
        /// Возвращает текстовое представление ячейки
        /// </summary>
        /// <param name="ObjectType">Тип объекта</param>
        /// <returns>string</returns>
        public string GetBinlabel(string ObjectType)
        {
            StringBuilder str = new StringBuilder();
            str.Append(ObjectType + " {");

            //if (!IsDefaultColor)
            str.AppendLine(" clr " + _color);

            //if (!IsDefaultSize)
            str.AppendLine(" size " + Length.ToString().Replace(",", ".") + " "
                                    + Width.ToString().Replace(",", ".") + " "
                                    + _height.ToString().Replace(",", ".") + " ");

            str.AppendLine(" pos " + X.ToString().Replace(",", ".") + " "
                                    + Y.ToString().Replace(",", ".") + " "
                                    + Z.ToString().Replace(",", ".") + " ");

            str.AppendLine("}");


            return str.ToString();
        }
        #endregion


        #region GetIzFirst
        /// <summary>
        /// Возвращает значение текста для первого ряда
        /// </summary>
        /// <returns></returns>
        private string GetIzFirst()
        {
            if (_izFirst != string.Empty)
            {
                return Environment.NewLine + "RText {clr 1 0 0 "
                                + "ref[\"" + _izFirst + "\"] "
                                + " size " + Utils.Utils.DoubleTo3DStr(_height)
                                + " pos " + _coord_isfirst
                                + " turn 1 0 0 -0.8 } " + Environment.NewLine
                    // Добавляем текст с описанием номера ячейки
                                + " LText { pos " + Utils.Utils.DoubleTo3DStr(X) + " "
                                + Utils.Utils.DoubleTo3DStr(Y) + " "
                                + Utils.Utils.DoubleTo3DStr(Z + Width / 2 + 0.01) + " "
                                + "size " + Utils.Utils.DoubleTo3DStr(Width / 5) + " "
                                + "ref [\"" + _binlabel + "\"] turn 1 0 0 0}";

            }
            else return string.Empty;
        }
        #endregion

        #endregion


        #region Overrides Methods

        #region GetVrmt
        /// <summary>
        /// Возвращает VRML представление объекта
        /// </summary>
        /// <returns></returns>
        public override string GetVrml()
        {
            /// {0} = X
            /// {1} = Y
            /// {2} = Z
            /// {3} = Color
            /// {4} = Transporent
            /// {5} = Width
            /// {6} = Heigth
            /// {7} = Length
            return NodeName + " { " + string.Format
                (Params
                        , Utils.Utils.DoubleTo3DStr(X)
                        , Utils.Utils.DoubleTo3DStr(Y)
                        , Utils.Utils.DoubleTo3DStr(Z)
                        , _color
                        , Utils.Utils.DoubleTo3DStr(_transporent)
                        , Utils.Utils.DoubleTo3DStr(Width)
                        , Utils.Utils.DoubleTo3DStr(_height)
                        , Utils.Utils.DoubleTo3DStr(Length)
                        ) + " }"
                        + GetIzFirst()
                        ;

            /// Старое определение
            /// {0} = X
            /// {1} = Y
            /// {2} = Z
            /// {3} = Color
            /// {4} = Transporent
            /// {5} = Width
            /// {6} = Heigth
            /// {7} = Length
            /// {8} = + Length/2 + 0.1
            /// {9} = - Length/2 + 0.1
            /// {10} = Binlabel
            /// {11} = Descript
            /// {12} = Размер надписи ячейки
            /*
            return NodeName + " { " + string.Format
                (Params
                        , Utils.Utils.DoubleTo3DStr(_x)
                        , Utils.Utils.DoubleTo3DStr(_y)
                        , Utils.Utils.DoubleTo3DStr(_z)
                        , _color
                        , Utils.Utils.DoubleTo3DStr(_transporent)
                        , Utils.Utils.DoubleTo3DStr(_width)
                        , Utils.Utils.DoubleTo3DStr(_height)
                        , Utils.Utils.DoubleTo3DStr(_length)
                        , Utils.Utils.DoubleTo3DStr(_length / 2.0 + 0.01)
                        , Utils.Utils.DoubleTo3DStr(-(_length / 2.0 + 0.01))
                        , _binlabel
                        , _description
                        , Utils.Utils.DoubleTo3DStr(_width / 4.2)
                ) + " }" + GetIzFirst();
            */
        }
        #endregion

        #endregion

    }
}
