﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ohe.Web.Model3D.Controls
{
    public abstract class Model3dAbstract
    {
        private double _length; // длина пола
        private double _width;  // Ширина пола

        /// <summary>
        /// Длина пола
        /// </summary>
        public double Length { get { return _length; } set { _length = value; } }

        /// <summary>
        /// Ширина
        /// </summary>
        public double Width { get { return _width; } set { _width = value; } }

        private double _x;
        public double X { get { return _x; } set { _x = value; } }

        private double _y;
        public double Y { get { return _y; } set { _y = value; } }

        private double _z;
        public double Z { get { return _z; } set { _z = value; } }


        public virtual string GetVrml()
        {
            return string.Empty;
        }
    }
}
