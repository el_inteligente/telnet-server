﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Xml;
using Ohe.Interfaces;
using Ohe.Configuration;

namespace Ohe.Utils
{
    #region Class OheLicenseRecord
    /// <summary>
    /// Описывает класс лицензии
    /// </summary>
    [Serializable]
    
    public sealed class OheLicenseRecord
    {
        public string Company = string.Empty;
        public string Modules = string.Empty;
        public string Version = "0.0.0000";
        public string Key = string.Empty;
        public int WebUsers = 0;
        public int TelnetUsers = 0;
        public string USER = string.Empty;
        public string PWD = string.Empty;
        public DateTime Date = DateTime.Now;
        public DateTime Expiry = DateTime.MinValue;
        public string Signature = string.Empty;
    }
    #endregion


    /// <summary>
    /// Основной класс лицензий, на клиенте
    /// </summary>
    public class OheLicenseObjectManagerClient : OhePlatformConnection
    {
        #region LObjectManagerConnection
        static private IOheLicense _lObjectConnectionManager = null;
        static private string _lObjectConnectionManagerLock = Guid.NewGuid().ToString();

        // Соединяется с сервером лицензий
        /// <summary>
        /// Возвращает интверфейс, который отдается Ядром системы Open HandHeld Engine
        /// </summary>
        public static IOheLicense LObjectManagerConnection
        {
            get
            {
                lock (_lObjectConnectionManagerLock)
                {
                    if (_lObjectConnectionManager == null)
                        ReloadKernelHostPort();

                    try
                    {
                        if (_lObjectConnectionManager != null)
                            RemotingServices.Disconnect((MarshalByRefObject)_lObjectConnectionManager);

                        _lObjectConnectionManager = (IOheLicense)Activator.GetObject(typeof(IOheLicense)
                            , "tcp://" + OhePlatformHost + ":" + OhePlatformServicePort.ToString() + "/OhePlatformService/OheLicenseManagerClass");
                    }
                    catch
                    {
                        _lObjectConnectionManager = null;
                         ReloadKernelHostPort();
                        _lObjectConnectionManager = (IOheLicense)Activator.GetObject(typeof(IOheLicense)
                            , "tcp://" + OhePlatformHost + ":" + OhePlatformServicePort.ToString() + "/OhePlatformService/OheLicenseManagerClass");
                    }

                    return _lObjectConnectionManager;
                }
            }
        }

        #endregion

        #region GetLicenseInfo - объект с информацией о лицензии OheLicenseRecord
        /// <summary>
        /// Возвращает всю информацию о лицензии пользователя
        /// </summary>
        /// <returns></returns>
        public static OheLicenseRecord GetLicenseInfo()
        {
            return LObjectManagerConnection.LicenseInfo();
        }
        #endregion


        #region UpdateLicense - обновляет лицензию
        /// <summary>
        /// Обновляет пользовательскую лицензию (Требует подтверждения)
        /// </summary>
        /// <param name="newLicense">Параметры лицензии</param>
        /// <returns></returns>
        public static bool UpdateLicense(string newLicense)
        {
            return LObjectManagerConnection.UpdateLicense(newLicense);
        }
        #endregion


        #region Добавление/Удаление активных пользователей из лицензии

        public static void AddTelnetUser(string[] User)
        {
            LObjectManagerConnection.AddTelnetUser(User);
        }

        public static void AddWebUser(string[] User)
        {
            LObjectManagerConnection.AddWebUser(User);
        }

        public static void RemoveTelnetUser(string User)
        {
            LObjectManagerConnection.RemoveTelnetUser(User);
        }

        public static void RemoveWebUser(string User)
        {
            LObjectManagerConnection.RemoveWebUser(User);
        }
        
        #endregion
        

        #region GetAvailableModules - возвращает список модулей доступных для использования
        public static string GetAvailableModules()
        {
            return LObjectManagerConnection.GetModules();
        }
        #endregion


        #region isModuleEnabled - доступен ли модуль для использования
        public static bool ModuleIsAvailable(string Module)
        {
            return LObjectManagerConnection.ModuleIsAvailable(Module);
        }
        /*
        public static bool isModuleEnabled(string moduleName)
        {
            RbLog.Write2LicenseLog("ObjectChecker.isModulEnabled:: trying to check if module '" + moduleName + "' is available.");

            moduleName = Utils.AllTrim(moduleName);

            if (moduleName.Length == 0)
                return true;

            // works on 'logic or' || method
            for (int i = 0; i < moduleName.Length; i++)
            {
                if (LObjectManagerConnection.isModule(moduleName.Substring(i, 1)))
                    return true;
            }

            // If we got here then none of the requested modules have been enabled.
            return false;
        }
        */
        #endregion


        #region GetMaxWebUsers - масимальное количество web пользователей
        public static int GetMaxWebUsers()
        {
            return LObjectManagerConnection.GetMaxWebUsers;
        }
        #endregion

        public static Hashtable GetLoginWebUsers()
        {
            return LObjectManagerConnection.GetLoginWebUsers;
        }

        public static Hashtable GetLoginTelnetUsers()
        {
            return LObjectManagerConnection.GetLoginTelnetUsers;
        }



        #region GetMaxTelnetUsers - максимальное количество Telnet пользователей
        public static int GetMaxTelnetUsers()
        {
            return LObjectManagerConnection.GetMaxTelnetUsers;
        }
        #endregion


        #region getLicenseMsg - информация о лицензии (comment)
        /*
        public static string getLicenseMsg()
        {
            return LObjectManagerConnection.GetText();
        }
        */
        #endregion


        #region GetWarningMessage - предупреждение о лицензии (comment)
        /*
        public static string GetWarningMessage()
        {
            return LObjectManagerConnection.GetWarningMessage();
        }
        */
        #endregion


        #region GetLicensedFormIDs (comment)
        /*
        public static string GetLicensedFormIDs()
        {
            return LObjectManagerConnection.GetLicensedFormIDs();
        }
        */
        #endregion


        #region isValid - bool Верна ли лицензия (comment)
        /*
        public static bool isValid()
        {
            return LObjectManagerConnection.isValid();
        }
        */
        #endregion


        #region GetTmpModules - список временных модулей (comment)
        /*
        public static string GetTmpModules()
        {
            return LObjectManagerConnection.GetTmpModuleList();
        }
        */
        #endregion


        #region GetModulesList - список всех модулей (comment)
        /*
        public static string GetModuleList()
        {
            return LObjectManagerConnection.GetModuleList();
        }
        */
        #endregion


        #region GetExpiredModules - список просроченных модулей (comment)
        /*
        public static string GetExpiredModules()
        {
            return LObjectManagerConnection.GetExpiredModuleList();
        }
        */
        #endregion


        #region isValid(string version) - bool верна ли лицензия (comment)
        /*
        public static bool isValid(string version)
        {
            return LObjectManagerConnection.isValid(version);
        }
        */
        #endregion


        #region IsSignatureValid - проверяет верна ли подпись лицензии (comment)
        /*
        public static bool IsSignatureValid()
        {
            return LObjectManagerConnection.isSignatureValid();
        }
        */
        #endregion


    }

}

