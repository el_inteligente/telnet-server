﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace Ohe.Utils
{
    #region VsEncryption
    
    public sealed class VsEncryption
    {
        #region private Constructor
        private VsEncryption()
        {
        }
        #endregion

        private static int seed = -1;
        private static int p = 335544319;   // начальное число
        private static int x = 2147483647;  // делитель the distribution factor (2^31-1)
        private static int y = 2147483647 / 335544319;
        private static int z = 2147483647 % 335544319;

        #region EncodeString / DecodeString - кодирование и декодирование строки
        /// <summary>
        /// Кодирует строку
        /// </summary>
        /// <param name="str2encode"></param>
        /// <returns></returns>
        public static string EncodeString(string str2encode)
        {
            Encoding enc = Encoding.Unicode;

            byte[] bts = enc.GetBytes(str2encode);

            for (int i = 0; i < bts.Length; i++)
                bts[i] += 1;

            char[] chs = new char[enc.GetCharCount(bts, 0, bts.Length)];
            enc.GetChars(bts, 0, bts.Length, chs, 0);
            return new string(chs);
        }

        /// <summary>
        /// Декодирует строку
        /// </summary>
        /// <param name="str2encode"></param>
        /// <returns></returns>
        public static string DecodeString(string str2encode)
        {
            Encoding enc = Encoding.Unicode;

            byte[] bts = enc.GetBytes(str2encode);

            for (int i = 0; i < bts.Length; i++)
                bts[i] -= 1;

            char[] chs = new char[enc.GetCharCount(bts, 0, bts.Length)];
            enc.GetChars(bts, 0, bts.Length, chs, 0);
            return new string(chs);
        }
        #endregion

        #region Scramble - шифрование
        /// <summary>
        /// Шифрует строку
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Scramble(string str)
        {
            if (str.Length == 0)
                return string.Empty;

            Encoding enc = Encoding.UTF8;

            byte[] bts = enc.GetBytes("ab" + str);

            for (int i = 1; i < bts.Length; i++)
                bts[i] ^= bts[i - 1];

            return System.Convert.ToBase64String(bts);
        }
        #endregion

        #region Descramble - обратное шифрование
        /// <summary>
        /// Расшивровывает строку
        /// </summary>
        /// <param name="str">Строка</param>
        /// <returns>Расшифрованная строка</returns>
        public static string Descramble(string str)
        {
            if (str.Length == 0)
                return string.Empty;

            byte[] bts = null;
            Encoding enc = Encoding.UTF8;
            try
            {
                bts = Convert.FromBase64String(str);
            }
            catch (Exception e)
            {
                throw new Exception("Can not convert string [" + str + "]" + Environment.NewLine + e.Message, e);
            }

            for (int i = 1; i < bts.Length; i++)
                bts[i - 1] ^= bts[i];

            char[] chs = new char[enc.GetCharCount(bts, 0, bts.Length - 1)];
            enc.GetChars(bts, 0, bts.Length - 1, chs, 0);
            return new string(chs);
        }
        #endregion

        #region Random - случайное число
        /// <summary>
        /// Случайное число
        /// </summary>
        /// <returns></returns>
        public static int Random()
        {
            if (seed == -1) seed = DateTime.Now.Millisecond;
            seed = Math.Abs(p * (seed % y) - z * (seed / y)) % x;
            return seed % 127;
        }
        #endregion
    }
    #endregion

    #region VsSymEncryption
    
    public sealed class VsSymEncryption
    {
        private const int KeyLength = 16;

        #region private Constructor
        private VsSymEncryption()
        {
        }
        #endregion


        #region CreateNewKey
        public static string CreateNewKey()
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            sa.GenerateKey();
            return Convert.ToBase64String(sa.Key);
        }
        #endregion

        #region CreateNewIV
        public static string CreateNewIV()
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            sa.GenerateIV();
            return Convert.ToBase64String(sa.IV);
        }
        #endregion

        #region Encrypt
        public static string Encrypt(string data, string password, string IV)
        {
            return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(data), password, IV));
        }

        public static byte[] Encrypt(byte[] data, string password, string IV)
        {
            SymmetricAlgorithm sa = Rijndael.Create();

            byte[] IVbytes = null;

            if (IV != null && IV.Length > 0)
                IVbytes = Convert.FromBase64String(IV);

            ICryptoTransform ct = sa.CreateEncryptor(
                (new PasswordDeriveBytes(password, IVbytes)).GetBytes(KeyLength),
                new byte[KeyLength]);

            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);

            cs.Write(data, 0, data.Length);
            cs.FlushFinalBlock();

            return ms.ToArray();
        }
        #endregion

        #region Decrypt
        static public byte[] Decrypt(byte[] data, string password, string IV)
        {
            BinaryReader br = new BinaryReader(InternalDecrypt(data, password, IV));
            return br.ReadBytes((int)br.BaseStream.Length);
        }

        static public string Decrypt(string data, string password, string IV)
        {
            CryptoStream cs = InternalDecrypt(Convert.FromBase64String(data), password, IV);
            StreamReader sr = new StreamReader(cs);
            return sr.ReadToEnd();
        }

        static CryptoStream InternalDecrypt(byte[] data, string password, string IV)
        {
            SymmetricAlgorithm sa = Rijndael.Create();

            byte[] IVbytes = null;

            if (IV != null && IV.Length > 0)
                IVbytes = Convert.FromBase64String(IV);

            ICryptoTransform ct = sa.CreateDecryptor(
                (new PasswordDeriveBytes(password, IVbytes)).GetBytes(KeyLength),
                new byte[KeyLength]);

            MemoryStream ms = new MemoryStream(data);

            return new CryptoStream(ms, ct, CryptoStreamMode.Read);
        }
        #endregion
    }
    #endregion

    #region VsAsmEncryption
    
    public sealed class VsAsmEncryption
    {
        private const int KeyBitSize = 384;

        [Serializable]
        public class RbRSAXmlKeyPair
        {
            public string publicXmlKey = string.Empty;
            public string privateXmlKey = string.Empty;
        }

        #region CreateNewKeyPair
        public static RbRSAXmlKeyPair CreateNewXmlKeyPair()
        {
            RSACryptoServiceProvider provider = new RSACryptoServiceProvider(KeyBitSize);
            RbRSAXmlKeyPair keyPair = new RbRSAXmlKeyPair();
            keyPair.publicXmlKey = provider.ToXmlString(false);
            keyPair.privateXmlKey = provider.ToXmlString(true);
            return keyPair;
        }
        #endregion

        #region Encrypt
        public static string Encrypt(string data, string xmlKey)
        {
            RSACryptoServiceProvider provider = new RSACryptoServiceProvider(KeyBitSize);
            provider.FromXmlString(xmlKey);

            return Convert.ToBase64String(provider.Encrypt(Encoding.UTF8.GetBytes(data), false));
        }
        #endregion

        #region Decrypt
        public static string Decrypt(string data, string xmlKey)
        {
            RSACryptoServiceProvider provider = new RSACryptoServiceProvider(KeyBitSize);
            provider.FromXmlString(xmlKey);

            return Encoding.UTF8.GetString(provider.Decrypt(Convert.FromBase64String(data), false));
        }
        #endregion
    }
    #endregion
}
