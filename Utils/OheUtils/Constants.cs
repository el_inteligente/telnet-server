//#define OPENWEB
#define RIA
using System;

namespace Ohe.Utils
{


    #region enum ConnectionType
    /// <summary>
    /// ��� ���������� ������������
    /// </summary>
    public enum OheConnectionType
    {
        Web = 0,
        HandHeld = 1,
        System = 2,
        Debug = 3,
    }
    #endregion


    #region enum Events
    /// <summary>
    /// ������ ������� ��������� ��� �������������
    /// </summary>
    public enum Events { UnBounded = 0, OnInit, OnChange, OnSubmit, OnEnd, OnExit }
    
    #endregion


    #region enum FormState
    /// <summary>
    /// ��������� ������� ��������� ��������� ��� �����
    /// Valid - ��� ���������
    /// InValid - ������������ ���� �������� ������ (��������� ������� ���������� ������). ��������� ��������� ����
    /// Exception - �������� ��������� ������ - �������� ������������
    /// MultiResult - �������� ��������� ���������� ��������� ����� 
    /// </summary>
    public enum FormState { Valid, InValid, Exception, MultiResult }
    
    #endregion


    #region enum LabelType
    /// <summary>
    /// ��� ��������
    /// </summary>
    public enum LabelType { SINGLE, CONTENT, REPORT, FAST_REPORT }
    
    #endregion


    #region OheLanguage
    public enum OheLanguage
    {
        Test = -1,
        Undefined = 0,
        English = 1033,
        Russian = 1049,
    }
    #endregion


    #region enum OheAlign
    public enum OheAlign
    {
        Left, Right, Center, Justify
    } 
    #endregion

    /// <summary>
	/// ��������� ��������� ������������
	/// </summary>
	public class WebConstant
	{

        public const string WEB_REF = "OheWebReports.aspx?ref=";
		public const string RB_POPUP_ERROR = @"
        <script language=javascript>
            var oWnd = window.open('" + WebConstant.WEB_REF + @"general_error', '������', 'width=620; height=350; directories=no; location=no; menubar=no; status=no; toolbar=no; resizable=yes, screenX=0, screenY=100');
            if (oWnd != null)
                oWnd.focus();
            else
                window.status='����������� ���� �������������. �������� ��.';
        </script>";

#if OPENWEB
		public const string WEB_TITLE = "SOABIT Platform";
#else
		public const string WEB_TITLE = "Open HandHeld.WEB";
#endif

        public const string RESIZE_POPUP = @"var formWidth = maintable.clientWidth;
var scrWidth = window.screen.availWidth;
var scrHeight = window.screen.availHeight;
var formDifWidth = 0;

 if ((formWidth+screenWidthTweak) > scrWidth)
   formDifWidth = formWidth - scrWidth - screenWidthTweak;

if(maintable.clientHeight + 10 > scrHeight )	
{
formdiv.style.height = scrHeight + 62;
formdiv.style.width = maintable.clientWidth+30;
window.resizeTo(maintable.clientWidth+45, scrHeight + 72);
}
else
window.resizeTo(maintable.clientWidth+25, maintable.clientHeight+72);
window.moveTo((scrWidth-maintable.clientWidth+25)/2,(scrHeight-maintable.clientHeight+72)/2);
";

        /// <summary>
        /// ������ ������
        /// {0} - ������������� ��������
        /// {1} - ������� Click
        /// {2} - ������
        /// {3} - ����� �� ������
        /// </summary>
        public const string TEMPLATE_BUTTON = "<button type=\"button\" id=\"{0}\" name=\"{0}\">{3}</button><script type=\"text/javascript\"> $(\"#{0}\").button( {{icons : {{ primary: \'{2}\' }}, label: \"{3}\" }}); $(\"#{0}\").button().click(function() {{ {1}; return false;}});</script>";

        [Obsolete("���������� ��������� ����������� html ��� ���������� �� a href")]
        public const string TEMPLATE_CLOSEPOPUP_BUTTON = "<a class=\"csButtonText2\" href=\"#\" onclick=\"csHidePopupReport('csBackground');\"  ><span><span>" + "�������" + "</span></span></a>";

        /// <summary>
        /// ������ ������������ ������
        /// {0} - Caption
        /// {1} - ���������� ������
        /// </summary>
        public const string TEMPLATE_POPUPFORM = "<div>{1}</div><script type=\"text/javascript\"> $(\"#csPopupReport\").dialog( {{title : \"{0}\", modal:true, width: 450 }});</script>";

        /// <summary>
        /// ������ ������������ ������
        /// {0} - ����� ���������
        /// {1} - ���� �������
        /// {2} - ������ ����������� ��������
        /// </summary>
        /// �������� ���� ������������ ��������� ��������� � ��������
        public const string TEMPLATE_EXCEPTION_POPUPFORM = "<div id=\"exception-dialog\" title=\"����������\"> <div class=\"ui-state-error ui-corner-all\"><p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 20px 0;\"></span>{0}</p></div> <p>���� �������:</p><div>{1}</div></div><script type=\"text/javascript\"> $(\"#exception-dialog\").dialog( {{ modal:true, width: 450, open: function () {{ $(this).parents(\".ui-dialog:first\").find(\".ui-dialog-titlebar\").addClass(\"ui-state-error\"); }},  buttons: {{ ��������: function() {{ {2} $( this ).dialog(\"close\");  }}, �������: function() {{ $( this ).dialog(\"close\"); }}  }}, close: function() {{ $(this).remove(); }} }});</script>";
        /// <summary>
        /// ������ ������������ ������
        /// {0} - ����� ���������
        /// {1} - ������ �������� ���� ����
        /// </summary>
        public const string TEMPLATE_INFORMATION_POPUPFORM_2BUTTON = "<div id=\"confirm-dialog\" title=\"����������\"><p>{0}</p></div><script type=\"text/javascript\"> $(\"#confirm-dialog\").dialog( {{ modal:true, width: 350, buttons: {{ Ok: function() {{ {1} $( this ).dialog(\"close\"); }}, �������: function() {{ $( this ).dialog(\"close\"); }}  }}, close: function() {{ $(this).remove(); }} }});</script>";
        public const string TEMPLATE_INFORMATION_POPUPFORM_1BUTTON = "<div id=\"confirm-dialog\" title=\"����������\"><p>{0}</p></div><script type=\"text/javascript\"> $(\"#confirm-dialog\").dialog( {{ modal:true, width: 350, buttons: {{ �������: function() {{ $( this ).dialog(\"close\"); }}  }}, close: function() {{ $(this).remove(); }} }});</script>";

        /// <summary>
        /// ������ ������������ ������
        /// {0} - ����� ���������
        /// {1} - ������ �������� ���� ����
        /// </summary>
        public const string TEMPLATE_WARNING_POPUPFORM = "<div id=\"warning-dialog\" title=\"��������������\"><p>{0}</p></div><script type=\"text/javascript\"> $(\"#warning-dialog\").dialog( {{ modal:true, width: 350, open: function () {{ $(this).parents(\".ui-dialog:first\").find(\".ui-dialog-titlebar\").addClass(\"ui-state-error\"); }}, buttons: {{ �������: function() {{ $( this ).dialog(\"close\"); }}  }}, close: function() {{ $(this).remove(); }} }});</script>";
        public const string TEMPLATE_WARNING_POPUPFORM_2BUTTON = "<div id=\"warning-dialog\" title=\"��������������\"><p>{0}</p></div><script type=\"text/javascript\"> $(\"#warning-dialog\").dialog( {{ modal:true, width: 350,  open: function () {{ $(this).parents(\".ui-dialog:first\").find(\".ui-dialog-titlebar\").addClass(\"ui-state-error\"); }}, buttons: {{ Ok: function() {{ {1} $( this ).dialog(\"close\"); }}, �������: function() {{ $( this ).dialog(\"close\"); }}  }}, close: function() {{ $(this).remove(); }} }});</script>";
        /*public const string TEMPLATE_WARNING_POPUPFORM = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#dfe8f6\">"
        + @"<tr>"
        + "<td width=\"2px\" style=\"background-image:url(images/cs_form_top_left_background.gif);\"></td>"
        + "<td height=\"2px\" style=\"background-image:url(images/cs_form_top_center_background.gif); background-repeat:repeat-x\"></td>"
        + "<td width=\"2px\" style=\"background-image:url(images/cs_form_top_right_background.gif);\"></td>"
        + "</tr>"
        + "<tr><td width=\"2px\" style=\"background-image:url(images/cs_form_left_background.gif); background-repeat:repeat-y;\"></td>"
        + "<td class=\"csReportHeader\">"
        + "<table cellspacing='0' cellpadding='1' width=100%>"
        + "<tr>"
        + "<td height=\"10\" class=\"cs_leftMenuHead\">"
        + "<div id=\"csPopupReportHeader\" style=\"width:100%;height:100%;cursor:move;\">����������</div></td>"
        + "<td align=\"right\"><img src=\"images/close.gif\" border=\"0\" onClick=\"csHidePopupReport('csBackground');\" style=\"cursor:hand;\">"
        + "</td>"
        + "</tr>"
        + "</table>"
        + "</td>"
        + "<td width=\"2px\" style=\"background-image:url(images/cs_form_right_background.gif);\"></td></tr>"
        + "<tr><td width=\"2px\" style=\"background-image:url(images/cs_form_left_background.gif); background-repeat:repeat-y;\"></td>"
        + "<td>"
        + "<div id=\"csPopupReportContent\">"
        + "<table cellspacing=\"0\" cellpadding=\"1\" width=\"100%\">"
        + "<tr><td valign=\"top\">"
        + "<img src=\"images/warning.gif\" border=\"0\"/>"
        + "</td><td>"
        + "{0}"
        + "</td></tr>{1}</table>"
        + "</div>"
        + "</td>"
        + "<td width=\"2px\" style=\"background-image:url(images/cs_form_right_background.gif);\"></td>"
        + "</tr>"
        + "<tr><td width=\"2px\" style=\"background-image:url(images/cs_form_bottom_left_background.gif);\"></td><td height=\"2px\" style=\"background-image:url(images/cs_form_bottom_center_background.gif); background-repeat:repeat-x;\"></td><td width=\"2px\" style=\"background-image:url(images/cs_form_bottom_right_background.gif);\"></td></tr>"
        + "</table>";
        */

        /// <summary>
        /// ������ ������ ����
        /// {0} - {0} - id
        /// {1} - {1} - ������� Onclick
        /// {2} - ������ �� ��������
        /// {3} - �����
        /// </summary>
        public const string TEMPLATE_MENU_BUTTON = "<button type=\"button\" id=\"{0}\" name=\"{0}\" value=\"{3}\">{3}</button><script type=\"text/javascript\"> $(\"#{0}\").button( {{icons : {{ primary: \'{2}\' }}, text:false }}); $(\"#{0}\").button().click(function() {{ {1}; return false;}});</script>";
        //public const string TEMPLATE_MENU_BUTTON = "<input type=\"submit\" id=\"{0}\" name=\"{0}\" value=\"{3}\"/><script type=\"text/javascript\"> $(\"#{0}\").button({{icons , {{ primary: \"{2}\" }} }}).click(function() {{ {1}; return false;}});</script>";


        /// <summary>
        /// ������ ������ ����
        /// {0} - {0} - id
        /// {1} - {1} - ������� Onclick
        /// {2} - ������ �� ��������
        /// {3} - �����
        /// </summary>
        public const string TEMPLATE_EXPORT_BUTTON = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">"
            + "<tr><td align='center'>"
            + "<div id='{0}' class='csButtonMenuText' "
            + "style='background-image:url(images/cs_center_button_menu_background.gif);background-repeat:repeat-x;' "
            + "onclick='{1}'>"
            + "<img src=\"images/{2}\" border=\"0\" style=\"padding:0px;margin:0px;\" vspace=\"0\"/>{3}</div>"
            + "</td>"
            + "</tr></table>";


        public static string GetTemplateErrorForm(Exception exc, string redirect)
        {
            if (redirect == string.Empty)
                return string.Format(TEMPLATE_EXCEPTION_POPUPFORM, exc.Message, exc.StackTrace,
                   "OnAsyncSendEmailMessage();");

            else
            {
                if (redirect.StartsWith(WEB_REF))
                {
                    redirect = redirect.Replace(WEB_REF, "ref=");
                }

                return string.Format(TEMPLATE_EXCEPTION_POPUPFORM, exc.Message, exc.StackTrace,
                    "OnAsyncRedirectToReport(" + (redirect.IndexOf("popup=1") < 0 ? "false" : "true") + ",true,\"" + redirect + "\");");
                    //+ "<a class=\"csButtonText2 primary\" href=\"#\" onclick='OnAsyncSendEmailMessage();' ><span><span>" + "��������� � ���������" + "</span></span></a>"
                    //+ "</td></tr>");
            }
        }

        public static string GetTemplateErrorForm(string message, string redirect)
        {
            if (redirect == string.Empty)
                return string.Format(TEMPLATE_EXCEPTION_POPUPFORM, message, "�� �������", "OnAsyncSendEmailMessage();");
            else
            {
                if (redirect.StartsWith(WEB_REF))
                {
                    redirect = redirect.Replace(WEB_REF, "ref=");
                }

                return string.Format(TEMPLATE_EXCEPTION_POPUPFORM, message, "�� �������",
                    "OnAsyncRedirectToReport(" + (redirect.IndexOf("popup=1") < 0 ? "false" : "true") + ",true,\"" + redirect + "\");");
                    //+ "<a class=\"csButtonText2 primary\" href=\"#\" onclick='OnAsyncSendEmailMessage();' ><span><span>" + "��������� � ���������" + "</span></span></a>"
                    //+ "</td></tr>");
            }
        }

        public static string GetTemplateInformationForm(string message, string redirect)
        {
            if (redirect == string.Empty)
                return string.Format(TEMPLATE_INFORMATION_POPUPFORM_1BUTTON, message);

            if (redirect.StartsWith(WEB_REF))
                redirect = redirect.Replace(WEB_REF, "ref=");

            return string.Format(TEMPLATE_INFORMATION_POPUPFORM_2BUTTON, message,
                    "OnAsyncRedirectToReport(" + (redirect.IndexOf("popup=1") < 0 ? "false" : "true") + ",true,\"" + redirect + "\");");
        }

        public static string GetTemplateWarningForm(string message, string redirect)
        {
            if (redirect == string.Empty)
                return string.Format(TEMPLATE_INFORMATION_POPUPFORM_1BUTTON, message);

            return string.Format(TEMPLATE_WARNING_POPUPFORM_2BUTTON, message,
                    "OnAsyncRedirectToReport(" + (redirect.IndexOf("popup=1") < 0 ? "false" : "true") + ",true,\"" + redirect + "\");");
        }

	}

	/// <summary>
	/// ��������� ��������� Ohe ������������
	/// </summary>
	public class OheConstant
	{
		public const string Version = "4.0.0";

		public const string TELNET_ESC_SEQ = "\u001b[";
		public const string TELNET_CLEAR_SEQ = TELNET_ESC_SEQ + "2J" + TELNET_ESC_SEQ + "1;0H";
        public const string TELNET_BEEP_SEQ =  "\a\a\a";

        public const string WEB_ONCLICK_EVENT = "";// onkeydown=\"javascript: document.onkeydown = ToggleShortcut\"; onChange=\"showCommentDialog();\" ";

		public const string TELNET_HELP_HEADMENU = "�������� �� ������ ����������� ������";

		public static string r = TELNET_CLEAR_SEQ + "3L"; // ��������� 3 ������ ������
		public static string d = TELNET_ESC_SEQ + "0;4;5m"; // ������� �������� ��� �������� ����� ������������� ������ ����� � blinking

		public enum OheTelnetControlType { Text, InputBox, ControlKey, Help, List, Form, Message }

        public static string TELNET_SYSTEM_EXCEPTON_WEBCONNECTION = 
                  "<div class=\"form\">"
                + "<input type=\"hidden\" name=\"ref\" value=\"-99\">"
                + "<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='�����'><br>"
                +"<span class=\"error\">��������� ������<br><br>"
                +"���������� �<br>"
                +"����������<br>"
                +"��������������"
                + "</span></div>";

        public static string TELNET_CONFIGURATION_EXCEPTON_WEBCONNECTION = 
                 "<div class=\"form\">"
                +"<input type=\"hidden\" name=\"ref\" value=\"-99\">"
                + "<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='�����'><br>"
                + "<span class=\"error\">������:<br><br>"
                + "���������� �<br>"
                + "����������<br>"
                + "��������������<br>"
                + "</span></div>" ;

        public static string TELNET_CONFIGURATION_MENU_EXCEPTON = 
                 "<div class=\"form\">"
                + "<input type=\"hidden\" name=\"ref\" value=\"-99\">"
                + "<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='�����'><br>"
                + "<span class=\"error\">������:<br><br>"
                + "�� ������ �����<br>"
                + "������-��������<br>"
                + "���������� �<br>"
                + "��������������<br>"
                + "</span></div>";


        public static string TELNET_ACTIVE_USER_MENU_EXCEPTON =
                 "<div class=\"form\">"
                + "<input type=\"hidden\" name=\"ref\" value=\"-99\">"
                + "<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='�����'><br>"
                + "<span class=\"error\">������:<br><br>"
                + "������������ ��� ��������<br>"
                + "����������� ������ �����<br>"
                + "��� ����������� ���<br>"
                + "���������� �<br>"
                + "��������������<br>"
                + "</span></div>";


        
        public static string TELNET_RESTORE_SESSION_MESSAGE = OheConstant.TELNET_CLEAR_SEQ + "������� �� ���������"+Environment.NewLine
                                                        + "�� ������ ����������" + Environment.NewLine
                                                        + "��." + Environment.NewLine + Environment.NewLine
                                                        + "�� ������ ������" + Environment.NewLine
                                                        + "���� �� ���������" + Environment.NewLine
                                                        + "�����" + Environment.NewLine;

        public static string TELNET_RESTORE_SESSION_MESSAGE_WEBCONNETION =
                  "<div class=\"form\">"
                + "<input type=\"hidden\" name=\"ref\" value=\"-99\">"
                + "<span class=\"error\">������� �� ����������� ������ ������������.�� ������ ���������� �� ��������� �����<br>"
                + "<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='�������' />"
                + "</div>";

        public static string TELNET_SYSTEM_ECXEPTION = TELNET_CLEAR_SEQ
                            + "  ��������� ������  "
                            + Environment.NewLine
                            + Environment.NewLine
                            + "  ���������� �      "
                            + Environment.NewLine
                            + "   ����������       "
                            + Environment.NewLine
                            + "  ��������������    ";


		/// <summary>
		/// ���������� � ��������� ������� ������� �������
		/// </summary>
		/// <param name="x">���������� X</param>
		/// <param name="y">���������� Y</param>
		/// <returns>string</returns>
		public static string SetCursorPosition(int x, int y)
		{
			return OheConstant.TELNET_ESC_SEQ
								 + y.ToString()
								 + ";"
								 + x.ToString() + "f";
		}

		#region Modules
		/// <summary>
		/// ��������� ������
		/// </summary>
        public const string INTERFACE = "I"; 
        /// <summary>
        /// ������ ������
        /// </summary>
        public const string TELNET = "T";
        /// <summary>
        /// ����� ������
        /// </summary>
        public const string PRINT = "P";
        /// <summary>
        /// ������ ������� (����������� �����)
        /// </summary>
        public const string TASK = "J";
        /// <summary>
        /// ����������������� 
        /// </summary>
        public const string CONFIGURATION = "C";
        /// <summary>
        /// ������ �������� ����� (�������������)
        /// </summary>
        public const string MAIL = "M";
        /// <summary>
        /// Web ������
        /// </summary>
        public const string WEB = "W";

        public const string ALLMODULES = "ITPJCMW";
		#endregion

        public static string GetModuleDescription(string module)
        {
            switch (module)
            {
                case "W":
                    return "Web-������ ����������";
                case "M":
                    return "������ �������� �����";
                case "C":
                    return "�������� ����������";
                case "J":
                    return "������ ��������� �������";
                case "P":
                    return "������ ������ ����������";
                case "T":
                    return "������ ������ � ���";
                case "I":
                    return "������ ���������� ������";
            }
            return "����������� ������";
        }

        public static string GetModuleCodeByDescription(string description)
        {
            switch (description)
            {
                case "Web-������ ����������":
                    return "W";

                case "������ �������� �����":
                    return "M";
                case "�������� ����������":
                    return "C";
                case "������ ��������� �������":
                    return "J";
                case "������ ������ ����������":
                    return "P";
                case "������ ������ � ���":
                    return "T";

                case "������ ���������� ������":
                    return "I";
            }
            return "����������� ������";

        }

        #region Templates
        public const string OHE_PLATFORM_CONSTANT = @"<?xml version=""1.0""?>
<OhePlatformConnection host=""localhost"" port=""5500"" />";
        #endregion

	}



}