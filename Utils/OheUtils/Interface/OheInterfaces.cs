﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Ohe.Utils;

// Описывает интерфейсы для удаленного соединения с телнет сервером
namespace Ohe.Interfaces
{
    /// <summary>
    /// Описывает интерфейс для удаленного соединения 
    /// с приложением отслеживающим работу пользователей 
    /// </summary>
    
    public interface IOheMonitor
    {
        bool IsAlive();
        ArrayList UsersConsoles { get; }

        // Старые методы не используются в приложении
        //ArrayList Users { get; }
        //void AddUser(string Login, string Pass, string Permiss, string Name, string Whse);
    }


    /// <summary>
    /// Описывает интерфейс для лицензирования продукта
    /// </summary>
    
    public interface IOheLicense
    {
        //bool IsAlive { get;}
        OheLicenseRecord LicenseInfo();
        string GetModules();
        int GetMaxTelnetUsers { get; }
        int GetMaxWebUsers { get; }
        bool ModuleIsAvailable(string Module);
        bool UpdateLicense(string newLicense);

        //void AddTelnetUser(string Login); // Добавляет пользователя к лицензии
        //void RemoveTelnetUser(string Login); // Удаляет пользователя из лицензии

        //void AddWebUser(string Login);   // Добавляет пользователя к лицензии
        //void RemoveWebUser(string Login); // Удаляет пользователя из лицензии
        void AddTelnetUser(string[] User); // Добавляет пользователя к лицензии
        void RemoveTelnetUser(string Login); // Удаляет пользователя из лицензии

        void AddWebUser(string[] User);   // Добавляет пользователя к лицензии
        void RemoveWebUser(string Login); // Удаляет пользователя из лицензии

        Hashtable GetLoginWebUsers { get; } // Список залогиненных пользователей
        Hashtable GetLoginTelnetUsers { get; } // Список залогиненных пользователей

    }
}
