﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Diagnostics;
using Ohe.Utils;
using Ohe.Utils.WCF;


namespace Ohe.Interfaces
{

    #region Events Contract - работает с событиями старта и остановки служб

    public enum EventType
    {
        Start = 1,
        Stop = 2,
        Remove = 4,
        AllEvents = Start | Stop | Remove
    }

    [ServiceContract(CallbackContract = typeof(IOheEvents), SessionMode = SessionMode.Required)]
    public interface IOheModuleEventContract
    {
        [OperationContract]
        void Subscribe(int Module, EventType mask);

        [OperationContract]
        void Unsubscribe(int Module, EventType mask);

        [OperationContract()]
        OheLicenseRecord LicenseInfo();


        [OperationContract(IsOneWay = true)]
        void GetModuleInfo(OheModuleInfo Module);


        [OperationContract()]
        string GetModules();


        [OperationContract]
        int GetMaxTelnetUsers();


        [OperationContract]
        int GetMaxWebUsers();


        [OperationContract]
        bool ModuleIsAvailable(string Module);


        [OperationContract]
        bool UpdateLicense(string newLicense);

        [OperationContract(IsOneWay = true)]
        //[OperationContract()]
        void AddUser(OheUser User);   // Добавляет пользователя к лицензии


        [OperationContract(IsOneWay = true)]
        //[OperationContract()]
        void DeleteTelnetUser(string Login); // Удаляет пользователя из лицензии


        [OperationContract(IsOneWay = true)]
        //[OperationContract()]
        void RemoveTelnetUsers(string [] Login); // Удаляет пользователя из лицензии

        
        [OperationContract(IsOneWay = true)]
        //[OperationContract()]
        void RemoveUser(string Login, OheConnectionType type); // Удаляет пользователя из лицензии


        [OperationContract(IsOneWay = true)]
        //[OperationContract()]
        void RemoteKillUsers(string [] Login, OheConnectionType type); // Удаляет пользователя из лицензии


        [OperationContract]
        List<OheUser> GetLoginWebUsers(); // Список залогиненных пользователей


        [OperationContract]
        List<OheUser> GetLoginTelnetUsers(); // Список залогиненных пользователей

        [OperationContract]
        bool isAlive();

    }

    /// <summary>
    /// Поддерживаемые удаленные события
    /// </summary>
    public interface IOheEvents
    {
        [OperationContract(IsOneWay = true)]
        void OnStart();

        [OperationContract(IsOneWay = true)]
        void OnStop();

        [OperationContract(IsOneWay = true)]
        void OnRemove(string [] Login);

    }
    
    #endregion


    #region License Contract - работает с пользователями и лицензиями
    /// <summary>
    /// Описывает интерфейс обмена через WCF
    /// </summary>
    [ServiceContract()]
    public interface IOheUserLicense
    {
        [OperationContract()]
        OheLicenseRecord LicenseInfo();


        [OperationContract()]
        string GetModules();


        [OperationContract()]
        int GetMaxTelnetUsers();


        [OperationContract()]
        int GetMaxWebUsers();


        [OperationContract()]
        bool ModuleIsAvailable(string Module);


        [OperationContract()]
        bool UpdateLicense(string newLicense);


        [OperationContract()]
        void AddTelnetUser(string[] User); // Добавляет пользователя к лицензии


        [OperationContract()]
        void RemoveTelnetUser(string Login); // Удаляет пользователя из лицензии


        [OperationContract()]
        void AddWebUser(string[] User);   // Добавляет пользователя к лицензии


        [OperationContract()]
        void RemoveWebUser(string Login); // Удаляет пользователя из лицензии


        [OperationContract()]
        Hashtable GetLoginWebUsers(); // Список залогиненных пользователей


        [OperationContract()]
        Hashtable GetLoginTelnetUsers(); // Список залогиненных пользователей

    }

    
    public interface IOheUserLicenseClientChannel : IOheUserLicense, IClientChannel { }
    
    #endregion



    /// <summary>
    /// Отображает всех активных пользователей в Open Source HH.WEB
    /// </summary>
    [ServiceContract()]
    public interface IOheUserManagementService
    {
        [OperationContract]
        ArrayList ActiveTelnetUser();

        /*
        [OperationContract]
        void RemoveUser(string [] User);
        */
    }
}
