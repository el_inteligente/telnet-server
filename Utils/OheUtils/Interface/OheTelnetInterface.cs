﻿using System.Collections.Generic;
using System.Xml;

namespace Ohe.Utils.Interface
{
    public interface IOheControlInterface
    {
        int Id { get; set; }

        string[] ToHtml(Dictionary<string, object> cache);

        string[] ToString(Dictionary<string, object> cache);

        void InitControlFromXML(XmlNode node);

        bool IsMark { get; set; }

        string Align { get; set; }

        string typeValue { get; set; }

        bool Mandatory { get; set; }

        bool ReadOnly { get; set; }

        bool IsValid { get; set; }

        string ValueID { get; set; }

        int X { get; set; }

        int Y { get; set; }
    }
}
