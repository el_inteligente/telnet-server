﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace Ohe.Utils
{
    public class OheFtpClient
    {

        public class FtpState
        {
            private ManualResetEvent wait;
            private FtpWebRequest _request;
            private string _fileName;
            private Exception _operationException = null;
            private string _status;

            public FtpState()
            {
                wait = new ManualResetEvent(false);
            }

            public ManualResetEvent OperationComplete
            {
                get { return wait; }
            }

            public FtpWebRequest Request
            {
                get { return _request; }
                set { _request = value; }
            }

            public string FileName
            {
                get { return _fileName; }
                set { _fileName = value; }
            }

            public Exception OperationException
            {
                get { return _operationException; }
                set { _operationException = value; }
            }

            public string StatusDescription
            {
                get { return _status; }
                set { _status = value; }
            }
          
        }

        #region private Values

        private string _ftpServer;
        private string _ftpLogin;
        private string _ftpPassword;
        private string _remotePATH;

        private string Uri
        {
            get { return "ftp://" + _ftpServer + _remotePATH; }
        }

        
        #endregion


        #region Constructor
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="ftpServer"></param>
        /// <param name="ftpLogin"></param>
        /// <param name="ftpPassword"></param>
        /// <param name="remotePath"></param>
        public OheFtpClient(string ftpServer, string ftpLogin, string ftpPassword, string remotePath)
        {
            _ftpServer = ftpServer;
            _ftpLogin = ftpLogin;
            _ftpPassword = ftpPassword;
            _remotePATH = remotePath;
        }
        
        #endregion


        #region Download

        public bool Download(string fileName, string localPath)
        {
            return Download(fileName, localPath, Encoding.GetEncoding("windows-1251"));
        }


        public bool Download(string fileName, string localPath, Encoding codePage)
        {
            try
            {
                // Create a Uri instance with the specified URI string. 
                // If the URI is not correctly formed, the Uri constructor 
                // will throw an exception.
                Uri target = new Uri(Uri + "/" + fileName);

                FtpWebRequest request = (FtpWebRequest) WebRequest.Create(target);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.KeepAlive = false;

                // This example uses anonymous logon. 
                // The request is anonymous by default; the credential does not have to be specified.  
                // The example specifies the credential only to 
                // control how actions are logged on the server.
                request.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);

                using (FtpWebResponse response = (FtpWebResponse) request.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            using (StreamReader reader = new StreamReader(responseStream, codePage))
                            {
                                using (StreamWriter destination = new StreamWriter(localPath + fileName, false, codePage))
                                {
                                    destination.Write(reader.ReadToEnd());
                                    destination.Flush();
                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Utils.WriteLog(
                    string.Format(
                        "При загрузке файла с сервера ftp [{0}, {1}, {2}] {3} Файл: {4} {3} Папка выгрузки {6} {3} Создалось исключение: {5}",
                        _ftpServer, _ftpLogin, _remotePATH, Environment.NewLine, fileName, ex.Message, localPath));
                return false;
            }
        }

        #endregion


        #region Upload

        public bool Upload(string fileName)
        {
            try
            {
                // Create a Uri instance with the specified URI string. 
                // If the URI is not correctly formed, the Uri constructor 
                // will throw an exception.
                ManualResetEvent waitObject;
                Uri target = new Uri(Uri + "/" + Path.GetFileName(fileName));
                //string fileName = args[1];
                FtpState state = new FtpState();
                FtpWebRequest request = (FtpWebRequest) WebRequest.Create(target);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.KeepAlive = false;

                // This example uses anonymous logon. 
                // The request is anonymous by default; the credential does not have to be specified.  
                // The example specifies the credential only to 
                // control how actions are logged on the server.
                request.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);

                // Store the request in the object that we pass into the 
                // asynchronous operations.
                state.Request = request;
                state.FileName = fileName;

                // Get the event to wait on.
                waitObject = state.OperationComplete;

                // Asynchronously get the stream for the file contents.
                request.BeginGetRequestStream(new AsyncCallback(EndGetStreamCallback), state);

                // Block the current thread until all operations are complete.
                waitObject.WaitOne();

                // The operations either completed or threw an exception. 
                if (state.OperationException != null)
                {
                    throw state.OperationException;
                }

                return true;
            }
            catch (Exception exc)
            {
                Utils.WriteLog("При выгрузке данных на ftp сервер возникли ошибки" + Environment.NewLine + "Описание ошибки:" + exc.Message);
                return false;
            }

        }
        
        private void EndGetStreamCallback(IAsyncResult ar)
        {
            FtpState state = (FtpState) ar.AsyncState;

            Stream requestStream = null;
            // End the asynchronous call to get the request stream. 
            try
            {
                requestStream = state.Request.EndGetRequestStream(ar);
                // Copy the file contents to the request stream. 
                const int bufferLength = 2048;

                byte[] buffer = new byte[bufferLength];
                int count = 0;
                int readBytes = 0;
                using (FileStream stream = File.OpenRead(state.FileName))
                {
                    do
                    {
                        readBytes = stream.Read(buffer, 0, bufferLength);
                        requestStream.Write(buffer, 0, readBytes);
                        count += readBytes;
                    } while (readBytes != 0);
                }

                // IMPORTANT: Close the request stream before sending the request.
                requestStream.Close();
                // Asynchronously get the response to the upload request.
                state.Request.BeginGetResponse(
                    new AsyncCallback(EndGetResponseCallback),
                    state
                    );
            }
                // Return exceptions to the main application thread. 
            catch (Exception e)
            {
                Utils.WriteLog(string.Format("Не могу загрузить поток загрузки данных на сервер.{0} парвметры ftp сервера: {1},{2},{3}", Environment.NewLine, _ftpServer,_ftpLogin,_remotePATH));
                state.OperationException = e;
                state.OperationComplete.Set();
                return;
            }

        }

        // The EndGetResponseCallback method   
        // completes a call to BeginGetResponse. 
        private void EndGetResponseCallback(IAsyncResult ar)
        {
            FtpState state = (FtpState) ar.AsyncState;
            FtpWebResponse response = null;
            try
            {
                response = (FtpWebResponse) state.Request.EndGetResponse(ar);
                response.Close();
                state.StatusDescription = response.StatusDescription;
                // Signal the main application thread that  
                // the operation is complete.
                state.OperationComplete.Set();
            }
                // Return exceptions to the main application thread. 
            catch (Exception e)
            {
                Utils.WriteLog("Ошибка получения ответа от сервера. описание ошибки:" + e.Message);
                state.OperationException = e;
                state.OperationComplete.Set();
            }
        }

        #endregion


        #region GetFileList
        public List<string> GetFileList()
        {
            return GetFileList("*.*");
        }

        public List<string> GetFileList(string prefix)
        {
            List<string> downloadFiles = new List<string>();
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Uri+"/"+ prefix));
                request.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.KeepAlive = false;

                FtpWebResponse response = null;
                try
                {
                    response = (FtpWebResponse) request.GetResponse();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            using (StreamReader reader = new StreamReader(responseStream))
                            {
                                while (!reader.EndOfStream)
                                {
                                    downloadFiles.Add(Path.GetFileName(reader.ReadLine()));
                                }
                            }
                        }
                    }
                    response.Close();
                }
                catch (InvalidOperationException ex)
                {
                    try
                    {
                        string status = ((FtpWebResponse)((WebException)ex).Response).StatusDescription;
                        if (status.IndexOf("No files") >= 0)
                        {
                            if(response != null) response.Close();
                            return new List<string>();
                        }

                        throw ex;
                    }
                    catch (Exception exc)
                    {
                        throw exc;
                    }
                }
                return downloadFiles;
            }
            catch (Exception ex)
            {
                
                Utils.WriteLog(string.Format("При получении списка файлов с сервера ftp [{0}, {1}, {2}{5}] {3} Создалось исключение: {4}", _ftpServer, _ftpLogin, _remotePATH, Environment.NewLine, ex.Message, prefix));
                return new List<string>();
            }
        }
        #endregion


        #region DeleteFile
        /// <summary>
        /// Удаляет файл с сервера
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool DeleteFile(string fileName)
        {
            try
            {

                // Get the object used to communicate with the server.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Uri + "/" + fileName));
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                request.KeepAlive = false;

                request.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();
                return true;
            }
            catch (Exception ex)
            {
                Utils.WriteLog(string.Format("При удалении файла с сервера ftp [{0}, {1}, {2}] {3} Файл: {4} {3} Создалось исключение: {5}", _ftpServer, _ftpLogin, _remotePATH, Environment.NewLine,  fileName, ex.Message));
                return false;
            }
        }
        #endregion

    }
}
