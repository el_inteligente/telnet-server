﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace Ohe.Utils
{
    /// <summary>
    /// Конфигурация платформы
    /// </summary>
    
    public class OhePlatformConfig
    {

        #region Private Values

        private static string _licenseRoot = "C:\\";

        #endregion


        #region Constructor
        static OhePlatformConfig()
        {
            ConfigureAppSettings();
        }
        #endregion


        #region Properties

        public static string GetLicenseRoot { get { return _licenseRoot;} }

        #endregion


        #region Private Methods

        /// <summary>
        /// Определяет все переменные конфигурации system
        /// </summary>
        private static void ConfigureAppSettings()
        {
            XmlTextReader xml = new XmlTextReader(Utils.assemblyPath + @"system.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(xml);
            XmlNodeList d = doc.GetElementsByTagName("configuration");

            foreach (XmlNode c in d)
            {
                XmlNodeList goodNode = c.ChildNodes;
                foreach (XmlNode child in goodNode)
                {
                    if (child.NodeType == XmlNodeType.Element)
                        switch (Utils.AllTrim(child.Attributes["key"].Value.ToLowerInvariant()))
                        {
                            #region String values
                            case "licenseroot":
                                _licenseRoot = Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            #endregion
                        }
                }
            }
            xml.Close();
        }

        #endregion

    }
}
