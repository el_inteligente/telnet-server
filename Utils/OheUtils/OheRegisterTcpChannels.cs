﻿using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using Ohe.Utils;

namespace Ohe.Net
{
    
    public sealed class OheChannelsHelper
    {
        private OheChannelsHelper()
        { }

        #region RegisterTcpChannel
        static public void RegisterTcpChannel()
        {
            RegisterTcpChannel(Utils.Utils.assemblyPath + "OhePlatformConnection.config");
        }

        static public void RegisterTcpChannel(string channelConfigPath)
        {
            bool isFound = false;
            foreach (IChannel chanel in ChannelServices.RegisteredChannels)
            {
                if (chanel.ChannelName.ToLower() == "tcp")
                {
                    isFound = true;
                    break;
                }
            }
            if (!isFound)
                RemotingConfiguration.Configure(channelConfigPath);
        }
        #endregion
    }
}
