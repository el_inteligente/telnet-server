﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ohe.Utils.WCF
{
    
    public sealed class OheModuleInfo
    {

        public int ModuleID { get; set; }

        public string Version { get; set; }

        public string ModuleName { get; set; }

        public string IP        { get; set; }

        public string WindowsVersion { get; set; }

        public string ProcessorCount { get; set; }

        public string Memory { get; set; }

        public string AppDomain { get; set; }

        public string Name { get; set; }

        public string LifeTime { get; set; }

        public string ModuleType { get; set; }

        /*
        public string Version 
        { 
            get
            {
                Version v = Assembly.GetEntryAssembly().GetName().Version;
                return String.Format("{0}.{1}.{2}.{3}", v.Major, v.MajorRevision, v.Minor, v.MinorRevision);
            } 
        }

        public string IP
        { 
            get 
            { 
                return System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString();
            }
        }

        public string WindowsVersion 
        {
            get 
            { 
                return Environment.OSVersion.VersionString + "(" + Environment.OSVersion.Platform.ToString() + ")"; 
            } 
        }

        public string ProcessorCount { get { return Environment.ProcessorCount.ToString(); } }

        public string Memory { get { return Environment.WorkingSet.ToString(); } }
        
        public string AppDomain { get { return Environment.UserDomainName; } }

        public string Name { get { return System.Net.Dns.GetHostName(); }}

        public string LifeTime { get { return (Environment.TickCount/(1000 * 60)).ToString(); } }
      */

    }
}
