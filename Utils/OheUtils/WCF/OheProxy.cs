﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Linq;
using System.Text;
using Ohe.Interfaces;


namespace Ohe.Utils.WCF
{

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    public partial class OheModuleEventContractClient : DuplexClientBase<IOheModuleEventContract>, IOheModuleEventContract
    {

        public bool isAlive() { return Channel.isAlive(); }

        public OheModuleEventContractClient(InstanceContext inputInstance)
            : base(inputInstance)
        { }

        public OheModuleEventContractClient(InstanceContext inputInstance, string endpointConfigurationName)
            : base(inputInstance, endpointConfigurationName)
        { }


        public void Subscribe(int Module,EventType mask)
        {
            Channel.Subscribe(Module, mask);
        }

        public void Unsubscribe(int Module, EventType mask)
        {
            Channel.Unsubscribe(Module, mask);
        }

        
        #region GetLicenseInfo - объект с информацией о лицензии OheLicenseRecord
        /// <summary>
        /// Возвращает всю информацию о лицензии пользователя
        /// </summary>
        /// <returns></returns>
        public OheLicenseRecord LicenseInfo()
        {
            return base.Channel.LicenseInfo();
        }
        #endregion


        #region UpdateLicense - обновляет лицензию
        /// <summary>
        /// Обновляет пользовательскую лицензию (Требует подтверждения)
        /// </summary>
        /// <param name="newLicense">Параметры лицензии</param>
        /// <returns></returns>
        public bool UpdateLicense(string newLicense)
        {
            return base.Channel.UpdateLicense(newLicense);
        }
        #endregion


        #region Добавление/Удаление активных пользователей из лицензии

        public void AddUser(OheUser User)
        {
            base.Channel.AddUser(User);
        }


        public void DeleteTelnetUser(string User)
        {
            base.Channel.DeleteTelnetUser(User);
        }


        public void RemoveTelnetUsers(string [] User)
        {
            base.Channel.RemoveTelnetUsers(User);
        }


        public void RemoveUser(string User, OheConnectionType type)
        {
            base.Channel.RemoveUser(User, type);
        }

        public void RemoteKillUsers(string [] User,OheConnectionType type)
        {
            base.Channel.RemoteKillUsers(User, type);
        }


        /*
        public void ClearWebUsers(int ModuleId)
        {
            base.Channel.ClearWebUsers(ModuleId);
        }

        public void ClearTelnetUsers(int ModuleId)
        {
            base.Channel.ClearTelnetUsers(ModuleId);
        }
        */

        #endregion


        #region GetAvailableModules - возвращает список модулей доступных для использования
        public string GetModules()
        {
            return base.Channel.GetModules();
        }
        #endregion


        #region isModuleEnabled - доступен ли модуль для использования
        public bool ModuleIsAvailable(string Module)
        {
            return base.Channel.ModuleIsAvailable(Module);
        }
        #endregion


        #region GetMaxWebUsers - масимальное количество web пользователей
        public int GetMaxWebUsers()
        {
            return base.Channel.GetMaxWebUsers();
        }
        #endregion

        public List<OheUser> GetLoginWebUsers()
        {
            return base.Channel.GetLoginWebUsers();
        }

        public List<OheUser> GetLoginTelnetUsers()
        {
            return base.Channel.GetLoginTelnetUsers();
        }



        #region GetMaxTelnetUsers - максимальное количество Telnet пользователей
        public int GetMaxTelnetUsers()
        {
            return base.Channel.GetMaxTelnetUsers();
        }
        #endregion


        public void GetModuleInfo(OheModuleInfo Module)
        {
            base.Channel.GetModuleInfo(Module);
        }

    }
}
