﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Xml;
using Ohe.Interfaces;
using Ohe.Configuration;

namespace Ohe.Utils.WCF
{
    /// <summary>
    /// Основной класс лицензий, на клиенте
    /// </summary>
        
    public class OheUserLicenseClient : ClientBase<IOheUserLicense>, IOheUserLicense
    {
        static private IOheUserLicense _lObjectConnectionManager = null;
        static private string _lObjectConnectionManagerLock = Guid.NewGuid().ToString();


        public OheUserLicenseClient() { }

        public OheUserLicenseClient(string endpointConfigurationName) :
            base(endpointConfigurationName) { }


        public OheUserLicenseClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public OheUserLicenseClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public OheUserLicenseClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }




        #region GetLicenseInfo - объект с информацией о лицензии OheLicenseRecord
        /// <summary>
        /// Возвращает всю информацию о лицензии пользователя
        /// </summary>
        /// <returns></returns>
        public OheLicenseRecord LicenseInfo()
        {
            return base.Channel.LicenseInfo();
        }
        #endregion


        #region UpdateLicense - обновляет лицензию
        /// <summary>
        /// Обновляет пользовательскую лицензию (Требует подтверждения)
        /// </summary>
        /// <param name="newLicense">Параметры лицензии</param>
        /// <returns></returns>
        public bool UpdateLicense(string newLicense)
        {
            return base.Channel.UpdateLicense(newLicense);
        }
        #endregion


        #region Добавление/Удаление активных пользователей из лицензии

        public void AddTelnetUser(string[] User)
        {
            base.Channel.AddTelnetUser(User);
        }

        public void AddWebUser(string[] User)
        {
            base.Channel.AddWebUser(User);
        }

        public void RemoveTelnetUser(string User)
        {
            base.Channel.RemoveTelnetUser(User);
        }

        public void RemoveWebUser(string User)
        {
            base.Channel.RemoveWebUser(User);
        }

        #endregion


        #region GetAvailableModules - возвращает список модулей доступных для использования
        public string GetModules()
        {
            return base.Channel.GetModules();
        }
        #endregion


        #region isModuleEnabled - доступен ли модуль для использования
        public bool ModuleIsAvailable(string Module)
        {
            return base.Channel.ModuleIsAvailable(Module);
        }
        #endregion


        #region GetMaxWebUsers - масимальное количество web пользователей
        public int GetMaxWebUsers()
        {
            return base.Channel.GetMaxWebUsers();
        }
        #endregion


        public Hashtable GetLoginWebUsers()
        {
            return base.Channel.GetLoginWebUsers();
        }


        public Hashtable GetLoginTelnetUsers()
        {
            return base.Channel.GetLoginTelnetUsers();
        }



        #region GetMaxTelnetUsers - максимальное количество Telnet пользователей
        public int GetMaxTelnetUsers()
        {
            return base.Channel.GetMaxTelnetUsers();
        }
        #endregion

    }

}

