﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Ohe.Interfaces;
using Ohe.Utils;
using Ohe.Utils.WCF;


namespace Ohe.Utils.WCF
{
    //[ServiceBehavior(IncludeExceptionDetailInFaults = true, AutomaticSessionShutdown = true, UseSynchronizationContext = false, InstanceContextMode = InstanceContextMode.PerCall)]
    public abstract class OheModuleEventSubscriberBase: IDisposable
    {
        private OheModuleEventContractClient m_Proxy;

        private int _id = 0;

        public int ModuleID { get { return _id; } }

        public bool isAlive() { return m_Proxy.isAlive(); }

        #region Constructor
        public OheModuleEventSubscriberBase()
        {
            InstanceContext context = new InstanceContext(this);
            m_Proxy = new OheModuleEventContractClient(context);

        }

        public OheModuleEventSubscriberBase(string Id)
        {
            _id = Id.GetHashCode();
            InstanceContext context = new InstanceContext(this);
            m_Proxy = new OheModuleEventContractClient(context);
        }
        #endregion


        public void Close()
        {
            try
            {
                if (m_Proxy != null)
                {
                    m_Proxy.Unsubscribe(ModuleID, EventType.Remove);
                    m_Proxy.Unsubscribe(ModuleID, EventType.Start);
                    m_Proxy.Unsubscribe(ModuleID, EventType.Stop);

                    m_Proxy.Close();
                }
            }
            catch
            {
                m_Proxy.Abort();
            }

        }

        public CommunicationState State { get { return m_Proxy.State; } }


        #region Events

        #region OnStart
        public virtual void OnStart() { }
        #endregion


        #region OnStart
        public virtual void OnStop() { }
        #endregion


        #region OnRemove
        public virtual void OnRemove(string [] Login) { }
        #endregion


        public virtual string[] Remove(int module) { return new[] { "" }; }

        #endregion


        public void OnSubscribe(int Module, EventType Event)
        {
            m_Proxy.Subscribe(Module, Event);
        }


        public void OnUnsubscribe(int Module, EventType Event)
        {
            m_Proxy.Unsubscribe(Module, Event);
        }


        #region GetLicenseInfo - объект с информацией о лицензии OheLicenseRecord
        /// <summary>
        /// Возвращает всю информацию о лицензии пользователя
        /// </summary>
        /// <returns></returns>
        public OheLicenseRecord LicenseInfo()
        {
            return m_Proxy.LicenseInfo();
        }
        #endregion


        #region UpdateLicense - обновляет лицензию
        /// <summary>
        /// Обновляет пользовательскую лицензию (Требует подтверждения)
        /// </summary>
        /// <param name="newLicense">Параметры лицензии</param>
        /// <returns></returns>
        public bool UpdateLicense(string newLicense)
        {
            return m_Proxy.UpdateLicense(newLicense);
        }
        #endregion


        #region Добавление/Удаление активных пользователей из лицензии

        public void AddUser(OheUser User)
        {
            m_Proxy.AddUser(User);
        }

        public void DeleteTelnetUser(string User)
        {
            m_Proxy.DeleteTelnetUser(User);
        }

        public void RemoveTelnetUsers(string [] logins)
        {
            m_Proxy.RemoveTelnetUsers(logins);
        }

        public void RemoveUser(string User, OheConnectionType type)
        {
            m_Proxy.RemoveUser(User, type);
        }

        public void RemoveWebUsers(string [] User, OheConnectionType type)
        {
            m_Proxy.RemoteKillUsers(User, type);
        }

        #endregion


        #region GetAvailableModules - возвращает список модулей доступных для использования
        public string GetModules()
        {
            return m_Proxy.GetModules();
        }
        #endregion


        #region isModuleEnabled - доступен ли модуль для использования
        public bool ModuleIsAvailable(string Module)
        {
            return m_Proxy.ModuleIsAvailable(Module);
        }
        #endregion


        #region GetMaxWebUsers - масимальное количество web пользователей
        public int GetMaxWebUsers()
        {
            return m_Proxy.GetMaxWebUsers();
        }
        #endregion


        public List<OheUser> GetLoginWebUsers()
        {
            return m_Proxy.GetLoginWebUsers();
        }


        public List<OheUser> GetLoginTelnetUsers()
        {
            return m_Proxy.GetLoginTelnetUsers();
        }
        

        #region GetMaxTelnetUsers - максимальное количество Telnet пользователей
        public int GetMaxTelnetUsers()
        {
            return m_Proxy.GetMaxTelnetUsers();
        }
        #endregion


        public void AddModuleInfo(OheModuleInfo Module)
        {
            m_Proxy.GetModuleInfo(Module);
        }


        public void Dispose()
        {
            Close();
            /*
           try
            {

                m_Proxy = null;
                Utils.WriteLog("Объект освобожден");
                Utils.WriteWebLog("Объект освобожден");
            }
            catch { }
             */
        }

    }
}
