﻿using System;
using System.ServiceModel;
using System.Diagnostics;
using System.Collections.Generic;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Linq;
using Ohe.Utils.WCF;
using Ohe.Interfaces;


namespace Ohe.Utils.WCF
{
    
    internal static class OheEndPointsHelper
    {
        const int MessageSizeMultiplier = 5;


        #region RegisterTcpChannel
        /*
        static public void RegisterTcpChannel()
        {
            RegisterTcpChannel(Utils.Utils.assemblyPath + "OhePlatformConnection.config");
        }

        static public void RegisterTcpChannel(string channelConfigPath)
        {
            bool isFound = false;
            foreach (IChannel chanel in ChannelServices.RegisteredChannels)
            {
                if (chanel.ChannelName.ToLower() == "tcp")
                {
                    isFound = true;
                    break;
                }
            }
            if (!isFound)
                RemotingConfiguration.Configure(channelConfigPath);
        }
        */
        #endregion


        public static void RegisterWCFEndPoint(string mexAddress, string contractName)
        {
            //return;
            /*
            ServiceEndpoint[] endpoints = GetEndpoints(mexAddress);
            List<string> operations = new List<string>();

            foreach (ServiceEndpoint endpoint in endpoints)
            {
                if (endpoint.Contract.Name == contractName)
                {
                    return;
                }
            }
            */
            //InstanceContext instanceContext = new InstanceContext(new OheTelnetModuleEventSubscriber());
            string address = "net.tcp://localhost:5500/OheEventManager";
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;
            DuplexChannelFactory<IOheModuleEventContract> factory = new DuplexChannelFactory<IOheModuleEventContract>(binding);
            IOheModuleEventContract proxy = factory.CreateChannel(new EndpointAddress(address));

        }


        private static ServiceEndpoint[] GetEndpoints(string mexAddress)
        {
            if (String.IsNullOrEmpty(mexAddress))
            {
                Debug.Assert(false, "Empty address");
                return null;
            }

            Uri address = new Uri(mexAddress);
            ServiceEndpointCollection endpoints = null;

            if (address.Scheme == "net.tcp")
            {
                TcpTransportBindingElement tcpBindingElement = new TcpTransportBindingElement();
                tcpBindingElement.MaxReceivedMessageSize *= MessageSizeMultiplier;
                endpoints = QueryMexEndpoint(mexAddress, tcpBindingElement);
            }

            return endpoints.ToArray();
        }


        static ServiceEndpointCollection QueryMexEndpoint(string mexAddress, BindingElement bindingElement)
        {
            CustomBinding binding = new CustomBinding(bindingElement);

            MetadataExchangeClient MEXClient = new MetadataExchangeClient(binding);
            MetadataSet metadata = MEXClient.GetMetadata(new EndpointAddress(mexAddress));
            MetadataImporter importer = new WsdlImporter(metadata);
            return importer.ImportAllEndpoints();
        }

    }
}
