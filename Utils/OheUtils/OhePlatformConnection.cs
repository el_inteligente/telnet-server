﻿using System;
using System.IO;
using System.Xml;
using System.Net;
using System.Collections.Generic;
using System.Text;
using Ohe.Net;
using Ohe.Utils;


namespace Ohe.Configuration
{
    /// <summary>
    /// Абстрактный класс, определяет все IP адреса
    /// ,которые соединяются с ядром, при необходимости создает соединение remoting
    /// И проверяет верна ли конфигурация
    /// </summary>
    
    public abstract class OhePlatformConnection
    {

        //protected delegate void OnRbCheckConfig();

        #region protected properties
        static private string _configId = Dns.GetHostName();
        static protected string ConfigId { get { return _configId; } }

        static protected readonly string _configKernelConLockObject = Guid.NewGuid().ToString();

        //protected static event OnRbCheckConfig OnCheckConfigEvent;
        #endregion


        #region Конструктор
        static OhePlatformConnection()
        {
            try
            {
                OheChannelsHelper.RegisterTcpChannel();
                ReloadKernelHostPort();
            }
            catch
            { }
        }
        #endregion


        #region HostName/IP - ядра
        static public readonly string HostName = Dns.GetHostName();

        /// <summary>
        /// Возвращает IP адрес ядра
        /// </summary>
        static public string HostIP
        {
            get
            {
                try
                {
                    return Dns.GetHostEntry(HostName).AddressList[0].ToString();
                }
                catch (Exception e)
                {
                    Utils.Utils.WriteLog(e.ToString());
                    return "Unknown";
                }
            }
        }
        #endregion


        #region OhePlatformHost - имя сервера ядра

        static private string _kernelServiceHost = "localhost"; // Имя сервера ядра
        static public string OhePlatformHost { get { return _kernelServiceHost; } }
        static public string KernelServiceNetworkHost { get { return Dns.Resolve(_kernelServiceHost).HostName.ToString(); } }

        #endregion


        #region OhePlatformServicePort - порт Ядра
        static private int _kernelServicePort = 5500;
        static public int OhePlatformServicePort { get { return _kernelServicePort; } }
        #endregion


        #region SetKernelHostPort - устанавливает IP и порт ядра
        static private readonly string _configConSetLockObject = Guid.NewGuid().ToString();

        static public bool SetKernelHostPort(string host, string port)
        {
            lock (_configConSetLockObject)
            {
                host = Utils.Utils.AllTrim(host);
                int iPort = Utils.Utils.Str2Int(port, 5500);

                string oldHost = _kernelServiceHost;
                int oldPort = _kernelServicePort;

                lock (_configKernelConLockObject)
                {
                    try
                    {
                        _kernelServiceHost = host;
                        _kernelServicePort = iPort;

                        WriteKernelHostPort(host, iPort);
                    }
                    catch (Exception e)
                    {
                        Utils.Utils.WriteLog(e.ToString());

                        _kernelServiceHost = oldHost;
                        _kernelServicePort = oldPort;

                        return false;
                    }
                }

                try
                {
                    //CheckConfigs();

                    return true;
                }
                catch (Exception e)
                {
                    Utils.Utils.WriteLog(e.ToString());

                    lock (_configKernelConLockObject)
                    {
                        _kernelServiceHost = oldHost;
                        _kernelServicePort = oldPort;

                        try { WriteKernelHostPort(oldHost, oldPort); }
                        catch { return false; }
                    }
                    try
                    { //CheckConfigs(); 
                    }
                    catch { }

                    return false;
                }
            }
        }
        #endregion


        #region WriteKernelHostPort - Записывает имя сервера ядра и порт в конфигурацию
        /// <summary>
        /// Записывает имя сервера ядра и порт в конфигурацию
        /// </summary>
        /// <param name="host">Имя сервера</param>
        /// <param name="port">Порт</param>
        private static void WriteKernelHostPort(string host, int port)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(
                @"<?xml version=""1.0""?>
<OhePlatformConnection host=""" + host + @""" port=""" + port.ToString() + @""" />");
            XmlTextWriter writer = new XmlTextWriter(Utils.Utils.getPath() + @"\OhePlatformConnection.xml", null);
            writer.Formatting = Formatting.Indented;
            doc.Save(writer);
            writer.Flush();
            writer.Close();
        }
        #endregion


        #region ReloadKernelHostPort - Обновляет значение порта ядра
        /// <summary>
        /// Обновляет значение порта ядра
        /// </summary>
        static protected void ReloadKernelHostPort()
        {
            // try to load kernel service host and port
            try
            {
                XmlDocument doc = new XmlDocument();

                string path = Utils.Utils.getPath();

                if (!File.Exists(Utils.Utils.getPath() + @"\OhePlatformConnection.xml"))
                {
                    doc.LoadXml(Utils.OheConstant.OHE_PLATFORM_CONSTANT);
                    using (XmlTextWriter writer = new XmlTextWriter(Utils.Utils.getPath() + @"\OhePlatformConnection.xml", null))
                    {
                        writer.Formatting = Formatting.Indented;
                        doc.Save(writer);
                    }
                }
                else
                    doc.Load(Utils.Utils.getPath() + @"\OhePlatformConnection.xml");

                _kernelServiceHost = doc["OhePlatformConnection"].Attributes["host"].Value;
                _kernelServicePort = Utils.Utils.Str2Int(doc["OhePlatformConnection"].Attributes["port"].Value, 5500);

                WriteKernelHostPort(_kernelServiceHost, _kernelServicePort);
            }
            catch (Exception e)
            {
                Utils.Utils.WriteLog(e.ToString());
            }
        }
        #endregion


        #if DEBUG // (comment)

                #region _rbConfig from IRbConfig (comment)
                    /*
                    static protected IRbConfig _rbConfig
                    {
                        get
                        {
                            lock (_configKernelConLockObject)
                            {
                                try
                                {
                                    return (IRbConfig)Activator.GetObject(typeof(IRbConfig)
                                        , "tcp://" + OhePlatformHost + ":" + OhePlatformServicePort.ToString() + "/RbKernelService/RbConfigServer");
                                }
                                catch (Exception e)
                                {
                                    RbLog.Write2ErrorLog(e.ToString());
                                    return null;
                                }
                            }
                        }
                    }
                    */
                    #endregion


                #region IsRbConfigServerAlive (comment)
                    /*
                    public static bool IsRbConfigServerAlive
                    {
                        get
                        {
                            try
                            {
                                return _rbConfig.IsAlive;
                            }
                            catch
                            {
                                return false;
                            }
                        }
                    }
                 */
                    #endregion


                    //public static string KernelRootPath { get { return _rbConfig.RootPath; } }

                    //public static string KernelVersion { get { return _rbConfig.Version; } }

                #region CheckConfigs (comment)
                    /*
                    public static void CheckConfigs()
                    {
                        if (OnCheckConfigEvent != null)
                            OnCheckConfigEvent();
                    }
                 */
                    #endregion


                #region ReloadAllConfigsOnServer (comment)
                    /*
                    public static void ReloadAllConfigsOnServer()
                    {
                        _rbConfig.ReloadAllConfigs();
                        RbLog.LogServerConnection.ReloadLogsConfig();

                        CheckConfigs();
                    }
                 */
                    #endregion


                #region GetConfigClients (comment)
                    /*
                    public static RbConfigClient[] ConfigurationsClients { get { return _rbConfig.GetConnectedClients(); } }
                 */
                    #endregion


                #region XmlConfigSource (comment)
                    /*
                    public static RbXmlConfigSource XmlConfigSource { get { return _rbConfig.XmlConfigSources; } }
                 */
                    #endregion


                #region IsDirty (comment)
                    /*
                    protected static bool IsDirty(RbConfigId cfgId)
                    {
                        try
                        {
                            return _rbConfig.IsDirty(ConfigId, cfgId);
                        }
                        catch
                        {
                            return true;
                        }
                    }
                 */
                    #endregion
                    /*
                    public static ArrayList ClientToolsInfo { get { return _rbConfig.ClientToolsInfo; } }
                    public static byte[] GetClientToolFile(string toolName)
                    {
                        return _rbConfig.GetClientToolFile(toolName);
                    }
                 */
                
        #endif
    }
}
