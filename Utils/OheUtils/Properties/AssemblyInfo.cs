﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Utils")]
[assembly: AssemblyDescription("Utils")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Консид Технологии")]
[assembly: AssemblyProduct("Utils")]
[assembly: AssemblyCopyright("Copyright © Консид Технологии  2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ee14a880-858c-49a3-bb65-db006cfd8d42")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("4.0.1")]
[assembly: AssemblyFileVersion("4.0.1")]
[assembly: AssemblyKeyFile("I:\\Проекты\\ConsID\\Open Source Products\\Source Code\\mykey.snk")]