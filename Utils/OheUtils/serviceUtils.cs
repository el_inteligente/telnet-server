using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Threading;

namespace Ohe.Utils
{
    [System.Reflection.Obfuscation(Exclude = true, ApplyToMembers = true)]
	public class ServiceManager : ServiceController
	{

		#region constants
		const int TIMEOUT = 30;
		const int SLEEP = 1000; //milliseonds
		#endregion


		#region DLLImport - from advapi32.dll

		#region OpenSCManager
		[DllImport("advapi32.dll")]
		public static extern IntPtr OpenSCManager(string lpMachineName, string lpSCDB, int scParameter);
		#endregion

		#region CreateService
		[DllImport("Advapi32.dll")]
		public static extern IntPtr CreateService(IntPtr SC_HANDLE, string lpSvcName, string lpDisplayName,
			 int dwDesiredAccess, int dwServiceType, int dwStartType, int dwErrorControl, string lpPathName,
			 string lpLoadOrderGroup, int lpdwTagId, string lpDependencies, string lpServiceStartName, string lpPassword);
		#endregion

		#region CloseServiceHandle
		[DllImport("advapi32.dll")]
		public static extern void CloseServiceHandle(IntPtr SCHANDLE);
		#endregion

		#region StartService
		[DllImport("advapi32.dll")]
		public static extern int StartService(IntPtr SVHANDLE, int dwNumServiceArgs, string lpServiceArgVectors);
		#endregion

		#region OpenService
		[DllImport("advapi32.dll", SetLastError = true)]
		public static extern IntPtr OpenService(IntPtr SCHANDLE, string lpSvcName, int dwNumServiceArgs);
		#endregion

		#region DeleteService
		[DllImport("advapi32.dll")]
		public static extern int DeleteService(IntPtr SVHANDLE);
		#endregion

		#region GetLastError - from kernel32.dll
		[DllImport("kernel32.dll")]
		public static extern int GetLastError();
		#endregion

		#endregion DLLImport


		#region constructor
		public ServiceManager() : base() { }
		public ServiceManager(string serviceName) : base(serviceName) { }
		public ServiceManager(string serviceName, string machineName) : base(serviceName, machineName) { }
		#endregion

		#region install service
		/// <summary>
		/// This method installs and runs the service in the service conrol manager.
		/// </summary>
		/// <param name="svcPath">The complete path of the service.</param>
		/// <param name="svcName">Name of the service.</param>
		/// <param name="svcDispName">Display name of the service.</param>
		/// <returns>True if the process went thro successfully. False if there was any error.</returns>
		public static bool InstallService(string svcPath, string svcName, string svcDispName)
		{
			#region Constants declaration

			int SC_MANAGER_CREATE_SERVICE = 0x0002;
			int STANDARD_RIGHTS_REQUIRED = 0xF0000;
			int SERVICE_AUTO_START = 0x00000002;
			int SERVICE_WIN32_OWN_PROCESS = 0x00000010;
			int SERVICE_ERROR_NORMAL = 0x00000001;
			int SERVICE_QUERY_CONFIG = 0x0001;
			int SERVICE_CHANGE_CONFIG = 0x0002;
			int SERVICE_QUERY_STATUS = 0x0004;
			int SERVICE_ENUMERATE_DEPENDENTS = 0x0008;
			int SERVICE_START = 0x0010;
			int SERVICE_STOP = 0x0020;
			int SERVICE_PAUSE_CONTINUE = 0x0040;
			int SERVICE_INTERROGATE = 0x0080;
			int SERVICE_USER_DEFINED_CONTROL = 0x0100;

			int SERVICE_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED |
				 SERVICE_QUERY_CONFIG |
				 SERVICE_CHANGE_CONFIG |
				 SERVICE_QUERY_STATUS |
				 SERVICE_ENUMERATE_DEPENDENTS |
				 SERVICE_START |
				 SERVICE_STOP |
				 SERVICE_PAUSE_CONTINUE |
				 SERVICE_INTERROGATE |
				 SERVICE_USER_DEFINED_CONTROL);
			#endregion Constants declaration.

			try
			{
				IntPtr sc_handle = OpenSCManager(null, null, SC_MANAGER_CREATE_SERVICE);

				if (sc_handle.ToInt32() != 0)
				{
					IntPtr sv_handle = CreateService(sc_handle, svcName, svcDispName, SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL, svcPath, null, 0, null, null, null);

					if (sv_handle.ToInt32() == 0)
					{
						CloseServiceHandle(sc_handle);
						return false;
					}
					else
					{
						//now trying to start the service
						int i = StartService(sv_handle, 0, null);
						// If the value i is zero, then there was an error starting the service.
						// note: error may arise if the service is already running or some other problem.
						if (i == 0)
						{
							//Console.WriteLine("Couldnt start service");
							return false;
						}
						//Console.WriteLine("Success");
						CloseServiceHandle(sc_handle);
						return true;
					}
				}
				else
					//Console.WriteLine("SCM not opened successfully");
					return false;

			}
			catch
			{
				return false;
			}
		}
		#endregion

		#region uninstall service
		/// <summary>
		/// This method uninstalls the service from the service conrol manager.
		/// </summary>
		/// <param name="svcName">Name of the service to uninstall.</param>
		public static bool UnInstallService(string svcName)
		{
			try
			{
				//stop service first
				StopService(svcName);

				int GENERIC_WRITE = 0x40000000;
				int SERVICE_DELETE = 0x10000;

				IntPtr sc_hndl = OpenSCManager(null, null, GENERIC_WRITE);

				if (sc_hndl.ToInt32() != 0)
				{
					IntPtr svc_hndl = OpenService(sc_hndl, svcName, SERVICE_DELETE);
					//Console.WriteLine(svc_hndl.ToInt32());
					if (svc_hndl.ToInt32() != 0)
					{
						//int j = ControlService(sv_handle,0,null);

						int i = DeleteService(svc_hndl);
						if (i != 0)
						{
							CloseServiceHandle(svc_hndl);
							CloseServiceHandle(sc_hndl);
							return true;
						}
						else
						{
							CloseServiceHandle(svc_hndl);
							CloseServiceHandle(sc_hndl);
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch
			{
				return false;
			}
		}
		#endregion

		#region start service
		public static void StartService(string svcName)
		{
			try
			{
				ServiceManager sc = new ServiceManager(svcName);

				StartDependendOnService(sc);

				if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) ||
					 (sc.Status.Equals(ServiceControllerStatus.StopPending)))
				{
					// Start the service if the current status is stopped.
					sc.Start();
				}
				else if ((sc.Status.Equals(ServiceControllerStatus.Paused)) ||
					 (sc.Status.Equals(ServiceControllerStatus.PausePending)))
				{
					// Start the service if the current status is paused.
					sc.Continue();
				}

				// Refresh and display the current service status.
				sc.Refresh();

				int start = DateTime.Now.TimeOfDay.Seconds;
				while (!sc.Status.Equals(ServiceControllerStatus.Running) && (DateTime.Now.TimeOfDay.Seconds - start) <= TIMEOUT)
				{
					Thread.Sleep(SLEEP);
					sc.Refresh();
				}

			}
			catch
			{
			}
		}
		#endregion

		#region start Dependent Services
		public static void StartDependentService(string svcName)
		{
			try
			{
				ServiceController sc = new ServiceController(svcName);
				ServiceController[] scDep = sc.DependentServices;
				int n = scDep.Length;
				if (n == 0) return;

				for (int i = 0; i < n; i++)
				{
					StartService(scDep[i].ServiceName);
				}

			}
			catch
			{
			}
		}
		#endregion

		#region start Dependended On Services
		public static void StartDependendOnService(ServiceManager sc)
		{
			try
			{
				ServiceController[] scDep = sc.ServicesDependedOn;
				int n = scDep.Length;
				if (n == 0) return;

				for (int i = 0; i < n; i++)
				{
					string[] status = GetServiceStatus(scDep[i].ServiceName);
					if (string.Compare(status[0], "running", true) != 0)
						StartService(scDep[i].ServiceName);
				}

			}
			catch
			{
			}
		}
		#endregion

		#region check Dependended On Services
		public static string CheckDependendOnService(string svcName)
		{
			try
			{
				string rez = string.Empty;
				ServiceController sc = new ServiceController(svcName);
				ServiceController[] scDep = sc.ServicesDependedOn;
				int n = scDep.Length;
				if (n == 0) return rez;

				for (int i = 0; i < n; i++)
				{
					if (!scDep[i].Status.Equals(ServiceControllerStatus.Running) &&
						 !scDep[i].Status.Equals(ServiceControllerStatus.StartPending))
						rez += Environment.NewLine + scDep[i].DisplayName;
				}
				return rez;

			}
			catch
			{
				return string.Empty;
			}
		}
		#endregion

		#region check Dependent Services
		public static string checkDependentService(string svcName)
		{
			return checkDependentService(svcName, false);
		}
		public static string checkDependentService(string svcName, bool checkStatus)
		{
			try
			{
				string rez = string.Empty;
				ServiceController sc = new ServiceController(svcName);
				ServiceController[] scDep = sc.DependentServices;
				int n = scDep.Length;
				if (n == 0) return rez;

				for (int i = 0; i < n; i++)
				{
					if (!checkStatus || (!scDep[i].Status.Equals(ServiceControllerStatus.Stopped) &&
						 !scDep[i].Status.Equals(ServiceControllerStatus.StopPending)))
						rez += Environment.NewLine + scDep[i].DisplayName;
				}
				return rez;

			}
			catch
			{
				return string.Empty;
			}
		}
		#endregion

		#region Restart service
		public static void RestartService(string svcName)
		{
			try
			{
				StopService(svcName);
				RefreshService(svcName);
				StartService(svcName);
				RefreshService(svcName);
			}
			catch
			{
			}
		}
		#endregion

		#region Pause service
		/// <summary>
		///  toggle between pause or continue
		/// </summary>
		/// <param name="svcName"></param>
		public static void PauseService(string svcName)
		{
			try
			{
				ServiceManager sc = new ServiceManager(svcName);

				if (sc.CanPauseAndContinue)
				{
					if ((sc.Status.Equals(ServiceControllerStatus.Paused)) ||
						 (sc.Status.Equals(ServiceControllerStatus.PausePending)))
					{
						sc.Continue();
					}
					else if ((sc.Status.Equals(ServiceControllerStatus.Running)) ||
						 (sc.Status.Equals(ServiceControllerStatus.StartPending)))
					{
						sc.Pause();
					}

					// Refresh and display the current service status.
					sc.Refresh();
				}
			}
			catch
			{
			}
		}
		#endregion

		#region Stop Service
		public static void StopService(string svcName)
		{
			try
			{
				ServiceManager sc = new ServiceManager(svcName);

				if (sc.Status == ServiceControllerStatus.Stopped)
					return;

				int start = DateTime.Now.TimeOfDay.Seconds;
				while (!sc.CanStop && (DateTime.Now.TimeOfDay.Seconds - start) <= TIMEOUT)
				{
					Thread.Sleep(SLEEP);
					sc.Refresh();
				}
				if (sc.CanStop) sc.Stop();
				else return;

				// Refresh and display the current service status.
				sc.Refresh();

				start = DateTime.Now.TimeOfDay.Seconds;
				while (!sc.Status.Equals(ServiceControllerStatus.Stopped) && (DateTime.Now.TimeOfDay.Seconds - start) <= TIMEOUT)
				{
					Thread.Sleep(SLEEP);
					sc.Refresh();
				}
				sc.Refresh();

			}
			catch
			{
			}
		}
		#endregion

		#region refresh service
		public static void RefreshService(string svcName)
		{
			try
			{
				ServiceManager sc = new ServiceManager(svcName);
				sc.Refresh();
			}
			catch
			{
			}
		}

		#endregion

		#region get Service Status
        /// <summary>
        /// ���������� ������
        /// [0] - ������� ������
        /// [1] - ��� ������� (��� ����� ������������)
        /// [2] - �����
        /// [3] = 1, ���� ������ �������� ��� ��������
        /// [4] = 1, ���� ������ ����� ���������� � ����� ���������
        /// [5] = 1, ���� ������ ����� ����������
        /// </summary>
        /// <param name="svcName"></param>
        /// <returns></returns>
		public static string[] GetServiceStatus(string svcName)
		{
			string[] rez = new string[] { "", "", "", "", "", "" };
			try
			{
				//ServiceInstaller si = new ServiceInstaller();

				ServiceManager sc = new ServiceManager(svcName);

				switch (sc.Status)
				{
					case ServiceControllerStatus.ContinuePending:
						rez[0] = ServiceControllerStatus.ContinuePending.ToString().Trim();
						break;
					case ServiceControllerStatus.Paused:
						rez[0] = ServiceControllerStatus.Paused.ToString().Trim();
						break;
					case ServiceControllerStatus.PausePending:
						rez[0] = ServiceControllerStatus.PausePending.ToString().Trim();
						break;
					case ServiceControllerStatus.Running:
						rez[0] = ServiceControllerStatus.Running.ToString().Trim();
						rez[3] = "1";
						break;
					case ServiceControllerStatus.StartPending:
						rez[0] = ServiceControllerStatus.StartPending.ToString().Trim();
						rez[3] = "1";
						break;
					case ServiceControllerStatus.Stopped:
						rez[0] = ServiceControllerStatus.Stopped.ToString().Trim();
						break;
					case ServiceControllerStatus.StopPending:
						rez[0] = ServiceControllerStatus.StopPending.ToString().Trim();
						break;
					default:
						rez[0] = "unknown";
						break;
				}
				rez[1] = sc.DisplayName.ToString().Trim();
				rez[4] = (sc.CanPauseAndContinue ? "1" : "0");
				rez[5] = (sc.CanStop ? "1" : "0");


			}
			catch
			{
				rez[0] = "not installed!";
			}
			return rez;

		}
		#endregion

		#region commented code just for backup
		/*
		/// <summary>
		/// This method uninstalls the service from the service conrol manager.
		/// </summary>
		/// <param name="svcName">Name of the service to uninstall.</param>
		public bool StartService(string svcName)
		{
			int GENERIC_WRITE = 0x40000000;
			IntPtr sc_hndl = OpenSCManager(null,null,GENERIC_WRITE);
 
			if(sc_hndl.ToInt32() !=0)
			{
				int START = 0x0010;
				IntPtr svc_hndl = OpenService(sc_hndl,svcName,START);
				//Console.WriteLine(svc_hndl.ToInt32());
				if(svc_hndl.ToInt32() !=0)
				{ 
					int i = StartService(svc_hndl,0,null);
					// If the value i is zero, then there was an error starting the service.
					// note: error may arise if the service is already running or some other problem.
					if(i==0)
					{
						//Console.WriteLine("Couldnt start service");
						return false;
					}
					//Console.WriteLine("Success");
					CloseServiceHandle(svc_hndl);
					return true;

				}
				else
					return false;
			}
			else
				return false;
		}

		public bool StopService(string svcName)
		{
			int GENERIC_WRITE = 0x40000000;

			#region definitions
			
			int STANDARD_RIGHTS_REQUIRED = 0xF0000;
			int SC_MANAGER_CONNECT             = 0x0001;
			int SC_MANAGER_CREATE_SERVICE      = 0x0002;
			int SC_MANAGER_ENUMERATE_SERVICE   = 0x0004;
			int SC_MANAGER_LOCK                = 0x0008;
			int SC_MANAGER_QUERY_LOCK_STATUS   = 0x0010;
			int SC_MANAGER_MODIFY_BOOT_CONFIG  = 0x0020;

			int SC_MANAGER_ALL_ACCESS           = (STANDARD_RIGHTS_REQUIRED      | 
				SC_MANAGER_CONNECT            | 
				SC_MANAGER_CREATE_SERVICE     |
				SC_MANAGER_ENUMERATE_SERVICE  |
				SC_MANAGER_LOCK               |
				SC_MANAGER_QUERY_LOCK_STATUS  |
				SC_MANAGER_MODIFY_BOOT_CONFIG);
				
			#endregion

			IntPtr sc_hndl = OpenSCManager(null,null,GENERIC_WRITE);
 
			if(sc_hndl.ToInt32() !=0)
			{
				int SERVICE_STOP         = 0x0020;
				int SERVICE_QUERY_STATUS = 0x0004;
				IntPtr svc_hndl = OpenService(sc_hndl,svcName,SERVICE_STOP | SERVICE_QUERY_STATUS );
				//Console.WriteLine(svc_hndl.ToInt32());
				if(svc_hndl.ToInt32() !=0)
				{ 
					int SERVICE_CONTROL_STOP = 0x00000001;
					int i = ControlService(svc_hndl,SERVICE_CONTROL_STOP,null);
					// If the value i is zero, then there was an error starting the service.
					// note: error may arise if the service is already running or some other problem.
					if(i==0)
					{
						MessageBox.Show("stop service failed");

						//Console.WriteLine("Couldnt start service");
						return false;
					}
					//Console.WriteLine("Success");
					CloseServiceHandle(svc_hndl);
					MessageBox.Show("stop service suceed");

					return true;

				}
				else
				{
					MessageBox.Show("OpenService failed");
					return false;
				}
			}
			else
			{
				MessageBox.Show("OpenSCManager failed!");
				return false;
			}
		}

		public bool QueryServiceStatus(string svcName)
		{
			//int GENERIC_WRITE = 0x40000000;
			int SC_MANAGER_QUERY_LOCK_STATUS   = 0x0010;

			IntPtr sc_hndl = OpenSCManager(null,null,SC_MANAGER_QUERY_LOCK_STATUS);
 
			if(sc_hndl.ToInt32() !=0)
			{
				int SERVICE_QUERY_STATUS  =   0x0004;
				IntPtr svc_hndl = OpenService(sc_hndl,svcName,SERVICE_QUERY_STATUS);
				//Console.WriteLine(svc_hndl.ToInt32());
				if(svc_hndl.ToInt32() !=0)
				{ 
					int i = QueryServiceStatus(svc_hndl,null);
					// If the value i is zero, then there was an error starting the service.
					// note: error may arise if the service is already running or some other problem.
					if(i==0)
					{
						MessageBox.Show("invalid service");
						return false;
					}
					else
					{
						MessageBox.Show(i.ToString().Trim());
					}

					//Console.WriteLine("Success");
					CloseServiceHandle(svc_hndl);
					return true;

				}
				else
					return false;
			}
			else
				return false;
		}
		*/
		#endregion

	}
}

