﻿using System;
using System.Collections;
using System.Runtime.Serialization;

namespace Ohe.Utils
{
    /// <summary>
    /// Описывает базовый класс пользователя
    /// </summary>
    [DataContract()]
    public class OheUser
    {

        #region Properties
        private string _password;
        private string _ipAddress;

        #region UserId - ID пользователя
        private string _userId;
        /// <summary>
        /// Уникальный идентификатор пользователя (Login)
        /// </summary>
        [DataMember]
        public string UserId { get { return _userId; } set { _userId = value; } }
        #endregion


        #region SessionID - идентификатор сессии
        private string _sessionId;
        /// <summary>
        /// Id сессии
        /// </summary>
        [DataMember]
        public string SessionID { get { return _sessionId; } set { _sessionId = value; } }
        #endregion


        #region IpAddress
        /// <summary>
        /// IP адрес пользователя
        /// </summary>
        [DataMember]
        public string IpAddress { get { return _ipAddress; } set { _ipAddress = value; } }
        #endregion


        #region Permission - привилегия пользователя
        private string _permiss;
        /// <summary>
        /// Привилегия пользователя.
        /// </summary>
        [DataMember]
        public string Permission { get { return _permiss; } set { _permiss = Utils.AllTrim(value); } }
        #endregion


        #region ConnectionType - тип соединения
        private OheConnectionType _connectionType;
        /// <summary>
        /// Тип подключения
        /// </summary>
        [DataMember]
        public OheConnectionType ConnectionType { get { return _connectionType; } set { _connectionType = value; } }
        #endregion


        #region UserLanguage - язык пользователя
        private OheLanguage _userLang = OheLanguage.Undefined;
        /// <summary>
        /// Язык пользователя
        /// </summary>
        [DataMember]
        public OheLanguage UserLanguage { get { return _userLang; } set { _userLang = value; } }
        #endregion


        #region Password
        /// <summary>
        /// Пароль
        /// </summary>
        [DataMember]
        public string Password { get { return _password; } set { _password = value; } }

        #endregion


        private DateTime _lastActivityDate = DateTime.Now;
        [DataMember]
        public DateTime LastActivityDate { get { return _lastActivityDate; } set { _lastActivityDate = value; } }



        private DateTime _connectDate = DateTime.Now;
        [DataMember]
        public DateTime ConnectedDate { get { return _connectDate; } set { _connectDate = value; } }

        private int _moduleId;
        [DataMember]
        public int ModuleId { get { return _moduleId; } set { _moduleId = value; } }
        #endregion


        #region Constructor
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="ip"></param>
        public OheUser(string login, string password, string ip)
        {
            _userId = Utils.AllTrim(login);
            _password = password;
            _ipAddress = Utils.AllTrim(ip);
        }


        public OheUser(string Login, string Permiss)
        {
            _userId = Login;
            _permiss = Permiss;
            _password = string.Empty;
        }

        #endregion
        

        #region LastState
        private string _lastState = string.Empty;
        /// <summary>
        /// Последний шаг
        /// </summary>
        public string LastState { get { return _lastState; } set { _lastState = Utils.AllTrim(value); } }
        #endregion


        #region SysUserParams

        private Hashtable _sysParams = new Hashtable(new OheCultureComparer());
        public Hashtable SysUserParams { get { return _sysParams; } }

        #endregion

    }
}
