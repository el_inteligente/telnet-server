using System;
using System.Configuration;
using System.Collections;
using System.Globalization;


namespace Ohe.Utils
{
    /// <summary>
    /// Summary description for QualityComparer
    /// </summary>
    [Serializable]
    public class OheCultureComparer : IEqualityComparer
    {
        public CaseInsensitiveComparer myComparer;

        public OheCultureComparer()
        {
            myComparer = CaseInsensitiveComparer.DefaultInvariant;
        }

        public OheCultureComparer(CultureInfo myCulture)
        {
            myComparer = new CaseInsensitiveComparer(myCulture);
        }

        public new bool Equals(object x, object y)
        {
            if (myComparer.Compare(x, y) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetHashCode(object obj)
        {
            return obj.ToString().ToLower().GetHashCode();
        }
    }
}