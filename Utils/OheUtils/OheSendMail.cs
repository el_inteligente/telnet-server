﻿using System;
//using System.Web.Mail;
using System.Net.Mail;
using Ohe.Web.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ohe.Utils
{
    public sealed class OheSmtpMail
    {
        private OheSmtpMail()   { }

        static private string syncObject = Guid.NewGuid().ToString();

        public static bool SendIncident(string subject, string message, out string errors)
        {
            string smtpServer = OheWebConfig.SmtpServer;
            string incedentMailAccount = OheWebConfig.SupportEmail;
            string adminMailAccount = OheWebConfig.AdminMail;

            if (smtpServer.Length == 0)
                smtpServer = "smtp.timeweb.ru";

            if (incedentMailAccount.Length == 0)
                incedentMailAccount = "support@konsid.ru";

            if (adminMailAccount.Length == 0)
                adminMailAccount = "info@konsid.ru";

            lock (syncObject)
            {
                try
                {
                    SmtpClient client = new SmtpClient(smtpServer);
                    client.Send(adminMailAccount, incedentMailAccount, subject, message);
                    errors = string.Empty;
                    return true;
                }
                catch (Exception e)
                {
                    errors = e.ToString();
                    return false;
                }
            }
        }

        /*
        static public string SendMail(string toAddress, string fromAddress, string subject, string body, string[] attachedFiles)
        {
            return SendMail(RbXmlConfig.smtpConStr, toAddress, fromAddress, subject, body, attachedFiles);
        }
        */

        static public string SendMail(string smtpServer, string toAddress, string fromAddress, string subject, string body, string[] attachedFiles)
        {
            try
            {
                MailMessage Message = new MailMessage();
                Message.To.Add(toAddress);
                Message.From = new MailAddress(fromAddress);
                Message.Subject = subject;
                Message.Body = body;

                // Build an IList of mail attachments using the files named in the string.
                foreach (string sSubstr in attachedFiles)
                {
                    Message.Attachments.Add(new Attachment(sSubstr));
                }

                lock (syncObject)
                {
                    SmtpClient client = new SmtpClient(smtpServer);
                    client.Send(Message);
                    //SmtpMail.SmtpServer = smtpServer;
                    //SmtpMail.Send(Message);
                }
                return string.Empty;
            }
            catch (Exception e)
            {
                Utils.WriteWebLog("Ошибка отправки сообщения " + toAddress + Environment.NewLine + e.Message);
                return "Ошибка отправки сообщения " + toAddress + "," + e.Message;
            }

        }

        static public bool SendSchedulerMail(string smtpServer, string toAddress, string fromAddress, string CC, int Port, string subject, string body, string[] attachedFiles)
        {
            try
            {
                MailMessage Message = new MailMessage();
                Message.To.Add(toAddress);
                Message.From = new MailAddress(fromAddress);
                if(!string.IsNullOrEmpty(CC))
                    Message.CC.Add(CC);
                Message.Subject = subject;
                Message.Body = body;

                // Build an IList of mail attachments using the files named in the string.
                if (attachedFiles != null && attachedFiles.Length > 0)
                    foreach (string sSubstr in attachedFiles)
                        Message.Attachments.Add(new Attachment(sSubstr));


                lock (syncObject)
                {
                    SmtpClient client = new SmtpClient(smtpServer, (Port > 0 ? Port : 25));
                    client.Credentials = new System.Net.NetworkCredential("d.rybakov@konsid.ru", "HJSPASS#1");

                    client.Send(Message);

                    //SmtpMail.SmtpServer = smtpServer;
                    //SmtpMail.Send(Message);
                }

                return true;
            }
            catch (Exception e)
            {
                Utils.WriteLog("Ошибка отправки сообщения " + toAddress + Environment.NewLine + e.Message);
                //return "Ошибка отправки сообщения " + toAddress + "," + e.Message;
                throw e;
            }

        }


        static public bool SendSchedulerMail(SmtpClient client, string toAddress, string fromAddress, string CC, string subject, string body, string[] attachedFiles)
        {
            try
            {
                MailMessage Message = new MailMessage();
                Message.To.Add(toAddress);
                Message.From = new MailAddress(fromAddress);
                if (!string.IsNullOrEmpty(CC))
                    Message.CC.Add(CC);
                Message.Subject = subject;
                Message.Body = body;

                // Build an IList of mail attachments using the files named in the string.
                if (attachedFiles != null && attachedFiles.Length > 0)
                    foreach (string sSubstr in attachedFiles)
                        Message.Attachments.Add(new Attachment(sSubstr));


                lock (syncObject)
                {
                    client.Send(Message);
                }

                return true;
            }
            catch (Exception e)
            {
                Utils.WriteLog("Ошибка отправки сообщения " + toAddress + Environment.NewLine + e.Message);
                //return "Ошибка отправки сообщения " + toAddress + "," + e.Message;
                throw e;
            }

        }

/*
        static public bool SendSchedulerMail(string smtpServer, string toAddress, string fromAddress, string subject, string body, string[] attachedFiles)
        {
            try
            {
                MailMessage Message = new MailMessage();
                Message.To = toAddress;
                Message.From = fromAddress;
                Message.Subject = subject;
                Message.Body = body;

                // Build an IList of mail attachments using the files named in the string.
                foreach (string sSubstr in attachedFiles)
                {
                    MailAttachment myAttachment = new MailAttachment(sSubstr);
                    Message.Attachments.Add(myAttachment);
                }

                lock (syncObject)
                {
                    SmtpMail.SmtpServer = smtpServer;
                    SmtpMail.Send(Message);
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.WriteLog("Ошибка отправки сообщения " + toAddress + Environment.NewLine + e.Message);
                //return "Ошибка отправки сообщения " + toAddress + "," + e.Message;
                throw e;
            }

        }
        */

    }
}
