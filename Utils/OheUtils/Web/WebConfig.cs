//#define OPENWEB
//#define OPENWEBDEMO
using System;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Xml;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ohe.Utils;

namespace Ohe.Web.Configuration
{
	/// <summary>
	/// ��������� ������������ ����������
	/// </summary>
    //
	public class OheWebConfig
    {
        #region Private Variables
        /// <summary>
        /// ���������� ���������� ������������ � xml 
        /// ������ ���������� ����� xml ����� ������������ ����
        /// </summary>
        private static string _menuConfig;

        private static string _crystallPath;

        private static int _timeout = -1;

        private static int _pageRows = -1;

        private static string _connectionstring;

        //private static XmlDocument _configuration = null; // ���� ������������ ��� ����������

        private static Hashtable _htCheckColumns = new Hashtable(new OheCultureComparer());

        private static Hashtable _webFormConfig = new Hashtable(new OheCultureComparer());

        private static string _formPath;

        private static string _dateFormat = "dd.MM.yyyy";

        private static string _dbType = "sql";

        private static bool _reloadButton = false;

        private static bool _excellButton = false;

        private static bool _menuAsButton = false;

        private static bool _dounloadMenu = false;

        private static int _maxRows = 5000;

        private static string _supportEmail = "support@konsid.ru";

        private static string _loginScript = string.Empty;

        private static string _smtpServer = string.Empty;

        private static string _adminMail = string.Empty;

        private static bool _isUseFastReport = false;
        
        #endregion
        

        #region Properties

        #region MenuXmlConfig - ���������� ���� �� ������������
		/// <summary>
		/// ���������� ���������� xml
		/// </summary>
        public static string MenuXmlConfig { get { return _menuConfig; } }
		#endregion


        #region getTreeViewSource - ���������� ����� � ���� ��� ������ ��������
        /// <summary>
        /// ���������� ���������� ������ ��� ���������
        /// </summary>
        public static string getTreeViewSource { get { return Utils.Utils.webconfigPath + "treeview/"; } }
        #endregion


        #region CrystallPath - ����� ������� Crystall Reports
        public static string CrystallPath { get { return Utils.Utils.webconfigPath + @"reports\"; } }
		#endregion


		#region SessionTimeOut - timeuot ������
		/// <summary>
		/// ���������� timeout ������ ��� ������������ �� ������������
		/// </summary>
        public static int SessionTimeOut { get { return _timeout; } }
		#endregion


		#region PageRows - ���������� ����� � �������
		/// <summary>
		/// ���������� ���������� ����� ��� ����� �� ����� ��������
		/// </summary>
        public static int PageRows { get { return _pageRows; } }
		#endregion


        #region ConnectionString - ������ c��������� � ����� ������
		/// <summary>
		/// ������ ����������  ����� ������
		/// </summary>
        public static string ConnectionString { get { return _connectionstring; } }
		#endregion
        

        /// <summary>
        /// ��� ���� ������
        /// </summary>
        public static string DbType { get { return _dbType; } }


        public static int MaxRows { get { return _maxRows; } }


        public static string DateFormat { get { return _dateFormat; } }


        public static string SupportEmail { get { return _supportEmail; } }

        
        public static bool IsReloadButton { get { return _reloadButton; } }


        public static bool IsShowExcelButton { get { return _excellButton; } }


        public static bool IsMenuAsButton { get { return _menuAsButton; } }


        public static bool IsShowDownLoadButton { get { return _dounloadMenu; } }


        public static bool IsUseFastReports { get { return _isUseFastReport; } }


        public static string LoginScript { get { return _loginScript; } }
        
        public static string SmtpServer { get { return _smtpServer; } }
        
        public static string AdminMail { get { return _adminMail; } }

        #endregion


        #region Constructor

        static OheWebConfig()
        {
            ConfigureAppSettings();
            getWebmenuConfig();
        }

        #endregion


        #region Private Methods

        private static void getWebmenuConfig()
        {
            XmlTextReader tr = new XmlTextReader(Utils.Utils.webconfigPath + @"webmenu.xml");
            XmlDocument d = new XmlDocument();
            d.Load(tr);
            _menuConfig = d.InnerXml;
            tr.Close();
        }

        #region getXmlConfigFile - ���������� ���������� ������������
        /*
        private static void getXmlConfigFile()
        {
            using (XmlTextReader tr = new XmlTextReader(Utils.Utils.webconfigPath + @"Ohewebconfig.xml"))
            {
                XmlDocument d = new XmlDocument();
                d.Load(tr);

                if (d != null)
                    _configuration = d;
                else
                    _configuration = new XmlDocument();
            }
        }
        */
        #endregion


        #region getCheckColumns - ���������� ����������������� ��������� ����� �������
        /// <summary>
        /// ��������� ������� �� ��������� CheckBox ��� ���������������� �������
        /// �����
        /// </summary>
        private static void getCheckColumns()
        {
            _htCheckColumns = new Hashtable(new OheCultureComparer());

            XmlTextReader tr = new XmlTextReader(Utils.Utils.webconfigPath + @"OhewebDataConfig.xml");
            XmlDocument d = new XmlDocument();
            d.Load(tr);

            XmlNodeList root = d.GetElementsByTagName("checkcolumns");

            if (root != null)
            {
                foreach (XmlNode checks in root)
                {
                    if (checks.HasChildNodes)
                    {
                        XmlNodeList goodNode = checks.ChildNodes;
                        foreach (XmlNode node in goodNode)
                        {
                            if (Utils.Utils.AllTrim(node.Name) == "checkcolumn")
                            {
                                XmlAttribute attrTable = node.Attributes["table"];
                                XmlAttribute attrColumn = node.Attributes["column"];

                                if (attrColumn != null && attrColumn != null)
                                {
                                    string _table = Utils.Utils.AllTrim(attrTable.Value);
                                    string _column = Utils.Utils.AllTrim(attrColumn.Value);

                                    if (!_htCheckColumns.ContainsKey(_table + _column))
                                        _htCheckColumns.Add(_table + _column, _table + _column);
                                }
                            }
                        }
                    }
                }
            }
            tr.Close();
        }
        #endregion

        /// <summary>
        /// ���������� ��� ���������� ������������ system
        /// </summary>
        private static void ConfigureAppSettings()
        {
            XmlTextReader xml = new XmlTextReader(Utils.Utils.webconfigPath + @"Ohewebconfig.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(xml);
            XmlNodeList d = doc.GetElementsByTagName("configuration");

            foreach (XmlNode c in d)
            {
                XmlNodeList goodNode = c.ChildNodes;
                foreach (XmlNode child in goodNode)
                {
                    if (child.NodeType == XmlNodeType.Element)
                        switch (Utils.Utils.AllTrim(child.Attributes["key"].Value.ToLowerInvariant()))
                        {
                            #region Switch

                            #region String values
                            case "connectionstring":
                                _connectionstring = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "dateformat":
                                _dateFormat = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            case "dbtype":
                                _dbType = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;

                            case "supportemail":
                                _supportEmail = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;

                            case "loginscript":
                                _loginScript = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;

                            case "smtpserver":
                                _smtpServer = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;

                            case "adminmail":
                                _smtpServer = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;

                            //adminmail   

                            /*
                            case "crystallpath":
                                _crystallPath = Utils.Utils.AllTrim(child.Attributes["value"].Value);
                                break;
                            */


                            #endregion


                            #region Int Values
                            case "pagerows":
                                int.TryParse(child.Attributes["value"].Value, out _pageRows);
                                break;
                            case "timeout":
                                int.TryParse(child.Attributes["value"].Value, out _timeout);
                                break;

                            case "maxrows":
                                int.TryParse(child.Attributes["value"].Value, out _maxRows);
                                break;
                            #endregion


                            #region Bool Values
                            case "showreloadbtn":
                                _reloadButton = Utils.Utils.StrToBool(Utils.Utils.AllTrim(child.Attributes["value"].Value));
                                break;

                            case "showexcelbutton":
                                _excellButton = Utils.Utils.StrToBool(Utils.Utils.AllTrim(child.Attributes["value"].Value));
                                break;

                            case "showmenuasbuttons":
                                _menuAsButton = Utils.Utils.StrToBool(Utils.Utils.AllTrim(child.Attributes["value"].Value));
                                break;

                            case "usefastreports":
                                _isUseFastReport = Utils.Utils.StrToBool(Utils.Utils.AllTrim(child.Attributes["value"].Value));
                                break;

                            case "showdownloadbutton":
                                _dounloadMenu = Utils.Utils.StrToBool(Utils.Utils.AllTrim(child.Attributes["value"].Value));
                                break;



                            #endregion

                            #endregion
                        }
                }
            }
            xml.Close();
        }

        #endregion


        #region Public Methods

        #region GetFromXmlConfig - ���������� �������� �� ����� �� ������������
        /// <summary>
		/// ��������� �������� ������������ �� ����� 
		/// ���� ������������, �������� (Ohewebconfig.xml)
		/// </summary>
		/// <param name="ConfigKey"></param>
		/// <returns></returns>
        /*
		public static string GetFromXmlConfig(string ConfigKey)
		{
			if (_configuration == null)
				getXmlConfigFile();

			XmlNode root = _configuration["configuration"];

			foreach (XmlNode node in root.ChildNodes)
			{
				if (node.Attributes["key"].Value == ConfigKey)
				{
					return node.Attributes["value"].Value;
				}

			}
			return string.Empty;
		}
        */
		#endregion


        #region GetCheckColumnsById - ������ ��������� ���������������� ������� ���� checkbox
        public static bool GetCheckColumnsById(string Key)
		{
			if (_htCheckColumns.Count == 0)
				getCheckColumns();

			if (_htCheckColumns.Contains(Key))
				return true;
			else
				return false;
		}
		#endregion


		#region GetFormXmlById - ���������� ����� �� ��������������
		/// <summary>
		/// ���������� ���������� ����� �� ��������������
		/// 
		/// ���������� xml ���� ������ �� ������������. 
		/// �������� ����� ������� ������ ����� � ����� �����,
		/// ��� ��� ����� � ����� �����. 
		/// ������������� - ���������� ��� �����. - ������� ������������� ����� �����
		/// �������� ���� ��� � ������ ��������� � ����. 
		/// �������� ������������ ������ ��� ��� �������
		/// ���� ������������ �������� (Ohewebforms.xml)
		/// </summary>
		/// <param name="formId">������������� �����</param>
		/// <returns></returns>
		public static string GetFormXmlById(string formId)
		{
            #if OPENWEB && OPENWEBDEMO
            int _maxForms = 1;
            #endif

			if (_webFormConfig.Count == 0)
            {
                #region ������ ���� �� ������������
                if (_formPath == null)
					_formPath = Utils.Utils.formPath;

                /*
                ResourceManager rm = new ResourceManager("Utils.Properties.Resources",
                              Assembly.GetExecutingAssembly());

                string _100 = rm.GetString("_100");
                */

                try
                {

				DirectoryInfo d = new DirectoryInfo(_formPath);
				foreach (FileInfo f in d.GetFiles())
				{
					#region ��������� ������ � �������
					try
					{
						string _name = f.Name.Substring(0, f.Name.LastIndexOf("."));
						if (!_webFormConfig.Contains(_name) && f.Extension.ToLowerInvariant() == ".xml")
						{
							XmlDocument doc = new XmlDocument();
							doc.Load(f.FullName);

							_webFormConfig.Add(_name, doc.InnerXml);
						}
					}
					catch (Exception)
					{
					}
					#endregion

#if OPENWEB && OPENWEBDEMO
                    if (_maxForms > 50)
                        break;
                    else
                        _maxForms++;
#endif
                }

                }
                catch (Exception)
                {

                    throw;
                }

                #endregion
            }

			if (_webFormConfig.Contains(formId))
				return (string)_webFormConfig[formId];
			else
				return string.Empty;
		}
		#endregion


		#region ClearWebForms - ������� �� �����
		public static void ClearWebForms()
		{
            _webFormConfig = new Hashtable(new OheCultureComparer());
		}
		#endregion


		#region ClearTreeView - ������� ���� ������������
		public static void ClearTreeView()
		{
			_menuConfig = null;
		} 
		#endregion


		#region ReloadConfig - ������������ ������������ �������������
		public static void ReloadConfig()
		{
			_connectionstring = null;
			ClearTreeView();
			ClearWebForms();

            ConfigureAppSettings();
            getWebmenuConfig();

            GetFormXmlById(string.Empty);

			//string rez = OheWebConfig.MenuXmlConfig;
            //OheWebConfig.GetFromXmlConfig("");
						
		}
		#endregion


		#region ���������� email support
		/// <summary>
		/// ���������� email ������������ - ������ �� ����������������� �����
		/// Ohewebconfig.xml
		/// </summary>
		/// <returns></returns>
		public static string GetSupportMail()
		{
			return "rybakov.dm@gmail.com";
		}
		
		#endregion


		#region Translate - ������� ��������
		/// <summary>
		/// ���������� ������� ���������
		/// �������� ����� ������������� ���� ������������ ��� ��������.
		/// �������� ��� ��������� - ����, ��������, ����.
		/// ����� ��������� ������� �������, ������, ���������� � �.�.
		/// ��������� �������� ����� Ohewebtranslate.xml
		/// (�� ��������)
		/// </summary>
		/// <param name="key">����</param>
		/// <returns></returns>
		public static string Translate(string key)
		{
			return key;
		}
		#endregion


		#region Translate - ������� �������� � ������� ������
		/// <summary>
		/// ���������� ������� � ������� ������
		/// ������ ����.
		/// </summary>
		/// <param name="key">����</param>
		/// <param name="lang">����</param>
		/// <returns></returns>
		public static string Translate(string key, OheLanguage lang)
		{
			return Translate(key);
		}
		#endregion


		#region Language - ������ ��������� ������
		/// <summary>
		/// ���������� ������ �������� ������ �� ������������
		/// ����� �� �������� � �����������, � ������ ������
		/// </summary>
		/// <returns></returns>
		public static string[] Language()
		{
			return new string[3] { "Russian", "English", "Test" };
		}
		#endregion


		#region GetQueryById - ���������� ������ �� ��������������
		/// <summary>
		/// ���������� ������ �� ��������������.
		/// �������� ����� ������������� ���� ������������ � ������� �������
		/// ������� ��� ������������� ���������.
		/// �������� ��� ��������� - ������������� �������, ��� ����������.
		/// 
		/// ��� ����� ������������ ��� ������������� ���������� � ����������
		/// ������ ������.
		/// 
		/// ������ ��������� ��� ������ ��� ��� �������, ������ �������� ���
		/// ������� ���������� � ����� ��������� � ������.
		/// 
		/// ��������� ��� ����� ������������
		/// Ohewebsql.xml
		/// </summary>
		/// <param name="id">������������� �������</param>
		/// <param name="type">��� ���������� � ����� ������</param>
		/// <returns></returns>
		public static string GetQueryById(string id, int type)
		{
			return string.Empty;
		}
		#endregion


		#region GetAnhorConfig - ������� � ������� ��� �������
		/// <summary>
		/// �������� ���� ����� ������� ������ ������ �� ����� � ������������.
		/// ���� ���������������� �����, ������� ��������� �� ������.
		/// 
		/// ��� ���� ��� ������������.
		/// �������� ���� ����� �� ������, ���������� ��� ������� � ���.
		/// �������� ��� ������ order_id 
		/// ����� � ������ �������, ������� ����� ��� ������� ������������� �����������
		/// ������ �� ����� �� ������.
		/// 
		/// ��� ����� ���������� ��������� � ������������ ���-�� ���� <order_id value="12?order_id=">
		/// �.�. ������ �� ������������� ������ �� ������.
		/// �� ��������
		/// 
		/// ������������ ���� ������ ��� ������������ �������,
		/// ���������� ��� ������� � ������ �� �����.
		/// 
		/// �������� � ������������ ��� ������ ���������� � ������ �������� �� �������
		/// </summary>
		/// <returns></returns>
		public static Hashtable GetAnhorConfig()
		{
			return null;
		}
		
		#endregion


        #endregion

    }

}