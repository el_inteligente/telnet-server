using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Ohe.Utils;

namespace Ohe.Web.Configuration
{
	/// <summary>
	/// ��������� ������� ������ ��� �����
	/// </summary>
    
	public class RbColumn2AnchorConfig
	{
		#region Constructor
		//static RbColumn2AnchorConfig(){}
		public RbColumn2AnchorConfig() { }
		#endregion

		#region Properties
        private Hashtable _sourceColId = new Hashtable(new OheCultureComparer());
		/// <summary>
		/// ������ ������� �� ������� �������� ��������
		/// </summary>
		public Hashtable SourceColId { get { return _sourceColId; } set { _sourceColId = value; } }

		private string _link = string.Empty;
		/// <summary>
		/// ������ �� ����� ���� ���� ����������
		/// </summary>
		public string Link { get { return _link; } set { _link = Utils.Utils.AllTrim(value); } }

		private string _header = string.Empty;
		/// <summary>
		/// ���������
		/// </summary>
		public string Header { get { return _header; } set { _header = Utils.Utils.AllTrim(value); } }

		private bool _isPopup = false;
		/// <summary>
		/// ������� �������� � ����� ����
		/// </summary>
		public bool IsPopup { get { return _isPopup; } set { _isPopup = value; } }

		private bool _isExists = false;
		/// <summary>
		/// ������� �������������
		/// </summary>
		public bool IsExists { get { return _isExists; } set { _isExists = value; } }

        private string _event = "submit";
        /// <summary>
        /// ������� ������� ������������ ��� �����
        /// </summary>
        public string Event { get { return _event; } set { _event = value; } }

        private string _alt = string.Empty;
        /// <summary>
        /// ��������� ��� ������ �� �����
        /// </summary>
        public string Alt { get { return _alt; } set { _alt = value; } }
        		
		#endregion

		#region �����������
		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="sourceColId">������ ������� ��� ������</param>
		/// <param name="link"></param>
		/// <param name="isPopup"></param>
		/// <param name="header"></param>
		public RbColumn2AnchorConfig(Hashtable sourceColId, string link, bool isPopup, string header)
		{
			_link = Utils.Utils.AllTrim(link);
			_isPopup = isPopup;
			_isExists = true;
			_sourceColId = sourceColId;
			_header = Utils.Utils.AllTrim(header);

			/*
			tables = Utils.Utils.AllTrim(tables);

			if (tables.Length > 0)
			{
				string[] tbls = tables.Split(',');
				foreach (string table in tbls)
				{
					string tableName = Utils.Utils.AllTrim(table).ToLower();
					if (tableName.Length > 0)
						_tables.Add(tableName);
				}
			}
			*/
		}
		#endregion

		#region CreateFromXml - ��������� ����� �� xml  (not use)
		[Obsolete("�� ���������� ��� ��������� �� �������!!!")]
		public static RbColumn2AnchorConfig CreateFromXml(XmlNode columnNode)
		{
			RbColumn2AnchorConfig rez = new RbColumn2AnchorConfig();

			if (columnNode == null)
				return rez;

			try
			{
				//rez._sourceColId = columnNode.Name;

				foreach (XmlAttribute attr in columnNode.Attributes)
				{
					switch (attr.Name.ToLower())
					{
						case "link":
						case "value":
							rez._link = Utils.Utils.AllTrim(attr.Value);
							break;

						case "popup":
							rez._isPopup = Utils.Utils.Str2Bool(Utils.Utils.AllTrim(attr.Value));
							break;

						case "header":
							rez._header = Utils.Utils.AllTrim(attr.Value);
							break;
					}
				}

				rez._isExists = true;
			}
			catch { }

			return rez;
		}
		#endregion

		/// <summary>
		/// ��������� ������ ������� � ����
		/// </summary>
		/// <param name="add"></param>
	    public void AddSoureColId(object key, object add)
		{
			if (!_sourceColId.Contains(key))
				_sourceColId.Add(key, add);
		}
        
		#region ���������� ����� ����� �� ������ �� ������������
		public static RbColumn2AnchorConfig Column2Anchor(string colName)
		{
			if (colName == null || colName.Length == 0)
				return new RbColumn2AnchorConfig();

			RbColumn2AnchorConfig anchor = null;// (Col2AnchorRecord)col2anchorConfig[Utils.Utils.AllTrim(colName).ToLower()];

			return (anchor == null ? new RbColumn2AnchorConfig() : anchor);
		}
		#endregion
	}
}