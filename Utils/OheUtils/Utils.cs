﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Web;
using System.Xml;
using Ohe.Web.Configuration;

namespace Ohe.Utils
{

	#region OheEncodingStringWriter
    
	public class OheEncodingStringWriter : StringWriter
	{
		private Encoding _enc;

		public OheEncodingStringWriter(StringBuilder sb, Encoding enc)
			: base(sb)
		{
			_enc = enc;
		}

		public override Encoding Encoding
		{
			get
			{
				return _enc;
			}
		}
	}
	#endregion


	public static class Utils
	{
		public static bool IsConsoleMode = false;
        private static readonly string SyncObject = Guid.NewGuid().ToString();

        public static string TELNET_HEAD_WEBMENUTOP(string help)
        {
            return "<tr><td class='menu_head'>Главное Меню " + help + "-Пом ESC-Вых" + "<input type='button' name='exit' class=\"exit_button\" onclick='ExitEvent()' value='Выход'></td></tr>";
        }


        #region GetCurrentRoot - корневой каталог
        /// <summary>
        /// Возвращает ссылку на Диск 
        /// </summary>
        /// <param name="Root"></param>
        /// <returns></returns>
        public static string GetCurrentRoot(string Root)
        {
            return Path.GetPathRoot(Root);
        }

        public static string GetCurrentRoot()
        {
            return Path.GetPathRoot(Environment.SystemDirectory);
        }
        
        #endregion


        #region Properties
		/// <summary>
		/// Возвращает заголовок телнет экрана
		/// </summary>
		/// <param name="help"></param>
		/// <returns></returns>
		public static string TELNET_HEAD_MENUTOP (string help)
		{
			return OheConstant.TELNET_ESC_SEQ + "7mГлавМеню "
															 + help
															 + "-Пом ESC-Вых"
															 + OheConstant.TELNET_ESC_SEQ + "0m";
	   }
		#endregion
		

		#region ConvertStrToDbObject - возвращает объект типа данных
		/// <summary>
		/// Возвращает объект сконвертированный в тип данных
		/// </summary>
		/// <param name="str">Значение для конвертации</param>
		/// <param name="Type">Тип в который надо сконвертировать</param>
		/// <param name="Default">Значение по умолчанию из конфигурации</param>
		/// <returns></returns>
		public static object ConvertStrToDbObject(string str, string Type,string Default)
		{
			str = Utils.AllTrim(str);
			try
			{
				switch (Type.ToUpperInvariant())
				{
					#region ПРоверка типов
                    case "XML":
                        try
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(str);
                            return doc.OuterXml;
                        }
                        catch (Exception exc)
                        {
                            throw new FormatException("Ошибка: Не могу сконвертировать"
                                              + Environment.NewLine
                                              + "Значение: " + str
                                              + Environment.NewLine
                                              + "Тип данных: " + Type
                                              + Environment.NewLine
                                              + "Внутренняя ошибка:" + exc.Message);
                        }
					case "CHAR":
					case "STRING":
					case "VARCHAR":
                        if (string.IsNullOrEmpty(str))
                            return Default;
                        else
                            return str;

					case "DATETIME":
						#region DATETIME
						DateTime rez;
						if (StrToDateTime(str, Default, out rez))
							return rez;
						else
							throw new FormatException("Ошибка: Не могу сконвертировать"
																	  + Environment.NewLine
																	  + "Значение: " + str
																	  + Environment.NewLine
																	  + "Тип данных: " + Type);

						#endregion                    
					case "DECIMAL":
					case "MONEY":
						#region MONEY
						try
						{
                            if (string.IsNullOrEmpty(str))
                            {
                                if (!string.IsNullOrEmpty(Default))
                                    return Decimal.Parse(Default, NumberStyles.Any, CultureInfo.InvariantCulture);
                                else
                                    return Decimal.Zero;
                            }

							return Decimal.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
						}
						catch
						{
							throw new FormatException("Ошибка: Не могу сконвертировать"
																+ Environment.NewLine
																+ "Значение: " + str
																+ Environment.NewLine
																+ "Тип данных: " + Type);
						}

						#endregion
					case "DOUBLE":
					case "FLOAT":
						#region FLOAT
						try
						{
                            NumberStyles styles;
                            styles = NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint;

                            if (string.IsNullOrEmpty(str))
                            {
                                if(!string.IsNullOrEmpty(Default))
                                    return Double.Parse(Default, styles, CultureInfo.GetCultureInfo(1033));
                                else
                                    return Double.Parse("0", styles, CultureInfo.GetCultureInfo(1033));
                            }

							return Double.Parse(str, styles, CultureInfo.GetCultureInfo(1033));
						}
						catch
						{
							throw new FormatException("Ошибка: Не могу сконвертировать"
								+ Environment.NewLine
								+ "Значение: " + str
								+ Environment.NewLine
								+ "Тип данных: " + Type);
						}
						#endregion

					case "INT":
					case "INT32":
                    case "SMALLINT":
						#region INT32
						try
						{
                            if (string.IsNullOrEmpty(str))
                            {
                                if (!string.IsNullOrEmpty(Default))
                                    return Int32.Parse(Default, NumberStyles.Any, CultureInfo.InvariantCulture);
                                else
                                    return Int32.Parse("0", NumberStyles.Any, CultureInfo.InvariantCulture);
                            }

                            if (str.IndexOf(",") >= 0 || str.IndexOf(".") >=0)
                                throw new FormatException("Ошибка преобразования формата данных");

							return Int32.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
						}
						catch
						{
							throw new FormatException("Ошибка: Не могу сконвертировать"
																+ Environment.NewLine
																+ "Значение: " + str
																+ Environment.NewLine
																+ "Тип данных: " + Type);
						}

						#endregion
					case "INT16":
					case "INT8":
						#region INT8
						try
						{

                            if (string.IsNullOrEmpty(str))
                            {
                                if (!string.IsNullOrEmpty(Default))
                                    return Int16.Parse(Default, NumberStyles.Any, CultureInfo.InvariantCulture);
                                else
                                    return Int16.Parse("0", NumberStyles.Any, CultureInfo.InvariantCulture);
                            }

                            if (str.IndexOf(",") >= 0 || str.IndexOf(".") >= 0)
                                throw new FormatException("Ошибка преобразования формата данных");


							return Int16.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
						}
						catch
						{
							throw new FormatException("Ошибка: Не могу сконвертировать"
																+ Environment.NewLine
																+ "Значение: " + str
																+ Environment.NewLine
																+ "Тип данных: " + Type);
						}

						#endregion
					case "INT64":
						#region INT64
						try
						{
                            if (string.IsNullOrEmpty(str))
                            {
                                if (!string.IsNullOrEmpty(Default))
                                    return Int64.Parse(Default, NumberStyles.Any, CultureInfo.InvariantCulture);
                                else
                                    return Int64.Parse("0", NumberStyles.Any, CultureInfo.InvariantCulture);
                            }

                            if (str.IndexOf(",") >= 0 || str.IndexOf(".") >= 0)
                                throw new FormatException("Ошибка преобразования формата данных");


                           return Int64.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
						}
						catch
						{
							throw new FormatException("Ошибка: Не могу сконвертировать"
																+ Environment.NewLine
																+ "Значение: " + str
																+ Environment.NewLine
																+ "Тип данных: " + Type);
						}

						#endregion
					case "BYTE":
						#region BYTE
						try
						{
                            if (str == string.Empty)
                                return null;

                            return byte.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
						}
						catch
						{
							throw new FormatException("Ошибка: Не могу сконвертировать"
																+ Environment.NewLine
																+ "Значение: " + str
																+ Environment.NewLine
																+ "Тип данных: " + Type);
						}

						#endregion
					case "BOOLEAN":
					case "BOOL":
						#region BOOL
						try
						{
							return StrToBool(str);
						}
						catch
						{
							throw new FormatException("Ошибка: Не могу сконвертировать"
																+ Environment.NewLine
																+ "Значение: " + str
																+ Environment.NewLine
																+ "Тип данных: " + Type);

						}

						#endregion
					default:
						throw new FormatException("Ошибка: Не могу сконвертировать"
																 + Environment.NewLine
																 + "Значение: " + str
																 + Environment.NewLine
																 + "Тип данных: " + Type);

					#endregion
				}
			}
			catch (FormatException ex)
			{
				throw new FormatException(ex.Message);
			}
			catch (Exception)
			{
				throw;
			}
		}
		#endregion


		#region StrToBool
		static public bool StrToBool(string str)
		{

            return Str2Bool(str);

            /*
			try
			{
				if (str == null || str.Length == 0)
					return false;

				switch (Utils.AllTrim(str.ToUpperInvariant()))
				{
					case "1":
					case "T":
					case "TRUE":
					case "Y":
					case "YES":
					case "ON":
						return true;

					default:
						return false;
				}
			}
			catch
			{
				return false;
			}
            */
		}
		#endregion


		#region StrToDateTime (true - converted) - конвертирует данные в формат datetime
		static public bool StrToDateTime(string str, string ShortDateTime, out DateTime rez)
		{
			try
			{
				if (str == null || str.Length == 0)
				{
                    rez = DateTime.Parse("01/01/2000", System.Globalization.CultureInfo.GetCultureInfo(1049));
					return true;
				}

				DateTimeFormatInfo fi = new DateTimeFormatInfo();
				fi.ShortDatePattern = ShortDateTime;
				if (fi.ShortDatePattern == null || fi.ShortDatePattern.Length == 0)
					fi.ShortDatePattern = "dd.MM.yyyy";

				rez = Convert.ToDateTime(str, fi);

                /*
                if (rez < DateTime.MinValue.AddYears(2000))
                    return false;
                */
				return true;
			}
			catch
			{
                rez = DateTime.Parse("01/01/2000", System.Globalization.CultureInfo.GetCultureInfo(1049)); ;
				return false;
			}
		}
		#endregion


		#region Формат даты в нужный
		/// <summary>
		/// Форматирует дату в нужный формат 
		/// </summary>
		/// <param name="Date">Дата</param>
		/// <param name="Part">нужный формат даты или ее часть</param>
		/// <param name="ShortDateTime">Формат даты из конфигурации</param>
		/// <returns>возвращает результат в соответствии с текущей культурой</returns>
		public static string DateTimeToStr(DateTime Date, string Part, string ShortDateTime)
		{
            if (Part == string.Empty) Part = ShortDateTime;
			DateTimeFormatInfo fi = new DateTimeFormatInfo();
			fi.ShortDatePattern = ShortDateTime;
			if (fi.ShortDatePattern == null || fi.ShortDatePattern.Length == 0)
				fi.ShortDatePattern = "dd.MM.yyyy";

			return Date.ToString(Part, fi);
		}
		#endregion


		#region SystemKey
        [System.Reflection.Obfuscation(Exclude = true)]
		[DllImport("kernel32.dll")]
		private static extern long GetVolumeInformation(
			 string PathName,
			 StringBuilder VolumeNameBuffer,
			 UInt32 VolumeNameSize,
			 ref UInt32 VolumeSerialNumber,
			 ref UInt32 MaximumComponentLength,
			 ref UInt32 FileSystemFlags,
			 StringBuilder FileSystemNameBuffer,
			 UInt32 FileSystemNameSize);

		/// <summary>
		/// Get Volume Serial Number as string
		/// </summary>
		/// <returns>string representation of Volume Serial Number</returns>
        
		public static string GetSystemKey()
		{
			uint serNum = 0;
			uint maxCompLen = 0;
			StringBuilder VolLabel = new StringBuilder(256);	// Label
			UInt32 VolFlags = new UInt32();
			StringBuilder FSName = new StringBuilder(256);	// File System Name
            GetVolumeInformation(GetCurrentRoot(), VolLabel, (UInt32)VolLabel.Capacity, ref serNum, ref maxCompLen, ref VolFlags, FSName, (UInt32)FSName.Capacity);
			return Convert.ToString(serNum);
		}

        /// <summary>
        /// Get Volume Serial Number as string
        /// </summary>
        /// <returns>string representation of Volume Serial Number</returns>
        public static string GetSystemKey(string LicenseRoot)
        {
            uint serNum = 0;
            uint maxCompLen = 0;
            StringBuilder VolLabel = new StringBuilder(256);	// Label
            UInt32 VolFlags = new UInt32();
            StringBuilder FSName = new StringBuilder(256);	// File System Name
            GetVolumeInformation(GetCurrentRoot(LicenseRoot), VolLabel, (UInt32)VolLabel.Capacity, ref serNum, ref maxCompLen, ref VolFlags, FSName, (UInt32)FSName.Capacity);
            return Convert.ToString(serNum);
        }

		#endregion


        #region SetConnString - установка соединения с базой
        /// <summary>
		/// Устанавливает строку подсоединения к базе
		/// </summary>
		/// <param name="str">Строка соединения</param>
        /// <param name="FileName">Ohewebconfig.xml или system.xml</param>
		/// <returns>string</returns>
		public static string SetConnString(string str, string FileName)
		{
            return SetConnString(str, FileName, OheConnectionType.HandHeld);
		}


        public static string SetConnString(string str, string FileName, OheConnectionType Type)
        {
            string _filename = string.Empty;
            try
            {
                switch (Type)
                {
                    case OheConnectionType.HandHeld:
                        _filename = assemblyPath + FileName;
                        break;
                    case OheConnectionType.Web:
                        _filename = webconfigPath +FileName;
                        break;
                    default:
                        _filename = assemblyPath + FileName;
                        break;
                }

                using (XmlTextReader xml = new XmlTextReader(_filename))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xml);

                    XmlNodeList d = doc.GetElementsByTagName("configuration");
                    // проходимся по всем вложенным узлам
                    foreach (XmlNode child in d)
                    {
                        // смотрим есть ли у элементы вложенные узлы
                        if (child.HasChildNodes)
                        {
                            // проходимся по вложенным узлам
                            XmlNodeList goodNode = child.ChildNodes;
                            foreach (XmlNode nd in goodNode)
                            {
                                if (nd.Name.Trim() == "add" && nd.Attributes["key"].Value == "connectionstring")
                                {
                                    nd.Attributes["value"].Value = str;
                                    break;
                                }
                            }
                        }
                    }
                    xml.Close();
                    doc.Save(_filename);
                }
                return string.Empty;
            }
            catch (Exception exc) 
            { 
                Utils.WriteWebLog("SetConnString Exception = " + exc.Message); 
                return exc.Message + _filename; 
            }
        }
		#endregion
       

		#region RestrictString
		public static string RestrictString(string source, int maxWidth, string restrictor, params char[] separators)
		{
			int width = 0;

			string src = Utils.AllTrim(source);
			string restr = Utils.AllTrim(restrictor);

			if (src.Length == 0)
				return string.Empty;

			StringBuilder rez = new StringBuilder();

			int startPos = 0;

			int pos = -1;

			while (startPos < src.Length && (pos = src.IndexOfAny(separators, startPos)) != -1)
			{
				rez.Append(Substring(src, startPos, pos - startPos + 1));
				width += pos - startPos + 1;

				if (width > maxWidth)
				{
					rez.Append(restr);
					width = 0;
				}

				startPos = pos + 1;
			}

			rez.Append(Substring(src, startPos, src.Length - startPos + 1));

			return rez.ToString();
		}
		#endregion


        #region WriteLog+WriteWebLog(string str) - записывает в лог
        /// <summary>
        /// Записывает данные в файл для web приложения
        /// </summary>
        /// <param name="str"></param>
        public static void WriteWebLog(string str) { WriteLog(str, OheConnectionType.Web, null); }

		/// <summary>
		/// Записывает данные в файл
		/// </summary>
		/// <param name="str"></param>
        public static void WriteLog(string str) { WriteLog(str, OheConnectionType.HandHeld,null); }
       

        /// <summary>
        /// Записывает данные в файл используется для версии Debug
        /// </summary>
        /// <param name="str"></param>
        public static void WriteDebugLog(string UserId, string str) 
        {
            WriteLog(str, OheConnectionType.Debug, UserId); 
        }


        public static void WriteSourceCode(string Source, string Filename)
        {
            lock (SyncObject)
            {
                FileInfo i = new FileInfo(Filename);
                if (!i.Exists)
                {
                    using (FileStream f = new FileStream(Filename, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write))
                    {
                        using (StreamWriter s = new StreamWriter(f))
                        {
                            s.WriteLine(Environment.NewLine
                                            + "--[ " + DateTime.Now.ToString() + " ]  "
                                            + Environment.NewLine + Source);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_source">List(string)</param>
        /// <param name="Source">Запрос</param>
        /// <param name="Exception">Описание ошибки</param>
        /// <returns></returns>
        public static string GetLast100Operations(List<string> _source, string Source, string Exception)
        {
            StringBuilder _header = new StringBuilder();
            _header.AppendLine("/*************************************************************/");
            _header.AppendLine("/** 100 Операций предшествующих ошибке                      ***/");

            foreach (string str in _source)
                _header.AppendLine(str);

            _header.AppendLine(Source);
            _header.AppendLine(Exception);
            _header.AppendLine("/********   Конец Ошибки                              *******/");
            _header.AppendLine("/************************************************************/");

            return _header.ToString();

        }


        /// <summary>
        /// Метод выполняет основные действия по записи данных в лог приложения
        /// </summary>
        /// <param name="str"></param>
        /// <param name="Type"></param>
        private static void WriteLog(string str, OheConnectionType Type, string _filename)
        {
            if (Utils.isEmpty(_filename))
            {
                switch (Type)
                {
                    case OheConnectionType.HandHeld:
                        _filename = assemblyPath + @"Log\log_" + DateTime.Now.ToString("dd_MM_yyyy") + ".txt";
                        break;
                    case OheConnectionType.Web:
                        _filename = webconfigPath + @"Log\log_" + DateTime.Now.ToString("dd_MM_yyyy") + ".txt";
                        break;
                    default:
                        _filename = assemblyPath + @"Log\log_" + DateTime.Now.ToString("dd_MM_yyyy") + ".txt";
                        break;
                }
            }
            else
            {
                _filename = assemblyPath + "Log\\" + _filename + "_log_" + DateTime.Now.ToString("dd_MM_yyyy") + ".txt";
            }

            lock (SyncObject)
            {
                FileInfo i = new FileInfo(_filename);
                if (i.Exists)
                {
                    using (FileStream f = new FileStream(_filename, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        using (StreamWriter s = new StreamWriter(f))
                        {
                            s.WriteLine(Environment.NewLine
                                            + Environment.NewLine
                                            + "[ " + DateTime.Now.ToString() + " ]  "
                                            + Environment.NewLine + str);
                        }
                    }
                }
                else
                {
                    using (FileStream f = new FileStream(_filename, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write))
                    {
                        using (StreamWriter s = new StreamWriter(f))
                        {
                            s.WriteLine(Environment.NewLine
                                            + Environment.NewLine
                                            + "[ " + DateTime.Now.ToString() + " ]  "
                                            + Environment.NewLine + str);
                        }
                    }
                }
            }
        }

		#endregion


        #region WriteFile
        /// <summary>
        /// Записывает данные в файл. Имя файла передается с папкой
        /// </summary>
        /// <param name="source">Содержимое файла</param>
        /// <param name="filename">Имя файла с путем</param>
        public static void WriteFile(string source, string filename)
        {
            lock (SyncObject)
            {
                FileInfo i = new FileInfo(filename);
                DirectoryInfo dr = new DirectoryInfo(Path.GetFullPath(filename));


                if (!dr.Exists)
                    dr.Create();

                using (FileStream f = new FileStream(filename, FileMode.CreateNew, FileAccess.Write))
                {
                    using (StreamWriter s = new StreamWriter(f))
                    {
                        s.WriteLine(source);
                    }
                }
            }
        }
        #endregion
        

        #region Write3DModelString(string str) - записывает результат
        /// <summary>
        /// Записывает данные в файл
        /// </summary>
        /// <param name="Path">Папка куда записывается результат</param>
        /// <param name="file">Имя файла</param>
        /// <param name="str">Содержимое файла</param>
        public static void Write3DModelString(string Path,string file,string str)
        {
            string _filename = string.Empty;
            try
            {
                DirectoryInfo dr = new DirectoryInfo(Path);
                if (!dr.Exists)
                    dr.Create();

                _filename = Path + "\\" + file;

                FileInfo i = new FileInfo(_filename);
                if (i.Exists)
                {
                    i.Delete();
                    using (FileStream f = new FileStream(_filename, System.IO.FileMode.Create, System.IO.FileAccess.Write))
                    {
                        using (StreamWriter s = new StreamWriter(f, Encoding.GetEncoding("windows-1251")))
                        {
                            s.WriteLine(str);
                        }
                    }
                }
                else
                {
                    using (FileStream f = new FileStream(_filename, System.IO.FileMode.Create, System.IO.FileAccess.Write))
                    {
                        using (StreamWriter s = new StreamWriter(f, Encoding.GetEncoding("windows-1251")))
                        {
                            s.WriteLine(str);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(_filename + Environment.NewLine + ex.Message, ex);
            }
        }

        #endregion
	
	
		#region Get Current Path/AssemblyPath/FormPath/WebPath
		private static string _curPath;
		private static string _assemblyPath;

		public static string assemblyPath
		{
			get
			{
				//if (_assemblyPath == null)
				//{
				_assemblyPath = Assembly.GetExecutingAssembly().CodeBase;

				if (_assemblyPath.StartsWith(@"file:///"))
					_assemblyPath = _assemblyPath.Substring(8, _assemblyPath.LastIndexOf('/') - 7);
				else
					_assemblyPath = _assemblyPath.Substring(0, _assemblyPath.LastIndexOf('/') + 1);
				//}

				_assemblyPath = _assemblyPath.Replace('/', Path.DirectorySeparatorChar);
				return _assemblyPath;
			}
		}

		public static string curPath
		{
			get
			{
				if (_curPath == null)
				{
					_curPath = assemblyPath;

					//remove to parent folder if Debug
					if (_curPath.EndsWith("debug" + Path.DirectorySeparatorChar) || _curPath.EndsWith("Debug" + Path.DirectorySeparatorChar))
						_curPath = _curPath.Substring(0, _curPath.Length - 6);

					//remove to parent folder if Release
					if (_curPath.EndsWith("release" + Path.DirectorySeparatorChar) || _curPath.EndsWith("Release" + Path.DirectorySeparatorChar))
						_curPath = _curPath.Substring(0, _curPath.Length - 8);

					//remove to parent folder if bin
					if (_curPath.EndsWith("bin" + Path.DirectorySeparatorChar))
						_curPath = _curPath.Substring(0, _curPath.Length - 4);

					// remove to parent folder
					_curPath = _curPath.Substring(0, _curPath.LastIndexOf(Path.DirectorySeparatorChar, _curPath.Length - 2) + 1);
				}
				return _curPath;
			}
		}

		private static string _webPath;
		/// <summary>
		/// Возвращает имя папки для конфигурации web generator
		/// </summary>
		public static string webconfigPath
		{
			get
			{
				if (_webPath == null)
				{
					_webPath = assemblyPath;

					//remove to parent folder if Debug
					if (_webPath.EndsWith("debug" + Path.DirectorySeparatorChar) || _webPath.EndsWith("Debug" + Path.DirectorySeparatorChar))
                        _webPath = _webPath.Substring(0, _webPath.Length - 6);

					//remove to parent folder if Release
					if (_webPath.EndsWith("release" + Path.DirectorySeparatorChar) || _webPath.EndsWith("Release" + Path.DirectorySeparatorChar))
						_webPath = _webPath.Substring(0, _webPath.Length - 8);

					//remove to parent folder if bin
					if (_webPath.EndsWith("bin" + Path.DirectorySeparatorChar))
						_webPath = _webPath.Substring(0, _webPath.Length - 4);

					// remove to parent folder
					//_curPath = _curPath.Substring(0, _curPath.LastIndexOf(Path.DirectorySeparatorChar, _curPath.Length - 2) + 1);
                    //Utils.WriteLog("Папка crystall reports: " + _webPath);
				}
				return _webPath;
			}
		}

		/// <summary>
		/// Возвращает папку для с отчетами xml
		/// </summary>
		public static string formPath 
		{
			get
			{
				string _forms = webconfigPath;
				return _forms + "forms" + Path.DirectorySeparatorChar.ToString();
			}
		}

		public static string getPath() { return curPath; }

        // Определяет существует или нет папка
        public static bool pathExists(string path)
        {
            try
            {
                DirectoryInfo f = new DirectoryInfo(@path.Trim());
                return f.Exists;
            }
            catch { return false; }
        }

		#endregion


		#region AllTrim обрезание пустых значений в строке
		public static string AllTrim(string str)
		{
			return ((str == null || str.Length == 0) ? string.Empty : str.Trim());
		}
		#endregion


		#region run Batch file
		/// <param name="batFile"></param>
		/// <param name="parameters"></param>
		/// <param name="timeout">by seconds</param>
		/// <returns>Если возвращает пустое значение то OK, иначе описание ОШИБКИ</returns>
		public static string run_bat(string batFile, string parameters, int timeout)
		{
			try
			{
				ProcessStartInfo startInfo = new ProcessStartInfo(batFile);
				startInfo.CreateNoWindow = true;
				startInfo.WindowStyle = ProcessWindowStyle.Normal;

				if (Utils.AllTrim(parameters) != string.Empty)
					startInfo.Arguments = parameters;

				Process rez = Process.Start(startInfo);

				int i = 0;
				while (!rez.HasExited)
				{
					//will break if reach the timeout setting (by seconds)
					if (timeout > 0 && (i / 10) > timeout)
					{
						break;
					}

					i++;

					//will try every 0.1 seconds
					Thread.Sleep(100);
				}

				if (!rez.HasExited)
				{
					rez.Kill();
					return "run " + batFile + " with " + parameters + " Timed out.";
				}
				return string.Empty;
			}
			catch (Exception e)
			{
				return e.Message;
			}

		}
		#endregion
	
	
		#region Str2Int - преобразование строки в число
		public static int Str2Int(string str)
		{
			return Str2Int(str, 0);
		}

		public static int Str2Int(string str, int defVal)
		{
			try
			{
				if (str == null || str.Length == 0)
					return defVal;
				str = AllTrim(str);
				if (!str.StartsWith("+") && !str.StartsWith("-") && !char.IsDigit(str[0]))  //now can deal with negative values //mz
					return defVal;
				return Int32.Parse(str, NumberStyles.Any);
			}
			catch
			{
				return defVal;
			}
		}
		#endregion


		#region ConvertStrToDbType - возвращает тип данных для хранимой процедуры
		/// <summary>
		/// Возвращает тип данных SqlDbType
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static SqlDbType ConvertStrToDbType(string str)
		{
			str = Utils.AllTrim(str);
			try
			{
				if (str != string.Empty)
					switch (str.ToUpperInvariant())
					{
						#region Проверка типов
						case "INT64":
							return SqlDbType.BigInt;
						case "CHAR":
							return SqlDbType.Char;
						case "VARCHAR":
							return SqlDbType.VarChar;
						case "XML":
							return SqlDbType.Xml;
						case "DATETIME":
							return SqlDbType.DateTime;
						case "DECIMAL":
							return SqlDbType.Decimal;
						case "MONEY":
							return SqlDbType.Money;
						case "DOUBLE":
						case "FLOAT":
							return SqlDbType.Float;
						case "INT":
						case "INT32":
							return SqlDbType.Int;
						case "STRING":
							return SqlDbType.Text;
						case "INT8":
							return SqlDbType.TinyInt;
						case "INT16":
                        case "SMALLINT":
							return SqlDbType.SmallInt;
						case "BYTE":
							return SqlDbType.Binary;
						case "BOOLEAN":
						case "BOOL":
							return SqlDbType.Bit;
						case "UNIQUEIDENTIFIER":
							return SqlDbType.UniqueIdentifier;

						default:
							throw new FormatException("Неизвестный формат данных (" + str + ")");

						#endregion
					}
				else
					throw new FormatException("Неверный тип данных (" + str + ")");
			}
			catch
			{
				throw;
			}
		}

		#endregion


		#region ConvertStrToDbType - возвращает тип данных для хранимой процедуры
		/// <summary>
		/// Возвращает тип данных SqlDbType
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static DbType ConvertStrToDbTypeSame(string str)
		{
			str = Utils.AllTrim(str);
			try
			{
				if (str != string.Empty)
					switch (str.ToUpperInvariant())
					{
						#region Проверка типов
						case "INT64":
						case "BIGINT":
							return DbType.Int64;
						case "CHAR":
						case "VARCHAR":
							return DbType.String;
						case "XML":
							return DbType.Xml;
						case "DATETIME":
							return DbType.DateTime;
						case "DECIMAL":
						case "MONEY":
							return DbType.Decimal;
						case "DOUBLE":
						case "FLOAT":
							return DbType.Double;
						case "INT":
						case "INT32":
							return DbType.Int32;
						case "STRING":
							return DbType.String;
						case "INT16":
						case "SMALLINT":
							return DbType.UInt16;
						case "BYTE":
							return DbType.Binary;
						case "BOOLEAN":
						case "BOOL":
						case "BIT":
							return DbType.Boolean;
						case "UNIQUEIDENTIFIER":
							return  DbType.Guid;

						default:
							throw new FormatException("Неизвестный формат данных (" + str + ")");

						#endregion
					}
				else
					throw new FormatException("Неверный тип данных (" + str + ")");
			}
			catch
			{
				throw;
			}
		}

		#endregion


        #region ParseXmlString
        /// <summary>
        /// Преобразование строки в корретный для xml формат
        /// </summary>
        /// <param name="ParseString"></param>
        /// <returns></returns>
        public static string ParseXmlString(string ParseString)
        {
            string rez = Utils.AllTrim(ParseString);
            rez = rez.Replace("\"", "&quot;");
            rez = rez.Replace(">", "&gt;");
            rez = rez.Replace("<", "&lt;");
            rez = rez.Replace("'", "&apos;");

            return rez;
        }
        #endregion


        #region WebContext2Url
        public static string WebContext2Url(HttpContext context)
		{
			StringBuilder rez = new StringBuilder();

			if (string.Compare(Utils.AllTrim(context.Request.HttpMethod), "post", true) == 0)
			{
				rez.Append(WebConstant.WEB_REF);


    			rez.Append(context.Request.Form["ref"]);

				foreach (string key in context.Request.Form.Keys)
					if (key != "ref" && key != "refreshLink")
					{
						rez.Append("&");
						rez.Append(key);
						rez.Append("=");
                        // Здесь параметр автоматически преобразуется в русский язык
						//rez.Append(context.Request.Form[key]);
                        rez.Append(context.Request.Form.GetValues(key)[0]);
					}
			}
			else
                // Исправлено. В Web клиенте все передается в кодировке UTF8
                // Если закомментировано 1 корректно работает WEB Dispatch,
                // Если незакомментировно 1 работает WEB Terminal
                // Проверено на IIS 6.0 Windows 2003 Server 
				//rez.Append(Utils.Substring(HttpUtility.UrlDecode(AllTrim(context.Request.RawUrl)), 20));
                rez.Append(Utils.Substring(context.Request.RawUrl, 20));

			return rez.ToString();
		}
		#endregion


        #region WebContext2Url
        public static Hashtable WebContext2Url2(HttpContext context)
        {
            Hashtable rez2 = new Hashtable(new OheCultureComparer());

            if (string.Compare(Utils.AllTrim(context.Request.HttpMethod), "post", true) == 0)
            {
                foreach (string key in context.Request.Form.Keys)
                {
                    string k = string.Empty;

                    if(!string.IsNullOrEmpty(key))
                        k = AllTrim(key.Replace("_popup", "").ToLowerInvariant());

                    if (!rez2.Contains(k))
                        rez2[k] = HttpUtility.UrlDecode(context.Request.Form.GetValues(key)[0]);
                }
            }
            else
                // Исправлено. В Web клиенте все передается в кодировке UTF8
                // Если закомментировано 1 корректно работает WEB Dispatch,
                // Если незакомментировно 1 работает WEB Terminal
                // Проверено на IIS 6.0 Windows 2003 Server 
                //rez.Append(Utils.Substring(HttpUtility.UrlDecode(AllTrim(context.Request.RawUrl)), 20));
                return ParseUrl(Utils.Substring(context.Request.RawUrl, 20));

            return rez2;
        }
        #endregion


		#region UserString2DateTime
		static public bool UserString2DateTime(string str, out DateTime rez)
		{
			try
			{
				if (str == null || str.Length == 0)
				{
					rez = DateTime.Now;
					return true;
				}

				DateTimeFormatInfo fi = new DateTimeFormatInfo();
				fi.ShortDatePattern = OheWebConfig.DateFormat;
				if (fi.ShortDatePattern == null || fi.ShortDatePattern.Length == 0)
					fi.ShortDatePattern = "MM/dd/yyyy";

				rez = Convert.ToDateTime(str, fi);
				return true;
			}
			catch
			{
				rez = DateTime.Now;
				return false;
			}
		}
		#endregion


		#region String2LanguageId
		public static OheLanguage String2LanguageId(string strlang)
		{
			switch (Utils.AllTrim(strlang).ToLower())
			{
				case "1033":
					return OheLanguage.English;

				case "1049":
					return OheLanguage.Russian;

				case "test":
					return OheLanguage.Test;

				default:
					return OheLanguage.Undefined;
			}

		}
		#endregion


		#region Don't use now

		/*
		
		#region IsIPAddressValid
		public static bool IsIPAddressValid(string ipAddress)
		{
			try
			{
				IPAddress.Parse(ipAddress);
				return true;
			}
			catch
			{
				return false;
			}
		}
		#endregion

		#region CompareIPAddresses
		public static int CompareIPAddresses(IPAddress addr1, IPAddress addr2)
		{
			if (addr1 == null && addr2 == null)
				return 0;

			if (addr1 == null)
				return -1;

			if (addr2 == null)
				return 1;

			byte[] addr1Bytes = addr1.GetAddressBytes();
			byte[] addr2Bytes = addr2.GetAddressBytes();

			for (int i=0; i < addr1Bytes.Length && i < addr2Bytes.Length; i++)
			{
				int rez = addr1Bytes[i].CompareTo(addr2Bytes[i]);
				if (rez != 0)
					return rez;
			}

			return addr1Bytes.Length.CompareTo(addr2Bytes.Length);
		}
		#endregion

		#region Array2SeparatedString
		public static string Array2SeparatedString(string[] arr)
		{
			if (arr == null || arr.Length == 0)
				return string.Empty;

			string tSeparatorChar = RbDbConfig.GetValue("CARET_CHAR");

			if (tSeparatorChar.Length == 0)
				tSeparatorChar = "^";

			// fields separator char
			char separatorChar = tSeparatorChar[0];

			StringBuilder rez = new StringBuilder();

			foreach (string str in arr)
			{
				rez.Append(Utils.AllTrim(str));
				rez.Append(separatorChar);
			}

			return rez.ToString();
		}
		#endregion


		#region GetIdFromGuid
		public static string GetIdFromGuid()
		{
			return Guid.NewGuid().ToString().Replace('-', '_');
		}
		#endregion

		#region search files starting from initPath in all subdirs by searchPattern
		public static ArrayList SearchFiles(string initPath, string searchPattern)
		{
			if (!Directory.Exists(initPath))
				return new ArrayList();

			ArrayList rez = new ArrayList();

			DirectoryInfo di = new DirectoryInfo(initPath);

			// check files
			foreach (FileSystemInfo fsi in di.GetFileSystemInfos(searchPattern))
			{
				if (fsi is FileInfo)
					rez.Add(fsi.FullName);
			}

			// check subdirs
			foreach (DirectoryInfo subdir in di.GetDirectories())
				rez.AddRange(SearchFiles(subdir.FullName, searchPattern));

			return rez;
		}
		#endregion

		#region restart iis
		public static string restart_iis()
		{
			try
			{
				ProcessStartInfo startInfo = new ProcessStartInfo("iisreset");
				startInfo.Arguments = "/noforce";
				startInfo.CreateNoWindow = true;
				startInfo.WindowStyle =ProcessWindowStyle.Hidden;

				Process rez = Process.Start(startInfo);

				int i = 0;
				while (!rez.HasExited && i<1000)
				{
					i++;
					Thread.Sleep(100);
				}

				if (!rez.HasExited) 
				{
					rez.Kill();
					return "Cannot restart IIS automatically.";
				}
				return string.Empty;
			}
			catch (Exception e)
			{
				return e.Message;
			}

		}
		#endregion

		#region run Batch file
		/// <param name="batFile"></param>
		/// <param name="parameters"></param>
		/// <param name="timeout">by seconds</param>
		/// <returns>empty string means succeed, otherwise the reason why failed</returns>
		public static string run_bat(string batFile, string parameters, int timeout)
		{
			try
			{
				ProcessStartInfo startInfo = new ProcessStartInfo(batFile);
				startInfo.CreateNoWindow = true;
				startInfo.WindowStyle =ProcessWindowStyle.Hidden;
            
				if (!Utils.isEmpty(parameters))
					startInfo.Arguments = parameters;

				Process rez = Process.Start(startInfo);

				int i = 0;
				while (!rez.HasExited)
				{
					//will break if reach the timeout setting (by seconds)
					if (timeout > 0 && (i/10)>timeout )
					{
						break;
					}

					i++;

					//will try every 0.1 seconds
					Thread.Sleep(100);
				}

				if (!rez.HasExited) 
				{
					rez.Kill();
					return "run "+batFile+" with "+parameters+" Timed out.";
				}
				return string.Empty;
			}
			catch (Exception e)
			{
				return e.Message;
			}

		}
		#endregion

		//returns the current DateTime
		public static DateTime getDate()
		{
			return DateTime.Now.Date;
		}

		#region WeekNo2Date
		public static DateTime WeekNo2Date(int nweekno)
		{
			int nyear;                                      // year of the week number
			int nweek;                                      // week number
			int dday;                                       // day no
			DateTime ddate1;                                // January 1st
			DateTime dMonday;                               // monday of the first week in year
			DateTime ddate;                                 // monday of the week number
			ArrayList aDay = new ArrayList();
			
			aDay.Add("Sunday");
			aDay.Add("Monday");
			aDay.Add("Tuesday");
			aDay.Add("Wednesday");
			aDay.Add("Thursday");
			aDay.Add("Friday");
			aDay.Add("Saturday");

			if (nweekno == 0)
			{
				nyear = 1900;
				nweek = 1;
			}
			else 
			{
				nyear = Utils.Str2Int(nweekno.ToString().Substring(0,3)) + 1900; // extract year
				nweek = Utils.Str2Int(nweekno.ToString().Substring(3,2));        // extract week number
			}
						
			ddate1  = new DateTime(nyear, 1, 1, 0, 0, 0);								// starting date of the year - January 1st
			dday = aDay.IndexOf(ddate1.DayOfWeek.ToString()) + 1;						// week day number of the date

			if (dday == 1)
				dMonday = ddate1.AddDays(1);			                                // Monday of the first week in a year (if January 1st is Sunday)
			else
				dMonday = ddate1.AddDays(-(dday - 2));									// Monday of the first week in a year
			
			ddate = dMonday.AddDays(( 7 * (nweek - 1)));								// Monday of the week number

			return ddate;
		}
		#endregion

		#region Date2WeekNo
		public static int Date2WeekNo(DateTime ddate)
		{

			int nyear;																// year of the week number
			int nweekno;															// week number
			nyear   = ddate.Year;													// extract year
			nweekno = (int)Math.Ceiling((double)ddate.DayOfYear / (double)7) + 1;	// get week no  

			if (ddate.Month == 12 && nweekno == 1)             // given date belongs to week number of previous year
				nyear = nyear + 1;
		
			nweekno = ((nyear - 1900) * 100) + nweekno;

			return nweekno;
		}

		#endregion


		#region Multiplier2String
		public static string Multiplier2String(int nMultiplier)
		{
			if (nMultiplier == 1)	return "";
			if (nMultiplier == 100) return "Hundred ";
			if (nMultiplier == 1000) return "Thousand ";
			if (nMultiplier == 1000000) return "Million ";
			if (nMultiplier == 1000000000) return "Billion ";
		
			return 'x' + nMultiplier.ToString();
		}
		#endregion

		#region GetMinMax (int nValue, int nInterval, int nLRoundUp)
		public static int GetMinMax(int nValue, int nInterval, int nLRoundUp)
		{
			int nReturn = nValue, i = 0; 
			int nSign = 0;
						 
			if (nValue > 0)
				nSign = 1;
			else
				nSign = -1;

			if (nInterval == 0)
				return nValue;
		
			nInterval = Math.Abs(nInterval);

			while (i * nInterval <= Math.Abs(nValue))
				i++;

			if (i > 0)
			{
				if ((nLRoundUp*nSign)>0)
					nReturn = nSign * (i * nInterval);
				else 
					nReturn = nSign * ((i-1) * nInterval);	
			}

			return nReturn;
		}
		#endregion

		#region Round
		public static decimal Round(decimal nValue, int nDecimal)
		{
			if (nDecimal >= 0)
				return Math.Round(nValue, nDecimal);

			double multi = Math.Pow(10,-nDecimal);
			double temp = multi*Math.Round((double)nValue/multi,0);

			return (decimal)temp;

		}
		#endregion

		#region Replicate
		public static string Replicate(string cStr, int nCount)
		{
			string rez = string.Empty;
			if (nCount > 0)
			{
				for (int i=0;i<nCount;i++)
				{
					rez += cStr;
				}
			}

			return rez;
		}
		#endregion

		#region ParseUrl
		public static Hashtable ParseUrl(string url)
		{
			Hashtable rez = new Hashtable(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());

			//url = HttpUtility.UrlDecode(AllTrim(url));
			url = AllTrim(url);

			if (url.Length == 0)
				return rez;

			if (url.IndexOf("reportmenuurl") != -1)
				url = url.Replace("%", "%25");

			int startPos = url.IndexOf('?');

			if (startPos != -1 && startPos < url.Length)
				url = Utils.Substring(url, startPos+1);

			string[] arr = url.Split('&');

			foreach (string node in arr)
			{
				if (node != null && node.Length != 0)
				{
					int pos = node.IndexOf('=');
					if (pos != -1)
					{
						if (rez.Contains(AllTrim(Left(node, pos)).ToLower()))
							rez[AllTrim(Left(node, pos)).ToLower()] = HttpUtility.UrlDecode(AllTrim(Substring(node, pos+1)));
						else
							rez.Add(AllTrim(Left(node, pos)).ToLower(), HttpUtility.UrlDecode(AllTrim(Substring(node, pos+1))));
					}
				}
			}

			return rez;
		}
		#endregion

		#region Xml2String
		public static string Xml2String(XmlDocument doc)
		{
			StringBuilder rez = new StringBuilder();
			XmlTextWriter writer = new XmlTextWriter(new OheEncodingStringWriter(rez, Encoding.UTF8));
			writer.Formatting = Formatting.Indented;
			doc.Save(writer);
			writer.Flush();
			writer.Close();

			return rez.ToString();
		}
		#endregion

		#region date_format
		static public string date_format(string cDate, string format)
		{
			// created by ilona 14/05/2004 im, translated from date_frm function in XBase. 
			string rez = string.Empty;
			string tempDate = string.Empty;
			DateTimeFormatInfo dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
			
			switch (format)
			{
				case "MM/DD/YY":
					dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
					break;
			
				case "MM/DD/YYYY":
					dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
					break;
				
				case "MM-DD-YY":
					dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
					break;
				
				case "MM-DD-YYYY":
					dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
					break;
				
				case "DD/MM/YY":
					dateFormat = new CultureInfo( "en-GB", false ).DateTimeFormat;
					break;
				
				case "DD/MM/YYYY":
					dateFormat = new CultureInfo( "en-GB", false ).DateTimeFormat;
					break;
				
				case "DD.MM.YY":
					dateFormat = new CultureInfo( "de-DE", false ).DateTimeFormat;
					break;
                
				case "DD.MM.YYYY":
					dateFormat = new CultureInfo( "de-DE", false ).DateTimeFormat;
					break;
				
				case "DD-MM-YY":
					dateFormat = new CultureInfo( "it-IT", false ).DateTimeFormat;
					break;
				
				case "DD-MM-YYYY":
					dateFormat = new CultureInfo( "it-IT", false ).DateTimeFormat;
					break;
				
				case "YY/MM/DD":
					dateFormat = new CultureInfo( "ja-JP", false ).DateTimeFormat;
					break;
				
				case "YYYY/MM/DD":
					dateFormat = new CultureInfo( "ja-JP", false ).DateTimeFormat;
					break;				
			
				case "YYYYMMDD":
					dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
					tempDate = cDate.Substring(6,2) + "/" + cDate.Substring(4,2) + "/" + cDate.Substring(0,4);				
					cDate = tempDate;
					break;
				
				case "YYMMDD":
					dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
					tempDate = cDate.Substring(4,2) + "/" + cDate.Substring(2,2) + "/" + cDate.Substring(0,2);				
					cDate = tempDate;
					break;
				
				case "DDMMYYYY":
					dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
					tempDate = cDate.Substring(0,2) + "/" + cDate.Substring(2,2) + "/" + cDate.Substring(4,4);				
					cDate = tempDate;
					break;
				
				case "DDMMYY":
					dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
					tempDate = cDate.Substring(0,2) + "/" + cDate.Substring(2,2) + "/" + cDate.Substring(4,2);				
					cDate = tempDate;
					break;
			}
						
			DateTime dt = DateTime.Parse(cDate, dateFormat, DateTimeStyles.NoCurrentDateDefault);
			rez = dt.ToShortDateString();
			dateFormat = new CultureInfo( "en-US", false ).DateTimeFormat;
			return rez;
		}
		#endregion

		#region CheckDate
		static public bool CheckDate(string cDate)
		{
			try
			{
				DateTimeFormatInfo fi = new DateTimeFormatInfo();
				fi.ShortDatePattern = RbXmlConfig.GetFromXmlConfig("shortDateFormat");
				if (fi.ShortDatePattern==null || fi.ShortDatePattern.Length==0)
					fi.ShortDatePattern = "MM/dd/yyyy";
				Convert.ToDateTime(cDate, fi);
				return true;
			} 
			catch
			{
				return false;
			}
		}
		#endregion

		#region CheckDate
		static public string UserCurrentDate
		{
			get
			{
				string pattern = RbXmlConfig.GetFromXmlConfig("shortDateFormat");
				if (pattern.Length == 0)
					pattern = "MM/dd/yyyy";

				return DateTime.Now.ToString(pattern);
			}
		}
		#endregion

		#region Array2DownloadXml
		public static string Array2DownloadXml(string[] arr, RbOrderType postType)
		{
			if (arr == null || arr.Length == 0)
				return string.Empty;

			XmlDocument doc = new XmlDocument();

			XmlNode rootNode = doc.CreateElement("TERSE_DOWNLOAD");

			switch (postType)
			{
				case RbOrderType.SalesOrder:
				case RbOrderType.WorkOrder:
				{
					XmlNode pickNode = doc.CreateElement("PICK");

					foreach (string line in arr)
					{
						string recName = Utils.Left(line, 2);
						
						XmlNode lineNode = doc.CreateElement(recName);
						lineNode.InnerText = line;

						switch (recName.ToLower())
						{
							case "ph":
								pickNode.InsertAfter(lineNode, null);
								break;

							default:
								pickNode.InsertBefore(lineNode, null);
								break;
						}
					}

					rootNode.AppendChild(pickNode);
				}
					break;

				case RbOrderType.PurchaseOrder:
				{
					XmlNode pickNode = doc.CreateElement("RECEIVE");

					foreach (string line in arr)
					{
						string recName = Utils.Left(line, 2);
						
						XmlNode lineNode = doc.CreateElement(recName);
						lineNode.InnerText = line;

						switch (recName.ToLower())
						{
							case "rh":
								pickNode.InsertAfter(lineNode, null);
								break;

							default:
								pickNode.InsertBefore(lineNode, null);
								break;
						}
					}

					rootNode.AppendChild(pickNode);
				}
					break;

				default:
				{
					XmlNode pickNode = doc.CreateElement("OTHER_DOWNLOAD");

					foreach (string line in arr)
					{
						string recName = Utils.Left(line, 2);
					
						XmlNode lineNode = doc.CreateElement(recName);
						lineNode.InnerText = line;
						pickNode.AppendChild(lineNode);
					}

					rootNode.AppendChild(pickNode);
				}
					break;
			}

			doc.AppendChild(rootNode);

			return doc.InnerXml;
		}
		#endregion

		#region RecordArray2String || created by (sergv) [July 28, 2004]
		// string[] in ArrayList in ArrayList
		public static string RecordArrStr2String(ArrayList records)
		{
			StringBuilder rez = new StringBuilder();

			string seperator = RbDbConfig.GetValue("CARET_CHAR");
			if (Utils.isEmpty(seperator))
				seperator = "^";

			try
			{
				foreach (string[] record in records)
				{
					foreach (string cell in record)
						rez.Append(Utils.AllTrim(cell)+seperator);
					rez.Append(Environment.NewLine);
				}

				return rez.ToString();
			}
			catch (Exception e)
			{
				return rez.ToString()+Environment.NewLine+"RecordArray2String exception:"+Environment.NewLine+e.ToString();
			}
		}

		public static string RecordArrArrStr2String(ArrayList records)
		{
			StringBuilder rez = new StringBuilder();

			string seperator = RbDbConfig.GetValue("CARET_CHAR");
			if (Utils.isEmpty(seperator))
				seperator = "^";

			try
			{
				foreach (ArrayList al in records)
				{
					foreach (string[] record in al)
					{
						foreach (string cell in record)
							rez.Append(Utils.AllTrim(cell)+seperator);
						rez.Append(Environment.NewLine);
					}
				}

				return rez.ToString();
			}
			catch (Exception e)
			{
				return rez.ToString()+Environment.NewLine+"RecordArray2String exception:"+Environment.NewLine+e.ToString();
			}
		}

		public static string RecordStr2String(string[][] records)
		{
			StringBuilder rez = new StringBuilder();
			string seperator = string.Empty;
			try
			{
				seperator = RbDbConfig.GetValue("CARET_CHAR");
			}
			catch
			{
				seperator = "^";
			}
			if (Utils.isEmpty(seperator))
				seperator = "^";

			try
			{
				foreach (string[] record in records)
				{
					foreach (string cell in record)
						rez.Append(Utils.AllTrim(cell)+seperator);
					rez.Append(Environment.NewLine);
				}

				return rez.ToString();
			}
			catch (Exception e)
			{
				return rez.ToString()+Environment.NewLine+"RecordArray2String exception:"+Environment.NewLine+e.ToString();
			}
		}

		public static string RecordArr2String(string[] records)
		{
			StringBuilder rez = new StringBuilder();

			try
			{
				foreach (string record in records)
					rez.Append(Utils.AllTrim(record)+Environment.NewLine);

				return rez.ToString();
			}
			catch (Exception e)
			{
				return rez.ToString()+Environment.NewLine+"RecordArray2String exception:"+Environment.NewLine+e.ToString();
			}
		}
		#endregion

		#region GetColor4OrderStatus || created by (sergv) [July 7, 2004]
		static public int GetColor4OrderStatus(RbOrderStatus status)
		{
			switch (status)
			{
				case RbOrderStatus.SOAP_Exception:
				case RbOrderStatus.Undefined:
				case RbOrderStatus.WrongParameter:
				case RbOrderStatus.NotInRB:
				case RbOrderStatus.HeldShort:
				case RbOrderStatus.HeldReplenishment:
				case RbOrderStatus.WaitForExternal:
				case RbOrderStatus.Suspended:
				case RbOrderStatus.Packhold:
				case RbOrderStatus.External:
				case RbOrderStatus.HeldFormat:
				case RbOrderStatus.AsmblShort:
				case RbOrderStatus.AsmblWaitRepl:
					return 255; // red

				case RbOrderStatus.ReadyToUpload:
				case RbOrderStatus.InHistory:
					return 255; // red
					//return 49152; // green

				default:
					return 16711680; //
			}
		}
		#endregion


		#region LanguageId2String || created by (sergv)
		public static string LanguageId2String(OheLanguage lang)
		{
			switch (lang)
			{
				case OheLanguage.English:
					return "English";

				case OheLanguage.Chinese:
					return "Chinese";

				case OheLanguage.Portugal:
					return "Portugal";

				case OheLanguage.Russian:
					return "Russian";

				case OheLanguage.French:
					return "French";

				case OheLanguage.German:
					return "German";

				case OheLanguage.Spanish:
					return "Spanish";

				case OheLanguage.Thai:
					return "Thai";

				case OheLanguage.Test:
					return "Test";

				case OheLanguage.Latvian:
					return "Latvian";

				case OheLanguage.Czech:
					return "Czech";

				case OheLanguage.Estonian:
					return "Estonian";

				case OheLanguage.Lithuanian:
					return "Lithuanian";

				case OheLanguage.Others:
					return "Others";

				default:
					return "Undefined";
			}
		}
		#endregion

		#region String2LanguageId  || created by (mz) - returns defaultlanguage if no match
		public static OheLanguage Str2LanguageId(string strlang)
		{
			if (strlang == null || strlang.Trim()==string.Empty)
				return RbILSConfig.defaultLanguage;

			OheLanguage lang = String2LanguageId(strlang);
			if (lang == OheLanguage.Undefined)
				lang = RbILSConfig.defaultLanguage;

			return lang;
		}
		#endregion

		static public bool isEmpty(object obj, bool isTestZero)
		{
			if (isTestZero)
				return isEmpty(obj);

			if (obj is DBNull || obj == null)
				return true;

			return false;
		}
		#endregion
		*/

		#endregion


		#region Obj2Bool
		public static bool Obj2Bool(object obj)
		{
			if (obj is DBNull || obj == null)
				return false;

			switch (obj.GetType().Name.ToLower())
			{
				case "boolean":
					return (bool)(obj);

				case "string":
					return Utils.Str2Bool((string)obj);

				case "char":
					return Utils.Str2Bool(((char)obj).ToString());

				default:
					try
					{
						return Convert.ToBoolean(obj);
					}
					catch
					{
						throw new InvalidCastException();
					}
			}
		}
		#endregion
		

		#region CheckDate
		static public bool CheckDate(string cDate)
		{
			try
			{
				DateTimeFormatInfo fi = new DateTimeFormatInfo();
				fi.ShortDatePattern = getSystemConfig("DateFormat");
				if (fi.ShortDatePattern == null || fi.ShortDatePattern.Length == 0)
					fi.ShortDatePattern = "dd.MM.yyyy";
				Convert.ToDateTime(cDate, fi);
				return true;
			}
			catch
			{
				return false;
			}
		}
		#endregion


		#region UserCurrentDate
		static public string UserCurrentDate
		{
			get
			{
				string pattern = getSystemConfig("DateFormat");
				if (pattern.Length == 0)
					pattern = "MM.dd.yyyy";

				return pattern;
			}
		}
		#endregion


		#region getSystemConfig
		// возвращает значения конфигурации из узла system
		// необходимо задать имя узла из system
		public static string getSystemConfig(string Cnf)
		{
			try
			{
				XmlTextReader tr = new XmlTextReader(Utils.assemblyPath + @"config.xml");
				XmlDocument d = new XmlDocument();
				d.Load(tr);
				tr.Close();

				XmlNode root = d["system"];

				foreach (XmlNode node in root.ChildNodes)
				{
					if (node.Name.ToString().Trim() == Cnf.Trim())
					{
						return node.InnerXml.ToString();
					}

				}
				return string.Empty;
			}
			catch (Exception e) { return e.Message.ToString(); }
		}


		#endregion


		#region Obj2Int
		public static int Obj2Int(object obj)
		{
			if (obj is DBNull || obj == null)
				return 0;

			switch (obj.GetType().Name.ToLower())
			{
				case "int64":
					return Convert.ToInt32((Int64)obj);

				case "int32":
					return (int)obj;

				case "int16":
					return (Int16)obj;

				case "decimal":
					return Decimal.ToInt32((Decimal)obj);

				case "string":
					return Str2Int((string)obj);

				case "single":
					return (int)Math.Round((Single)obj);

				case "double":
					return (int)Math.Round((Double)obj);

				default:
					throw new InvalidCastException();
			}
		}
		#endregion


		#region isEmpty
		static public bool isEmpty(object obj)
		{
			if (obj is DBNull || obj == null)
				return true;

			switch (obj.GetType().Name)
			{
				case "Int16":
					return (((Int16)obj) == 0);

				case "Int32":
					return (((int)obj) == 0);

				case "Decimal":
					return (((Decimal)obj) == 0);

				case "Single":
					return (((Single)obj) == 0);

				case "Double":
					return (((double)obj) == 0);

				default:
					return (obj.ToString().Length == 0 || AllTrim(obj.ToString()).Length == 0);
			}
		}

		#endregion


		#region Obj2Str
		public static string Obj2Str(object obj)
		{
			if (obj is DBNull || obj == null)
				return string.Empty;

			switch (obj.GetType().Name)
			{
				case "DateTime":
                    return Obj2DbStringValue(obj);

                case "Int16":
                    return ((Int16)obj).ToString(CultureInfo.InvariantCulture);

                case "Int32":
                    return ((int)obj).ToString(CultureInfo.InvariantCulture);

                case "Decimal":
                    return ((Decimal)obj).ToString(CultureInfo.InvariantCulture);

                case "Single":
                    return ((Single)obj).ToString(CultureInfo.InvariantCulture);

                case "Double":
                    return ((double)obj).ToString(CultureInfo.InvariantCulture);

				default:
                    return AllTrim(obj.ToString());
			}
		}
		#endregion


        #region Obj2DbStringValue
        static public string Obj2DbStringValue(object obj)
        {
            if (obj is DBNull || obj == null)
                return "";
            else
                return Obj2DbStringValue(obj, obj.GetType());
        }


        public static string Obj2DbStringValue(object obj, Type Type)
        {

            if (obj is DBNull || Type == null)
                return "";

            switch (Type.Name)
            {
                case "Int16":
                    return ((Int16)obj).ToString(CultureInfo.InvariantCulture);

                case "Int32":
                    return ((int)obj).ToString(CultureInfo.InvariantCulture);

                case "Decimal":
                    return ((Decimal)obj).ToString(CultureInfo.InvariantCulture);

                case "Single":
                    return ((Single)obj).ToString(CultureInfo.InvariantCulture);

                case "Double":
                    return ((double)obj).ToString(CultureInfo.InvariantCulture);

                case "DateTime":
                    {
                        DateTime dt = (DateTime)obj;
                        if (dt == DateTime.MinValue || dt == DateTime.MaxValue)
                            return string.Empty;

                        try
                        {
                            string val = DateTime2UserString((DateTime)obj);
                            if (val == "01.01.1900")
                                return string.Empty;
                            else
                                return val;
                        }
                        catch { return ((DateTime)obj).ToString(CultureInfo.CurrentCulture); }
                    }

                case "String":
                    return AllTrim(obj.ToString());


                default:
                    return (AllTrim(obj.ToString()) == string.Empty ? "" : AllTrim(obj.ToString()));
            }

        }

        #endregion


		#region DateTime2UserString
		static public string DateTime2UserString(DateTime date)
		{
            if (date == null)
                return "";
			string format = OheWebConfig.DateFormat;
			if (format == null || format.Length == 0)
				format = "dd.MM.yyyy";

			return date.ToString(format);
		}
		#endregion


		#region DateTime2ServerString
        /*
		static public string DateTime2ServerString(DateTime date)
		{
			string format = OheWebConfig.GetFromXmlConfig("serverdateformat");
			if (format == null || format.Length == 0)
				format = "MM/dd/yyyy";

			return date.ToString(format,System.Globalization.CultureInfo.InstalledUICulture);
		}
        */
		#endregion


		#region Str2DateTime  (format is "DD.MM.YYYY")
		static public DateTime Str2DateTime(string str)
		{
			try
			{
				if (str == null || str.Length == 0)
					return new DateTime(0);

				//date format is "DD.MM.YYYY";
				int year = Convert.ToInt32(str.Substring(6, 4));
				int month = Convert.ToInt32(str.Substring(3, 2));
				int day = Convert.ToInt32(str.Substring(0, 2));

				return (new DateTime(year, month, day));
			}
			catch
			{
				return new DateTime(0);
			}
		}
		#endregion


        #region Str2Time  (format is "hh:mm.ss")
        static public TimeSpan Str2Time(string str)
        {
            try
            {
                if (string.IsNullOrEmpty(str))
                    return new TimeSpan(0,0,0);

                //date format is "DD.MM.YYYY";
                int hour = Convert.ToInt32(str.Substring(0, 2));
                int minute = Convert.ToInt32(str.Substring(3, 2));
                int second = Convert.ToInt32(str.Substring(6, 2));

                return (new TimeSpan(hour, minute, second));
            }
            catch
            {
                return Str2TimeNoSeconds(str);
            }
        }
        #endregion


        #region Str2Time  (format is "hh:mm")
        static public TimeSpan Str2TimeNoSeconds(string str)
        {
            try
            {
                if (string.IsNullOrEmpty(str))
                    return new TimeSpan(0, 0, 0);

                //date format is "DD.MM.YYYY";
                int hour = Convert.ToInt32(str.Substring(0, 2));
                int minute = Convert.ToInt32(str.Substring(3, 2));

                return (new TimeSpan(hour, minute, 0));
            }
            catch
            {
                return new TimeSpan(0, 0, 0);
            }
        }
        #endregion


		#region strIsEmpty
		public static bool strIsEmpty(string str)
		{
			if (str.Length == 0 || str == string.Empty) return true;
			else return false;
		} 
		#endregion


		#region Str2Bool
		static public bool Str2Bool(string str)
		{
			try
			{
				if (string.IsNullOrEmpty(str))
					return false;

				switch (str.ToLower())
				{
					case "1":
					case "t":
					case "true":
					case "y":
					case "yes":
					case "on":
						return true;

					default:
						return false;
				}
			}
			catch
			{
				return false;
			}
		}
		#endregion


		#region Substring
		public static string Substring(string str, int start, int len)
		{
			if (string.IsNullOrEmpty(str))
				return string.Empty;

			if (start < 0)
				start = 0;

			if (str.Length < start)
				return string.Empty;

			if (len < 0)
				return str.Substring(start);

			if (len == 0)
				return string.Empty;


			return (str.Length < start + len
				? str.Substring(start)
				: str.Substring(start, len));
		}

		public static string Substring(string str, int start)
		{
			if (start < 0)
				start = 0;
			return ((str == null || str.Length <= start) ? string.Empty : str.Substring(start));
		}
		#endregion


		#region Left
		public static string Left(string str, int length)
		{
			if (str == null || length == 0)
				return string.Empty;

			if (str.Length <= length)
				return str;

			return str.Substring(0, length);
		}
		#endregion


        #region Right
        public static string Right(string str, int length)
        {
            if (str == null || length == 0)
                return string.Empty;

            if (str.Length <= length)
                return str;

            string ss = str.Substring(str.Length - length);

            return str.Substring(str.Length-length);
        }
        #endregion
        

		#region ParseUrl
        /// <summary>
        /// Парсинг параметров контролов пришедших с формы.
        /// Параметры могут иметь одно значение. Если имеют одно название в таблице и на форме
        /// В этом случае берется первое название параметра
        /// Надо учитывать порядок отправки параметров в приложение
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
		public static Hashtable ParseUrl(string url)
		{
            Hashtable rez = new Hashtable(new OheCultureComparer());

			//url = HttpUtility.UrlDecode(AllTrim(url));//,Encoding.GetEncoding("windows-1251");
			url = AllTrim(url);

			if (url.Length == 0)
				return rez;

			if (url.IndexOf("reportmenuurl") != -1)
				url = url.Replace("%", "%25");

            // Исправлено. В Web клиенте все передается в кодировке UTF8
            // Если НЕкомментировано корректно работает WEB Dispatch, 
            // Если ЗАзакомментировно работает WEB Terminal
            // Проверено на IIS 6.0 Windows 2003 Server 
            //url = url.Replace("+", "%2b");

            // Преобразовываем в пробел, т.к. перед этой функцией вызывается UrlDecode, которая преобразовывает " " в +
            // Тут мы делаем обратное преобразование.
            url = url.Replace("+", "%20");
            

			int startPos = url.IndexOf('?');

			if (startPos != -1 && startPos < url.Length)
				url = Utils.Substring(url, startPos + 1);

			string[] arr = url.Split('&');

			foreach (string node in arr)
			{
				if (node != null && node.Length != 0)
				{
                    string key = node.Replace("_popup", "");
                    int pos = key.IndexOf('=');
					if (pos != -1)
					{
                        if (!rez.Contains(AllTrim(Left(key, pos)).ToLower()))
                        {
                            // Исправлено. В Web клиенте все передается в кодировке UTF8
                            // Если НЕкомментировано корректно работает WEB Dispatch, 
                            // Если ЗАзакомментировно работает WEB Terminal
                            // Проверено на IIS 6.0 Windows 2003 Server 
                            rez[AllTrim(Left(key, pos)).ToLower()] = HttpUtility.UrlDecode(AllTrim(Substring(key, pos + 1)));
                        }
					}
				}
			}
			return rez;
		}
		#endregion


		#region Obj2Decimal
		static public decimal Obj2Decimal(object obj)
		{
			if (obj is DBNull || obj == null)
				return 0m;

			switch (obj.GetType().Name)
			{
				case "Decimal":
					return (decimal)obj;

				case "Single":
					return Convert.ToDecimal((Single)obj);

				//				case "Int32":
				//					return (decimal)obj;

				//				case "Int64":
				//					return (decimal)obj;

				case "String":
					return Decimal.Parse((string)obj, NumberStyles.Any, CultureInfo.InvariantCulture);

				default:
					try
					{
						return Convert.ToDecimal(obj);
					}
					catch
					{
						throw new InvalidCastException("Не могу преобразовать " + obj.GetType().Name + " в decimal.");
					}
			}
		}
		#endregion


		#region Obj2DateTime
		static public DateTime Obj2DateTime(object obj)
		{
			if (obj is DBNull || obj == null)
				return new DateTime(0);

			switch (obj.GetType().Name)
			{
				case "DateTime":
					return (DateTime)obj;

				case "String":
					return Str2DateTime((string)obj);

				default:
                    throw new InvalidCastException();
			}
		}
		#endregion


        #region Obj2Time
        static public TimeSpan Obj2Time(object obj)
        {
            if (obj is DBNull || obj == null)
                return new TimeSpan(0,0,0);

			switch (obj.GetType().Name)
			{
				case "DateTime":
					return ((DateTime)obj).TimeOfDay;

				case "String":
					return Str2Time((string)obj);

                case "TimeSpan":
                    return (TimeSpan)obj;

				default:
					
                    throw new InvalidCastException();
			}
		}
		#endregion
        

        #region Obj2Float
        static public float Obj2Float(object obj)
        {
            if (obj is DBNull || obj == null)
                return 0;

            switch (obj.GetType().Name)
            {
                case "Decimal":
                    //return obj.ToString();
                case "Single":
                    //return obj.ToString();
                case "Double":
                    return float.Parse(obj.ToString());

                default:
                    try
                    {
                        return (float)obj;
                    }
                    catch
                    {
                        throw new InvalidCastException("Can't convert " + obj.GetType().Name + " to double.");
                    }
            }
        }
        #endregion
        

		#region Obj2Double
		static public double Obj2Double(object obj)
		{
			if (obj is DBNull || obj == null)
				return 0;

			switch (obj.GetType().Name)
			{
				case "Decimal":
					return Decimal.ToDouble((Decimal)obj);

				case "Single":
					return (Single)obj;

				case "Double":
					return (double)obj;

				default:
					try
					{
						return Convert.ToDouble(obj);
					}
					catch
					{
						throw new InvalidCastException("Can't convert " + obj.GetType().Name + " to double.");
					}
			}
		}
		#endregion


		#region String2DataType
		static public object String2DataType(string str, Type rezType, bool isRaiseExceptions)
		{
			if (str == null)
				return null;

			str = AllTrim(str);

			try
			{
				switch (rezType.Name.ToLower())
				{
					case "int64":
						return Int64.Parse(str, NumberStyles.Any);

					case "char":
						return (str.Length > 0 ? str[0] : ' ');

					case "datetime":
						DateTime rez;
						if (UserString2DateTime(str, out rez))
							return rez;
						else
						{
							if (isRaiseExceptions)
								throw new FormatException("Unparsable datetime passed [" + str + "] expected format [" +
								  OheWebConfig.DateFormat + "]");
							else
								return rez;
						}

					case "decimal":
						return Decimal.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);

					case "double":
					case "float":
						return Double.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);

					case "int":
					case "int32":
						return Int32.Parse(str, NumberStyles.Any);

					case "string":
						return str;

					case "single":
						return Single.Parse(str, NumberStyles.Any);

					case "int16":
						return Int16.Parse(str, NumberStyles.Any);

					case "byte":
						return byte.Parse(str, NumberStyles.Any);

					case "boolean":
					case "bool":
						return Str2Bool(str);

					default:
						throw new FormatException("Unknown format type [" + rezType.Name + "] parameter [" + str + "]");
				}
			}
			catch
			{
				if (isRaiseExceptions)
					throw;
				else
					return null;
			}
		}
		#endregion


        #region StringIsNumber
        static public bool StringIsNumber(string str)
        {
            try
            {
				switch (str.ToUpperInvariant())
					{
						#region Проверка типов
						case "INT64":
						case "BIGINT":
                            return true;
						case "CHAR":
						case "VARCHAR":
                            return false;
						case "XML":
                            return false;
						case "DATETIME":
                            return false;
						case "DECIMAL":
						case "MONEY":
                            return true;
						case "DOUBLE":
						case "FLOAT":
                            return true;
						case "INT":
                        case "SMALLINT":
						case "INT32":
                            return true;
						case "STRING":
                            return false;
						case "INT16":
                            return true;
						case "BYTE":
                            return true;
						case "BOOLEAN":
						case "BOOL":
						case "BIT":
                            return false;
						case "UNIQUEIDENTIFIER":
                            return false;

						default:
							throw new FormatException("Неизвестный формат данных (" + str + ")");

						#endregion
					}
            }
            catch
            {
                    return false;
            }
        }
        #endregion


        #region Str2ToJson
        public static string Str2ToJson(string str)
        {
            if (string.IsNullOrEmpty(str))
                return "null";

            return str.Replace("\n", "").Replace("\r", "").Replace("\"", "'").Replace("\\", "?");
        }
        #endregion


        #region Str2Html

	    private static string Str2HtmlConvert(string i)
	    {
	        switch (i)
	        {
	            case "'":
	                return "&#039;";
                case "\"":
                    return "&quot;";
	            case "&":
	                return "&amp;";
	            case "<":
	                return "&lt;";
	            case ">":
	                return "&gt;";
	            case "‘":
	                return "&lsquo;";
	            case "’":
	                return "&rsquo;";
	            case "‚":
	                return "&sbquo;";
	            case "“":
	                return "&ldquo;";
	            case "”":
	                return "&rdquo;";
	            case "„":
	                return "&bdquo;";
	            case "″":
	                return "&Prime;";
	            case "«":
	                return "&laquo;";
	            case "»":
	                return "&raquo;";
	            case "‹":
	                return "&#8249;";
	            case "›":
	                return "&#8250;";
	            case "(":
	                return "&#40;";
	            case ")":
	                return "&#41;";
	            case "/":
	                return "&#47;";
	            case "$":
	                return "&#36;";
	            case "%":
	                return "&#37;";
	            case "\\":
	                return "&#92;";
	            case "[":
	                return "&#91;";
	            case "]":
	                return "&#93;";
	            case "^":
	                return "&#94;";
	            case "\t":
	                return " ";
	            case "\n\r":
	                return "<br/>";
	            case "\n":
	                return "<br/>";
	            case "\r":
	                return "";
	            default:
	                {
	                    return i;
	                }
	        }
	    }

	    public static string Str2Html(string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";
            
            if (str.StartsWith("<"))
                return str;

            // Эта функция некорректно работает в некоторых ситуациях для html отображение.
            //return HttpUtility.HtmlEncode(str);

	        var rez = new  StringBuilder();
            
            for (int i = 0; i< str.Length; i++)
            {
                rez.Append(Str2HtmlConvert(str.Substring(i, 1)));
            }
/*
                rez += switch( str.Substring(i,1)).Replace("'", "&#039;")
                          .Replace("&", "&amp;")
                          .Replace("<", "&lt;")
                          .Replace(">", "&gt;")
                          .Replace(">", "&gt;")
                          .Replace("‘", "&lsquo;")
                          .Replace("’", "&rsquo;")
                          .Replace("‚", "&sbquo;")
                          .Replace("“", "&ldquo;")
                          .Replace("”", "&rdquo;")
                          .Replace("„", "&bdquo;")
                          .Replace("″", "&Prime;")
                          .Replace("«", "&laquo;")
                          .Replace("»", "&raquo;")
                          .Replace("‹", "&#8249;")
                          .Replace("›", "&#8250;")
                          .Replace("(", "&#40;")
                          .Replace(")", "&#41;")
                          .Replace("/", "&#47;")
                          .Replace("$", "&#36;")
                          .Replace("%", "&#37;")
                          .Replace("\\", "&#92;")
                          .Replace("[", "&#91;")
                          .Replace("]", "&#93;")
                          .Replace("^", "&#94;")
                          .Replace("\t", " ")
                          .Replace("\n\r", "<br/>")
                          .Replace("\n", "<br/>")
                          .Replace("\r", "");
                */
            return rez.ToString();
        }
        #endregion


        #region Str2Telnet
        public static string Str2Telnet(string str)
        {
            if(string.IsNullOrEmpty(str))
                return string.Empty;

            return AllTrim(str.Replace("\r", string.Empty)
                              .Replace("\n", string.Empty)
                              .Replace("{0}", Environment.NewLine)
                              .Replace("{", string.Empty)
                              .Replace("}", string.Empty)
                );

        }
        #endregion 

        #region Str2TlnServerString
        /// <summary>
        /// Удаляет лишние данные пришедшие от сканирования в телнет
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Str2TlnServerString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            return AllTrim(str.Replace("я", "")
                     .Replace("ь", "")
                     .Replace("&", "")
                     .Replace("э", "")
                     .Replace("ю", "")
                     .Replace("&#x6;", "")
                     .Replace("#x6;", "")
                     .Replace(",", ""));
        }
        #endregion


        #region DoubleTo3DStr
        public static string DoubleTo3DStr(double str)
        {
            try
            {
                if (str == null)
                    return "0.0";
                return Utils.AllTrim(str.ToString().Replace(",","."));//Double.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
            }
            catch
            {
                return "0.0";
            }
        }
        #endregion


        #region Str2Decimal
        public static Decimal Str2Decimal(string str)
		{
			try
			{
				if (str == null || str.Length == 0)
					return 0m;

				if (str[0] == '.' || str[0] == ',')
					str = "0" + str;
				else
					if (!char.IsDigit(str[0]))
						return 0m;

				return Decimal.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
			}
			catch
			{
				return 0m;
			}
		}

		public static Decimal Str2Decimal(string str, Decimal defaultValue)
		{
			try
			{
				if (str == null || str.Length == 0 || !char.IsDigit(str[0]))
					return defaultValue;
				return Decimal.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
			}
			catch
			{
				return defaultValue;
			}
		}
		#endregion


        #region Str2Double  || created by (sergv)
        public static double Str2Double(string str)
        {
            try
            {
                if (str == null || str.Length == 0 || (!char.IsDigit(str[0]) && str[0] != '.'))
                    return 0.0;

                return Double.Parse((str[0] == '.' ? '0' + str : str), NumberStyles.Any, CultureInfo.InvariantCulture);
            }
            catch
            {
                return 0.0;
            }
        }

        public static double Str2Double(string str, double defVal)
        {
            try
            {
                if (str == null || str.Length == 0 || !char.IsDigit(str[0]))
                    return defVal;
                return Double.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);
            }
            catch
            {
                return defVal;
            }
        }
        #endregion


        #region GetParamValue - возвращает значение параметра
        /// <summary>
        /// Возвращает значения параметра из кеша приложения
        /// </summary>
        /// <param name="prm"></param>
        public static string GetParamValue(string Id, string Column, Dictionary<string, object> Cache)
        {
            if (Id == string.Empty)
                return Id;
            // Определяем пустое значение параметра если записи
            // нет в глобальном кеше
            if (!Cache.ContainsKey(Id))
            {
                return "N";
                //return string.Empty;
            }

            var obj = (object)Cache[Id];
            if (obj is DataTable)
            {
                int col = -99;

                try
                {
                    // Используем колонки для записи если их нашли
                    if (Column != "-99")
                    {
                        col = ((DataTable)obj).Columns[Column].Ordinal;
                    }
                    else if (col == -99)
                        col = 0;

                    return ((DataTable)obj).Rows[0].ItemArray[col].ToString();
                }
                catch { return string.Empty; }
            }
            else if (obj is string)
            {
                return Utils.AllTrim(((string)obj));
            }
            return string.Empty;
        }

        #endregion


		/// <summary>
		/// Возвращает название объекта из строки
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static string GetObjName(string obj)
		{
		    if (!string.IsNullOrEmpty(obj))
			{
			    // Возвращаем только название записи
				if (obj.Contains("(") && obj.Contains(")"))
					return AllTrim(obj.Substring(0, obj.IndexOf("(")));

			    return AllTrim(obj);
			}

		    return string.Empty;
		}

	    /// <summary>
		/// Возвращает название колонки из строки
		/// </summary>
		/// <param name="Obj"></param>
		/// <returns></returns>
		public static string GetObjColumn(string Obj)
	    {
	        if (Obj.Contains("(") && Obj.Contains(")"))
			{
				string d = Obj.Substring(Obj.IndexOf("(") + 1, Obj.Length - Obj.IndexOf("(") - 2);
				if (d.Length > 0)
					return d;

			    return "0";
			}

	        return "0";
	    }

	    #region UploadStatusFile
        /// <summary>
        /// Записывает подтверждения о статусах в файл
        /// </summary>
        /// <param name="str">Содержимое которое надо записать в файл</param>
        /// <param name="fname">Имя файла с путем</param>
        public static bool UploadStatusFile(string str, string fname, string Collate)
        {
            try
            {
                // iso-8859-5
                Encoding ascii = Encoding.GetEncoding(Collate);
                Encoding unicode = Encoding.Unicode;

                // Convert the string into a byte[].
                byte[] unicodeBytes = unicode.GetBytes(str);

                // Perform the conversion from one encoding to the other.
                byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

                // Convert the new byte[] into a char[] and then into a string.
                // This is a slightly different approach to converting to illustrate
                // the use of GetCharCount/GetChars.
                char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
                ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
                string asciiString = new string(asciiChars);

                if (!File.Exists(fname))
                {
                    using (StreamWriter s = new StreamWriter(fname, false, ascii))
                        s.Write(asciiString);
                    return true;
                }
                return false;
            }
            catch { return false; }
        }
        #endregion


        //input a string to be converted to Sql format
        //Возвращает формат строки SQL для MSSQL сервера
        public static string Prm2Sql(string var)
        {
            return string.IsNullOrEmpty(var) ? "" : AllTrim(var.Replace("'", "''"));
        }

	    #region Don't use now
		/*


		#region ObjArray2StringArray  || created by (sergv)
		public static string[] ObjArray2StringArray(object[] arr)
		{
			if (arr == null || arr.Length == 0)
				return new string[0];
			string[] rez = new string[arr.Length];
			for (int i=0; i<arr.Length; i++)
				rez[i] = (arr[i] == null ? string.Empty : AllTrim(arr[i].ToString()));
			return rez;
		}
		#endregion

		#region Str2SqlStr || created by (sergv)
		static public string Str2SqlStr(string val, string valType)
		{
			switch (valType.ToLower())
			{
				case "int16":
				case "int32":
					return Str2Int(val, 0).ToString();

				case "decimal":
					return Str2Decimal(val, 0).ToString();

				case "single":
				case "double":
					return Str2Double(val, 0).ToString();

				default:
					return ("'"+AllTrim(val)+"'");
			}
		}
		#endregion
		



		static public bool Str2Bool(string str, bool defVal)
		{
			try
			{
				if (str == null || str.Length == 0)
					return defVal;
				
				switch (str.ToLower())
				{
					case "1":
					case "t":
					case "true":
					case "y":
					case "yes":
					case "on":
						return true;

					default:
						return false;
				}
			}
			catch
			{
				return false;
			}
		}
		#endregion

		#region Bool2Str  || created by (sergv)
		static public string Bool2Str(bool val, string trueValue, string falseValue)
		{
			trueValue	= Utils.AllTrim(trueValue);
			falseValue	= Utils.AllTrim(falseValue);

			return (val ? (trueValue.Length == 0 ? "true" : trueValue) : (falseValue.Length == 0 ? "false" : falseValue));
		}
		#endregion


		#region DateTime2LongUserString
		static public string DateTime2LongUserString(DateTime date)
		{
			string format = RbXmlConfig.GetFromXmlConfig("longDateFormat");
			if (format == null || format.Length == 0)
				format = "MM/dd/yyyy hh:mm:ss";

			return date.ToString(format);
		}
		#endregion

		#region UserString2LongDateTime (true - converted)
		static public bool UserString2LongDateTime(string str, out DateTime rez)
		{
			try
			{
				if (str == null || str.Length == 0)
				{
					rez = DateTime.Now;
					return true;
				}

				DateTimeFormatInfo fi = new DateTimeFormatInfo();
				fi.LongDatePattern = RbXmlConfig.LongDateFormat;

				rez = Convert.ToDateTime(str, fi);
				return true;
			}
			catch
			{
				rez = DateTime.Now;
				return false;
			}
		}
		#endregion

		#region UserString2TimeSpan (true - converted)
		static public bool UserString2TimeSpan(string str, out TimeSpan rez)
		{
			try
			{
				if (str == null || str.Length == 0)
				{
					rez = TimeSpan.MinValue;
					return true;
				}

				rez = TimeSpan.Parse(str);
				return true;
			}
			catch
			{
				rez = TimeSpan.MinValue;
				return false;
			}
		}
		#endregion

		#region DateTime2Str(format is "YYYYMMDD")  || created by (sergv)
		static public string DateTime2Str(DateTime dt)
		{
			return dt.ToString("yyyyMMdd");
		}
		#endregion


		#region BuildFromToDateTitleString  || created by (sergv)
		static public string BuildFromToDateTitleString(DateTime fromDate, DateTime toDate)
		{
			return RbILSConfig.Translate("from", OheLanguage.English)+" "+fromDate.ToShortDateString()
				+" "+RbILSConfig.Translate("to", OheLanguage.English)+" "+toDate.ToShortDateString();
		}
		#endregion

		#region BuildOnDateTitleString  || created by (sergv)
		static public string BuildOnDateTitleString(DateTime onDate)
		{
			return RbILSConfig.Translate("on", OheLanguage.English)+" "+onDate.ToShortDateString();
		}
		#endregion

		#region BuildClientTitleString  || created by (sergv)
		static public string BuildClientTitleString(string clientName)
		{
			return (clientName == null || clientName.Length == 0 ? string.Empty : RbILSConfig.Translate("client", OheLanguage.English)+" "+clientName);
		}
		#endregion


		#region ExpandStringArray  || created by (sergv)
		public static string[] ExpandStringArray(string[] ar, int newSize)
		{
			string[] rez = new string[Math.Max(ar.Length, newSize)];
			ar.CopyTo(rez, 0);
			for (int i=0; i < rez.Length; i++)
				if (rez[i]==null)
					rez[i] = string.Empty;
			return rez;
		}
		#endregion

		#region Add2StringArray  || created by (sergv)
		public static void Add2StringArray(ref string[] ar, string[] values)
		{
			string[] rez = new string[ar.Length+values.Length];
			if (ar.Length > 0)
				ar.CopyTo(rez, 0);
			
			for (int i=ar.Length, j=0; i < rez.Length; i++, j++)
				rez[i] = values[j];
			ar = rez;
		}
		#endregion


		#region transferBO2Order || created by (sergv) [11/21/2003]
		public static string transferBO2Order(string bo)
		{
			bo = Utils.AllTrim(bo);
			if (bo.Length == 0)
				return string.Empty;

			int boPos = bo.IndexOf('-');

			if (boPos > 0)
				return bo.Substring(0, boPos);
			else
				return bo;
		}
		#endregion


		#region Left (safe version) || created by (sergv)
		public static string Left(string str, int length)
		{
			if (str == null || length == 0)
				return string.Empty;

			if (str.Length <= length)
				return str;

			return str.Substring(0, length);
		}
		#endregion

		#region Right (safe version) || created by (sergv)
		public static string Right(string str, int len)
		{
			if (str == null || len <= 0)
				return string.Empty;

			if (str.Length <= len)
				return str;

			return str.Substring(str.Length-len);
		}
		#endregion

		#region Substring (safe version) || created by (sergv)
		public static string Substring(string str, int start, int len)
		{
			if (str == null || str == string.Empty)
				return string.Empty;

			if (start < 0)
				start = 0;

			if (str.Length < start)
				return string.Empty;
			
			if (len < 0)
				return str.Substring(start);
			
			if (len == 0)
				return string.Empty;


			return (str.Length < start+len
				? str.Substring(start)
				: str.Substring(start, len));
		}

		public static string Substring(string str, int start)
		{
			if (start < 0)
				start = 0;
			return ((str == null || str.Length <= start)?string.Empty:str.Substring(start));
		}
		#endregion

		#region Replace (safe version) || created by (sergv)
		public static string Replace(string str, char from, char to)
		{
			if (str == null || str.Length == 0)
				return string.Empty;

			return str.Replace(from, to);
		}

		public static string Replace(string str, string from, string to)
		{
			if (str == null || str.Length == 0)
				return string.Empty;

			return str.Replace(from, to);
		}
		#endregion
		
		#region CountSubstr (safe version) || created by (sergv)
		static public int CountSubstr(string str, string substr)
		{
			if (str==null || str.Length==0 || substr==null || substr.Length==0)
				return 0;

			int count =0;

			for (int start=0; (start+1)<str.Length && start>0; start++, count++)
				start = str.IndexOf(substr, start+1);

			return count;
		}
		#endregion


		#region BeforeAtNum/AfterAtNum || created by (sergv) [11/21/2003]
		static public string BeforeAtNum(string str, string substr, int count)
		{
			if (str == null || str.Length == 0)
				return string.Empty;

			string rez = Left(str, str.LastIndexOf(substr, count));

			return (rez.Length>0?rez:str);
		}

		static public string AfterAtNum(string str, string substr, int count)
		{
			if (str == null || str.Length == 0)
				return string.Empty;

			int pos = str.LastIndexOf(substr, 0, count);

			if (pos == -1)
				return str;

			string rez = Right(str, str.Length-pos);

			return (rez.Length>0?rez:str);
		}
		#endregion

		#region Add item || created by (sergv) [11/21/2003]
		static public bool AddItem(ref ArrayList arr, object item)
		{
			if (arr.IndexOf(item) != -1)
				return false;

			arr.Add(item);
			return true;
		}
		#endregion

		#region Convert packslip seq string into array || created by (sergv) [11/21/2003]
		static private ArrayList DividePksFromRaw(string rawData)
		{
			rawData = AllTrim(rawData);
			if (rawData.Length == 0)
				return null;

			ArrayList packslips = new ArrayList();
			int nStart = 0;
			int nEnd = 0;
			while (nEnd != -1)
			{
				nEnd = rawData.IndexOf(",{", nStart+1);
				packslips.Add(rawData.Substring(nStart, (nEnd==-1?rawData.Length-nStart:nEnd-nStart)));
				nStart = nEnd;
			}
			return packslips;
		}


		// rawdata like {'pks1', 'seq1'},{'pks2', 'seq2'},{'pks3',}
		static public string[] ConvertPks2PksArr(string rawData)
		{
			ArrayList packslips = DividePksFromRaw(rawData);

			if (packslips == null || packslips.Count == 0)
				return new string[0];

			string[] pks = new string[packslips.Count];
			for (int i=0; i<pks.Length; i++)
			{
				string ar = (string)packslips[i];
				if (ar[0] == ',')
					ar = ar.Substring(3,ar.Length-5);
				else
					ar = ar.Substring(2,ar.Length-4);
				int pksEndPos = ar.IndexOf("'");
				if (pksEndPos == -1)
					pksEndPos = ar.IndexOf("\"");
				pks[i] = ar.Substring(0, pksEndPos);
			}
			return pks;
		}
		static public string[] ConvertExts2ExtsArr(string rawData)
		{
			ArrayList Extendeds = DividePksFromRaw(rawData);

			if (Extendeds == null || Extendeds.Count == 0)
				return new string[0];

			string[] pks = new string[Extendeds.Count];
			for (int i=0; i<pks.Length; i++)
			{
				string ar = (string)Extendeds[i];
				if (ar[0] == ',')
					ar = ar.Substring(2,ar.Length-3);
				else
					ar = ar.Substring(1,ar.Length-2);
				pks[i] = ar;
			}
			return pks;
		}

		static public string[][] ConvertPks2Arr(string rawData)
		{
			ArrayList packslips = DividePksFromRaw(rawData);

			if (packslips == null || packslips.Count == 0)
				return new string[0][];

			string[][] pks = new string[packslips.Count][];
			for (int i=0; i<pks.Length; i++)
			{
				pks[i] = new string[2];
				string ar = (string)packslips[i];
				if (ar[0] == ',')
					ar = ar.Substring(3,ar.Length-5);
				else
					ar = ar.Substring(2,ar.Length-4);
				int pksEndPos = ar.IndexOf("'");
				pks[i][0] = ar.Substring(0, pksEndPos);
				int seqStrtPos = ar.IndexOf(",'");
				if (seqStrtPos==-1)
					pks[i][1] = null;
				else
				{
					ar = ar.Substring(seqStrtPos+2);
					pks[i][1] = (ar==null || ar.Length==0?null:ar);
				}
			}
			return pks;
		}
		#endregion

		#region convert update string to update array
		//rawdata like {{field, value},{field,value}}
		static public string[][] ConvertUpdates2Arr(string rawData)
		{
			//assume no space is inside any field/value
			//remove all spaces
			rawData = rawData.Replace(" ",string.Empty);

			if (rawData.Length >4)
			{
				//remove begin {{ and end }}
				rawData = rawData.Substring(2,rawData.Length-4);

				//remove }, from array seperator, leave only {
				rawData = rawData.Replace("},", string.Empty);

				//first level array
				string[] temp = rawData.Split("{".ToCharArray());

				if (temp.Length > 0)
				{
					ArrayList rezAL = new ArrayList();
					for (int i=0; i< temp.Length; i++)
					{
						//add an array to ArrayList
						rezAL.Add(temp[i].Split(",".ToCharArray()));
					}

					string[][] rez = new string[rezAL.Count][];
					rezAL.CopyTo(rez);

					return rez;
				}
			}

			return new string[][] {};
		}
		#endregion

		#region get location/shipvia/manifest
		static public string[][] GetManifestViaFromPar(string rawData)
		{
			rawData = Utils.AllTrim(rawData);

			if (rawData.Length == 0)
				return new string[0][];

			string[] manifestVia = rawData.Split(',');

			if (manifestVia == null || manifestVia.Length == 0)
				return new string[0][];

			string[][] pks = new string[manifestVia.Length][];
			for (int i=0; i<pks.Length; i++)
			{
				pks[i] = manifestVia[i].Split('|');
			}
			return pks;
		}	
		#endregion

		#region Convert NameValueCollection into string array with filter || created by (sergv) [11/21/2003]
		static public string[][] ConvertCol2Ar(NameValueCollection cltn, string filter)
		{
			ArrayList prms = new ArrayList();
			foreach (string key in cltn.AllKeys)
				if (key.StartsWith(filter))
					prms.Add(new string[2] {key, cltn[key]});

			if (prms.Count == 0)
				return null;

			string[][] opts = new string[prms.Count][];

			for (int i=0; i<opts.Length; i++)
				opts[i] = (string[])prms[i];

			return opts;
		}
		#endregion

		#region GetStrFromXml || created by (sergv) [11/21/2003]
		public static string GetStrFromXml(XmlNode node, string name, string defval)
		{
			try
			{
				XmlNode tmpnode = node[name];
				return ((tmpnode == null)?defval:tmpnode.InnerXml);
			}
			catch
			{
				return defval;
			}
		}
		#endregion

		#region GetAttrsFromXml || created by (sergv) [11/21/2003]
		public static Hashtable GetAttrsFromXml(XmlNode node, string name)
		{
			Hashtable rez = new Hashtable();

			try
			{
				XmlNode tmpnode = node[name];
				if (tmpnode == null || tmpnode.Attributes == null)
					return rez;
				foreach (XmlAttribute attr in tmpnode.Attributes)
					rez.Add(attr.Name, attr.Value);
				return rez;
			}
			catch
			{
				return rez;
			}
		}
		#endregion

		#region PutStr2Xml || created by (sergv) [11/21/2003]
		public static XmlElement PutStr2Xml(XmlDocument doc, XmlNode node, string name, string val)
		{
			try
			{
				XmlElement element = doc.CreateElement(name.Replace(' ', '_'));
				if (val != null)
					element.InnerText = val;
				node.AppendChild(element);
				return element;
			}
			catch
			{
				return null;
			}
		}
		#endregion

		#region PutAttrs2Xml || created by (sergv) [11/21/2003]
		public static XmlElement PutAttrs2Xml(XmlDocument doc, XmlNode node, string name, Hashtable attrs, string val)
		{
			try
			{
				XmlElement element = doc.CreateElement(name.Replace(' ', '_'));
				if (val != null)
					element.InnerText = val;
				if (attrs != null)
					foreach (String attrName in attrs.Keys)
					{
						XmlAttribute attr = doc.CreateAttribute(attrName);
						attr.Value = (attrs[attrName]==null ? string.Empty : attrs[attrName].ToString());
						element.Attributes.Append(attr);
					}
				node.AppendChild(element);
				return element;
			}
			catch
			{
				return null;
			}
		}
		#endregion

		#region SaveXml || created by (sergv) [11/21/2003]
		public static bool SaveXml(XmlDocument doc, string fileName)
		{
			try
			{
				XmlTextWriter writer = new XmlTextWriter(fileName, null);
				writer.Formatting = Formatting.Indented;
				doc.Save(writer);
				writer.Flush();
				writer.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}
		#endregion

		#region SaveString || created by (sergv) [11/21/2003]
		public static bool SaveString(string fileName, string str)
		{
			return SaveString(fileName, str, false);
		}

		public static bool SaveString(string fileName, string str, bool isThrowException)
		{
			try
			{
				using (StreamWriter sw = new StreamWriter(fileName))
				{
					sw.Write(str);
					sw.Close();
				}
				return true;
			}
			catch
			{
				if (isThrowException)
					throw;
					
				return false;
			}
		}
		#endregion

		#region LoadString || created by (sergv) [11/21/2003]
		public static string LoadString(string fileName)
		{
			return LoadString(fileName, false);
		}

		public static string LoadString(string fileName, bool rethrowExceptions)
		{
			try
			{
				fileName = AllTrim(fileName);

				if (fileName.Length == 0)
					return string.Empty;

				using (StreamReader sr = new StreamReader(fileName))
				{
					return AllTrim(sr.ReadToEnd());
				}
			}
			catch
			{
				if (rethrowExceptions)
					throw;
				else
					return string.Empty;
			}
		}
		#endregion

		#region FixSqlString || created by (sergv) [11/21/2003]
		public static string FixSqlString(string val, int len)
		{
			if (val == null || val.Length == 0)
				return val;
			return Utils.Left(val, len).Replace("'", "''");
		}
		#endregion

		//=============================================================================================================================



		//==================================================================================================
		#region
		//convert to T-SQL syntex of determine if a field is null, use empty string instead.
		public static string SQLIsNull(string cExpr)
		{
			return "ISNULL(" + cExpr + ",'')";
		}

		//input a string to be converted to Sql format
		//returns sql format of the string
		public static string var2sql(string var)
		{
			if (var==null)
				return "''";
			return "'"+(var.IndexOf("'")>=0?var.Replace("'","''"):var)+"'";  //assume argument var is not null and has already been trimmed.
		}

		public static string var2sqlNull(string var)
		{
			if (var==null) var=string.Empty;
			return var2sql(var);
		}

		//input an integer to be converted to Sql format
		//returns sql format of the integer
		public static string var2sql(int var)
		{
			return var.ToString().Trim().Replace(",",string.Empty); //assume argument var is not null
		}

		//input a double to be converted to Sql format
		//returns sql format of the double
		public static string var2sql(double var)
		{
			string ret = var.ToString().Trim(); //assume argument var is not null
			if (ret.IndexOf(",")>=0 || ret.IndexOf(" ")>=0) //some fancy european format
			{
				string gSep = CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator;
				if (gSep.Length > 0)
					ret = ret.Replace(gSep,string.Empty);  //just a group seperator, get rid of it.

				string dSep = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
				if (dSep != ".")
					ret = ret.Replace(dSep,".");          //this is a decimal seperator, replace with "."
			}
			return ret; 
		}

		//input a decimal to be converted to Sql format
		//returns sql format of the decimal
		public static string var2sql(decimal var)
		{
			string ret = var.ToString().Trim(); //assume argument var is not null
			if (ret.IndexOf(",")>=0 || ret.IndexOf(" ")>=0) //some fancy european format
			{
				string gSep = CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator;
				if (gSep.Length > 0)
					ret = ret.Replace(gSep,string.Empty);  //just a group seperator, get rid of it.

				string dSep = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
				if (dSep != ".")
					ret = ret.Replace(dSep,".");          //this is a decimal seperator, replace with "."
			}
			return ret; 
		}

		//input a DateTime to be converted to Sql format
		//returns sql format of the decimal
		public static string var2sql(DateTime var)
		{
			//return "'"+System.Convert.ToString(var).Trim()+"'"; //assume argument var is not null
			return "'"+var.ToString("G", DateTimeFormatInfo.InvariantInfo).Trim()+"'";
		}
		#endregion

		#region DateTime
		//returns the date as a string in the format of "yyyyMMdd"
		public static string Date()
		{
			return DateTime.Now.ToString("yyyyMMdd");
		}

		//returns the date as a string by giving format
		public static string Date(string cFormat)
		{
			//return DateTime.Now.ToString("d", DateTimeFormatInfo.InvariantInfo);
			return DateTime.Now.ToString(cFormat);
		}

		///returns the 24hr time as strng in the format hh:mm:ss
		public static string Time()
		{
			return DateTime.Now.TimeOfDay.ToString().Substring(0,8);
		}
		public static string Time(int length)
		{
			return DateTime.Now.TimeOfDay.ToString().Substring(0,length);
		}

		public static string ShortTime()
		{
			return DateTime.Now.TimeOfDay.ToString().Substring(0,5);
		}
		
		//return Time string by giving length, full length (16) is "hh:mm:ss.mssssss"
		public static string ShortTime(int length)
		{
			if (length.Equals(null) || length <1) length = 1;
			if (length > 16) length = 16;
			return DateTime.Now.TimeOfDay.ToString().Substring(0,length);
		}

		///returns the date and time as string in format of yyyyMMdd hh:mm:ss.ms  (24hr format)
		public static string SQLDateTime()
		{
			return Utils.Left(DateTime.Now.ToString("yyyyMMdd HH:mm:ss.ms"),20); //changed hh to HH because we use 24 hour model not 12 hour model
		}
		#endregion

		//returns a string with a random number concived from the date and time
		static private string _PRandomLock = Guid.NewGuid().ToString();
		public static string PRandom()
		{
			lock(_PRandomLock)
			{
				return Guid.NewGuid().ToString(); // 24Aug2004 im
				//return DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ((DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second ) *100).ToString() + DateTime.Now.Millisecond.ToString();
			}
		}

		#region first_non_blank
		//input 4 strings
		//returns the first string that is found that is not empty, blank string otherwise
		public static string first_non_blank(string c1, string c2, string c3, string c4)
		{
			if(!(c1==null || c1 =="")) return c1;
			if(!(c2==null || c2 =="")) return c2;
			if(!(c3==null || c3 =="")) return c3;
			if(!(c4==null || c4 =="")) return c4;
			return "";
		}

		//input 4 decimals
		//returns the first decimal that is found that is not empty, 0 otherwise
		public static decimal first_non_blank(decimal c1, decimal c2, decimal c3, decimal c4)
		{
			if(!(c1.Equals(null) || c1 == 0)) return c1;
			if(!(c2.Equals(null) || c2 == 0)) return c2;
			if(!(c3.Equals(null) || c3 == 0)) return c3;
			if(!(c4.Equals(null) || c4 == 0)) return c4;
			return 0;
		}

		//input 4 integers
		//returns the first integer that is found that is not empty, 0 otherwise
		public static int first_non_blank(int c1, int c2, int c3, int c4)
		{
			if(!(c1.Equals(null) || c1 == 0)) return c1;
			if(!(c2.Equals(null) || c2 == 0)) return c2;
			if(!(c3.Equals(null) || c3 == 0)) return c3;
			if(!(c4.Equals(null) || c4 == 0)) return c4;
			return 0;
		}
		//end of first_non_blank(int c1, int c2, int c3, int c4)
		#endregion


		#region Date2Period
		//get integer period number from a gib=ven Date based on period type
		public static int Date2Period( DateTime dDate)
		{
			return Date2Period(dDate, "M");
		}

		public static int Date2Period( DateTime dDate, string cPeriodType )
		{
			int nReturn =0;
			if (cPeriodType==null || cPeriodType.Length == 0)
				cPeriodType = "M" ;  //default to Month

			if ("_SMW".IndexOf(cPeriodType)>0)
			{
				if (cPeriodType=="S" )
					nReturn = Season(dDate);
				else if (cPeriodType=="M")
					nReturn = dDate.Month;
				else
					nReturn = Week(dDate);
			}

			nReturn = (dDate.Year-1900)*100 + nReturn;
			return nReturn;
		}

		//get integer season number from given date, 1~4 for spring to winter.
		public static int Season(DateTime dDate)
		{
			return (((dDate.Month-1)/3)+1);
		}

		//week of the year, based on given date
		public static int Week(DateTime dDate)
		{
			return ((dDate.DayOfYear/7)+1);
		}
		#endregion

		#region getPeriod
		public static int getPeriod(int startPeriod, int interval)
		{
			return getPeriod(startPeriod, interval, "M"); //default is by month
		}
		public static int getPeriod(int startPeriod, int interval, string cPeriodType)
		{
			DateTime startDate = Utils.Period2Date(startPeriod);
			//current only deal with month, will expend it with week/season later
			int year = startDate.Year;
			int month = startDate.Month + interval;

			int addYear  = month/12;  //how many years to add
			month = month%12;         //what month after add interval

			if (month <= 0)  //if month <= 0, add 12 months and reduce a year
			{
				addYear --;
				month += 12;
			}

			return Date2Period(new DateTime(year+addYear, month,1));



			
		}

		public static int getPeriod(int interval)
		{
			return getPeriod(interval, "M"); //default is by month
		}
		/// <summary>
		///    get period value based on today plus interval
		/// </summary>
		/// <param name="interval">if by month, this is how many month, by week - how many weeks, ...</param>
		/// <param name="cPeriodType">by month, week or season, default is by month</param>
		/// <returns></returns>
		public static int getPeriod(int interval, string cPeriodType)
		{
			//current only deal with month, will expend it with week/season later
			int year = DateTime.Today.Year;
			int month = DateTime.Today.Month + interval;

			int addYear  = month/12;  //how many years to add
			month = month%12;         //what month after add interval

			if (month <= 0)  //if month <= 0, add 12 months and reduce a year
			{
				addYear --;
				month += 12;
			}

			return Date2Period(new DateTime(year+addYear, month,1));



			
		}
		#endregion

		#region Period2Name
		public static string Period2Name(int nPeriod)
		{			
			if (!Utils.isEmpty(nPeriod))
			{
				DateTime dDate = Period2Date(nPeriod);
				string[] aMonth = {" ", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
			
				return aMonth[dDate.Month] + dDate.Year.ToString().Substring(2,2);
			}

			return null;
		}
		#endregion

		#region Period2Date
		public static DateTime Period2Date(int nPeriod)
		{
			string  cYear = string.Empty, cMonth = string.Empty, cDay = string.Empty; 

			int  nYear, nMonth, nDay;

			if (nPeriod < 100000)  //nperiod without day information
			{
				nYear = nPeriod / 100;
				nMonth = nPeriod - (100 * nYear);
				if(nMonth < 1)
					nMonth = 1;

				if(nMonth > 12) 
					nMonth = 12;

				cMonth = Convert.ToString(nMonth);

				if (nYear >= 100)
					cYear = Convert.ToString((2000 + (nYear-100 * (nYear/100))));
				else
					cYear = Convert.ToString(1900 + nYear);
				
				cDay = "15";
			}
			else
			{
				nYear  = nPeriod/10000;
				nMonth = (nPeriod - 10000*nYear)/100;
				nDay   = nPeriod - 100 * (nPeriod/100);
				
				if(nMonth < 1)
					nMonth = 1; 
				
				if(nMonth > 12)
					nMonth = 12;
				
				cMonth = Convert.ToString(nMonth);
				
				if (nYear >= 100)
					cYear = Convert.ToString(2000 + (nYear-100 * (nYear/100)));
				else
					cYear = Convert.ToString(1900 + nYear);
								
				cDay = Convert.ToString(nDay);
		

				// just use the system STOD tool to figure out the date - otherwise you need ot know WEB_DATE_FORMAT
				//return (stod(cYear+strzero(val(cmonth),2)+strzero(val(cday),2)))
			}
			return new DateTime(Convert.ToInt32(cYear), Convert.ToInt32(cMonth), Convert.ToInt32(cDay), 0, 0, 0);			
		}		
		#endregion	



		#region aScan
		//input: a one dimension int array to be scanned, a int value to search for
		//returns: the iposition in the array of first found,  -1 if the number is not found
		public static int aScan(int[] array, int val)
		{
			for(int j = 0; j < array.Length; j++)
				if(array[j] == val)
					return j;

			return -1;
		}

		//input: a one dimension string array to be scanned, a string value to search for
		//returns: the position in the array of first found,  -1 if the number is not found
		public static int aScan(string[] stringArray, string val)
		{
			for(int j = 0; j < stringArray.Length; j++)
				if(stringArray[j] == val)
					return j;

			return -1;
		}

		//input: a two dimension string array to be scanned, a string value to search for
		//returns: the position in the array of first found,  -1 if the number is not found
		public static int aScan(string[,] stringArray, int which, string val)
		{
			int i = -1;
			if(which != 1 && which != 0) return -1;

			int nLen = stringArray.Length/2;

			for(int j = 0; j < nLen; j++)
			{
				if(stringArray[j,which] == val)
				{
					i = j;
					break;
				}
			}
			return i;
		}

		public static int aScan(string[][] stringArray, int which, string val)
		{
			int i = -1;
			if(which != 1 && which != 0) return -1;

			for(int j = 0; j < stringArray.Length; j++)
			{
				if(stringArray[j][which] == val)
				{
					i = j;
					break;
				}
			}
			return i;
		}
		#endregion

		/// <summary>
		///    find an empty(null) Tag(first column) position in a two dimension string array,
		///    and insert new tag and value
		/// </summary>
		/// <param name="Array"></param>
		/// <param name="Tag"></param>
		/// <param name="Val"></param>
		/// <returns></returns>
		public static int aAdd(string[,] Array, string Tag, string Val)
		{
			for (int i = 0; i < Array.Length/2; i++)
			{
				if (Array[i,0] == null)
				{
					Array[i,0] = Tag; Array[i,1] = Val;
					return i;
				}
			}
			return -1;
		}

		//check if the given bin is one of the system location /*** to be improved
		//array of system binlabels
		public static readonly string[] SYSTEM_LOC_ARRAY = new string[] {"TRANS","RETURNS","BATCHPCK","RECEIVE","REPLEN","#"}; //INFINITE ???
		public static bool isSystemLocation(string cBin)
		{
			if (cBin==null || cBin == "") return true;
			for (int i=0; i<SYSTEM_LOC_ARRAY.Length;i++)
			{
				if ( cBin.StartsWith(SYSTEM_LOC_ARRAY[i])) return true;
			}
			return false;
		}

		//*** to be done
		public static string RF_Location()
		{
			return "";
		}

		//*** to be done
		public static string Credit_Card_Authorize(string cCustomer,string cPackslip,string ICFunction,string CC_Number,string CC_Expiry,
			decimal nAmount,string cZip,string Response, string cClient)
		{
			return "";
		}

		//*** to be done
		public static string xt_crypt(string cStr,int nSeed)
		{
			return cStr;
		}

		public static string codeblock_xb2cs(string xbCode)
		{
			if (xbCode == null || xbCode == "" || xbCode.Length < 2 || xbCode == "{|x|x}" ) return ""; //return "" if xb Code is empty;
			
			//will be removed once we don't need to bother xbase codeblock
			if (xbCode == "{|x,l,w,r,p|x}") return "";  //xlat-dnloadl
			if (xbCode == "{|PH,PD|PD}") return "";     //xlat_pd
			if (xbCode == "{|H,D,W|W}") return "";      //xlat_pw
			if (xbCode == "{|RH,RD|RD}") return "";     //xlat_rd

			return xbCode;

			////get parameters between "|"s
			//string[] param  = xbCode.Substring(2, xbCode.LastIndexOf("|")-2).Split(",".ToCharArray());

			////get rid of "{|...|" and "}"
			//int i1 = xbCode.LastIndexOf("|")+1;
			//string csCode = xbCode.Substring(i1,xbCode.Length-i1-1).ToUpper();

			//for (int i=0; i<param.Length; i++)
			//{
			//	csCode = csCode.Replace(param[i].Trim().ToUpper(),"args["+i.ToString().Trim()+"]");
			//}
			//csCode = csCode.Replace("RTRIM","Rbeacon.RBUtils.Utils.RTRIM");
			//csCode = csCode.Replace("ALLTRIM","Rbeacon.RBUtils.Utils.AllTrim");
			//return "{return "+csCode+";}";
		}

		#region Path / curPath / AssemblyPath
		
		public static string getRFBasePath()
		{
			if (Utils.isConsoleMode)
				return curPath.ToLower().Replace(Path.DirectorySeparatorChar+@"tools"+Path.DirectorySeparatorChar
					,Path.DirectorySeparatorChar+@"services"+Path.DirectorySeparatorChar)+"xbase++"+Path.DirectorySeparatorChar;
			else
				return curPath+"RbHandHeld"+Path.DirectorySeparatorChar;
		}

		public static string getLogPath()
		{
			return getPath()+"Logs"+Path.DirectorySeparatorChar;
		}

		
		#endregion

		/// <summary>
		/// this function calculates the MOD 10 checkdigit (12th digit) of an UPC-A barcode
		/// </summary>
		/// <param name="UPC_11">string (11 characters) of UPC</param>
		public static string UPC_mod_10(string UPC_11)
		{
			if (UPC_11.Length != 11) return UPC_11;

			int odd_sum = 0;                 // add values of odd numbered position digits
			for (int i=0; i<11; i=i+2)
			{
				odd_sum += Utils.Str2Int(UPC_11.Substring(i,1));
			}

			int sum = odd_sum * 3;           // multiply by 3 the results

			int even_sum = 0;                // add values of even numbered positions
			for (int i=1; i< 10; i=i+2)
			{
				even_sum += Utils.Str2Int(UPC_11.Substring(i,1));
			}

			sum += even_sum;             // add results of odd and even sums


			return  (UPC_11 + "09876543210".Substring(sum%10,1));
		}

		#region codeblock functions, will be changed to dll and handled by a user-chanagable manager

		#region codeblock used in SO download

		[Obsolete]
		public static void xlat_ph(string[] PHArray)
		{
		}
		[Obsolete]
		public static void xlat_pd(string[] PHArray, string[] PDArray)
		{
		}
		[Obsolete]
		public static void xlat_pw(string[] PHArray, string[] PDArray, string[] PWArray)
		{
		}
		[Obsolete]
		public static void xlat_pc(string[] PCArray)
		{
		}
		[Obsolete]
		public static string xlat_dnload(string UPC)
		{
			return UPC;
		}
		[Obsolete]
		public static string xlat_dnloadp(string Product)
		{
			return Product;
		}
		[Obsolete]
		public static string xlat_dnloadl(string Location, string Company, string Warehouse, string[] RHArray, string[] PHArray)
		{
			return Location;
		}
		[Obsolete]
		public static int xlat_wave(string[] PHArray, string[] PDArray)
		{
			return 0;
		}
		[Obsolete]
		public static string dnx_packslip(string CustomerNumber, string OrderNumber, string BONumber,string Store, string Department,string client,string warehouse,string location)
		{
			return OrderNumber.Trim()+BONumber.Trim();
		}
		[Obsolete]
		public static string dnx_horder(string CustomerNumber, string OrderNumber, string BONumber,string client)
		{
			return OrderNumber.Trim()+BONumber.Trim();
		}

		#endregion

		#region codeblock used in PO download

		[Obsolete]
		public static void xlat_rh(string[] RHArray)
		{
		}
		[Obsolete]
		public static void xlat_rd(string[] RHArray, string[] RDArray)
		{
		}
		[Obsolete]
		public static string dnx_ponum(string PONumber, string cBackOrder, string clientname)
		{
			return (Utils.Str2Int(cBackOrder)>1 ? PONumber+"-"+cBackOrder : PONumber);
		}

		#endregion

		#region codeblock used in inventory adjustments
		[Obsolete]
		public static void xlat_ca(string[] CAArray)
		{
		}
		[Obsolete]
		public static void xlat_va(string[] VAArray)
		{
			if (!Utils.isEmpty(VAArray[7]) && VAArray[7].Length == 11)
			{
				VAArray[7] = Utils.UPC_mod_10(VAArray[7]);
			}

		}
		[Obsolete]
		public static void xlat_bx(string[] BXArrar)
		{
		}

		#endregion

		#region codeblock used in upload
		[Obsolete]
		public static string xlat_tiebreaker(string tiebreaker)
		{
			return tiebreaker;
		}

		#endregion

		#region codeblock used n other places
		public static string ship_xlat_cn(string country)
		{
			return "";  // 11Mar2005 rd
		}

		#endregion

		#endregion

		#region labels definition
		private enum CustLabelsId
		{
			CUST_LABELS		= 1,
			SHIP_LABELS		= 2,
			CONTENTS_LABELS	= 3,
			NOFM_LABELS		= 4,
		}

		#region GetLabelsById
		private static string[] GetLabelsById(CustLabelsId labelsId)
		{
			string config_value = RbDbConfig.GetValue(labelsId.ToString());

			if (config_value.Length == 0)
			{
				switch (labelsId)
				{
					case CustLabelsId.CONTENTS_LABELS:
					case CustLabelsId.NOFM_LABELS:
						return new string[] {"CARTON_C"};

					default:
						return new string[] {"BIGTEXT"};
				}
			}

			// comma seperated string,   like "BIGTEXT,UPS5,FEDEX,AIRBORN"
			string[] aTemp = config_value.Split(',');

			if (aTemp.Length <= 0)
			{
				switch (labelsId)
				{
					case CustLabelsId.CONTENTS_LABELS:
					case CustLabelsId.NOFM_LABELS:
						return new string[] {"CARTON_C"};

					default:
						return new string[] {"BIGTEXT"};
				}
			}

			for (int i =0; i < aTemp.Length; i++)
				aTemp[i] = AllTrim(aTemp[i]).ToUpper();

			return aTemp;
		}
		#endregion

		#region GetLabelsDropdownById
		private static string[][] GetLabelsDropdownById(CustLabelsId labelsId)
		{
			string[] arr = GetLabelsById(labelsId);

			string[][] rez = new string[arr.Length][];

			for (int i=0; i < arr.Length; i++)
			{
				rez[i] = new string[2];
				rez[i][0] = arr[i];
				rez[i][1] = arr[i];
			}

			return rez;
		}
		#endregion

		public static string[] CustLabelsList {get { return GetLabelsById(CustLabelsId.CUST_LABELS);}}
		public static string[][] CustLabelsDropDownList {get {return GetLabelsDropdownById(CustLabelsId.CUST_LABELS);}}

		public static string[] ShipLabels {get { return GetLabelsById(CustLabelsId.SHIP_LABELS);}}
		public static string[][] ShipLabelsDropDownList {get {return GetLabelsDropdownById(CustLabelsId.SHIP_LABELS);}}

		public static string[] ContentsLabels {get { return GetLabelsById(CustLabelsId.CONTENTS_LABELS);}}
		public static string[][] ContentsLabelsDropDownList { get {return GetLabelsDropdownById(CustLabelsId.CONTENTS_LABELS);}}

		public static string[] NOFMLabels {get {return GetLabelsById(CustLabelsId.NOFM_LABELS);}}
		public static string[][] NOFMLabelsDropDownList {get {return GetLabelsDropdownById(CustLabelsId.NOFM_LABELS);}
		}
		#endregion

		/// <summary>
		///		Returns the number of times cSearchString was found in cString. 
		/// </summary>
		/// <param name="sSearch">string to search for</param>
		/// <param name="cStr">string to be searched in</param>
		/// <returns>how many time cSearchString was found in cString. </returns>
		static public int NumAt(string sSearch, string cStr)
		{
			if (sSearch == null || sSearch.Length == 0 || cStr == null || cStr.Length == 0)
				return 0;

			int count = 0;
			for (int index = 0; index != -1 && index < cStr.Length;)
			{
				index = cStr.IndexOf(sSearch, index);
				if (index != -1)
				{
					count++;
					index++;
				}
			}

			return count;
		}

		#region from Smith (have to merge)
		// Like <afterAtNum> in xbase dialects.
		public static string afterAtNum(char token, string literal, int nOccurence)
		{
			string ret=string.Empty;
			int position = literal.LastIndexOf(token);
			if (position >= 0)
			{
				if (nOccurence==0)
				{
					return literal.Substring(position+1);
				}
				else
				{
					int k=1;
					for (int i=0; i < literal.Length; i++)
					{
						if (literal[i]==token)
						{
							if (k==nOccurence)
								return literal.Substring(i+1);
							else
								k++;
						}
					}
					if (k > 1 && k >= nOccurence) return literal;
				}
			}
			return ret;
		}

		// Like <beforatnum> in xbase dialects.
		public static string beforAtNum(char token, string literal, int nOccurence)
		{
			string ret=string.Empty;
			int position = literal.LastIndexOf(token);
			if (position >= 0)
			{
				if (nOccurence==0)
				{
					return literal.Substring(0, position);
				}
				else
				{
					int k=1;
					for (int i=0; i < literal.Length; i++)
					{
						if (literal[i]==token)
						{
							if (k==nOccurence)
								return literal.Substring(0,i);
							else
								k++;
						}
					}
				}
			}
			return ret;
		}
		#endregion

		static public string XQty( int nQty, int nMult)
		{
			if (Utils.isEmpty(nMult) || nMult == 1) return nQty.ToString().Trim();

			return Utils.Obj2Str((decimal)nQty/(decimal)nMult);
		}

		// TODO:: has to be moved to codeblock
		static public bool SystemLocsCB(string binLabel)
		{
			if (Left(binLabel,6)=="REPLEN" || binLabel=="RECEIVE" || (Left(binLabel,5)=="TRANS" && binLabel!="TRANS_FK")
				|| binLabel=="BATCHPCK" || binLabel=="RETURNS" || Left(binLabel,1)=="#")
				return true;
			return false;
		}


		/// <summary>
		///		to get label names array and validate if the given label name is one of them
		/// </summary>
		/// <param name="cLabelType"></param>
		/// <param name="label"></param>
		/// <returns></returns>
		static public bool Valid_PickShip_Labels(string cLabelType, string label)
		{
			if (cLabelType==null || cLabelType=="" ||label==null || label =="") return false;
			string[] aShip_Labels;

			switch (cLabelType.ToUpper().Trim())
			{
				case "CUST_LABELS":
					aShip_Labels = Utils.CustLabelsList;
					break;

				case "SHIP_LABELS":
					aShip_Labels = Utils.ShipLabels;
					break;

				case "CONTENTS_LABELS":
					aShip_Labels = Utils.ContentsLabels;
					break;

				default:
					return false;
			}

			if (Utils.aScan(aShip_Labels,label) < 0) return false;

			return true;
		}

		/// <summary>
		/// not sure whether to pad or trim a string ?
		/// this function forces it to the right length
		/// </summary>
		/// <param name="str">string to be processed</param>
		/// <param name="len">length to force to</param>
		/// <returns></returns>
		[Obsolete]
		static public string left_padr(string str,int len)
		{
			return left_padr(str, len, ' ');
		}
		[Obsolete]
		static public string left_padr(string str,int len, char cc)
		{
			if (str.Length == len) return str;
			else if (str.Length > len) return str.Substring(0,len);
			else return str.PadRight(len, cc);
		}
		static public string padr(string str,int len)
		{
			return padr(str, len, ' ');
		}
		static public string padr(string str,int len, char cc)
		{
			if (str.Length == len) return str;
			else if (str.Length > len) return str.Substring(0,len);
			else return str.PadRight(len, cc);
		}

		static public string padl(string str,int len)
		{
			return padl(str, len, ' ');
		}
		static public string padl(string str,int len, char cc)
		{
			if (str.Length == len) return str;
			else if (str.Length > len) return str.Substring(0,len);
			else return str.PadLeft(len, cc);
		}

		static public bool isOneof(int one, int i1,int i2,int i3,int i4,int i5)
		{
			if (one == i1 || one == i2 || one == i3 || one == i4 || one == i5) return true;
			return false;
		}
		
		#region Set value to connection.xml || created by (Andy) [2/13/2003]
		static public string SetSystemConfig(string key, string theValue)
		{
			string filePath = Utils.getPath() + "RbConfig.xml";

			try
			{
				switch (Utils.AllTrim(key).ToLower())
				{
					case "dbconnectstring":
					case "ediconnection":
						theValue = RbSymmetricCrypt.Encrypt(theValue, RbConstants.ConfigSKey, RbConstants.ConfigSIV);
						break;
				}

				// Create an XmlDocument
				XmlDocument myXmlDocument = (new RbConfigXmlTemplate()).CreateDocFromFile(filePath);

				foreach (XmlNode defNode in myXmlDocument["RbConfig"]["system"])
				{	
					if (defNode.Name.ToLower().Trim() == key.ToLower().Trim()) 
						defNode.Attributes["value"].Value = theValue.ToString();
				}

				// Write out data as XML
				myXmlDocument.Save(filePath);
				return string.Empty;
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
		static public string SetConnectionConfig(string connection, string key, string theValue)
		{
			string filePath = Utils.getPath() + "RbConfig.xml";

			try
			{				
				// Create an XmlDocument
				XmlDocument myXmlDocument = (new RbConfigXmlTemplate()).CreateDocFromFile(filePath);

				foreach (XmlNode defNode in myXmlDocument["RbConfig"]["connections"])
				{	
					string name = Utils.AllTrim(defNode.Attributes["name"].Value).ToLower();
					if (Utils.AllTrim(connection).ToLower() == name)
					{
						foreach (XmlNode conn in defNode.ChildNodes)
						{
							if (conn.Name.ToLower().Trim() == key.ToLower().Trim()) 
							{
								conn.Attributes["value"].Value = theValue.ToString();
								break;
							}
						}
						break;
					}

				}

				// Write out data as XML
				myXmlDocument.Save(filePath);
				return string.Empty;
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
		static public string SetRbKernelConnection(string host, string port)
		{
			if (Utils.isEmpty(host) || Utils.isEmpty(port))
				return string.Empty;

			string filePath = Utils.getPath() + "RbKernelConnection.xml";

			try
			{				
				// Create an XmlDocument
				XmlDocument myXmlDocument = new XmlDocument();
				if (!File.Exists(filePath))
				{
					myXmlDocument.LoadXml(RbConstants.OHE_PLATFORM_CONSTANT);
					XmlTextWriter writer = new XmlTextWriter(filePath, null);
					writer.Formatting = Formatting.Indented;
					myXmlDocument.Save(writer);
					writer.Flush();
					writer.Close();
				}
				else
					myXmlDocument.Load (filePath);

				XmlNode defNode = myXmlDocument["RbKernelConnection"];
				defNode.Attributes["host"].Value = host;
				defNode.Attributes["port"].Value = port;

				// Write out data as XML
				myXmlDocument.Save(filePath);
				return string.Empty;
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
		
		#endregion

		#region Append key and value to connection.xml || created by (Andy) [2/16/2003]
		static public string AddConnectionConfig(string key, string theValue, string type, bool opt)
		{
			string filePath = Utils.getPath() + "RbConfig.xml";

			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(filePath);

				//Create a new node.
				XmlElement node = doc.CreateElement(key);
				
				XmlAttribute attr = doc.CreateAttribute("value");
				attr.Value = theValue;
				node.Attributes.Append(attr);

				//				attr = doc.CreateAttribute("type");
				//				attr.Value = type;
				//				node.Attributes.Append(attr);
				//
				//				attr = doc.CreateAttribute("opt");
				//				if (opt)
				//					attr.Value = "yes";
				//				else
				//					attr.Value = "no";
				//				node.Attributes.Append(attr);

				//Add the node to the document.
				doc["RbConfig"]["system"].AppendChild(node);

				doc.Save(filePath);

				return string.Empty;
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
		#endregion

		#region Remove child from connection.xml || created by (Andy) [2/16/2003]
		static public string RemoveConnectionConfig(string key)
		{
			string filePath = Utils.getPath() + "RbConfig.xml";

			try
			{				
				// Create an XmlDocument
				XmlDocument doc = new XmlDocument();
				doc.Load (filePath);		

				XmlNode node = doc.SelectSingleNode("descendant::add[@key='" + key + "']");
				doc.DocumentElement.RemoveChild(node);

				// Write out data as XML
				doc.Save(filePath);
				return string.Empty;
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
		#endregion		

		#region isTransitBin || mz
		static public bool isTransitBin(string bin)
		{
			if (bin.Length != 8 || !bin.StartsWith("#"))
				return false;

			for (int i=1;i<8;i++)
			{
				if (!Char.IsNumber(bin, i))
					return false;
			}
			return true;
		}
		#endregion

		#region Transit_Bin_Name || mz
		static public string Transit_Bin_Name(bool MoveMineOnly, string user,string location)
		{
			string retval = string.Empty;
			if (MoveMineOnly)
			{
				retval = Utils.Left("#"+user,8);    // only first seven chars of userid are used
			}
			else
			{
				if (Utils.isEmpty(location))
				{
					retval = "TRANSIT";
				}
				else
				{
					retval = "TRANS" + Utils.Left(location,3);   // only first three chars of location are used
				}
			}
			return retval;
		}
		#endregion

		#region SystemKey
		[DllImport("kernel32.dll")]
		private static extern long GetVolumeInformation(
			string PathName, 
			StringBuilder VolumeNameBuffer, 
			UInt32 VolumeNameSize, 
			ref UInt32 VolumeSerialNumber, 
			ref UInt32 MaximumComponentLength, 
			ref UInt32 FileSystemFlags, 
			StringBuilder FileSystemNameBuffer, 
			UInt32 FileSystemNameSize);

		/// <summary>
		/// Get Volume Serial Number as string
		/// </summary>
		/// <returns>string representation of Volume Serial Number</returns>
		public static string GetSystemKey()
		{
			uint serNum = 0;
			uint maxCompLen = 0;
			StringBuilder VolLabel = new StringBuilder(256);	// Label
			UInt32 VolFlags = new UInt32();
			StringBuilder FSName = new StringBuilder(256);	// File System Name
			GetVolumeInformation(Path.GetPathRoot(Environment.SystemDirectory), VolLabel, (UInt32)VolLabel.Capacity, ref serNum, ref maxCompLen, ref VolFlags, FSName, (UInt32)FSName.Capacity);
			return Convert.ToString(serNum);
		}
		#endregion
	}
	*/
		#endregion
        
	}
}

