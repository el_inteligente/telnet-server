﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Ohe.Utils;


// Описывает интерфейсы для удаленного соединения с телнет сервером
namespace Ohe.Interfaces
{
    /// <summary>
    /// Описывает интерфейс для удаленного соединения 
    /// с приложением отслеживающим работу пользователей 
    /// </summary>
    /*
    public interface IOheMonitor
    {
        bool IsAlive();
        ArrayList UsersConsoles { get;}
        ArrayList Users { get;}
        void AddUser(string Login, string Pass, string Permiss,string Name, string Whse);
    }
    */

    /// <summary>
    /// Описывает интерфейс для лицензирования продукта
    /// </summary>
    public interface IOheLicense
    {
        //bool IsAlive { get;}
        OheLicenseRecord LicenseInfo();
        int GetMaxTelnetUsers{ get;}
        int GetMaxWebUsers { get; }
    }
}
