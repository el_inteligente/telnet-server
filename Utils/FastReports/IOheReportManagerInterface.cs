﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Ohe.Web.FastReports
{
    public interface IOheReportManagerInterface
    {
        void GetReportData(HttpContext context, OheExportType type);
    }

    public enum OheExportType
    {
         Pdf
        ,Html
        ,Excel
        ,Word
    }
}
