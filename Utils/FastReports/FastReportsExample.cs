﻿#define FASTREPORTS

#if FASTREPORTS
using System;
using System.Data;
using System.IO;
using FastReport;
using System.Web;
using Ohe.Data;
using Ohe.Web;
using Ohe.Web.Configuration;
using WebReportGenerator.Constrols;
using Utils =  Ohe.Utils;

namespace Ohe.Web.FastReports
{
    /// <summary>
    /// Генерирует экпорт данных в отчеты различных форматов
    /// Word, Excel, Pdf
    /// </summary>
    public class OheReportManager //: IOheReportManagerInterface
    {

        #region GetReportData
        /// <summary>
        /// Экспорт отчета в различные форматы данных.
        /// </summary>
        /// <param name="context">Контекст http вызова</param>
        /// <param name="type">Тип экспорта</param>
        public static void GetReportData(HttpContext context, OheReportParams reportParams)
        {

            #region парсинг параметров

            //string reportName = OheWebConfig.CrystallPath + "report.frx";//OheWebConfig.CrystallPath + reportParams.GetParameterById("reportname");
            string reportName = OheWebConfig.CrystallPath + reportParams.GetParameterById("reportname");
            string Query = reportParams.GetParameterById("query").Replace("~~", "'");
            string type = reportParams.GetParameterById("exporttype");
            #endregion

            OheDataTable dt = GetDataReportDataSource(Query);


            switch (type)
            {
                case "pdf":
                    ExportToPdf(context, dt.GetDataTable, reportName, dt.Name);
                    break;
                case "word":
                    ExportToWord(context, dt.GetDataTable, reportName, dt.Name);
                    break;
                case "xls":
                    //ExportToWord(context, dt.GetDataTable, reportName, dt.Name);
                    break;
            }
        }
        #endregion


        #region private GetDataReportDataSource
        /// <summary>
        /// Возвращает OheDataTable с содержимым отчета
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private static OheDataTable GetDataReportDataSource(string query)
        {
            return OheDataClass.GetTableFromQuery(query, null);
        }
        
        #endregion


        #region ExportToPdf
        /// <summary>
        /// Генерирует pdf представление отчета
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="dataReport">Таблица с содержимым</param>
        /// <param name="ReportName">Ссылка на шаблон отчета</param>
        /// <param name="TableName">Имя таблицы</param>
        private static void ExportToPdf(HttpContext context, DataTable dataReport, string ReportName, string TableName)
        {
            FastReport.Utils.Config.WebMode = true;

            using (Report report = new Report())
            {
                report.Load(ReportName);
                report.Dictionary.DataSources.Clear();
                report.RegisterData(dataReport, TableName);

                FastReport.Data.DataSourceBase dataSource = report.Report.GetDataSource(TableName);
                dataSource.Enabled = true;

                // Data1 - жестко зашитый параметр в шаблоне отчета.
                DataBand dband = report.Report.FindObject("Data1") as DataBand;
                (report.Report.FindObject("Data1") as DataBand).DataSource = dataSource;//report.Report.GetDataSource("MyData");
                report.Prepare();

                // Export report to PDF stream
                FastReport.Export.Pdf.PDFExport pdfExport = new FastReport.Export.Pdf.PDFExport();
                using (MemoryStream strm = new MemoryStream())
                {
                    report.Export(pdfExport, strm);

                    // Stream the PDF back to the client as an attachment
                    context.Response.Buffer = true;
                    context.Response.ContentType = "Application/PDF";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=" + Guid.NewGuid().ToString() + ".pdf");
                    strm.Position = 0;
                    strm.WriteTo(context.Response.OutputStream);
                }

            }
        }
        #endregion


        #region ExportToWord
        /// <summary>
        /// Генерирует ExportToWord представление отчета
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="dataReport">Таблица с содержимым</param>
        /// <param name="ReportName">Ссылка на шаблон отчета</param>
        /// <param name="TableName">Имя таблицы</param>
        private static void ExportToWord(HttpContext context, DataTable dataReport, string ReportName, string TableName)
        {
            FastReport.Utils.Config.WebMode = true;

            using (Report report = new Report())
            {
                report.Load(ReportName);
                report.Dictionary.DataSources.Clear();
                report.RegisterData(dataReport, TableName);

                FastReport.Data.DataSourceBase dataSource = report.Report.GetDataSource(TableName);
                dataSource.Enabled = true;

                // Data1 - жестко зашитый параметр в шаблоне отчета.
                DataBand dband = report.Report.FindObject("Data1") as DataBand;
                (report.Report.FindObject("Data1") as DataBand).DataSource = dataSource;//report.Report.GetDataSource("MyData");
                report.Prepare();

                // Export report to PDF stream
                FastReport.Export.RichText.RTFExport pdfExport = new FastReport.Export.RichText.RTFExport();
                using (MemoryStream strm = new MemoryStream())
                {
                    report.Export(pdfExport, strm);

                    // Stream the PDF back to the client as an attachment
                    context.Response.Buffer = true;
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    context.Response.ContentType = "application/vnd.word";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=" + Guid.NewGuid().ToString() + ".pdf");
                    strm.Position = 0;
                    strm.WriteTo(context.Response.OutputStream);
                }

            }
        }
        #endregion


        #region ExportToExcel
        /// <summary>
        /// Генерирует ExportToExcel представление отчета
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="dataReport">Таблица с содержимым</param>
        /// <param name="ReportName">Ссылка на шаблон отчета</param>
        /// <param name="TableName">Имя таблицы</param>
        private static void ExportToExcel(HttpContext context, DataTable dataReport, string ReportName, string TableName)
        {
            FastReport.Utils.Config.WebMode = true;

            using (Report report = new Report())
            {
                report.Load(ReportName);
                report.Dictionary.DataSources.Clear();
                report.RegisterData(dataReport, TableName);

                FastReport.Data.DataSourceBase dataSource = report.Report.GetDataSource(TableName);
                dataSource.Enabled = true;

                // Data1 - жестко зашитый параметр в шаблоне отчета.
                DataBand dband = report.Report.FindObject("Data1") as DataBand;
                (report.Report.FindObject("Data1") as DataBand).DataSource = dataSource;//report.Report.GetDataSource("MyData");
                report.Prepare();

                // Export report to PDF stream
                FastReport.Export.Csv.CSVExport pdfExport = new FastReport.Export.Csv.CSVExport();
                using (MemoryStream strm = new MemoryStream())
                {
                    report.Export(pdfExport, strm);

                    // Stream the PDF back to the client as an attachment
                    context.Response.Buffer = true;
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    context.Response.ContentType = "application/octet-stream";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=" + Guid.NewGuid().ToString() + ".pdf");
                    strm.Position = 0;
                    strm.WriteTo(context.Response.OutputStream);
                }

            }
        }
        #endregion


#if DEBUG
        /// <summary>
        /// Экспорт отчета в различные форматы данных.
        /// </summary>
        /// <param name="context">Контекст http вызова</param>
        /// <param name="type">Тип экспорта</param>
        public static void GetTestReportData(HttpContext context, OheReportParams reportParams)
        {

            #region парсинг параметров

            //string reportName = OheWebConfig.CrystallPath + "report.frx";//OheWebConfig.CrystallPath + reportParams.GetParameterById("reportname");
            string reportName = OheWebConfig.CrystallPath + "report.frx"; // reportParams.GetParameterById("reportname");
            string Query = reportParams.GetParameterById("query").Replace("~~", "'");
            string type = reportParams.GetParameterById("exporttype");
            #endregion

            //OheDataTable dt = GetDataReportDataSource(Query);

            DataTable dt = CreateDataSet();

            ExportToPdf(context, dt, reportName, "Employees");
        }

        private static DataTable CreateDataSet()
        {
            // create simple dataset with one table
            //DataSet FDataSet = new DataSet("MyData");

            DataTable table = new DataTable();
            table.TableName = "Employees";
            //FDataSet.Tables.Add(table);

            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("Name", typeof(string));

            table.Rows.Add(1, "Andrew Fuller");
            table.Rows.Add(2, "Nancy Davolio");
            table.Rows.Add(3, "Margaret Peacock");

            //return FDataSet;
            return table;
        }
#endif


    }
}

#endif